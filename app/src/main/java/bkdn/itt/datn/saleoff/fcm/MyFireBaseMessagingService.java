package bkdn.itt.datn.saleoff.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.global.SaleOffApplication;
import bkdn.itt.datn.saleoff.view.activity.ActivitySplash;

/**
 * Created by TuDLT on 3/14/2018.
 */
public final class MyFireBaseMessagingService extends FirebaseMessagingService {
    private LocalBroadcastManager mLocalBroadcastManager;

    @Override
    public void onCreate() {
        super.onCreate();
        mLocalBroadcastManager = LocalBroadcastManager.getInstance(this);
    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage.getNotification() != null && remoteMessage.getData() != null) {
            if (SaleOffApplication.isActivityVisible()) {
                Intent intent = new Intent("Notification");
                mLocalBroadcastManager.sendBroadcast(intent);
                return;
            }
            getNotificationContent(remoteMessage.getNotification(), remoteMessage.getData());
        }
    }

    private void getNotificationContent(RemoteMessage.Notification notification,
                                        Map<String, String> data) {
        NotificationBody notificationBody = new NotificationBody();
        notificationBody.setTitle(notification.getTitle());
        notificationBody.setMessage(notification.getBody());
        notificationBody.setIcon(notification.getIcon());
        notificationBody.setType(Integer.parseInt(data.get("type")));
        Log.e("TAG","type: " + notificationBody.getType());
        if (data.get("post") != null) {
            notificationBody.setData(data.get("post"));
        } else if (data.get("shop") != null) {
            notificationBody.setData(data.get("shop"));
        } else if (data.get("user") != null) {
            notificationBody.setData(data.get("user"));
        } else if (data.get("comment") != null) {
            notificationBody.setData(data.get("comment"));
        } else if (data.get("role") != null) {
            notificationBody.setData(data.get("role"));
        } else if (data.get("event") != null) {
            notificationBody.setData(data.get("event"));
        }
        sendNotification(notificationBody);
    }

    private void sendNotification(NotificationBody notificationBody) {
        Bundle bundle = new Bundle();
        int notificationId = 1;
        Intent notifyIntent =
                new Intent(Intent.makeMainActivity(new ComponentName(this, ActivitySplash.class)));
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                Intent.FLAG_ACTIVITY_CLEAR_TASK);
        bundle.putParcelable(GlobalDefine.KEY_BUNDLE_NOTIFICATION, notificationBody);
        notifyIntent.putExtras(bundle);

        PendingIntent pendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        notifyIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        Bitmap iconBitmap = null;
        try {
            iconBitmap = Glide.with(getApplicationContext()).
                    asBitmap().
                    load(notificationBody.getIcon()).
                    into(50, 50). // Width and height
                    get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this,
                getApplicationContext().getString(R.string.app_name))
                .setColor(ContextCompat.getColor(getBaseContext(), R.color.colorAccent))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(iconBitmap)
                .setContentTitle(notificationBody.getTitle())
                .setContentText(notificationBody.getMessage())
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notificationId, notificationBuilder.build());
    }

}
