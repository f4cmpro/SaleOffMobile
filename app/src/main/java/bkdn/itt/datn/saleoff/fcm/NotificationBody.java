package bkdn.itt.datn.saleoff.fcm;

import android.os.Parcel;
import android.os.Parcelable;

import bkdn.itt.datn.saleoff.model.model.Post;
import bkdn.itt.datn.saleoff.model.model.Shop;
import bkdn.itt.datn.saleoff.model.model.User;

/**
 * Created by tu doan le on 11/29/17.
 */

public class NotificationBody implements Parcelable {
    private String title;
    private String message;
    private String icon;
    private String data;
    private int type;

    public NotificationBody() {
    }

    protected NotificationBody(Parcel in) {
        title = in.readString();
        message = in.readString();
        icon = in.readString();
        data = in.readString();
        type = in.readInt();
    }

    public static final Creator<NotificationBody> CREATOR = new Creator<NotificationBody>() {
        @Override
        public NotificationBody createFromParcel(Parcel in) {
            return new NotificationBody(in);
        }

        @Override
        public NotificationBody[] newArray(int size) {
            return new NotificationBody[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(message);
        dest.writeString(icon);
        dest.writeString(data);
        dest.writeInt(type);
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
