package bkdn.itt.datn.saleoff.fcm;

/**
 * Created by hainguyen on 11/30/17.
 */

public class NotificationType {

    public static final int USER_FOLLOW_USER = 1;
    public static final int USER_NEW_POST = 2;
    public static final int SHOP_NEW_POST = 3;
    public static final int COMMENT = 4;
    public static final int LIKE_POST = 5;
    public static final int ADDED_EMPLOYEE = 6;
    public static final int DELETED_EMPLOYEE = 7;
    public static final int SHOP_ACTIVED = 8;
    public static final int SHOP_UN_ACTIVED = 9;
    public static final int POST_ACTIVED = 10;
    public static final int POST_UN_ACTIVED = 11;
    public static final int DELETED_SHOP = 12;
    public static final int EVENT = 13;
    public static final int EVENT_ALARM = 14;
}
