package bkdn.itt.datn.saleoff.global;

import android.content.Intent;

/**
 * Created by PhuocDH on 12/26/17.
 */

public class ConstantDefine {

    public static final int RESPONSE_SUCCESS_CODE = 200;
    public static final int ERROR_INVALID_TOKEN = 401;
    public static final int RC_SIGN_IN = 9001;
    public static final int MALE = 0;
    public static final int FEMALE = 1;
    public static final int FIRST_PAGE = 1;
    public static final int DEFAULT_LAST_ID = 0;
    public static final int REPORT_POST = 1;
    public static final int REPORT_USER = 2;
    public static final int REPORT_SHOP = 3;

    public static final int SHOP = 0;
    public static final int USER = 1;
    public static final int PRODUCT = 2;
    public static final int PUBLIC = 0;
    public static final int PRIVATE = 1;

    public static final int EVENT_JOINED = 0;
    public static final int EVENT_GOING_HAPPEN = 1;
    public static final int EVENT_HAPPENING = 2;
    public static final int EVENT_HAPPENED = 3;
    public static final int EVENT_OF_SHOP = 4;

    public static final int PICK_IMAGE_REQUEST = 100;
    public static final int RESULT_CROP_IMG = 101;
    public static final int SELECT_CATEGORY_REQUEST = 102;
    public static final int PICK_IMAGES_REQUEST = 103;
    public static final int CREATE_SHOP_REQUEST = 104;
    public static final int ADD_SHOP_MEMBERS_REQUEST = 105;
    public static final int CREATE_MY_POST_REQUEST = 106;
    public static final int READ_EXTERNAL_SDCARD_REQUEST = 107;
    public static final int EDIT_SHOP_POST_REQUEST = 108;
    public static final int EDIT_USER_POST_REQUEST = 109;
    public static final int CREATE_HOME_POST_REQUEST = 110;
    public static final int CREATE_SHOP_POST_REQUEST = 111;
    public static final int EDIT_SHOP_INFO_REQUEST = 112;
    public static final int EDIT_PROFILE_REQUEST = 113;
    public static final int IMAGE_CAPTURE_REQUEST = 114;
    public static final int MY_LOCATION_REQUEST = 115;
    public static final int CREATE_EVENT_REQUEST = 116;

}
