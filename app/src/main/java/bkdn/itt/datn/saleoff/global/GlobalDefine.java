package bkdn.itt.datn.saleoff.global;

/**
 * Created by PhuocDH
 * on 12/26/17.
 */
public class GlobalDefine {
    public static final String FORMAT_DATE_DEFAULT = "yyyy-MM-dd hh:mm:ss";
    public static final String FORMAT_DATE_SECOND_DEFAULT = "yyyy-MM-dd";
    public static final String FORMAT_DATE_THIRD_DEFAULT = "dd/MM/yyyy";
    public static final String FORMAT_DATE_FOUR_DEFAULT = "dd/MM/yyyy hh:mm a";
    public static final String FORMAT_TIME_DEFAULT = "hh:mm a";
    public static final String FORMAT_TIME_24H = "hh:mm";
    public static final String FORMAT_DATE_WEEK_DEFAULT = "EEE dd MMM yyyy ";
    public static final String FORMAT_DAY = "dd";
    public static final String FORMAT_DAY_OF_WEEK = "E";

    public static final String KEY_BUNDLE_EMPLOYEE_ID = "KEY_BUNDLE_EMPLOYEE_ID";
    public static final String KEY_BUNDLE_PROFILE_EDIT = "KEY_BUNDLE_PROFILE_EDIT";
    public static final String KEY_BUNDLE_LOGIN_FACEBOOK = "KEY_BUNDLE_LOGIN_FACEBOOK";
    public static final String KEY_BUNDLE_ERROR_MESSAGE = "KEY_BUNDLE_ERROR_MESSAGE";
    public static final String KEY_INTENT_POST_ID = "KEY_INTENT_POST_ID";
    public static final String KEY_BUNDLE_SLIDER = "KEY_BUNDLE_SLIDER";
    public static final String KEY_INTENT_PICK_IMAGES = "KEY_INTENT_PICK_IMAGES";
    public static final String KEY_INTENT_CATEGORY = "KEY_INTENT_CATEGORY";
    public static final String KEY_INTENT_CREATE_SHOP = "KEY_INTENT_CREATE_SHOP";
    public static final String KEY_INTENT_SELECT_SUB_CATEGORY = "KEY_INTENT_SELECT_SUB_CATEGORY";
    public static final String KEY_INTENT_SHOP_ID = "KEY_INTENT_SHOP_ID";
    public static final String KEY_BUNDLE_SHOP_INFO = "KEY_BUNDLE_SHOP_INFO";
    public static final String KEY_INTENT_POST = "KEY_INTENT_POST";
    public static final String KEY_INTENT_SHOP_INFO = "KEY_INTENT_SHOP_INFO";
    public static final String KEY_INTENT_USER_INFO = "KEY_INTENT_USER_INFO";
    public static final String KEY_BUNDLE_IS_MY_SHOP = "KEY_BUNDLE_IS_MY_SHOP";
    public static final String KEY_BUNDLE_STATUS = "KEY_BUNDLE_STATUS";
    public static final String KEY_INTENT_SHOP = "KEY_INTENT_SHOP";
    public static final String KEY_INTENT_USER = "KEY_INTENT_USER";


    public static final String KEY_BUNDLE_NOTIFICATION = "KEY_BUNDLE_NOTIFICATION";
    public static final String KEY_INTENT_USER_ID = "KEY_INTENT_USER_ID";
    public static final String KEY_BUNDLE_CREATE_NEW_POST = "KEY_BUNDLE_CREATE_NEW_POST";
    public static final String OWNER_SHOP = "owner";
    public static final String MEMBER_SHOP = "member";
    public static final String ROLE = "role";
    public static final String KEY_INTENT_EVENT = "KEY_INTENT_EVENT";
    public static final String KEY_INTENT_EVENT_ID = "KEY_INTENT_EVENT_ID";
    public static final String KEY_INTENT_OWNER = "KEY_INTENT_OWNER";
}


