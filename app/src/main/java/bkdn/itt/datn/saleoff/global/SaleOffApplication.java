package bkdn.itt.datn.saleoff.global;

import android.app.Application;


import com.cloudinary.android.MediaManager;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Tú Đoàn Lê on 2/26/2018.
 */

public class SaleOffApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Map config = new HashMap();
        config.put("cloud_name", "minori");
        MediaManager.init(this, config);
    }

    public static boolean isActivityVisible() {
        return activityVisible;
    }

    public static void activityResumed() {
        activityVisible = true;
    }

    public static void activityPaused() {
        activityVisible = false;
    }

    private static boolean activityVisible;
}
