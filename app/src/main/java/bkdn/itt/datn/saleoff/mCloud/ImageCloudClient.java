package bkdn.itt.datn.saleoff.mCloud;

import android.content.Context;

import com.cloudinary.Cloudinary;
import com.cloudinary.Transformation;

import java.util.HashMap;

import bkdn.itt.datn.saleoff.R;

/**
 * Created by TuDLT on 3/22/2018.
 */
public class ImageCloudClient {

    public static String getAvatar(Context context, String imgName) {
        Cloudinary cloud = new Cloudinary(MyConfiguration.getMyConfigs());
        //MANIPULATION
        Transformation t = new Transformation();
        t.height(context.getResources().getDimensionPixelSize(R.dimen._96sdp));
        t.width(context.getResources().getDimensionPixelSize(R.dimen._96sdp));
        t.crop("fill");
        t.quality(80);
        return cloud.url().transformation(t).generate(imgName);
    }
}
