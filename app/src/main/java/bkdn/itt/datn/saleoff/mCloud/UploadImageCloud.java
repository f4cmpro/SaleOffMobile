package bkdn.itt.datn.saleoff.mCloud;

import android.content.Context;
import android.net.Uri;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.cloudinary.android.preprocess.BitmapDecoder;
import com.cloudinary.android.preprocess.BitmapEncoder;
import com.cloudinary.android.preprocess.ImagePreprocessChain;
import com.cloudinary.android.preprocess.Limit;

import java.io.File;
import java.util.Map;

import bkdn.itt.datn.saleoff.R;

/**
 * Created by TuDLT on 4/8/2018.
 */
public class UploadImageCloud {
    private static UploadImageCloud sUploadImageCloud;

    public static UploadImageCloud getInstance() {
        if (sUploadImageCloud == null) {
            sUploadImageCloud = new UploadImageCloud();
        }
        return sUploadImageCloud;
    }

    public void uploadPostPhotos(Context context, File realFile, OnUpLoadPostPhotosListener callBack) {
        if (!realFile.exists()) {
            callBack.onUpLoadFail("File not exists!");
            return;
        }
        String filePath = realFile.getPath();
        String imgName = realFile.getName().substring(0, realFile.getName().lastIndexOf("."));
        MediaManager.get()
                .upload(filePath)
                .unsigned("sale_off")
                .option("resource_type", "image")
                .option("folder", "photos_of_post/")
                .option("public_id", imgName)
                .callback(new UploadCallback() {
                    @Override
                    public void onStart(String requestId) {

                    }

                    @Override
                    public void onProgress(String requestId, long bytes, long totalBytes) {

                    }

                    @Override
                    public void onSuccess(String requestId, Map resultData) {
                        if (resultData != null && resultData.get("url") != null) {
                            callBack.onUpLoadPostPhotos((String) resultData.get("url"));
                        } else {
                            callBack.onUpLoadFail("Cannot upload Images!");
                        }
                    }

                    @Override
                    public void onError(String requestId, ErrorInfo error) {
                        callBack.onUpLoadFail(error.getDescription());
                    }

                    @Override
                    public void onReschedule(String requestId, ErrorInfo error) {

                    }
                })
                .dispatch(context);
    }

    public void uploadUserAvatar(Context context, File realFile, OnUpLoadAvatarListener callBack) {
        String filePath = realFile.getPath();
        String imgName = realFile.getName().substring(0, realFile.getName().lastIndexOf("."));
        MediaManager.get()
                .upload(filePath)
                .unsigned("sale_off")
                .option("public_id", imgName)
                .callback(new UploadCallback() {
                    @Override
                    public void onStart(String requestId) {

                    }

                    @Override
                    public void onProgress(String requestId, long bytes, long totalBytes) {

                    }

                    @Override
                    public void onSuccess(String requestId, Map resultData) {
                        if (resultData != null && resultData.get("url") != null) {
                            callBack.onUploadAvatar((String) resultData.get("url"));
                        } else {
                            callBack.onUpLoadFail("Cannot upload Images!");
                        }
                    }

                    @Override
                    public void onError(String requestId, ErrorInfo error) {
                        callBack.onUpLoadFail(error.getDescription());
                    }

                    @Override
                    public void onReschedule(String requestId, ErrorInfo error) {

                    }
                })
                .dispatch(context);
    }

    public void uploadUserCover(Context context, File realFile, OnUploadCoverListener callBack) {
        int maxWidth = context.getResources().getDimensionPixelSize(R.dimen._600sdp);
        int maxHeight = context.getResources().getDimensionPixelSize(R.dimen._300sdp);
        String filePath = realFile.getPath();
        String imgName = realFile.getName().substring(0, realFile.getName().lastIndexOf("."));
        MediaManager.get()
                .upload(filePath)
                .unsigned("sale_off")
                .option("public_id", imgName)
                .preprocess(new ImagePreprocessChain()
                        .addStep(new Limit(maxHeight, maxWidth))
                        .saveWith(new BitmapEncoder(BitmapEncoder.Format.JPEG, 80)))
                .callback(new UploadCallback() {
                    @Override
                    public void onStart(String requestId) {

                    }

                    @Override
                    public void onProgress(String requestId, long bytes, long totalBytes) {

                    }

                    @Override
                    public void onSuccess(String requestId, Map resultData) {
                        if (resultData != null && resultData.get("url") != null) {
                            callBack.onUploadCover((String) resultData.get("url"));
                        } else {
                            callBack.onUpLoadFail("Cannot upload Images!");
                        }
                    }

                    @Override
                    public void onError(String requestId, ErrorInfo error) {
                        callBack.onUpLoadFail(error.getDescription());
                    }

                    @Override
                    public void onReschedule(String requestId, ErrorInfo error) {

                    }
                })
                .dispatch(context);
    }

    public void uploadPostCover(Context context, Uri uri, OnUpLoadPostPhotosListener callBack) {
        MediaManager.get()
                .upload(uri)
                .unsigned("sale_off")
                .option("resource_type", "image")
                .option("folder", "photos_of_post/")
                .option("public_id", uri.getPath())
                .callback(new UploadCallback() {
                    @Override
                    public void onStart(String requestId) {

                    }

                    @Override
                    public void onProgress(String requestId, long bytes, long totalBytes) {

                    }

                    @Override
                    public void onSuccess(String requestId, Map resultData) {
                        if (resultData != null && resultData.get("url") != null) {
                            callBack.onUpLoadPostPhotos((String) resultData.get("url"));
                        } else {
                            callBack.onUpLoadFail("Cannot upload Images!");
                        }
                    }

                    @Override
                    public void onError(String requestId, ErrorInfo error) {
                        callBack.onUpLoadFail(error.getDescription());
                    }

                    @Override
                    public void onReschedule(String requestId, ErrorInfo error) {

                    }
                })
                .dispatch(context);
    }


    public interface OnUpLoadPostPhotosListener {
        void onUpLoadPostPhotos(String imgUrl);

        void onUpLoadFail(String errorMessage);
    }

    public interface OnUpLoadAvatarListener {
        void onUploadAvatar(String avatarUrl);

        void onUpLoadFail(String errorMessage);
    }

    public interface OnUploadCoverListener {
        void onUploadCover(String coverUrl);

        void onUpLoadFail(String errorMessage);
    }
}
