package bkdn.itt.datn.saleoff.model.local;

import android.provider.BaseColumns;

/**
 * Created by TuDLT on 4/28/2018.
 */
public class DBContract {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private DBContract() {
    }

    //NAME DATA BASE
    public static final String DATABASE_NAME = "SaleOff.db";
    //DB VERSION
    public static final int DATABASE_VERSION = 1;
    public static final int DATABASE_VERSION_2 = 2;

    /* Inner class that defines the table contents */
    public static class Category implements BaseColumns {
        public static final String TABLE_NAME = "category";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_CREATED_AT = "created_at";
        public static final String COLUMN_UPDATED_AT = "updated_at";
        public static final String COLUMN_IMV_RES = "imv_res";
    }

    public static class SubCategory implements BaseColumns {
        public static final String TABLE_NAME = "sub_category";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_CATEGORY_ID = "category_id";
        public static final String COLUMN_CREATED_AT = "created_at";
        public static final String COLUMN_UPDATED_AT = "updated_at";
    }

    public static class Notification implements BaseColumns {
        public static final String TABLE_NAME = "notification";
        public static final String COLUMN_DATA = "data";
    }

    //sql create category table
    public static final String SQL_CREATE_CATEGORY_TABLE =
            "CREATE TABLE " + Category.TABLE_NAME + " (" +
                    Category._ID + " INTEGER PRIMARY KEY," +
                    Category.COLUMN_NAME + " TEXT," +
                    Category.COLUMN_CREATED_AT + " TEXT," +
                    Category.COLUMN_UPDATED_AT + " TEXT," +
                    Category.COLUMN_IMV_RES + " INTEGER)";

    //sql create sub category table
    public static final String SQL_CREATE_SUB_CATEGORY_TABLE =
            "CREATE TABLE " + SubCategory.TABLE_NAME + " (" +
                    SubCategory._ID + " INTEGER PRIMARY KEY," +
                    SubCategory.COLUMN_NAME + " TEXT," +
                    SubCategory.COLUMN_CATEGORY_ID + " INTEGER," +
                    SubCategory.COLUMN_CREATED_AT + " TEXT," +
                    SubCategory.COLUMN_UPDATED_AT + " TEXT)";

    //sql create notification table
    public static final String SQL_CREATE_NOTIFICATION_TABLE =
            "CREATE TABLE " + Notification.TABLE_NAME + " (" +
                    Notification._ID + " INTEGER PRIMARY KEY, " +
                    Notification.COLUMN_DATA + " TEXT NOT NULL)";

    //sql delete category
    public static final String SQL_DELETE_CATEGORY_TABLE =
            "DROP TABLE IF EXISTS " + Category.TABLE_NAME;
    //sql delete sub category
    public static final String SQL_DELETE_SUB_CATEGORY_TABLE =
            "DROP TABLE IF EXISTS " + SubCategory.TABLE_NAME;

    //sql delete notification
    public static final String SQL_DELETE_NOTIFICATION_TABLE =
            "DROP TABLE IF EXISTS " + Notification.TABLE_NAME;
    //sql add new column
    public static final String SQL_ADD_IMV_RES_COLUMN =
            "ALTER TABLE " + Category.TABLE_NAME + " ADD COLUMN "
                    + Category.COLUMN_IMV_RES + " INTEGER";
}
