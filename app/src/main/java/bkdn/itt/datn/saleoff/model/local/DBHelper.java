package bkdn.itt.datn.saleoff.model.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.Category;
import bkdn.itt.datn.saleoff.model.model.Notification;
import bkdn.itt.datn.saleoff.model.model.Post;
import bkdn.itt.datn.saleoff.model.model.SubCategory;

/**
 * Created by TuDLT on 4/28/2018.
 */
public class DBHelper extends SQLiteOpenHelper {


    public DBHelper(Context context) {
        super(context, DBContract.DATABASE_NAME, null, DBContract.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DBContract.SQL_CREATE_CATEGORY_TABLE);
        db.execSQL(DBContract.SQL_CREATE_SUB_CATEGORY_TABLE);
        db.execSQL(DBContract.SQL_CREATE_NOTIFICATION_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > oldVersion) {
            db.execSQL(DBContract.SQL_DELETE_CATEGORY_TABLE);
            db.execSQL(DBContract.SQL_DELETE_SUB_CATEGORY_TABLE);
            db.execSQL(DBContract.SQL_DELETE_NOTIFICATION_TABLE);
            onCreate(db);
        }
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
    }

    public boolean isCategoryExist() {
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM " + DBContract.Category.TABLE_NAME;
        Cursor cursor = db.rawQuery(count, null);
        cursor.moveToFirst();
        if (cursor.getInt(0) > 0) {
            return true;
        }
        return false;

    }

    public boolean isSubCategoryExist() {
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM " + DBContract.SubCategory.TABLE_NAME;
        Cursor cursor = db.rawQuery(count, null);
        cursor.moveToFirst();
        if (cursor.getInt(0) > 0) {
            return true;
        }
        return false;
    }

    public void setCategoryList(List<Category> categories) {
        int[] mCatImgs = new int[]{R.drawable.tat_ca_danh_muc, R.drawable.food,
                R.drawable.do_dien_tu, R.drawable.xe_co, R.drawable.the_thao,
                R.drawable.me_va_be, R.drawable.sach, R.drawable.dien_gia_dung,
                R.drawable.nha_cua, R.drawable.thoi_trang, R.drawable.my_pham,
                R.drawable.giai_tri, R.drawable.khac};
        // Gets the data repository in write mode
        SQLiteDatabase db = this.getWritableDatabase();
        for (Category item : categories) {
            // Create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();
            values.put(DBContract.Category._ID, item.getId());
            values.put(DBContract.Category.COLUMN_NAME, item.getName());
            values.put(DBContract.Category.COLUMN_IMV_RES, mCatImgs[categories.indexOf(item)]);
            values.put(DBContract.Category.COLUMN_CREATED_AT, item.getCreatedAt());
            values.put(DBContract.Category.COLUMN_UPDATED_AT, item.getUpdatedAt());
            // Insert the new row, returning the primary key value of the new row
            long newRowId = db.insert(DBContract.Category.TABLE_NAME, null, values);
        }
    }

    public void setSubCategoryList(List<SubCategory> subCategories) {
        // Gets the data repository in write mode
        SQLiteDatabase db = this.getWritableDatabase();
        for (SubCategory item : subCategories) {
            // Create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();
            values.put(DBContract.SubCategory._ID, item.getId());
            values.put(DBContract.SubCategory.COLUMN_NAME, item.getName());
            values.put(DBContract.SubCategory.COLUMN_CATEGORY_ID, item.getCateId());
            values.put(DBContract.SubCategory.COLUMN_CREATED_AT, item.getCreateDate());
            values.put(DBContract.SubCategory.COLUMN_UPDATED_AT, item.getUpdateDate());

            // Insert the new row, returning the primary key value of the new row
            long newRowId = db.insert(DBContract.SubCategory.TABLE_NAME, null, values);
        }
    }

    public List<Category> getAllCategory() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Category> categories = new ArrayList<>();
        String query = "SELECT * FROM " + DBContract.Category.TABLE_NAME + " WHERE 1";
        Cursor cs = db.rawQuery(query, null);
        cs.moveToFirst();
        while (!cs.isAfterLast()) {
            Category category = new Category();
            category.setId(cs.getInt(cs.getColumnIndexOrThrow(DBContract.Category._ID)));
            category.setName(cs.getString(cs.getColumnIndexOrThrow(DBContract.Category.COLUMN_NAME)));
            category.setCreatedAt(cs.getString(cs.getColumnIndexOrThrow(DBContract.Category.COLUMN_CREATED_AT)));
            category.setUpdatedAt(cs.getString(cs.getColumnIndexOrThrow(DBContract.Category.COLUMN_UPDATED_AT)));
            category.setImvRes(cs.getInt(cs.getColumnIndexOrThrow(DBContract.Category.COLUMN_IMV_RES)));
            categories.add(category);
            cs.moveToNext();
        }
        cs.close();
        return categories;
    }

    public List<Category> getNotAllCategory() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Category> categories = new ArrayList<>();
        String query = "SELECT * FROM " + DBContract.Category.TABLE_NAME + " WHERE "
                + DBContract.Category._ID + " != 1";
        Cursor cs = db.rawQuery(query, null);
        cs.moveToFirst();
        while (!cs.isAfterLast()) {
            Category category = new Category();
            category.setId(cs.getInt(cs.getColumnIndexOrThrow(DBContract.Category._ID)));
            category.setName(cs.getString(cs.getColumnIndexOrThrow(DBContract.Category.COLUMN_NAME)));
            category.setCreatedAt(cs.getString(cs.getColumnIndexOrThrow(DBContract.Category.COLUMN_CREATED_AT)));
            category.setUpdatedAt(cs.getString(cs.getColumnIndexOrThrow(DBContract.Category.COLUMN_UPDATED_AT)));
            category.setImvRes(cs.getInt(cs.getColumnIndexOrThrow(DBContract.Category.COLUMN_IMV_RES)));
            categories.add(category);
            cs.moveToNext();
        }
        cs.close();
        return categories;
    }

    public List<SubCategory> getAllSubCategory() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<SubCategory> subCategories = new ArrayList<>();
        String query = "SELECT * FROM " + DBContract.SubCategory.TABLE_NAME + " WHERE 1";
        Cursor cs = db.rawQuery(query, null);
        cs.moveToFirst();
        while (!cs.isAfterLast()) {
            SubCategory subCategory = new SubCategory();
            subCategory.setId(cs.getInt(cs.getColumnIndexOrThrow(DBContract.SubCategory._ID)));
            subCategory.setId(cs.getInt(cs.getColumnIndexOrThrow(DBContract.SubCategory.COLUMN_CATEGORY_ID)));
            subCategory.setName(cs.getString(cs.getColumnIndexOrThrow(DBContract.SubCategory.COLUMN_NAME)));
            subCategory.setCreateDate(cs.getString(cs.getColumnIndexOrThrow(DBContract.SubCategory.COLUMN_CREATED_AT)));
            subCategory.setUpdateDate(cs.getString(cs.getColumnIndexOrThrow(DBContract.SubCategory.COLUMN_UPDATED_AT)));
            subCategories.add(subCategory);
            cs.moveToNext();
        }
        cs.close();
        return subCategories;
    }


    public List<SubCategory> getListSubCatByCat(int cateId) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<SubCategory> subCategories = new ArrayList<>();
        //SELECT columns
        String[] columns = {DBContract.SubCategory._ID,
                DBContract.SubCategory.COLUMN_NAME,
                DBContract.SubCategory.COLUMN_CATEGORY_ID,
                DBContract.SubCategory.COLUMN_CREATED_AT,
                DBContract.SubCategory.COLUMN_UPDATED_AT};
        //WHERE condition = conditionParameter
        String condition = DBContract.SubCategory.COLUMN_CATEGORY_ID + " = ?";
        String[] parameters = {String.valueOf(cateId)};
        Cursor cs = db.query(DBContract.SubCategory.TABLE_NAME, columns, condition, parameters,
                null, null, null);
        cs.moveToFirst();
        while (!cs.isAfterLast()) {
            SubCategory subCategory = new SubCategory();
            subCategory.setId(cs.getInt(cs.getColumnIndexOrThrow(DBContract.SubCategory._ID)));
            subCategory.setName(cs.getString(cs.getColumnIndexOrThrow(DBContract.SubCategory.COLUMN_NAME)));
            subCategory.setCateId(cs.getInt(cs.getColumnIndexOrThrow(DBContract.SubCategory.COLUMN_CATEGORY_ID)));
            subCategory.setCreateDate(cs.getString(cs.getColumnIndexOrThrow(DBContract.SubCategory.COLUMN_CREATED_AT)));
            subCategory.setUpdateDate(cs.getString(cs.getColumnIndexOrThrow(DBContract.SubCategory.COLUMN_UPDATED_AT)));
            subCategories.add(subCategory);
            cs.moveToNext();
        }
        cs.close();
        return subCategories;
    }


    public void insertNotificationDB(List<Notification> notifications) {
        SQLiteDatabase db = this.getWritableDatabase();
        for (int i = notifications.size() - 1; i >= 0; i--) {
            String json = new Gson().toJson(notifications.get(i));
            ContentValues contentValues = new ContentValues();
            contentValues.put(DBContract.Notification._ID, notifications.get(i).getId());
            contentValues.put(DBContract.Notification.COLUMN_DATA, json);
            db.insert(DBContract.Notification.TABLE_NAME, null, contentValues);
        }
    }

    public List<Notification> getNotifications() {
        List<Notification> notifications = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + DBContract.Notification.TABLE_NAME
                + " WHERE 1 LIMIT 20";
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            String data = cursor.getString(cursor.getColumnIndexOrThrow(DBContract.Notification.COLUMN_DATA));
            Notification notification = new Gson().fromJson(data, Notification.class);
            notifications.add(notification);
        }
        return notifications;
    }

    public boolean isNotificationExist() {
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM " + DBContract.Notification.TABLE_NAME;
        Cursor cursor = db.rawQuery(count, null);
        cursor.moveToFirst();
        if (cursor.getInt(0) > 0) {
            return true;
        }
        return false;
    }

    public void updateNotificationRecord(Notification notification) {
        String json = new Gson().toJson(notification);
        String update = "UPDATE " + DBContract.Notification.TABLE_NAME
                + " SET " + DBContract.Notification.COLUMN_DATA + " = " + json
                + " WHERE " + DBContract.Notification._ID + " = " + notification.getId();
    }


}
