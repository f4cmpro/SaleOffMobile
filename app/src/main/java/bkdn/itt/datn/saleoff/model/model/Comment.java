package bkdn.itt.datn.saleoff.model.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TuDLT on 4/20/2018.
 */
public class Comment  extends ModelBase implements Parcelable {
    @SerializedName("id")
    private int id;
    @SerializedName("post_id")
    private int postId;
    @SerializedName("user_id")
    private int userId;
    @SerializedName("type")
    private String type;
    @SerializedName("comment")
    private String comment;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updated_at")
    private String updateAt;
    @SerializedName("author")
    private Author author;

    public Comment(){

    }


    protected Comment(Parcel in) {
        id = in.readInt();
        postId = in.readInt();
        userId = in.readInt();
        type = in.readString();
        comment = in.readString();
        createdAt = in.readString();
        updateAt = in.readString();
        author = in.readParcelable(Author.class.getClassLoader());
    }

    public static final Creator<Comment> CREATOR = new Creator<Comment>() {
        @Override
        public Comment createFromParcel(Parcel in) {
            return new Comment(in);
        }

        @Override
        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(String updateAt) {
        this.updateAt = updateAt;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(postId);
        dest.writeInt(userId);
        dest.writeString(type);
        dest.writeString(comment);
        dest.writeString(createdAt);
        dest.writeString(updateAt);
        dest.writeParcelable(author, flags);
    }

}
