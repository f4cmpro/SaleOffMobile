package bkdn.itt.datn.saleoff.model.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TuDLT on 5/27/2018.
 */
public class Event extends ModelBase implements Parcelable {
    @SerializedName("id")
    private int eventId;
    @SerializedName("shop_id")
    private int shopId;
    @SerializedName("total")
    private int total;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updated_at")
    private String upDatedAt;
    @SerializedName("name")
    private String title;
    @SerializedName("detail")
    private String detail;
    @SerializedName("from")
    private String startDate;
    @SerializedName("to")
    private String endDate;
    @SerializedName("images")
    private String[] images;
    @SerializedName("cover")
    private String cover;
    @SerializedName("address")
    private String address;
    @SerializedName("status")
    private int status;
    @SerializedName("is_join")
    private int isJoin;
    @SerializedName("share_code")
    private int shareCode;
    @SerializedName("shop")
    private ShopInfo shopInfo;

    public Event() {

    }

    protected Event(Parcel in) {
        eventId = in.readInt();
        shopId = in.readInt();
        total = in.readInt();
        createdAt = in.readString();
        upDatedAt = in.readString();
        title = in.readString();
        detail = in.readString();
        startDate = in.readString();
        endDate = in.readString();
        images = in.createStringArray();
        cover = in.readString();
        address = in.readString();
        status = in.readInt();
        isJoin = in.readInt();
        shareCode = in.readInt();
        shopInfo = in.readParcelable(ShopInfo.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(eventId);
        dest.writeInt(shopId);
        dest.writeInt(total);
        dest.writeString(createdAt);
        dest.writeString(upDatedAt);
        dest.writeString(title);
        dest.writeString(detail);
        dest.writeString(startDate);
        dest.writeString(endDate);
        dest.writeStringArray(images);
        dest.writeString(cover);
        dest.writeString(address);
        dest.writeInt(status);
        dest.writeInt(isJoin);
        dest.writeInt(shareCode);
        dest.writeParcelable(shopInfo, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }


    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpDatedAt() {
        return upDatedAt;
    }

    public void setUpDatedAt(String upDatedAt) {
        this.upDatedAt = upDatedAt;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String[] getImages() {
        return images;
    }

    public void setImages(String[] images) {
        this.images = images;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public ShopInfo getShopInfo() {
        return shopInfo;
    }

    public void setShopInfo(ShopInfo shopInfo) {
        this.shopInfo = shopInfo;
    }

    public int getIsJoin() {
        return isJoin;
    }

    public void setIsJoin(int isJoin) {
        this.isJoin = isJoin;
    }

    public int getShareCode() {
        return shareCode;
    }

    public void setShareCode(int shareCode) {
        this.shareCode = shareCode;
    }
}
