package bkdn.itt.datn.saleoff.model.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TuDLT on 3/8/2018.
 */
public class Login {
    @SerializedName("token")
    private String token;
    @SerializedName("is_first")
    private int isFirstLogin;

}
