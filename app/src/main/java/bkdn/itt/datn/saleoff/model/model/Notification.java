package bkdn.itt.datn.saleoff.model.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TuDLT on 3/16/2018.
 */
public class Notification implements Parcelable {
    @SerializedName("body")
    private NotificationBody body;
    @SerializedName("target")
    private NotificationTarget target;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("sender_id")
    private int sender_id;
    @SerializedName("type_sender")
    private String type_sender;
    @SerializedName("id")
    private String id;

    private boolean isRead;

    public Notification(){

    }

    protected Notification(Parcel in) {
        body = in.readParcelable(NotificationBody.class.getClassLoader());
        target = in.readParcelable(NotificationTarget.class.getClassLoader());
        created_at = in.readString();
        sender_id = in.readInt();
        type_sender = in.readString();
        id = in.readString();
        isRead = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(body, flags);
        dest.writeParcelable(target, flags);
        dest.writeString(created_at);
        dest.writeInt(sender_id);
        dest.writeString(type_sender);
        dest.writeString(id);
        dest.writeByte((byte) (isRead ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Notification> CREATOR = new Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel in) {
            return new Notification(in);
        }

        @Override
        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };

    public NotificationBody getBody() {
        return body;
    }

    public void setBody(NotificationBody body) {
        this.body = body;
    }

    public NotificationTarget getTarget() {
        return target;
    }

    public void setTarget(NotificationTarget target) {
        this.target = target;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getSender_id() {
        return sender_id;
    }

    public void setSender_id(int sender_id) {
        this.sender_id = sender_id;
    }

    public String getType_sender() {
        return type_sender;
    }

    public void setType_sender(String type_sender) {
        this.type_sender = type_sender;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }
}
