package bkdn.itt.datn.saleoff.model.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TuDLT on 5/2/2018.
 */
public class NotificationBody implements Parcelable {
    @SerializedName("content")
    private String content;
    @SerializedName("avatar")
    private String avatar;

    protected NotificationBody(Parcel in) {
        content = in.readString();
        avatar = in.readString();
    }

    public static final Creator<NotificationBody> CREATOR = new Creator<NotificationBody>() {
        @Override
        public NotificationBody createFromParcel(Parcel in) {
            return new NotificationBody(in);
        }

        @Override
        public NotificationBody[] newArray(int size) {
            return new NotificationBody[size];
        }
    };

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(content);
        dest.writeString(avatar);
    }
}
