package bkdn.itt.datn.saleoff.model.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TuDLT on 5/2/2018.
 */
public class NotificationTarget implements Parcelable {
    @SerializedName("type")
    private String type;
    @SerializedName("id")
    private int id;

    protected NotificationTarget(Parcel in) {
        type = in.readString();
        id = in.readInt();
    }

    public static final Creator<NotificationTarget> CREATOR = new Creator<NotificationTarget>() {
        @Override
        public NotificationTarget createFromParcel(Parcel in) {
            return new NotificationTarget(in);
        }

        @Override
        public NotificationTarget[] newArray(int size) {
            return new NotificationTarget[size];
        }
    };

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeInt(id);
    }
}
