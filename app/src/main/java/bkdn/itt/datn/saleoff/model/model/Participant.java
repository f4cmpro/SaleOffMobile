package bkdn.itt.datn.saleoff.model.model;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TuDLT on 6/1/2018.
 */
public class Participant extends ModelBase{
    @SerializedName("id")
    private int participantId;
    @SerializedName("avatar")
    private String avatar;
    @SerializedName("username")
    private String participantName;
    @SerializedName("share_code")
    private int shareCode;
    @SerializedName("point")
    private int point;
    @SerializedName("is_used")
    private int isUsed;

    private boolean isSelect;

    public Participant() {
    }

    protected Participant(Parcel in) {
        participantId = in.readInt();
        avatar = in.readString();
        participantName = in.readString();
        shareCode = in.readInt();
        point = in.readInt();
        isUsed = in.readInt();
    }

    public static final Creator<Participant> CREATOR = new Creator<Participant>() {
        @Override
        public Participant createFromParcel(Parcel in) {
            return new Participant(in);
        }

        @Override
        public Participant[] newArray(int size) {
            return new Participant[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(participantId);
        dest.writeString(avatar);
        dest.writeString(participantName);
        dest.writeInt(shareCode);
        dest.writeInt(point);
        dest.writeInt(isUsed);
    }

    public int getParticipantId() {
        return participantId;
    }

    public void setParticipantId(int participantId) {
        this.participantId = participantId;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getParticipantName() {
        return participantName;
    }

    public void setParticipantName(String participantName) {
        this.participantName = participantName;
    }

    public int getShareCode() {
        return shareCode;
    }

    public void setShareCode(int shareCode) {
        this.shareCode = shareCode;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public int getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(int isUsed) {
        this.isUsed = isUsed;
    }


    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }
}
