package bkdn.itt.datn.saleoff.model.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TuDLT on 3/12/2018.
 */
public class Post extends ModelBase implements Parcelable {
    @SerializedName("id")
    private int postId;
    @SerializedName("shop_id")
    private int shopId;
    @SerializedName("product_id")
    private int productId;
    @SerializedName("user_id")
    private int userId;
    @SerializedName("sale_percent")
    private int salePercent;
    @SerializedName("view")
    private int viewNumber;
    @SerializedName("comment")
    private int commentNumber;
    @SerializedName("like")
    private int likeNumber;
    @SerializedName("cover")
    private String cover;
    @SerializedName("images")
    private String[] postImages;
    @SerializedName("title")
    private String postTitle;
    @SerializedName("description")
    private String postDescribe;
    @SerializedName("start_date")
    private String fromDate;
    @SerializedName("end_date")
    private String toDate;
    @SerializedName("created_at")
    private String createdDate;
    @SerializedName("updated_at")
    private String updateDate;
    @SerializedName("is_checked")
    private int isChecked;
    @SerializedName("is_trust")
    private int isTrust;
    @SerializedName("address")
    private String shopAddress;
    @SerializedName("user")
    private UserInfo user;
    @SerializedName("shop")
    private ShopInfo shop;
    @SerializedName("is_like")
    private int isLiked;
    @SerializedName("is_share")
    private int isShare;
    @SerializedName("post")
    private Post sharedPost;
    @SerializedName("category_name")
    private String catName;
    @SerializedName("product_name")
    private String subCatName;
    @SerializedName("is_event")
    private int isEvent;
    @SerializedName("event")
    private Event event;


    public Post() {

    }


    protected Post(Parcel in) {
        postId = in.readInt();
        shopId = in.readInt();
        productId = in.readInt();
        userId = in.readInt();
        salePercent = in.readInt();
        viewNumber = in.readInt();
        commentNumber = in.readInt();
        likeNumber = in.readInt();
        cover = in.readString();
        postImages = in.createStringArray();
        postTitle = in.readString();
        postDescribe = in.readString();
        fromDate = in.readString();
        toDate = in.readString();
        createdDate = in.readString();
        updateDate = in.readString();
        isChecked = in.readInt();
        isTrust = in.readInt();
        shopAddress = in.readString();
        user = in.readParcelable(UserInfo.class.getClassLoader());
        shop = in.readParcelable(ShopInfo.class.getClassLoader());
        isLiked = in.readInt();
        isShare = in.readInt();
        sharedPost = in.readParcelable(Post.class.getClassLoader());
        catName = in.readString();
        subCatName = in.readString();
        isEvent = in.readInt();
        event = in.readParcelable(Event.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(postId);
        dest.writeInt(shopId);
        dest.writeInt(productId);
        dest.writeInt(userId);
        dest.writeInt(salePercent);
        dest.writeInt(viewNumber);
        dest.writeInt(commentNumber);
        dest.writeInt(likeNumber);
        dest.writeString(cover);
        dest.writeStringArray(postImages);
        dest.writeString(postTitle);
        dest.writeString(postDescribe);
        dest.writeString(fromDate);
        dest.writeString(toDate);
        dest.writeString(createdDate);
        dest.writeString(updateDate);
        dest.writeInt(isChecked);
        dest.writeInt(isTrust);
        dest.writeString(shopAddress);
        dest.writeParcelable(user, flags);
        dest.writeParcelable(shop, flags);
        dest.writeInt(isLiked);
        dest.writeInt(isShare);
        dest.writeParcelable(sharedPost, flags);
        dest.writeString(catName);
        dest.writeString(subCatName);
        dest.writeInt(isEvent);
        dest.writeParcelable(event, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Post> CREATOR = new Creator<Post>() {
        @Override
        public Post createFromParcel(Parcel in) {
            return new Post(in);
        }

        @Override
        public Post[] newArray(int size) {
            return new Post[size];
        }
    };

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getSalePercent() {
        return salePercent;
    }

    public void setSalePercent(int salePercent) {
        this.salePercent = salePercent;
    }

    public int getViewNumber() {
        return viewNumber;
    }

    public void setViewNumber(int viewNumber) {
        this.viewNumber = viewNumber;
    }

    public int getCommentNumber() {
        return commentNumber;
    }

    public void setCommentNumber(int commentNumber) {
        this.commentNumber = commentNumber;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostDescribe() {
        return postDescribe;
    }

    public void setPostDescribe(String postDescribe) {
        this.postDescribe = postDescribe;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public int isChecked() {
        return isChecked;
    }

    public void setChecked(int checked) {
        isChecked = checked;
    }

    public int isTrust() {
        return isTrust;
    }

    public void setTrust(int trust) {
        isTrust = trust;
    }

    public void setLiked(int liked) {
        isLiked = liked;
    }

    public int isLiked() {
        return isLiked;
    }

    public int getLikeNumber() {
        return likeNumber;
    }

    public void setLikeNumber(int likeNumber) {
        this.likeNumber = likeNumber;
    }

    public String[] getPostImages() {
        return postImages;
    }

    public void setPostImages(String[] postImages) {
        this.postImages = postImages;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public UserInfo getUser() {
        return user;
    }

    public void setUser(UserInfo user) {
        this.user = user;
    }

    public ShopInfo getShop() {
        return shop;
    }

    public void setShop(ShopInfo shop) {
        this.shop = shop;
    }

    public Post getSharedPost() {
        return sharedPost;
    }

    public void setSharedPost(Post sharedPost) {
        this.sharedPost = sharedPost;
    }
    public int getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(int isChecked) {
        this.isChecked = isChecked;
    }

    public int getIsTrust() {
        return isTrust;
    }

    public void setIsTrust(int isTrust) {
        this.isTrust = isTrust;
    }

    public int getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(int isLiked) {
        this.isLiked = isLiked;
    }

    public int getIsShare() {
        return isShare;
    }

    public void setIsShare(int isShare) {
        this.isShare = isShare;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getSubCatName() {
        return subCatName;
    }

    public void setSubCatName(String subCatName) {
        this.subCatName = subCatName;
    }

    public int getIsEvent() {
        return isEvent;
    }

    public void setIsEvent(int isEvent) {
        this.isEvent = isEvent;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
}
