package bkdn.itt.datn.saleoff.model.model;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by TuDLT on 3/17/2018.
 */
public class ProductCategory extends ModelBase{
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("created_at")
    private String createdDate;
    @SerializedName("updated_at")
    private String updatedDate;
    @SerializedName("mSubCategories")
    private List<SubCategory> mSubCategories;

    public ProductCategory() {
    }

    protected ProductCategory(Parcel in) {
        id = in.readInt();
        name = in.readString();
        createdDate = in.readString();
        updatedDate = in.readString();
        mSubCategories = in.createTypedArrayList(SubCategory.CREATOR);
    }

    public static final Creator<ProductCategory> CREATOR = new Creator<ProductCategory>() {
        @Override
        public ProductCategory createFromParcel(Parcel in) {
            return new ProductCategory(in);
        }

        @Override
        public ProductCategory[] newArray(int size) {
            return new ProductCategory[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public List<SubCategory> getSubCategories() {
        return mSubCategories;
    }

    public void setSubCategories(List<SubCategory> subCategories) {
        this.mSubCategories = subCategories;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(createdDate);
        dest.writeString(updatedDate);
        dest.writeTypedList(mSubCategories);
    }
}
