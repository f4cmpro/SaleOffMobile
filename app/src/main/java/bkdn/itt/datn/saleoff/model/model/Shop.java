package bkdn.itt.datn.saleoff.model.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


/**
 * Created by TuDLT on 3/13/2018.
 */
public class Shop extends ModelBase implements Parcelable {
    @SerializedName("id")
    private int shopId;
    @SerializedName("owner_id")
    private int ownerId;
    @SerializedName("shop_cat_id")
    private int cateId;
    @SerializedName("category")
    private String cateName;
    @SerializedName("name")
    private String name;
    @SerializedName("phone")
    private String phone;
    @SerializedName("address")
    private String address;
    @SerializedName("avatar")
    private String avatar;
    @SerializedName("cover")
    private String cover;
    @SerializedName("web")
    private String web;
    @SerializedName("description")
    private String description;
    @SerializedName("rate")
    private float rate;
    @SerializedName("total_rate")
    private int totalRate;
    @SerializedName("is_active")
    private int isActive;
    @SerializedName("created_at")
    private String createdDate;
    @SerializedName("updated_at")
    private String updateDate;
    @SerializedName("follows")
    private int followers;
    @SerializedName("is_follow")
    private int isFollow;

    public Shop() {

    }

    protected Shop(Parcel in) {
        shopId = in.readInt();
        ownerId = in.readInt();
        cateId = in.readInt();
        name = in.readString();
        phone = in.readString();
        address = in.readString();
        avatar = in.readString();
        cover = in.readString();
        web = in.readString();
        description = in.readString();
        rate = in.readFloat();
        isActive = in.readInt();
        createdDate = in.readString();
        updateDate = in.readString();
        cateName = in.readString();
        totalRate = in.readInt();
        followers = in.readInt();
        isFollow = in.readInt();
    }

    public static final Creator<Shop> CREATOR = new Creator<Shop>() {
        @Override
        public Shop createFromParcel(Parcel in) {
            return new Shop(in);
        }

        @Override
        public Shop[] newArray(int size) {
            return new Shop[size];
        }
    };

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public int getCateId() {
        return cateId;
    }

    public void setCateId(int cateId) {
        this.cateId = cateId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setActive(int active) {
        isActive = active;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getCateName() {
        return cateName;
    }

    public void setCateName(String cateName) {
        this.cateName = cateName;
    }

    public int getTotalRate() {
        return totalRate;
    }

    public void setTotalRate(int totalRate) {
        this.totalRate = totalRate;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public int getFollowers() {
        return followers;
    }

    public void setFollowers(int followers) {
        this.followers = followers;
    }

    public int getIsFollow() {
        return isFollow;
    }

    public void setIsFollow(int isFollow) {
        this.isFollow = isFollow;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(shopId);
        dest.writeInt(ownerId);
        dest.writeInt(cateId);
        dest.writeString(name);
        dest.writeString(phone);
        dest.writeString(address);
        dest.writeString(avatar);
        dest.writeString(cover);
        dest.writeString(web);
        dest.writeString(description);
        dest.writeFloat(rate);
        dest.writeInt(isActive);
        dest.writeString(createdDate);
        dest.writeString(updateDate);
        dest.writeString(cateName);
        dest.writeInt(totalRate);
        dest.writeInt(followers);
        dest.writeInt(isFollow);
    }


}
