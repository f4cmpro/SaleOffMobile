package bkdn.itt.datn.saleoff.model.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TuDLT on 3/31/2018.
 */
public class ShopInfo extends ModelBase implements Parcelable {
    @SerializedName("id")
    private int mId;
    @SerializedName("avatar")
    private String mAvatar;
    @SerializedName("name")
    private String mName;
    @SerializedName("follows")
    private int mFollowsNumber;
    @SerializedName("posts")
    private int mPostsNumber;
    @SerializedName("rate")
    private float mRate;



    public ShopInfo() {
    }

    protected ShopInfo(Parcel in) {
        mId = in.readInt();
        mFollowsNumber = in.readInt();
        mPostsNumber = in.readInt();
        mRate = in.readFloat();
        mAvatar = in.readString();
        mName = in.readString();
    }

    public static final Creator<ShopInfo> CREATOR = new Creator<ShopInfo>() {
        @Override
        public ShopInfo createFromParcel(Parcel in) {
            return new ShopInfo(in);
        }

        @Override
        public ShopInfo[] newArray(int size) {
            return new ShopInfo[size];
        }
    };

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getAvatar() {
        return mAvatar;
    }

    public void setAvatar(String avatar) {
        mAvatar = avatar;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }


    public int getFollowsNumber() {
        return mFollowsNumber;
    }

    public void setFollowsNumber(int followsNumber) {
        mFollowsNumber = followsNumber;
    }

    public int getPostsNumber() {
        return mPostsNumber;
    }

    public void setPostsNumber(int postsNumber) {
        mPostsNumber = postsNumber;
    }

    public float getRate() {
        return mRate;
    }

    public void setRate(float rate) {
        mRate = rate;
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeInt(mFollowsNumber);
        dest.writeInt(mPostsNumber);
        dest.writeFloat(mRate);
        dest.writeString(mAvatar);
        dest.writeString(mName);
    }

}
