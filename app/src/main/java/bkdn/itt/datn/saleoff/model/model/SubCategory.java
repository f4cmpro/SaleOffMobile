package bkdn.itt.datn.saleoff.model.model;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TuDLT on 3/14/2018.
 */
public class SubCategory extends ModelBase {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("category_id")
    private int cateId;
    @SerializedName("created_at")
    private String createDate;
    @SerializedName("updated_at")
    private String updateDate;

    public SubCategory(){

    }

    protected SubCategory(Parcel in) {
        id = in.readInt();
        name = in.readString();
        cateId = in.readInt();
        createDate = in.readString();
        updateDate = in.readString();
    }

    public static final Creator<SubCategory> CREATOR = new Creator<SubCategory>() {
        @Override
        public SubCategory createFromParcel(Parcel in) {
            return new SubCategory(in);
        }

        @Override
        public SubCategory[] newArray(int size) {
            return new SubCategory[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCateId() {
        return cateId;
    }

    public void setCateId(int cateId) {
        this.cateId = cateId;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeInt(cateId);
        dest.writeString(createDate);
        dest.writeString(updateDate);
    }
}
