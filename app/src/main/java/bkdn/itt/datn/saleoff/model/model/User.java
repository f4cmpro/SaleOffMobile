package bkdn.itt.datn.saleoff.model.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import butterknife.BindView;

/**
 * Created by TuDLT on 3/14/2018.
 */
public class User extends ModelBase implements Parcelable {
    @SerializedName("id")
    private int id;
    @SerializedName("user_id")
    private String userId;
    @SerializedName("email")
    private String email;
    @SerializedName("username")
    private String username;
    @SerializedName("address")
    private String address;
    @SerializedName("gender")
    private int gender;
    @SerializedName("birthday")
    private String birthday;
    @SerializedName("phone")
    private String phone;
    @SerializedName("is_notify")
    private int isNotify;
    @SerializedName("avatar")
    private String avatar;
    @SerializedName("cover")
    private String cover;
    @SerializedName("created_at")
    private String createdDate;
    @SerializedName("updated_at")
    private String updateDate;
    @SerializedName("follows")
    private int followNumber;
    @SerializedName("is_follow")
    private int isFollow;
    @SerializedName("cares")
    private List<Integer> interestedSubjects;
    @SerializedName("bonus_code")
    private int bonus;
    @SerializedName("is_block")
    private int isBlock;
    @SerializedName("total")
    private int total;
    @SerializedName("share_code")
    private int shareCode;

    private boolean isSelect;

    public User() {
    }


    protected User(Parcel in) {
        id = in.readInt();
        userId = in.readString();
        email = in.readString();
        username = in.readString();
        address = in.readString();
        gender = in.readInt();
        birthday = in.readString();
        phone = in.readString();
        isNotify = in.readInt();
        avatar = in.readString();
        cover = in.readString();
        createdDate = in.readString();
        updateDate = in.readString();
        followNumber = in.readInt();
        isFollow = in.readInt();
        bonus = in.readInt();
        isBlock = in.readInt();
        total = in.readInt();
        shareCode = in.readInt();
        isSelect = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(userId);
        dest.writeString(email);
        dest.writeString(username);
        dest.writeString(address);
        dest.writeInt(gender);
        dest.writeString(birthday);
        dest.writeString(phone);
        dest.writeInt(isNotify);
        dest.writeString(avatar);
        dest.writeString(cover);
        dest.writeString(createdDate);
        dest.writeString(updateDate);
        dest.writeInt(followNumber);
        dest.writeInt(isFollow);
        dest.writeInt(bonus);
        dest.writeInt(isBlock);
        dest.writeInt(total);
        dest.writeInt(shareCode);
        dest.writeByte((byte) (isSelect ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getIsNotify() {
        return isNotify;
    }

    public void setIsNotify(int isNotify) {
        this.isNotify = isNotify;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public int getFollowNumber() {
        return followNumber;
    }

    public void setFollowNumber(int followNumber) {
        this.followNumber = followNumber;
    }


    public int getIsFollow() {
        return isFollow;
    }

    public void setIsFollow(int isFollow) {
        this.isFollow = isFollow;
    }

    public List<Integer> getInterestedSubjects() {
        return interestedSubjects;
    }

    public void setInterestedSubjects(List<Integer> interestedSubjects) {
        this.interestedSubjects = interestedSubjects;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public int getBonus() {
        return bonus;
    }

    public void setBonus(int bonus) {
        this.bonus = bonus;
    }

    public int getIsBlock() {
        return isBlock;
    }

    public void setIsBlock(int isBlock) {
        this.isBlock = isBlock;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public int getShareCode() {
        return shareCode;
    }

    public void setShareCode(int shareCode) {
        this.shareCode = shareCode;
    }
}
