package bkdn.itt.datn.saleoff.model.model;

import android.os.Parcel;

/**
 * Created by TuDLT on 3/8/2018.
 */
public class UserFacebook extends ModelBase {

    private String userId;
    private String avatar;
    private String firstName;
    private String lastName;
    private String gender;
    private String birthday;
    private String email;
    private String phone;

    public UserFacebook(String userId, String firstName, String lastName, String birthday,
                        String gender, String email, String phone, String avatar) {
        this.userId = userId;
        this.avatar = avatar;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.birthday = birthday;
        this.email = email;
        this.phone = phone;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeString(avatar);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(birthday);
        dest.writeString(gender);
        dest.writeString(email);
        dest.writeString(phone);
    }

    protected UserFacebook(Parcel in){
        userId = in.readString();
        avatar = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        birthday = in.readString();
        gender = in.readString();
        email = in.readString();
        phone = in.readString();
    }

    public static final Creator<UserFacebook> CREATOR = new Creator<UserFacebook>() {
        @Override
        public UserFacebook createFromParcel(Parcel in) {
            return new UserFacebook(in);
        }

        @Override
        public UserFacebook[] newArray(int size) {
            return new UserFacebook[size];
        }
    };
}
