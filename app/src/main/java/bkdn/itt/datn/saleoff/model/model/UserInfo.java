package bkdn.itt.datn.saleoff.model.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TuDLT on 4/10/2018.
 */
public class UserInfo implements Parcelable {
    @SerializedName("id")
    private int mId;
    @SerializedName("username")
    private String mUserName;
    @SerializedName("avatar")
    private String mAvatar;
    @SerializedName("follows")
    private int mFollowsNumber;
    @SerializedName("posts")
    private int mPostsNumber;

    private boolean isChecked;

    public UserInfo() {
    }


    protected UserInfo(Parcel in) {
        mId = in.readInt();
        mUserName = in.readString();
        mAvatar = in.readString();
        mFollowsNumber = in.readInt();
        mPostsNumber = in.readInt();
        isChecked = in.readByte() != 0;
    }

    public static final Creator<UserInfo> CREATOR = new Creator<UserInfo>() {
        @Override
        public UserInfo createFromParcel(Parcel in) {
            return new UserInfo(in);
        }

        @Override
        public UserInfo[] newArray(int size) {
            return new UserInfo[size];
        }
    };

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }

    public String getAvatar() {
        return mAvatar;
    }

    public void setAvatar(String avatar) {
        mAvatar = avatar;
    }

    public int getFollowsNumber() {
        return mFollowsNumber;
    }

    public void setFollowsNumber(int followsNumber) {
        this.mFollowsNumber = followsNumber;
    }

    public int getPostsNumber() {
        return mPostsNumber;
    }

    public void setPostsNumber(int postsNumber) {
        this.mPostsNumber = postsNumber;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(mUserName);
        dest.writeString(mAvatar);
        dest.writeInt(mFollowsNumber);
        dest.writeInt(mPostsNumber);
        dest.writeByte((byte) (isChecked ? 1 : 0));
    }
}
