package bkdn.itt.datn.saleoff.model.net;

/**
 * Created by $USER_NAME on 2/26/2018.
 */

public abstract class DataCallBack<T> {
    public abstract void onSuccess(T result);

    public abstract void onError(String errorMessage);

}