package bkdn.itt.datn.saleoff.model.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.concurrent.TimeUnit;

import bkdn.itt.datn.saleoff.utils.SharedPreferencesUtils;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by PhuocDH on 12/26/17.
 */

public class NetworkUtils {
    private static final String AUTHORIZATION = "Authorization";
    private static final String AUTHORIZATION_TYPE = "Bearer ";
    private static final long TIME_OUT = 5000;

    private static NetworkUtils mInstance;
    private Context context;
    private Retrofit retrofitWithAuthor;
    private ServerRequest serviceWithAuthor;
    private String authToken;

    public NetworkUtils() {
    }

    public static synchronized NetworkUtils getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new NetworkUtils();
            mInstance.setContext(context);
        }
        return mInstance;
    }

    public static synchronized NetworkUtils getInstance() {
        if (mInstance == null) {
            try {
                throw new Exception("The newInstance() function have to call after newInstance(Context context) function.");
            } catch (Exception var1) {
                var1.printStackTrace();
            }
        }
        return mInstance;
    }

    public static NetworkInfo getNetworkInfo(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    /**
     * Check internet connect
     *
     * @param context
     * @return
     */
    public static boolean isConnected(Context context) {
        NetworkInfo info = getNetworkInfo(context);
        return info != null && info.isConnected();
    }

    public Context getContext() {
        return this.context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public ServerRequest getRetrofitService() {
        // Init OkHttpClient
        authToken = SharedPreferencesUtils.getInstance(getContext()).getKeySaveToken();
        OkHttpClient.Builder okHttpBuilder = new OkHttpClient().newBuilder();
        okHttpBuilder.connectTimeout(TIME_OUT, TimeUnit.MILLISECONDS);
        okHttpBuilder.readTimeout(TIME_OUT, TimeUnit.MILLISECONDS);
        okHttpBuilder.addInterceptor(chain -> {
            Request original = chain.request();
            Request.Builder builder = original.newBuilder()
                    .addHeader("Content-Type", "application/json")
                    .addHeader(AUTHORIZATION, AUTHORIZATION_TYPE + authToken);
            Request request = builder.build();
            return chain.proceed(request);
        });

        if (null == retrofitWithAuthor) {
            retrofitWithAuthor = new Retrofit.Builder()
                    .baseUrl(ServerPath.URL_BASE)
                    .client(okHttpBuilder.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        if (null == serviceWithAuthor)
            serviceWithAuthor = retrofitWithAuthor.create(ServerRequest.class);
        return serviceWithAuthor;
    }

    public ServerRequest getRetrofitServiceWithOutHeader() {
        // Init OkHttpClient
        authToken = SharedPreferencesUtils.getInstance(getContext()).getKeySaveToken();
        OkHttpClient.Builder okHttpBuilder = new OkHttpClient().newBuilder();
        okHttpBuilder.connectTimeout(TIME_OUT, TimeUnit.MILLISECONDS);
        okHttpBuilder.readTimeout(TIME_OUT, TimeUnit.MILLISECONDS);
        okHttpBuilder.addInterceptor(chain -> {
            Request original = chain.request();

            Request.Builder builder = original.newBuilder()
                    .addHeader("Content-Type", "application/json");

            Request request = builder.build();
            return chain.proceed(request);
        });

        if (null == retrofitWithAuthor) {
            retrofitWithAuthor = new Retrofit.Builder()
                    .baseUrl(ServerPath.URL_BASE)
                    .client(okHttpBuilder.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        if (null == serviceWithAuthor)
            serviceWithAuthor = retrofitWithAuthor.create(ServerRequest.class);
        return serviceWithAuthor;
    }

    public ServerRequest getRetrofitServiceWithFormData() {
        // Init OkHttpClient
        authToken = SharedPreferencesUtils.getInstance(getContext()).getKeySaveToken();
        OkHttpClient.Builder okHttpBuilder = new OkHttpClient().newBuilder();
        okHttpBuilder.connectTimeout(TIME_OUT, TimeUnit.MILLISECONDS);
        okHttpBuilder.readTimeout(TIME_OUT, TimeUnit.MILLISECONDS);
        okHttpBuilder.addInterceptor(chain -> {
            Request original = chain.request();

            Request.Builder builder = original.newBuilder()
                    .addHeader(AUTHORIZATION, AUTHORIZATION_TYPE + authToken);

            Request request = builder.build();
            return chain.proceed(request);
        });

        if (null == retrofitWithAuthor) {
            retrofitWithAuthor = new Retrofit.Builder()
                    .baseUrl(ServerPath.URL_BASE)
                    .client(okHttpBuilder.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        if (null == serviceWithAuthor)
            serviceWithAuthor = retrofitWithAuthor.create(ServerRequest.class);
        return serviceWithAuthor;
    }
}
