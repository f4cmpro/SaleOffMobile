package bkdn.itt.datn.saleoff.model.net;

/**
 * Created by TuDLT on 2/26/2018.
 */
public class ServerPath {
    public static final String URL_BASE = "http://18.216.126.225:3333/";
    public static final String URL_MEDIA = "http://res.cloudinary.com/minori/image/upload/";
    public static final String API_LOGIN = "api/v1/login";
    public static final String API_LOGOUT = "api/v1/logout";
    public static final String API_PROFILE = "api/v1/user/profile";
    public static final String API_PROFILE_UPDATE = "api/v1/user/update-profile";
    public static final String API_GET_ALL_CATEGORY = "api/v1/category/get";
    public static final String API_GET_CATEGORIES_FOR_CREATE_SHOP = "api/v1/category/get-for-create";
    public static final String API_GET_SUB_CAT_BY_CAT_ID = "api/v1/product/category/{categoryId}";
    public static final String API_GET_ALL_SUB_CAT = "api/v1/product/get";
    public static final String API_CREATE_SHOP = "api/v1/shop/create";
    public static final String API_GET_OWNER_SHOP = "api/v1/shops";
    public static final String API_GET_WORKING_SHOP = "api/v1/shops/working";
    public static final String API_CREATE_POST = "api/v1/post/create";
    public static final String API_SEARCH_USER_TO_ADD_SHOP_MEMBER = "api/v1/shop/{shopId}/employee/find";
    public static final String API_GET_SHOP_DETAIL = "api/v1/shop/{shopId}/info";
    public static final String API_GET_SHOP_EMPLOYEES = "api/v1/shop/{shopId}/employee";
    public static final String API_ADD_SHOP_MEMBERS = "api/v1/shop/{shopId}/add-employee";
    public static final String API_GET_SHOP_POST = "api/v1/post/{page}/shop/{shopId}";
    public static final String API_GET_MY_POST = "api/v1/post/{page}/yours";
    public static final String API_GET_SHOP_FOLLOW = "api/v1/shop/followed";
    public static final String API_DELETE_SHOP_POST = "api/v1/post/{postId}/delete/{shopId}/shop";
    public static final String API_EDIT_SHOP_POST = "api/v1/post/{postId}/edit/{shopId}/shop";
    public static final String API_EDIT_YOUR_POST = "api/v1/post/{postId}/edit-your-post";
    public static final String API_DELETE_YOUR_POST = "api/v1/post/{postId}/delete-your-post";
    public static final String API_FOLLOWING_NEWS_FEED = "api/v1/newsfeed/following";
    public static final String API_NOT_FOLLOWING_NEWS_FEED = "api/v1/newsfeed/not-following";
    public static final String API_CARE_FOLLOW = "api/v1/newsfeed/care-follow";
    public static final String API_CARE_NOT_FOLLOW = "api/v1/newsfeed/care-not-follow";
    public static final String API_ADD_COMMENT = "api/v1/post/comment/{postId}/add";
    public static final String API_EDIT_COMMENT = "api/v1/post/comment/{commentId}/edit";
    public static final String API_GET_COMMENT = "api/v1/post/comments";
    public static final String API_LIKE_POST = "api/v1/post/{postId}/like";
    public static final String API_UN_LIKE_POST = "api/v1/post/{postId}/unlike";
    public static final String API_POST_DETAIL = "api/v1/post/{postId}/get";
    public static final String API_SEARCH_POST = "api/v1/search/post";
    public static final String API_SEARCH_SHOP = "api/v1/search/shop";
    public static final String API_GET_SEARCH_USER = "api/v1/search/user";
    public static final String API_GET_SUGGESTED_SHOP = "api/v1/shop/follow/search";
    public static final String API_FOLLOW_SHOP = "api/v1/shop/{shopId}/follow";
    public static final String API_UN_FOLLOW_SHOP = "api/v1/shop/{shopId}/unfollow";
    public static final String API_GET_RECENT_POST = "api/v1/post/{postId}/relative";
    public static final String API_RATING_SHOP = "api/v1/shop/{shopId}/rate";
    public static final String API_GET_USER_FOLLOW = "api/v1/user-followed";
    public static final String API_GET_USER_TO_FOLLOW = "api/v1/user/follow/search";
    public static final String API_FOLLOW_USER = "api/v1/user/{userId}/follow";
    public static final String API_UN_FOLLOW_USER = "api/v1/user/{userId}/unfollow";
    public static final String API_GET_NOTIFICATIONS= "api/v1/notification/get";
    public static final String API_DELETE_NOTIFICATION = "api/v1/notification/{id}/delete";
    public static final String API_GET_USER_POST = "api/v1/post/{userId}/user";
    public static final String API_GET_USER_DETAIL = "/api/v1/user/{userId}";
    public static final String API_DELETE_SHOP_EMPLOYEE = "api/v1/shop/{shopId}/remove-employee";
    public static final String API_EDIT_SHOP_INFO = "api/v1/shop/{shopId}/update";
    public static final String API_DELETE_SHOP = "api/v1/shop/{shopId}/delete";
    public static final String API_SHARE_POST = "api/v1/post/share";
    public static final String API_GET_POSITIVE_MEMBERS = "api/v1/shop/{shopId}/share-members";
    public static final String API_SEND_REPORT = "api/v1/report/send";
    public static final String API_GET_LIST_REPORT = "api/v1/report/{type}/get";

    public static final String API_MINUS_BONUS = "api/v1/shop/{shopId}/use-bonus";
    public static final String API_CREATE_EVENT = "api/v1/events/create";
    public static final String API_GET_ALL_SHOP_EVENTS = "api/v1/events/{shopId}/shop";
    public static final String API_GET_HOT_EVENTS = "api/v1/events/{page}/recently/{type}";
    public static final String API_GET_MY_EVENTS = "api/v1/events";
    public static final String API_GET_EVENT_DETAIL = "api/v1/events/{eventId}/detail";
    public static final String API_GET_PARTICIPANTS = "api/v1/events/{eventId}/participants";
    public static final String API_JOIN_EVENT = "api/v1/events/join";
    public static final String API_USE_EVENT_POINT = "api/v1/events/{eventId}/user/{userId}/use";
}
