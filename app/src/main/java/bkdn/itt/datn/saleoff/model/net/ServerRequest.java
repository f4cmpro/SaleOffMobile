package bkdn.itt.datn.saleoff.model.net;

import java.util.HashMap;
import java.util.Map;

import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.model.response.CommentListResponse;
import bkdn.itt.datn.saleoff.model.response.CommentResponse;
import bkdn.itt.datn.saleoff.model.response.CreateShopResponse;
import bkdn.itt.datn.saleoff.model.response.EventListResponse;
import bkdn.itt.datn.saleoff.model.response.EventResponse;
import bkdn.itt.datn.saleoff.model.response.JoinEventResponse;
import bkdn.itt.datn.saleoff.model.response.NotificationResponse;
import bkdn.itt.datn.saleoff.model.response.ParticipantsResponse;
import bkdn.itt.datn.saleoff.model.response.PostDetailResponse;
import bkdn.itt.datn.saleoff.model.response.PostResponse;
import bkdn.itt.datn.saleoff.model.response.LoginResponse;
import bkdn.itt.datn.saleoff.model.response.ShopResponse;
import bkdn.itt.datn.saleoff.model.response.SubCategoryResponse;
import bkdn.itt.datn.saleoff.model.response.ProfileResponse;
import bkdn.itt.datn.saleoff.model.response.CategoryResponse;
import bkdn.itt.datn.saleoff.model.response.ShopDetailResponse;
import bkdn.itt.datn.saleoff.model.response.ShopInfoResponse;
import bkdn.itt.datn.saleoff.model.response.UserDetailResponse;
import bkdn.itt.datn.saleoff.model.response.UserInfoResponse;
import bkdn.itt.datn.saleoff.model.response.UserResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by TuDLT on 2/26/2018.
 */
public interface ServerRequest {

    /*Get*/
    @GET(ServerPath.API_PROFILE)
    Call<ProfileResponse> getProfile();

    @PUT(ServerPath.API_PROFILE_UPDATE)
    Call<ProfileResponse> editProfile(@Body HashMap<String, Object> map);

    @GET(ServerPath.API_GET_CATEGORIES_FOR_CREATE_SHOP)
    Call<CategoryResponse> getCategoriesForCreateShop();

    @GET(ServerPath.API_GET_OWNER_SHOP)
    Call<ShopInfoResponse> getOwnerShop();

    @GET(ServerPath.API_GET_WORKING_SHOP)
    Call<ShopInfoResponse> getWorkingShop();


    /*Post*/
    @POST(ServerPath.API_LOGIN)
    Call<LoginResponse> doLogin(@Body HashMap<String, Object> map);

    @GET(ServerPath.API_LOGOUT)
    Call<BaseResponse> doLogOut();

    @POST(ServerPath.API_CREATE_SHOP)
    Call<CreateShopResponse> createShop(@Body HashMap<String, Object> map);

    @GET(ServerPath.API_GET_SUB_CAT_BY_CAT_ID)
    Call<SubCategoryResponse> getSubCategories(@Path("categoryId") int catId);

    @POST(ServerPath.API_CREATE_POST)
    Call<BaseResponse> createPost(@Body HashMap<String, Object> map);

    @FormUrlEncoded
    @POST(ServerPath.API_SEARCH_USER_TO_ADD_SHOP_MEMBER)
    Call<UserInfoResponse> getUsersToAddShopMember(@Path("shopId") int shopId, @Field("text") String searchText);

    @GET(ServerPath.API_GET_SHOP_DETAIL)
    Call<ShopDetailResponse> getShopDetail(@Path("shopId") int shopId);

    @GET(ServerPath.API_GET_SHOP_EMPLOYEES)
    Call<UserResponse> getShopEmployees(@Path("shopId") int shopId);

    @GET(ServerPath.API_GET_SHOP_POST)
    Call<PostResponse> getShopPosts(@Path("page") int page, @Path("shopId") int shopId);

    @POST(ServerPath.API_ADD_SHOP_MEMBERS)
    Call<BaseResponse> addShopMembers(@Path("shopId") int shopId, @Body HashMap<String, Object> map);

    @GET(ServerPath.API_GET_MY_POST)
    Call<PostResponse> getMyPosts(@Path("page") int page);

    @GET(ServerPath.API_GET_SHOP_FOLLOW)
    Call<ShopInfoResponse> getFollowedShop();

    @DELETE(ServerPath.API_DELETE_SHOP_POST)
    Call<BaseResponse> deleteShopPost(@Path("postId") int postId, @Path("shopId") int shopId);

    @PUT(ServerPath.API_EDIT_SHOP_POST)
    Call<BaseResponse> editShopPost(@Path("postId") int postId, @Path("shopId") int shopId,
                                    @Body HashMap<String, Object> map);

    @PUT(ServerPath.API_EDIT_YOUR_POST)
    Call<BaseResponse> editUserPost(@Path("postId") int postId, @Body HashMap<String, Object> map);

    @DELETE(ServerPath.API_DELETE_YOUR_POST)
    Call<BaseResponse> deleteUserPost(@Path("postId") int postId);


    @POST(ServerPath.API_FOLLOWING_NEWS_FEED)
    Call<PostResponse> getFollowingNewsFeeds(@Body HashMap<String, Object> map);

    @POST(ServerPath.API_NOT_FOLLOWING_NEWS_FEED)
    Call<PostResponse> getNotFollowingNewsFeeds(@Body HashMap<String, Object> map);

    @POST(ServerPath.API_CARE_FOLLOW)
    Call<PostResponse> getCareFollow(@Body HashMap<String, Object> map);

    @POST(ServerPath.API_CARE_NOT_FOLLOW)
    Call<PostResponse> getCareNotFollow(@Body HashMap<String, Object> map);

    @GET(ServerPath.API_GET_ALL_CATEGORY)
    Call<CategoryResponse> getAllCategory();

    @GET(ServerPath.API_GET_ALL_SUB_CAT)
    Call<SubCategoryResponse> getAllSubCategory();

    @FormUrlEncoded
    @POST(ServerPath.API_ADD_COMMENT)
    Call<CommentResponse> addComment(@Path("postId") int postId, @Field("comment") String comment);

    @FormUrlEncoded
    @PUT(ServerPath.API_EDIT_COMMENT)
    Call<BaseResponse> editComment(@Path("commentId") int commentId, @Field("comment") String comment);

    @POST(ServerPath.API_GET_COMMENT)
    Call<CommentListResponse> getComment(@Body() HashMap<String, Object> map);

    @GET(ServerPath.API_LIKE_POST)
    Call<BaseResponse> likePost(@Path("postId") int postId);

    @GET(ServerPath.API_UN_LIKE_POST)
    Call<BaseResponse> unLikePost(@Path("postId") int postId);

    @GET(ServerPath.API_POST_DETAIL)
    Call<PostDetailResponse> getPostDetail(@Path("postId") int postId);

    @POST(ServerPath.API_SEARCH_POST)
    Call<PostResponse> searchPost(@Body HashMap<String, Object> map);

    @POST(ServerPath.API_SEARCH_SHOP)
    Call<ShopResponse> searchShop(@Body HashMap<String, Object> map);

    @POST(ServerPath.API_GET_SEARCH_USER)
    Call<UserResponse> searchUser(@Body HashMap<String, Object> map);

    @GET(ServerPath.API_GET_SUGGESTED_SHOP)
    Call<ShopResponse> getSuggestedShop();

    @POST(ServerPath.API_FOLLOW_SHOP)
    Call<BaseResponse> followShop(@Path("shopId") int shopId, @Body HashMap<String, Object> map);

    @GET(ServerPath.API_UN_FOLLOW_SHOP)
    Call<BaseResponse> unFollowShop(@Path("shopId") int shopId);

    @GET(ServerPath.API_GET_RECENT_POST)
    Call<PostResponse> getRecentPost(@Path("postId") int id);

    @POST(ServerPath.API_RATING_SHOP)
    Call<BaseResponse> ratingShop(@Path("shopId") int shopId, @Body HashMap<String, Object> map);

    @GET(ServerPath.API_GET_USER_FOLLOW)
    Call<UserInfoResponse> getFollowedUser();

    @GET(ServerPath.API_GET_USER_TO_FOLLOW)
    Call<UserResponse> getSuggestedUser();

    @GET(ServerPath.API_FOLLOW_USER)
    Call<BaseResponse> followUser(@Path("userId") int userId);

    @GET(ServerPath.API_UN_FOLLOW_USER)
    Call<BaseResponse> unFollowUser(@Path("userId") int userId);

    @GET(ServerPath.API_GET_NOTIFICATIONS)
    Call<NotificationResponse> getNotifications();

    @DELETE(ServerPath.API_DELETE_NOTIFICATION)
    Call<BaseResponse> deleteNotById(@Path("id") String notId);

    @POST(ServerPath.API_GET_USER_POST)
    Call<PostResponse> getUserPost(@Path("userId") int userId, @Body HashMap<String, Object> map);

    @GET(ServerPath.API_GET_USER_DETAIL)
    Call<UserDetailResponse> getUserById(@Path("userId") int shopId);

    @POST(ServerPath.API_DELETE_SHOP_EMPLOYEE)
    Call<BaseResponse> deleteShopEmployee(@Path("shopId") int shopId, @Body HashMap<String, Object> map);

    @PUT(ServerPath.API_EDIT_SHOP_INFO)
    Call<BaseResponse> editShop(@Path("shopId") int shopId, @Body HashMap<String, Object> map);

    @GET(ServerPath.API_DELETE_SHOP)
    Call<BaseResponse> deleteShop(@Path("shopId") int shopId);

    @POST(ServerPath.API_SHARE_POST)
    Call<BaseResponse> sharePost(@Body Map<String, Object> map);

    @GET(ServerPath.API_GET_POSITIVE_MEMBERS)
    Call<UserResponse> getShopMembers(@Path("shopId") int shopId);

    @POST(ServerPath.API_SEND_REPORT)
    Call<BaseResponse> sendReport(@Body HashMap<String, Object> map);

    @GET(ServerPath.API_GET_LIST_REPORT)
    Call<BaseResponse> getListReport(@Path("type") int type);

    @POST(ServerPath.API_MINUS_BONUS)
    Call<BaseResponse> minusBonus(@Path("shopId") int shopId, @Body HashMap<String, Object> map);

    @POST(ServerPath.API_CREATE_EVENT)
    Call<BaseResponse> createEvent(@Body HashMap<String, Object> map);

    @GET(ServerPath.API_GET_ALL_SHOP_EVENTS)
    Call<EventListResponse> getAllShopEvents(@Path("shopId") int shopId);

    @GET(ServerPath.API_GET_HOT_EVENTS)
    Call<EventListResponse> getAllEvents(@Path("page") int page, @Path("type") int type);

    @GET(ServerPath.API_GET_MY_EVENTS)
    Call<EventListResponse> getAllMyEvents();

    @GET(ServerPath.API_GET_EVENT_DETAIL)
    Call<EventResponse> getEventDetail(@Path("eventId") int eventId);

    @GET(ServerPath.API_GET_PARTICIPANTS)
    Call<ParticipantsResponse> getParticipants(@Path("eventId") int eventId);

    @POST(ServerPath.API_JOIN_EVENT)
    Call<JoinEventResponse> joinEvent(@Body HashMap<String, Object> map);

    @GET(ServerPath.API_USE_EVENT_POINT)
    Call<BaseResponse> useEventPoint(@Path("eventId") int eventId, @Path("userId") int participantId);
}
