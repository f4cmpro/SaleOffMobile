package bkdn.itt.datn.saleoff.model.repository;

import android.content.Context;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.net.NetworkUtils;
import bkdn.itt.datn.saleoff.model.response.SubCategoryResponse;
import bkdn.itt.datn.saleoff.utils.Utils;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by TuDLT on 5/18/2018.
 */
public class RepositoryCategorySub {

    private static RepositoryCategorySub sRepositoryCategorySub;

    public static synchronized RepositoryCategorySub getInstance() {
        if (sRepositoryCategorySub == null) {
            sRepositoryCategorySub = new RepositoryCategorySub();
        }
        return sRepositoryCategorySub;
    }

    public void getAllSubCategory(Context context, DataCallBack<SubCategoryResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().getAllSubCategory()
                    .enqueue(new retrofit2.Callback<SubCategoryResponse>() {
                        @Override
                        public void onResponse(Call<SubCategoryResponse> call,
                                               Response<SubCategoryResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<SubCategoryResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });

        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));
        }
    }

}
