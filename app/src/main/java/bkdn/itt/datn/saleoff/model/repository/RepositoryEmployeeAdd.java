package bkdn.itt.datn.saleoff.model.repository;

import android.content.Context;

import java.util.HashMap;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.net.NetworkUtils;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.model.response.UserInfoResponse;
import bkdn.itt.datn.saleoff.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TuDLT on 5/18/2018.
 */
public class RepositoryEmployeeAdd {

    private static RepositoryEmployeeAdd sRepositoryEmployeeAdd;

    public static RepositoryEmployeeAdd getInstance() {
        if (sRepositoryEmployeeAdd == null) {
            sRepositoryEmployeeAdd = new RepositoryEmployeeAdd();
        }
        return sRepositoryEmployeeAdd;
    }


    public void getUsersToAddEmployee(Context context, int shopId, String searchText,
                                      DataCallBack<UserInfoResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().
                    getUsersToAddShopMember(shopId, searchText).enqueue(new Callback<UserInfoResponse>() {
                @Override
                public void onResponse(Call<UserInfoResponse> call, Response<UserInfoResponse> response) {
                    Utils.checkAndReceiveResponse(context, response, callBack);
                }

                @Override
                public void onFailure(Call<UserInfoResponse> call, Throwable t) {
                    callBack.onError(t.getMessage());
                }
            });
        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));
        }
    }

    public void addShopEmployees(Context context, int shopId, List<Integer> userIds, DataCallBack<BaseResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            Integer[] ids = new Integer[userIds.size()];
            userIds.toArray(ids);
            HashMap<String, Object> map = new HashMap<>();
            map.put("ids", ids);
            NetworkUtils.getInstance(context).getRetrofitService().addShopMembers(shopId, map)
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));
        }
    }

}
