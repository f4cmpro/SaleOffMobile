package bkdn.itt.datn.saleoff.model.repository;

import android.content.Context;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.net.NetworkUtils;
import bkdn.itt.datn.saleoff.model.response.EventListResponse;
import bkdn.itt.datn.saleoff.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TuDLT on 5/29/2018.
 */
public class RepositoryEvent {
    private static RepositoryEvent sRepositoryEvent;

    public static RepositoryEvent getInstance() {
        if (sRepositoryEvent == null) {
            sRepositoryEvent = new RepositoryEvent();
        }
        return sRepositoryEvent;
    }


    public void getHappeningEvents(Context context, int happeningPage, DataCallBack<EventListResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().getAllEvents(happeningPage, ConstantDefine.EVENT_HAPPENING)
                    .enqueue(new Callback<EventListResponse>() {
                        @Override
                        public void onResponse(Call<EventListResponse> call, Response<EventListResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<EventListResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getString(R.string.error_not_connect));
        }
    }

    public void getGoingHappenEvents(Context context, int goingHappen, DataCallBack<EventListResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().getAllEvents(goingHappen, ConstantDefine.EVENT_GOING_HAPPEN)
                    .enqueue(new Callback<EventListResponse>() {
                        @Override
                        public void onResponse(Call<EventListResponse> call, Response<EventListResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<EventListResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getString(R.string.error_not_connect));
        }
    }

    public void getHappenedEvents(Context context, int happenedPage, DataCallBack<EventListResponse> callBack) {

        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().getAllEvents(happenedPage, ConstantDefine.EVENT_HAPPENED)
                    .enqueue(new Callback<EventListResponse>() {
                        @Override
                        public void onResponse(Call<EventListResponse> call, Response<EventListResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<EventListResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getString(R.string.error_not_connect));
        }
    }

    public void getJoinedEvents(Context context, DataCallBack<EventListResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().getAllMyEvents()
                    .enqueue(new Callback<EventListResponse>() {
                        @Override
                        public void onResponse(Call<EventListResponse> call, Response<EventListResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<EventListResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getString(R.string.error_not_connect));
        }
    }

}
