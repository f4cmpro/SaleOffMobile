package bkdn.itt.datn.saleoff.model.repository;

import android.content.Context;

import java.util.HashMap;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.Event;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.net.NetworkUtils;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TuDLT on 5/29/2018.
 */
public class RepositoryEventCreate {
    private static RepositoryEventCreate sRepositoryEventCreate;

    public static RepositoryEventCreate getInstance() {
        if (sRepositoryEventCreate == null) {
            sRepositoryEventCreate = new RepositoryEventCreate();
        }

        return sRepositoryEventCreate;
    }

    public void createEvent(Context context, Event createdEvent, DataCallBack<BaseResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("shop_id", createdEvent.getShopId());
            map.put("address", createdEvent.getAddress());
            map.put("name", createdEvent.getTitle());
            map.put("detail", createdEvent.getDetail());
            map.put("cover", createdEvent.getCover());
            map.put("images", createdEvent.getImages());
            map.put("from", createdEvent.getStartDate());
            map.put("to", createdEvent.getEndDate());
            NetworkUtils.getInstance(context).getRetrofitService().createEvent(map)
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getString(R.string.error_not_connect));
        }
    }

}
