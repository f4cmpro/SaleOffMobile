package bkdn.itt.datn.saleoff.model.repository;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.net.NetworkUtils;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.model.response.EventResponse;
import bkdn.itt.datn.saleoff.model.response.JoinEventResponse;
import bkdn.itt.datn.saleoff.model.response.ParticipantsResponse;
import bkdn.itt.datn.saleoff.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TuDLT on 5/31/2018.
 */
public class RepositoryEventDetail {
    private static RepositoryEventDetail sRepositoryEventDetail;

    public static RepositoryEventDetail getInstance() {
        if (sRepositoryEventDetail == null) {
            sRepositoryEventDetail = new RepositoryEventDetail();
        }
        return sRepositoryEventDetail;
    }

    public void getEventDetail(Context context, int eventId, DataCallBack<EventResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().getEventDetail(eventId)
                    .enqueue(new Callback<EventResponse>() {
                        @Override
                        public void onResponse(Call<EventResponse> call, Response<EventResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<EventResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getString(R.string.error_not_connect));
        }
    }

    public void getParticipants(Context context, int eventId, DataCallBack<ParticipantsResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().getParticipants(eventId)
                    .enqueue(new Callback<ParticipantsResponse>() {
                        @Override
                        public void onResponse(Call<ParticipantsResponse> call, Response<ParticipantsResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<ParticipantsResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getString(R.string.error_not_connect));
        }
    }

    public void joinEvent(Context context, int eventId, int presenterCode, String shareDescription,
                          DataCallBack<JoinEventResponse> callBack) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("event_id", eventId);
        if (presenterCode != 0) {
            map.put("share_code", presenterCode);
        }
        if (shareDescription != null && !shareDescription.isEmpty()) {
            map.put("description", shareDescription);
        }
        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().joinEvent(map)
                    .enqueue(new Callback<JoinEventResponse>() {
                        @Override
                        public void onResponse(Call<JoinEventResponse> call, Response<JoinEventResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<JoinEventResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getString(R.string.error_not_connect));
        }

    }
}
