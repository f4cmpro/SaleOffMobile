package bkdn.itt.datn.saleoff.model.repository;

import android.content.Context;

import java.util.HashMap;

import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.net.NetworkUtils;
import bkdn.itt.datn.saleoff.model.response.LoginResponse;
import bkdn.itt.datn.saleoff.model.response.ProfileResponse;
import bkdn.itt.datn.saleoff.utils.SharedPreferencesUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TuDLT on 3/7/2018.
 */
public class RepositoryLogin {
    private static RepositoryLogin sRepositoryLogin;

    public static RepositoryLogin getInstance() {
        if (sRepositoryLogin == null) {
            sRepositoryLogin = new RepositoryLogin();
        }
        return sRepositoryLogin;
    }

    public void doLogin(Context context, String token, DataCallBack<LoginResponse> callBack) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("id_token", token);
        map.put("device_token", SharedPreferencesUtils.getInstance().getKeySaveGcmRegistrationDeviceToken());
        map.put("platform", "android");
        NetworkUtils.getInstance(context).getRetrofitService().doLogin(map).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                Utils.checkAndReceiveResponse(context, response, callBack);
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                callBack.onError(t.getMessage());
            }
        });

    }

    public void getProfile(Context context, DataCallBack<ProfileResponse> callBack) {
        NetworkUtils.getInstance(context).getRetrofitService().getProfile().
                enqueue(new Callback<ProfileResponse>() {
                    @Override
                    public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                        Utils.checkAndReceiveResponse(context, response, callBack);
                    }

                    @Override
                    public void onFailure(Call<ProfileResponse> call, Throwable t) {
                        callBack.onError(t.getMessage());
                    }
                });
    }





}
