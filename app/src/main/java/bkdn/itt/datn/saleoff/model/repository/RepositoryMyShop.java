package bkdn.itt.datn.saleoff.model.repository;

import android.content.Context;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.net.NetworkUtils;
import bkdn.itt.datn.saleoff.model.response.ShopInfoResponse;
import bkdn.itt.datn.saleoff.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TuDLT on 5/18/2018.
 */
public class RepositoryMyShop {

    public static RepositoryMyShop sRepositoryMyShop;

    public static synchronized RepositoryMyShop getInstance() {
        if (sRepositoryMyShop == null) {
            sRepositoryMyShop = new RepositoryMyShop();
        }
        return sRepositoryMyShop;
    }
    public void getOwnedShop(Context context, DataCallBack<ShopInfoResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().getOwnerShop().enqueue(new Callback<ShopInfoResponse>() {
                @Override
                public void onResponse(Call<ShopInfoResponse> call, Response<ShopInfoResponse> response) {
                    Utils.checkAndReceiveResponse(context, response, callBack);
                }

                @Override
                public void onFailure(Call<ShopInfoResponse> call, Throwable t) {
                    callBack.onError(t.getMessage());
                }
            });
        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));
        }
    }

    public void getWorkingShop(Context context, DataCallBack<ShopInfoResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().getWorkingShop().enqueue(new Callback<ShopInfoResponse>() {
                @Override
                public void onResponse(Call<ShopInfoResponse> call, Response<ShopInfoResponse> response) {
                    Utils.checkAndReceiveResponse(context, response, callBack);
                }

                @Override
                public void onFailure(Call<ShopInfoResponse> call, Throwable t) {
                    callBack.onError(t.getMessage());
                }
            });
        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));
        }
    }
}
