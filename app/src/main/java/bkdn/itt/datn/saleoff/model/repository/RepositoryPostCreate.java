package bkdn.itt.datn.saleoff.model.repository;

import android.content.Context;

import java.util.HashMap;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.Post;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.net.NetworkUtils;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TuDLT on 5/18/2018.
 */
public class RepositoryPostCreate {
    public static RepositoryPostCreate sRepositoryPostCreate;

    public static synchronized RepositoryPostCreate getInstance() {
        if (sRepositoryPostCreate == null) {
            sRepositoryPostCreate = new RepositoryPostCreate();
        }
        return sRepositoryPostCreate;
    }

    public void createPost(Context context, Post createdPost, DataCallBack<BaseResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("title", createdPost.getPostTitle());
            map.put("description", createdPost.getPostDescribe());
            map.put("sale_percent", createdPost.getSalePercent());
            if (createdPost.getPostImages() != null) {
                map.put("cover", createdPost.getPostImages()[0]);
            }
            map.put("images", createdPost.getPostImages());
            map.put("start_date", createdPost.getFromDate());
            map.put("end_date", createdPost.getToDate());
            map.put("address", createdPost.getShopAddress());
            map.put("product_id", createdPost.getProductId());
            if (createdPost.getShopId() != 0) {
                map.put("shop_id", createdPost.getShopId());
            }
            NetworkUtils.getInstance(context).getRetrofitService().createPost(map).enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    Utils.checkAndReceiveResponse(context, response, callBack);
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    callBack.onError(t.getMessage());
                }
            });
        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));
        }
    }
}
