package bkdn.itt.datn.saleoff.model.repository;

import android.content.Context;

import java.util.HashMap;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.Post;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.net.NetworkUtils;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TuDLT on 5/18/2018.
 */
public class RepositoryPostEdit {
    public static RepositoryPostEdit sRepositoryPostEdit;

    public static synchronized RepositoryPostEdit getInstance() {
        if (sRepositoryPostEdit == null) {
            sRepositoryPostEdit = new RepositoryPostEdit();
        }
        return sRepositoryPostEdit;
    }
    public void editShopPost(Context context, Post post, DataCallBack<BaseResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("title", post.getPostTitle());
            map.put("description", post.getPostDescribe());
            map.put("sale_percent", post.getSalePercent());
            map.put("cover", post.getCover());
            map.put("images", post.getPostImages());
            map.put("start_date", post.getFromDate());
            map.put("end_date", post.getToDate());
            map.put("address", post.getShopAddress());
            map.put("product_id", post.getProductId());

            NetworkUtils.getInstance(context).getRetrofitService().editShopPost(post.getPostId(),
                    post.getShopId(), map).enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    Utils.checkAndReceiveResponse(context, response, callBack);
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    callBack.onError(t.getMessage());
                }
            });
        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));

        }
    }

    public void editUserPost(Context context, Post post, DataCallBack<BaseResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("title", post.getPostTitle());
            map.put("description", post.getPostDescribe());
            map.put("sale_percent", post.getSalePercent());
            map.put("cover", post.getCover());
            map.put("images", post.getPostImages());
            map.put("start_date", post.getFromDate());
            map.put("end_date", post.getToDate());
            map.put("address", post.getShopAddress());
            map.put("product_id", post.getProductId());

            NetworkUtils.getInstance(context).getRetrofitService().editUserPost(post.getPostId(), map)
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));

        }
    }

}
