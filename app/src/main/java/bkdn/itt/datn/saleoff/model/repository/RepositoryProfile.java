package bkdn.itt.datn.saleoff.model.repository;

import android.content.Context;

import java.util.HashMap;

import bkdn.itt.datn.saleoff.model.model.User;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.net.NetworkUtils;
import bkdn.itt.datn.saleoff.model.response.ProfileResponse;
import bkdn.itt.datn.saleoff.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TuDLT on 3/17/2018.
 */
public class RepositoryProfile {
    public static RepositoryProfile sRepositoryProfile;

    public static RepositoryProfile getInstance() {
        if (sRepositoryProfile == null) {
            sRepositoryProfile = new RepositoryProfile();
        }
        return sRepositoryProfile;
    }

    public void editProfile(Context context, User user, DataCallBack<ProfileResponse> callBack) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("username", user.getUsername());
        map.put("avatar", user.getAvatar());
        map.put("cover", user.getCover());
        map.put("address", user.getAddress());
        map.put("gender", String.valueOf(user.getGender()));
        if(user.getBirthday().contains("-")){
            map.put("birthday", user.getBirthday());
        }
        map.put("phone", user.getPhone());
        map.put("cares", user.getInterestedSubjects());
        NetworkUtils.getInstance(context).getRetrofitService().editProfile(map).enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                Utils.checkAndReceiveResponse(context, response, callBack);
            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
                callBack.onError(t.getMessage());
            }
        });


    }


}
