package bkdn.itt.datn.saleoff.model.repository;

import android.content.Context;

import java.util.HashMap;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.net.NetworkUtils;
import bkdn.itt.datn.saleoff.model.response.PostResponse;
import bkdn.itt.datn.saleoff.model.response.ShopResponse;
import bkdn.itt.datn.saleoff.model.response.UserResponse;
import bkdn.itt.datn.saleoff.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TuDLT on 4/27/2018.
 */
public class RepositorySearch {
    public static RepositorySearch sRepositorySearch;

    public static synchronized RepositorySearch getInstance() {
        if (sRepositorySearch == null) {
            sRepositorySearch = new RepositorySearch();
        }
        return sRepositorySearch;
    }

    public void searchPost(Context context, int page, String type, String textKey, boolean isFollow, int catId, int subCatId, DataCallBack<PostResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("text", textKey);
            map.put("page", page);
            map.put("type", type);
            map.put("follow", isFollow);
            map.put("category_id", catId);
            if(catId != 1 && subCatId != 0){
                map.put("sub_category_id", subCatId);
            }

            NetworkUtils.getInstance(context).getRetrofitService().searchPost(map)
                    .enqueue(new Callback<PostResponse>() {
                        @Override
                        public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<PostResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getString(R.string.error_not_connect));
        }
    }


    public void searchShop(Context context, int page, String textKey, boolean isFollow, int catId,
                           DataCallBack<ShopResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("page", page);
            map.put("text", textKey);
            map.put("type", isFollow);
            map.put("category_id", catId);
            NetworkUtils.getInstance(context).getRetrofitService().searchShop(map)
                    .enqueue(new Callback<ShopResponse>() {
                        @Override
                        public void onResponse(Call<ShopResponse> call, Response<ShopResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<ShopResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getString(R.string.error_not_connect));
        }
    }

    public void searchUser(Context context, int page, String textKey, boolean isFollow,
                           DataCallBack<UserResponse> callBack) {

        if (NetworkUtils.isConnected(context)) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("page", page);
            map.put("text", textKey);
            map.put("type", isFollow);
            NetworkUtils.getInstance(context).getRetrofitService().searchUser(map)
                    .enqueue(new Callback<UserResponse>() {
                        @Override
                        public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<UserResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getString(R.string.error_not_connect));
        }
    }
}
