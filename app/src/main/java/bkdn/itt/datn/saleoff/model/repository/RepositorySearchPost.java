package bkdn.itt.datn.saleoff.model.repository;

import android.content.Context;

import java.util.HashMap;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.net.NetworkUtils;
import bkdn.itt.datn.saleoff.model.response.CategoryResponse;
import bkdn.itt.datn.saleoff.model.response.PostResponse;
import bkdn.itt.datn.saleoff.model.response.SubCategoryResponse;
import bkdn.itt.datn.saleoff.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TuDLT on 5/18/2018.
 */
public class RepositorySearchPost {

    public static RepositorySearchPost sRepositorySearchPost;

    public static synchronized RepositorySearchPost getInstance() {
        if (sRepositorySearchPost == null) {
            sRepositorySearchPost = new RepositorySearchPost();
        }
        return sRepositorySearchPost;
    }

    public void searchPost(Context context, int page, String type, String textKey,
                           boolean isFollow, int catId, int subCatId, DataCallBack<PostResponse> callBack) {


        if (NetworkUtils.isConnected(context)) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("text", textKey);
            map.put("page", page);
            map.put("type", type);
            map.put("follow", isFollow);
            map.put("category_id", catId);
            if(catId != 1 && subCatId != 0){
                map.put("sub_category_id", subCatId);
            }

            NetworkUtils.getInstance(context).getRetrofitService().searchPost(map)
                    .enqueue(new Callback<PostResponse>() {
                        @Override
                        public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<PostResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getString(R.string.error_not_connect));
        }
    }

    public void getAllCategory(Context context, DataCallBack<CategoryResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().getAllCategory()
                    .enqueue(new retrofit2.Callback<CategoryResponse>() {
                        @Override
                        public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<CategoryResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });

        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));
        }
    }

    public void getAllSubCategory(Context context, DataCallBack<SubCategoryResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().getAllSubCategory()
                    .enqueue(new retrofit2.Callback<SubCategoryResponse>() {
                        @Override
                        public void onResponse(Call<SubCategoryResponse> call,
                                               Response<SubCategoryResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<SubCategoryResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });

        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));
        }
    }

}
