package bkdn.itt.datn.saleoff.model.repository;

import android.content.Context;

import java.util.HashMap;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.net.NetworkUtils;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.model.response.CategoryResponse;
import bkdn.itt.datn.saleoff.model.response.ShopResponse;
import bkdn.itt.datn.saleoff.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TuDLT on 5/18/2018.
 */
public class RepositorySearchShop {
    private static RepositorySearchShop sRepositorySearchShop;

    public static synchronized RepositorySearchShop getInstance() {
        if (sRepositorySearchShop == null) {
            sRepositorySearchShop = new RepositorySearchShop();
        }
        return sRepositorySearchShop;
    }
    public void searchShop(Context context, int page, String textKey, boolean isFollow, int catId,
                           DataCallBack<ShopResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("page", page);
            map.put("text", textKey);
            map.put("type", isFollow);
            map.put("category_id", catId);
            NetworkUtils.getInstance(context).getRetrofitService().searchShop(map)
                    .enqueue(new Callback<ShopResponse>() {
                        @Override
                        public void onResponse(Call<ShopResponse> call, Response<ShopResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<ShopResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getString(R.string.error_not_connect));
        }
    }
    public void getAllCategory(Context context, DataCallBack<CategoryResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().getAllCategory()
                    .enqueue(new retrofit2.Callback<CategoryResponse>() {
                        @Override
                        public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<CategoryResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });

        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));
        }
    }

    public void followShop(Context context, int shopId, int bonus, DataCallBack<BaseResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("code", bonus);
            NetworkUtils.getInstance(context).getRetrofitService().followShop(shopId, map)
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));

        }
    }

    public void unFollowShop(Context context, int shopId, DataCallBack<BaseResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().unFollowShop(shopId)
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));

        }
    }
}
