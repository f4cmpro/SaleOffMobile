package bkdn.itt.datn.saleoff.model.repository;

import android.content.Context;

import java.util.HashMap;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.net.NetworkUtils;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.model.response.UserResponse;
import bkdn.itt.datn.saleoff.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TuDLT on 5/18/2018.
 */
public class RepositorySearchUser {
    public static RepositorySearchUser sRepositorySearchUser;

    public static synchronized RepositorySearchUser getInstance() {
        if (sRepositorySearchUser == null) {
            sRepositorySearchUser = new RepositorySearchUser();
        }
        return sRepositorySearchUser;
    }
    public void searchUser(Context context, int page, String textKey, boolean isFollow,
                           DataCallBack<UserResponse> callBack) {

        if (NetworkUtils.isConnected(context)) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("page", page);
            map.put("text", textKey);
            map.put("type", isFollow);
            NetworkUtils.getInstance(context).getRetrofitService().searchUser(map)
                    .enqueue(new Callback<UserResponse>() {
                        @Override
                        public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<UserResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getString(R.string.error_not_connect));
        }
    }

    public void followUser(Context context, int userId, DataCallBack<BaseResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().followUser(userId)
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));

        }
    }

    public void unFollowUser(Context context, int userId, DataCallBack<BaseResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().unFollowUser(userId)
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));

        }
    }
}
