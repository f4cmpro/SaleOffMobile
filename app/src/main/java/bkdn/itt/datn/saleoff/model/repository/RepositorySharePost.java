package bkdn.itt.datn.saleoff.model.repository;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.net.NetworkUtils;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TuDLT on 5/17/2018.
 */
public class RepositorySharePost {
    public static RepositorySharePost sRepositorySharePost;

    public static RepositorySharePost getInstance() {
        if (sRepositorySharePost == null) {
            sRepositorySharePost = new RepositorySharePost();
        }
        return sRepositorySharePost;
    }

    public void sharePost(Context context, String des, int postId, DataCallBack<BaseResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            Map<String, Object> map = new HashMap<>();
            map.put("description", des);
            map.put("post_id", postId);
            NetworkUtils.getInstance(context).getRetrofitService()
                    .sharePost(map).enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    Utils.checkAndReceiveResponse(context, response, callBack);
                }
                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    callBack.onError(t.getMessage());
                }
            });
        }else {
            callBack.onError(context.getString(R.string.error_not_connect));
        }
    }
}
