package bkdn.itt.datn.saleoff.model.repository;

import android.content.Context;

import java.util.HashMap;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.Shop;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.net.NetworkUtils;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.model.response.CreateShopResponse;
import bkdn.itt.datn.saleoff.model.response.CategoryResponse;
import bkdn.itt.datn.saleoff.model.response.ShopDetailResponse;
import bkdn.itt.datn.saleoff.model.response.ShopInfoResponse;
import bkdn.itt.datn.saleoff.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TuDLT on 3/18/2018.
 */
public class RepositoryShop {
    private static RepositoryShop sRepositoryShop;

    public static synchronized RepositoryShop getInstance() {
        if (sRepositoryShop == null) {
            sRepositoryShop = new RepositoryShop();
        }
        return sRepositoryShop;
    }

    public void getShopDetailById(Context context, int shopId, DataCallBack<ShopDetailResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().getShopDetail(shopId).
                    enqueue(new Callback<ShopDetailResponse>() {
                        @Override
                        public void onResponse(Call<ShopDetailResponse> call, Response<ShopDetailResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<ShopDetailResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));
        }
    }

    public void createShop(Context context, Shop createdShop, DataCallBack<CreateShopResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("shop_cat_id", createdShop.getCateId());
            map.put("name", createdShop.getName());
            map.put("phone", createdShop.getPhone());
            map.put("address", createdShop.getAddress());
            map.put("avatar", createdShop.getAvatar());
            map.put("cover", createdShop.getCover());
            map.put("web", createdShop.getWeb());
            map.put("description", createdShop.getDescription());
            NetworkUtils.getInstance(context).getRetrofitService().createShop(map).enqueue(new Callback<CreateShopResponse>() {
                @Override
                public void onResponse(Call<CreateShopResponse> call, Response<CreateShopResponse> response) {
                    Utils.checkAndReceiveResponse(context, response, callBack);
                }

                @Override
                public void onFailure(Call<CreateShopResponse> call, Throwable t) {
                    callBack.onError(t.getMessage());
                }
            });
        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));
        }
    }

    public void editShop(Context context, Shop editedShop, DataCallBack<BaseResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("name", editedShop.getName());
            map.put("phone", editedShop.getPhone());
            map.put("address", editedShop.getAddress());
            map.put("avatar", editedShop.getAvatar());
            map.put("cover", editedShop.getCover());
            map.put("web", editedShop.getWeb());
            map.put("description", editedShop.getDescription());
            NetworkUtils.getInstance(context).getRetrofitService().editShop(editedShop.getShopId(), map).enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    Utils.checkAndReceiveResponse(context, response, callBack);
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    callBack.onError(t.getMessage());
                }
            });
        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));
        }
    }

    public void getShopCategory(Context context, DataCallBack<CategoryResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().getCategoriesForCreateShop().enqueue(new Callback<CategoryResponse>() {
                @Override
                public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                    Utils.checkAndReceiveResponse(context, response, callBack);
                }

                @Override
                public void onFailure(Call<CategoryResponse> call, Throwable t) {
                    callBack.onError(t.getMessage());
                }
            });
        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));
        }
    }


    public void ratingShop(Context context, int shopId, int rate, DataCallBack<BaseResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("point", rate);
            NetworkUtils.getInstance(context).getRetrofitService().ratingShop(shopId, map)
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));
        }
    }

    public void deleteShop(Context context, int shopId, DataCallBack<BaseResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().deleteShop(shopId)
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));
        }
    }
}
