package bkdn.itt.datn.saleoff.model.repository;

import android.content.Context;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.net.NetworkUtils;
import bkdn.itt.datn.saleoff.model.response.CategoryResponse;
import bkdn.itt.datn.saleoff.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TuDLT on 5/18/2018.
 */
public class RepositoryShopCategory {
    private static RepositoryShopCategory sRepositoryShopCategory;

    public static synchronized RepositoryShopCategory getInstance() {
        if (sRepositoryShopCategory == null) {
            sRepositoryShopCategory = new RepositoryShopCategory();
        }
        return sRepositoryShopCategory;
    }
    public void getShopCategory(Context context, DataCallBack<CategoryResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().getAllCategory().enqueue(new Callback<CategoryResponse>() {
                @Override
                public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                    Utils.checkAndReceiveResponse(context, response, callBack);
                }

                @Override
                public void onFailure(Call<CategoryResponse> call, Throwable t) {
                    callBack.onError(t.getMessage());
                }
            });
        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));
        }
    }

}
