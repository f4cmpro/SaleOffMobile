package bkdn.itt.datn.saleoff.model.repository;

import android.content.Context;

import java.util.HashMap;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.Shop;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.net.NetworkUtils;
import bkdn.itt.datn.saleoff.model.response.CreateShopResponse;
import bkdn.itt.datn.saleoff.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TuDLT on 5/18/2018.
 */
public class RepositoryShopCreate {
    private static RepositoryShopCreate sRepositoryShop;

    public static synchronized RepositoryShopCreate getInstance() {
        if (sRepositoryShop == null) {
            sRepositoryShop = new RepositoryShopCreate();
        }
        return sRepositoryShop;
    }

    public void createShop(Context context, Shop createdShop, DataCallBack<CreateShopResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("shop_cat_id", createdShop.getCateId());
            map.put("name", createdShop.getName());
            map.put("phone", createdShop.getPhone());
            map.put("address", createdShop.getAddress());
            map.put("avatar", createdShop.getAvatar());
            map.put("cover", createdShop.getCover());
            map.put("web", createdShop.getWeb());
            map.put("description", createdShop.getDescription());
            NetworkUtils.getInstance(context).getRetrofitService().createShop(map).enqueue(new Callback<CreateShopResponse>() {
                @Override
                public void onResponse(Call<CreateShopResponse> call, Response<CreateShopResponse> response) {
                    Utils.checkAndReceiveResponse(context, response, callBack);
                }

                @Override
                public void onFailure(Call<CreateShopResponse> call, Throwable t) {
                    callBack.onError(t.getMessage());
                }
            });
        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));
        }
    }

}
