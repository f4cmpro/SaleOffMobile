package bkdn.itt.datn.saleoff.model.repository;

import android.content.Context;

import java.util.HashMap;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.Shop;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.net.NetworkUtils;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TuDLT on 5/18/2018.
 */
public class RepositoryShopEdit {

    private static RepositoryShopEdit sRepositoryShopEdit;

    public static synchronized RepositoryShopEdit getInstance() {
        if (sRepositoryShopEdit == null) {
            sRepositoryShopEdit = new RepositoryShopEdit();
        }
        return sRepositoryShopEdit;
    }

    public void editShop(Context context, Shop editedShop, DataCallBack<BaseResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("name", editedShop.getName());
            map.put("phone", editedShop.getPhone());
            map.put("address", editedShop.getAddress());
            map.put("avatar", editedShop.getAvatar());
            map.put("cover", editedShop.getCover());
            map.put("web", editedShop.getWeb());
            map.put("description", editedShop.getDescription());
            NetworkUtils.getInstance(context).getRetrofitService().editShop(editedShop.getShopId(), map).enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    Utils.checkAndReceiveResponse(context, response, callBack);
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    callBack.onError(t.getMessage());
                }
            });
        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));
        }
    }

}
