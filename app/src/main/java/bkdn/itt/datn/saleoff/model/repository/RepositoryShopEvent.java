package bkdn.itt.datn.saleoff.model.repository;

import android.content.Context;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.net.NetworkUtils;
import bkdn.itt.datn.saleoff.model.response.EventListResponse;
import bkdn.itt.datn.saleoff.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TuDLT on 5/29/2018.
 */
public class RepositoryShopEvent {
    private static RepositoryShopEvent sRepositoryShopEvent;

    public static RepositoryShopEvent getInstance() {
        if (sRepositoryShopEvent == null) {
            sRepositoryShopEvent = new RepositoryShopEvent();
        }
        return sRepositoryShopEvent;
    }


    public void getAllShopEvents(Context context, int shopId, DataCallBack<EventListResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().getAllShopEvents(shopId)
                    .enqueue(new Callback<EventListResponse>() {
                        @Override
                        public void onResponse(Call<EventListResponse> call, Response<EventListResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<EventListResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getString(R.string.error_not_connect));
        }
    }
}
