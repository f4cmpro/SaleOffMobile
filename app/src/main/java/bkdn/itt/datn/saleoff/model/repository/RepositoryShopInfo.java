package bkdn.itt.datn.saleoff.model.repository;

import android.content.Context;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.net.NetworkUtils;
import bkdn.itt.datn.saleoff.model.response.ShopDetailResponse;
import bkdn.itt.datn.saleoff.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TuDLT on 5/18/2018.
 */
public class RepositoryShopInfo {

    private static RepositoryShopInfo sRepositoryShopInfo;

    public static synchronized RepositoryShopInfo getInstance() {
        if (sRepositoryShopInfo == null) {
            sRepositoryShopInfo = new RepositoryShopInfo();
        }
        return sRepositoryShopInfo;
    }

    public void getShopDetailById(Context context, int shopId, DataCallBack<ShopDetailResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().getShopDetail(shopId).
                    enqueue(new Callback<ShopDetailResponse>() {
                        @Override
                        public void onResponse(Call<ShopDetailResponse> call, Response<ShopDetailResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<ShopDetailResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));
        }
    }

}
