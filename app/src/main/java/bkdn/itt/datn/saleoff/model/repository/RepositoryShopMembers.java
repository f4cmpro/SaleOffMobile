package bkdn.itt.datn.saleoff.model.repository;

import android.content.Context;

import java.util.HashMap;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.net.NetworkUtils;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.model.response.UserResponse;
import bkdn.itt.datn.saleoff.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TuDLT on 5/18/2018.
 */
public class RepositoryShopMembers {

    private static RepositoryShopMembers sRepositoryShopMembers;

    public static synchronized RepositoryShopMembers getInstance() {
        if (sRepositoryShopMembers == null) {
            sRepositoryShopMembers = new RepositoryShopMembers();
        }
        return sRepositoryShopMembers;
    }

    public void getShopMembers(Context context, int shopId, DataCallBack<UserResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().getShopMembers(shopId)
                    .enqueue(new Callback<UserResponse>() {
                        @Override
                        public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<UserResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getString(R.string.error_not_connect));
        }
    }

    public void minusBonus(Context context, int shopId, int userId, int minusPoint, DataCallBack<BaseResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("user_id", userId);
            map.put("point", minusPoint);
            NetworkUtils.getInstance(context).getRetrofitService().minusBonus(shopId, map)
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getString(R.string.error_not_connect));
        }
    }
}
