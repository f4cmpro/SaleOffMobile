package bkdn.itt.datn.saleoff.model.repository;

import android.content.Context;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.net.NetworkUtils;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.model.response.PostResponse;
import bkdn.itt.datn.saleoff.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TuDLT on 5/18/2018.
 */
public class RepositoryShopPost {

    private static RepositoryShopPost sRepositoryShopPost;

    public static synchronized RepositoryShopPost getInstance() {
        if (sRepositoryShopPost == null) {
            sRepositoryShopPost = new RepositoryShopPost();
        }
        return sRepositoryShopPost;
    }

    public void getShopPost(Context context, int page, int shopId, DataCallBack<PostResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().getShopPosts(page, shopId)
                    .enqueue(new Callback<PostResponse>() {
                        @Override
                        public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<PostResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));
        }
    }

    public void deleteShopPost(Context context, int postId, int shopId, DataCallBack<BaseResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().deleteShopPost(postId, shopId)
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));
        }
    }

    public void likeShopPost(Context context, int postId, DataCallBack<BaseResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().likePost(postId)
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));
        }
    }

    public void unLikeShopPost(Context context, int postId, DataCallBack<BaseResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().unLikePost(postId)
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));
        }
    }


}
