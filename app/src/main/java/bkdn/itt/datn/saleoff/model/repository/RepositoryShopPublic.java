package bkdn.itt.datn.saleoff.model.repository;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.net.NetworkUtils;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.model.response.ShopDetailResponse;
import bkdn.itt.datn.saleoff.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TuDLT on 5/18/2018.
 */
public class RepositoryShopPublic {
    private static RepositoryShopPublic sRepositoryShopPublic;

    public static synchronized RepositoryShopPublic getInstance() {
        if (sRepositoryShopPublic == null) {
            sRepositoryShopPublic = new RepositoryShopPublic();
        }
        return sRepositoryShopPublic;
    }

    public void getShopDetailById(Context context, int shopId, DataCallBack<ShopDetailResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            NetworkUtils.getInstance(context).getRetrofitService().getShopDetail(shopId).
                    enqueue(new Callback<ShopDetailResponse>() {
                        @Override
                        public void onResponse(Call<ShopDetailResponse> call, Response<ShopDetailResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<ShopDetailResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));
        }
    }

    public void ratingShop(Context context, int shopId, int rate, DataCallBack<BaseResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("point", rate);
            NetworkUtils.getInstance(context).getRetrofitService().ratingShop(shopId, map)
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getResources().getString(R.string.error_not_connect));
        }
    }

    public void sendReportShop(Context context, int shopId, int reasonId, DataCallBack<BaseResponse> callBack) {
        if (NetworkUtils.isConnected(context)) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("type", ConstantDefine.REPORT_SHOP);
            map.put("reason", reasonId);
            map.put("target_id", shopId);
            NetworkUtils.getInstance(context).getRetrofitService().sendReport(map)
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            Utils.checkAndReceiveResponse(context, response, callBack);
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            callBack.onError(t.getMessage());
                        }
                    });
        } else {
            callBack.onError(context.getString(R.string.error_not_connect));
        }
    }

}
