package bkdn.itt.datn.saleoff.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Category;

/**
 * Created by TuDLT on 3/17/2018.
 */
public class CategoryResponse extends BaseResponse {
    @SerializedName("data")
    private CategoryData mCategoryData;

    public CategoryResponse() {
        mCategoryData = new CategoryData();
    }

    public List<Category> getCategories() {
        return mCategoryData.categories;
    }

    private class CategoryData {
        @SerializedName("categories")
        List<Category> categories;
    }
}
