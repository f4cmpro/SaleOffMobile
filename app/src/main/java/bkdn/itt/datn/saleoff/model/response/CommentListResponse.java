package bkdn.itt.datn.saleoff.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Comment;

/**
 * Created by TuDLT on 4/20/2018.
 */
public class CommentListResponse extends BaseResponse {
    @SerializedName("data")
    private CommentData mCommentData;

    public CommentData getCommentData() {
        return mCommentData;
    }

    public void setCommentData(CommentData commentData) {
        mCommentData = commentData;
    }


    public class CommentData {
        @SerializedName("comments")
        private List<Comment> mComments;
        @SerializedName("last_id")
        private int mLastId;

        public List<Comment> getComments() {
            return mComments;
        }

        public void setComments(List<Comment> comments) {
            mComments = comments;
        }

        public int getLastId() {
            return mLastId;
        }

        public void setLastId(int lastId) {
            mLastId = lastId;
        }
    }
}
