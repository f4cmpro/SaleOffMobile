package bkdn.itt.datn.saleoff.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Comment;

/**
 * Created by TuDLT on 4/20/2018.
 */
public class CommentResponse extends BaseResponse {
    @SerializedName("data")
    private CommentData mCommentData;

    public CommentData getCommentData() {
        return mCommentData;
    }

    public void setCommentData(CommentData commentData) {
        mCommentData = commentData;
    }


    public class CommentData {
        @SerializedName("comment")
        private Comment mComment;

        public Comment getComment() {
            return mComment;
        }

        public void setComment(Comment comment) {
            mComment = comment;
        }
    }
}
