package bkdn.itt.datn.saleoff.model.response;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Shop;

/**
 * Created by TuDLT on 3/14/2018.
 */
public class CreateShopResponse extends BaseResponse {
    @SerializedName("data")
    private ShopData data;

    public CreateShopResponse(){
        data = new ShopData();
    }

    public Shop getShop() {
        try {
            return data.shop;
        } catch (NullPointerException exc) {
            Log.e("CreateShopResponse", exc.getMessage());
            return null;
        }
    }

    public void setShop(Shop shop){
        try {
            data.shop = shop;
        } catch (NullPointerException exc) {
            Log.e("CreateShopResponse", exc.getMessage());
        }
    }

    private class ShopData {
        @SerializedName("shop")
        Shop shop;
    }
}
