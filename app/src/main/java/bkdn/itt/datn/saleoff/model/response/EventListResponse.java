package bkdn.itt.datn.saleoff.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Event;

/**
 * Created by TuDLT on 5/29/2018.
 */
public class EventListResponse extends BaseResponse {
    @SerializedName("data")
    private EventData mData;

    public EventData getData() {
        return mData;
    }

    public void setData(EventData data) {
        mData = data;
    }


    public class EventData{
        @SerializedName("events")
        private List<Event> mEvents;

        public List<Event> getEvents() {
            return mEvents;
        }

        public void setEvents(List<Event> events) {
            mEvents = events;
        }
    }
}
