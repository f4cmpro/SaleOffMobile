package bkdn.itt.datn.saleoff.model.response;

import com.google.gson.annotations.SerializedName;

import bkdn.itt.datn.saleoff.model.model.Event;

/**
 * Created by TuDLT on 5/31/2018.
 */
public class EventResponse extends BaseResponse {
    @SerializedName("data")
    private EventData mData;

    public EventData getData() {
        return mData;
    }

    public void setData(EventData data) {
        mData = data;
    }

    public class EventData{
        @SerializedName("event")
        private Event mEvent;

        public Event getEvent() {
            return mEvent;
        }

        public void setEvent(Event event) {
            mEvent = event;
        }
    }
}
