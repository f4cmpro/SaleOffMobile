package bkdn.itt.datn.saleoff.model.response;

import com.google.gson.annotations.SerializedName;

import bkdn.itt.datn.saleoff.model.model.Event;

/**
 * Created by TuDLT on 5/31/2018.
 */
public class JoinEventResponse extends BaseResponse {
    @SerializedName("data")
    private JointEventData mData;

    public JointEventData getData() {
        return mData;
    }

    public void setData(JointEventData data) {
        mData = data;
    }

    public class JointEventData{
        @SerializedName("share_code")
        private int mShareCode;

        public int getShareCode() {
            return mShareCode;
        }

        public void setShareCode(int shareCode) {
            mShareCode = shareCode;
        }
    }
}
