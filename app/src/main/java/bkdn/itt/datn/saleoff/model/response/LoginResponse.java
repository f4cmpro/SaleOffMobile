package bkdn.itt.datn.saleoff.model.response;

import com.google.gson.annotations.SerializedName;

import bkdn.itt.datn.saleoff.model.model.Login;

/**
 * Created by TuDLT on 3/17/2018.
 */
public class LoginResponse extends BaseResponse {
    @SerializedName("data")
    LoginData mLogin;

    public LoginResponse(){
        mLogin = new LoginData();
    }
    public String getToken() {
        try {
            return mLogin.token;
        }catch (NullPointerException exc){
            return null;
        }
    }


    public int getIsFistLogin() {
        return mLogin.isFirstLogin;

    }


    private class LoginData{
        @SerializedName("token")
        String token;
        @SerializedName("is_first")
        int isFirstLogin;
    }
}
