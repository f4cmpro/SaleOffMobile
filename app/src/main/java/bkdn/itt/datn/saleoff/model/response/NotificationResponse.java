package bkdn.itt.datn.saleoff.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Notification;

/**
 * Created by TuDLT on 5/2/2018.
 */
public class NotificationResponse extends BaseResponse {
    @SerializedName("data")
    private NotificationData mData;

    public NotificationData getData() {
        return mData;
    }

    public void setData(NotificationData data) {
        mData = data;
    }


    public class NotificationData {
        @SerializedName("notifications")
        private List<Notification> mNotifications;

        public List<Notification> getNotifications() {
            return mNotifications;
        }

        public void setNotifications(List<Notification> notifications) {
            mNotifications = notifications;
        }
    }
}
