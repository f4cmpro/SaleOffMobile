package bkdn.itt.datn.saleoff.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Participant;

/**
 * Created by TuDLT on 6/1/2018.
 */
public class ParticipantsResponse extends BaseResponse {
    @SerializedName("data")
    private ParticipantsData mData;

    public ParticipantsData getData() {
        return mData;
    }

    public void setData(ParticipantsData data) {
        mData = data;
    }


    public class ParticipantsData {
        @SerializedName("participants")
        private List<Participant> mParticipants;

        public List<Participant> getParticipants() {
            return mParticipants;
        }

        public void setParticipants(List<Participant> participants) {
            this.mParticipants = participants;
        }
    }
}
