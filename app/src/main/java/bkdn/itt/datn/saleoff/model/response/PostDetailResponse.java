package bkdn.itt.datn.saleoff.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Post;

/**
 * Created by TuDLT on 4/13/2018.
 */
public class PostDetailResponse extends BaseResponse {
    @SerializedName("data")
    private PostData data;

    public PostData getData() {
        return data;
    }

    public void setData(PostData data) {
        this.data = data;
    }

    public class PostData{
        @SerializedName("post")
        private Post mPost;

        public Post getPost() {
            return mPost;
        }

        public void setPost(Post post) {
            mPost = post;
        }
    }
}
