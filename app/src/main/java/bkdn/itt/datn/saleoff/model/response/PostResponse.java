package bkdn.itt.datn.saleoff.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Post;

/**
 * Created by TuDLT on 4/13/2018.
 */
public class PostResponse extends BaseResponse {
    @SerializedName("data")
    private PostData data;

    public PostData getData() {
        return data;
    }

    public void setData(PostData data) {
        this.data = data;
    }

    public class PostData{
        @SerializedName("posts")
        private List<Post> posts;
        @SerializedName("last_id")
        private int lastId;

        public List<Post> getPosts() {
            return posts;
        }

        public void setPosts(List<Post> posts) {
            this.posts = posts;
        }

        public int getLastId() {
            return lastId;
        }

        public void setLastId(int lastId) {
            this.lastId = lastId;
        }
    }
}
