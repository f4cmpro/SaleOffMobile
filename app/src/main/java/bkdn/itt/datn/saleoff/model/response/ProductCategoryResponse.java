package bkdn.itt.datn.saleoff.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.ProductCategory;

/**
 * Created by TuDLT on 3/17/2018.
 */
public class ProductCategoryResponse extends BaseResponse {
    @SerializedName("data")
    CategoryData mCategoryData;
    public ProductCategoryResponse(){
        mCategoryData = new CategoryData();
    }

    public List<ProductCategory> getCategories(){
        return mCategoryData.categories;
    }

    private class CategoryData{
        @SerializedName("categories")
        List<ProductCategory> categories;
    }
}
