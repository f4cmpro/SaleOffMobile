package bkdn.itt.datn.saleoff.model.response;

import com.google.gson.annotations.SerializedName;

import bkdn.itt.datn.saleoff.model.model.User;

/**
 * Created by TuDLT on 3/17/2018.
 */
public class ProfileResponse extends BaseResponse{
    @SerializedName("data")
    private UserData data;
    public ProfileResponse(){
        data = new UserData();
    }
    public User getUser() {
        return data.user;
    }

    public void setUser(User data) {
        this.data.user = data;
    }

    private class UserData{
        @SerializedName("user")
        User user;
    }
}
