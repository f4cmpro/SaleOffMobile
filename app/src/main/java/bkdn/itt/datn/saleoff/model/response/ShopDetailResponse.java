package bkdn.itt.datn.saleoff.model.response;

import com.google.gson.annotations.SerializedName;

import bkdn.itt.datn.saleoff.model.model.Shop;

/**
 * Created by TuDLT on 3/14/2018.
 */
public class ShopDetailResponse extends BaseResponse {
    @SerializedName("data")
    private ShopData data;

    public ShopData getData() {
        return data;
    }

    public void setData(ShopData data) {
        this.data = data;
    }


    public class ShopData {
        @SerializedName("shop")
        private Shop shop;

        public Shop getShop() {
            return shop;
        }

        public void setShop(Shop shop) {
            this.shop = shop;
        }
    }
}
