package bkdn.itt.datn.saleoff.model.response;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.ShopInfo;
import bkdn.itt.datn.saleoff.model.model.User;

/**
 * Created by TuDLT on 4/4/2018.
 */
public class ShopInfoResponse extends BaseResponse {
    @SerializedName("data")
    private ShopInfoData mData;

    public ShopInfoData getData() {
        return mData;
    }

    public void setData(ShopInfoData data) {
        mData = data;
    }


    public class ShopInfoData{
        @SerializedName("shops")
        private List<ShopInfo> mShopInfoList;

        public List<ShopInfo> getShopInfoList() {
            return mShopInfoList;
        }

        public void setShopInfoList(List<ShopInfo> shopInfoList) {
            mShopInfoList = shopInfoList;
        }
    }
}
