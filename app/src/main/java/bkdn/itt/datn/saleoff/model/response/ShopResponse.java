package bkdn.itt.datn.saleoff.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Shop;

/**
 * Created by TuDLT on 4/29/2018.
 */
public class ShopResponse extends BaseResponse {
    @SerializedName("data")
    private ShopData data;

    public ShopData getData() {
        return data;
    }

    public void setData(ShopData data) {
        this.data = data;
    }

    public class ShopData {
        @SerializedName("shops")
        private List<Shop> shops;

        public List<Shop> getShops() {
            return shops;
        }

        public void setShops(List<Shop> shops) {
            this.shops = shops;
        }

    }
}
