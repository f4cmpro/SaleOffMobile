package bkdn.itt.datn.saleoff.model.response;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.SubCategory;

/**
 * Created by TuDLT on 3/14/2018.
 */
public class SubCategoryResponse extends BaseResponse {
    @SerializedName("data")
    private SubCategoryData data;

    public SubCategoryResponse(){
        data = new SubCategoryData();
    }

    public List<SubCategory> getListSubCategory() {
        try {
            return data.mSubCategories;
        } catch (NullPointerException exc) {
            Log.e("SubCategoryResponse", exc.getMessage());
            return null;
        }
    }

    public void setListSubCategory(List<SubCategory> subCategories){
        try {
            data.mSubCategories = subCategories;
        } catch (NullPointerException exc) {
            Log.e("SubCategoryResponse", exc.getMessage());
        }

    }

    private class SubCategoryData {
        @SerializedName("products")
        List<SubCategory> mSubCategories;
    }
}
