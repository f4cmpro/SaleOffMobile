package bkdn.itt.datn.saleoff.model.response;

import com.google.gson.annotations.SerializedName;

import bkdn.itt.datn.saleoff.model.model.Shop;
import bkdn.itt.datn.saleoff.model.model.User;

/**
 * Created by TuDLT on 5/4/2018.
 */
public class UserDetailResponse extends BaseResponse {
    @SerializedName("data")
    private UserData data;

    public UserData getData() {
        return data;
    }

    public void setData(UserData data) {
        this.data = data;
    }


    public class UserData {
        @SerializedName("user")
        private User mUser;

        public User getUser() {
            return mUser;
        }

        public void setUser(User user) {
            this.mUser = user;
        }
    }
}
