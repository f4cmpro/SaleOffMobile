package bkdn.itt.datn.saleoff.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.UserInfo;

/**
 * Created by TuDLT on 4/10/2018.
 */
public class UserInfoResponse extends BaseResponse {
    @SerializedName("data")
    private UserInfoData mData;

    public UserInfoData getData() {
        return mData;
    }

    public void setData(UserInfoData data) {
        mData = data;
    }


    public class UserInfoData {
        @SerializedName("users")
        private List<UserInfo> users;

        public List<UserInfo> getUsers() {
            return users;
        }

        public void setUsers(List<UserInfo> users) {
            this.users = users;
        }
    }
}
