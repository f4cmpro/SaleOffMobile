package bkdn.itt.datn.saleoff.model.response;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.User;

/**
 * Created by TuDLT on 3/14/2018.
 */
public class UserResponse extends BaseResponse {
    @SerializedName("data")
    private UserData data;

    public UserResponse() {
        data = new UserData();
    }

    public UserData getData() {
        return data;
    }

    public void setData(UserData data) {
        this.data = data;
    }


    public class UserData {
        @SerializedName("users")
        private List<User> users;

        public List<User> getListUser() {
            return users;

        }

        public void setListUser(List<User> users) {
            this.users = users;
        }
    }
}
