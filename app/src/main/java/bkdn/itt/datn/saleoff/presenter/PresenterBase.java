package bkdn.itt.datn.saleoff.presenter;

import android.content.Context;

import bkdn.itt.datn.saleoff.view.iface.IViewBase;


/**
 * Created by TuDLT on 12/26/17.
 */

public abstract class PresenterBase<I extends IViewBase> {

    private I iFace;

    public PresenterBase() {
    }

    public I getIFace() {
        return this.iFace;
    }

    public void setIFace(I iFace) {
        this.iFace = iFace;
    }

    protected Context getContext() {
        return this.iFace != null ? this.iFace.getContext() : null;
    }

    protected String getTag() {
        return this.getClass().getName();
    }

    public void onInit() {
    }
}
