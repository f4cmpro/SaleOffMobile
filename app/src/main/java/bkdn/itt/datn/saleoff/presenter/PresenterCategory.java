package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryCategory;
import bkdn.itt.datn.saleoff.model.response.CategoryResponse;
import bkdn.itt.datn.saleoff.view.iface.IActivityCategory;

/**
 * Created by TuDLT on 4/6/2018.
 */
public class PresenterCategory extends PresenterBase<IActivityCategory> {

    private RepositoryCategory mRepositoryCategory;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryCategory = RepositoryCategory.getInstance();
    }

    public void getAllCategories() {
        getIFace().showLoading();
        mRepositoryCategory.getAllCategory(getContext(), new DataCallBack<CategoryResponse>() {
            @Override
            public void onSuccess(CategoryResponse result) {
                getIFace().getAllCategorySuccess(result.getCategories());
                getIFace().hideLoading();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getAllCategoryFail(errorMessage);
                getIFace().hideLoading();
            }
        });
    }
}
