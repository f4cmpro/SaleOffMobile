package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryComment;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.model.response.CommentListResponse;
import bkdn.itt.datn.saleoff.model.response.CommentResponse;
import bkdn.itt.datn.saleoff.view.iface.IActivityComment;

/**
 * Created by TuDLT on 4/20/2018.
 */
public class PresenterComment extends PresenterBase<IActivityComment> {
    private RepositoryComment mRepositoryComment;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryComment = RepositoryComment.getInstance();
    }

    public void getAllComment(int postId, int lastId) {
        mRepositoryComment.getComments(getContext(), postId, lastId, new DataCallBack<CommentListResponse>() {
            @Override
            public void onSuccess(CommentListResponse result) {
                getIFace().hideLoading();
                if (result.getCommentData() != null && result.getCommentData().getComments() != null &&
                        !result.getCommentData().getComments().isEmpty()) {
                    getIFace().getCommentsSuccess(result.getCommentData().getComments(),
                            result.getCommentData().getLastId());
                }
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().hideLoading();
                getIFace().getError(errorMessage);
            }
        });
    }

    public void addComment(int postId, String comment) {
        mRepositoryComment.addComment(getContext(), postId, comment, new DataCallBack<CommentResponse>() {
            @Override
            public void onSuccess(CommentResponse result) {
                if (result.getCommentData() != null) {
                    getIFace().addCommentSuccess(result.getCommentData().getComment());
                } else {
                    getIFace().getError(result.getMessage());
                }
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
            }
        });
    }

    public void editComment(int commentId, String comment) {
        getIFace().showLoading();
        mRepositoryComment.editComment(getContext(), commentId, comment, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {
                getIFace().hideLoading();
                if (result.getStatus() == ConstantDefine.RESPONSE_SUCCESS_CODE) {
                    getIFace().editCommentSuccess();
                } else {
                    getIFace().getError(result.getMessage());
                }
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().hideLoading();
                getIFace().getError(errorMessage);
            }
        });
    }
}
