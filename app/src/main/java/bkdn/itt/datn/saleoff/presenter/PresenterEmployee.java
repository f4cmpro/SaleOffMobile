package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryEmployee;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.model.response.UserResponse;
import bkdn.itt.datn.saleoff.view.iface.IFragmentEmployee;

/**
 * Created by TuDLT on 5/6/2018.
 */
public class PresenterEmployee extends PresenterBase<IFragmentEmployee> {

    private RepositoryEmployee mRepositoryEmployee;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryEmployee = RepositoryEmployee.getInstance();
    }

    public void getAllEmployees(int shopId) {
        mRepositoryEmployee.getAllEmployees(getContext(), shopId, new DataCallBack<UserResponse>() {
            @Override
            public void onSuccess(UserResponse result) {
                getIFace().hideLoading();
                if (result.getData() != null && result.getData().getListUser() != null) {
                    getIFace().getAllEmployeesSuccess(result.getData().getListUser());
                }
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }

    public void deleteEmployee(int shopId, int userId) {
        mRepositoryEmployee.deleteEmployee(getContext(), shopId, userId, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {
                getIFace().deleteEmployeeSuccess();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
            }
        });
    }
}
