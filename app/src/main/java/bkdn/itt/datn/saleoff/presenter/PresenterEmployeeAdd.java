package bkdn.itt.datn.saleoff.presenter;

import java.util.List;

import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryEmployeeAdd;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.model.response.UserInfoResponse;
import bkdn.itt.datn.saleoff.view.iface.IActivityAddMember;

/**
 * Created by TuDLT on 3/23/2018.
 */
public class PresenterEmployeeAdd extends PresenterBase<IActivityAddMember> {
    private RepositoryEmployeeAdd mRepositoryEmployeeAdd;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryEmployeeAdd = RepositoryEmployeeAdd.getInstance();
    }

    public void searchUsers(int shopId, String searchText) {
        mRepositoryEmployeeAdd.getUsersToAddEmployee(getContext(), shopId, searchText, new DataCallBack<UserInfoResponse>() {
            @Override
            public void onSuccess(UserInfoResponse result) {
                if(result.getData() != null) {
                    getIFace().getUsersToAddMemberSuccess(result.getData().getUsers());
                }
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
            }
        });
    }

    public void addShopMembers(int shopId, List<Integer> userIds) {
        getIFace().showLoading();
        if(shopId == 0 || userIds == null || userIds.isEmpty()){
            getIFace().getError("can not add members!");
            getIFace().hideLoading();
            return;
        }
        mRepositoryEmployeeAdd.addShopEmployees(getContext(), shopId, userIds, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {
                if(result.getStatus() == ConstantDefine.RESPONSE_SUCCESS_CODE) {
                    getIFace().addMembersSuccess();
                }
                getIFace().hideLoading();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().hideLoading();
                getIFace().getError(errorMessage);
            }
        });
    }
}
