package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryEvent;
import bkdn.itt.datn.saleoff.model.response.EventListResponse;
import bkdn.itt.datn.saleoff.view.iface.IFragmentEvent;

/**
 * Created by TuDLT on 5/27/2018.
 */
public class PresenterEvent extends PresenterBase<IFragmentEvent> {
    private RepositoryEvent mRepositoryEvent;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryEvent = RepositoryEvent.getInstance();
    }

    public void getEventHappening(boolean isFirst, int happeningCurrentPage) {
        if(isFirst){
            getIFace().showLoading();
        }
        mRepositoryEvent.getHappeningEvents(getContext(), happeningCurrentPage, new DataCallBack<EventListResponse>() {
            @Override
            public void onSuccess(EventListResponse result) {
                if(result.getData() != null){
                    getIFace().getEventHappeningSuccess(result.getData().getEvents());
                }
                getIFace().hideLoading();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getEventHappeningError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }

    public void getEventGoingHappen(boolean isFirst, int goingHappenPage) {
        if(isFirst){
            getIFace().showLoading();
        }
        mRepositoryEvent.getGoingHappenEvents(getContext(), goingHappenPage, new DataCallBack<EventListResponse>() {
            @Override
            public void onSuccess(EventListResponse result) {
                if(result.getData() != null){
                    getIFace().getEventGoingHappenSuccess(result.getData().getEvents());
                }
                getIFace().hideLoading();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getEventGoingHappenError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }

    public void getEventHappened(boolean isFirst, int happenedCurrentPage) {
        if(isFirst){
            getIFace().showLoading();
        }
        mRepositoryEvent.getHappenedEvents(getContext(), happenedCurrentPage, new DataCallBack<EventListResponse>() {
            @Override
            public void onSuccess(EventListResponse result) {
                if(result.getData() != null){
                    getIFace().getEventHappenedSuccess(result.getData().getEvents());
                }
                getIFace().hideLoading();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getEventHappenedError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }

    public void getEventJoined(boolean isFirst) {
        if(isFirst){
            getIFace().showLoading();
        }
        mRepositoryEvent.getJoinedEvents(getContext(),new DataCallBack<EventListResponse>() {
            @Override
            public void onSuccess(EventListResponse result) {
                if(result.getData() != null){
                    getIFace().getEventJoinedSuccess(result.getData().getEvents());
                }
                getIFace().hideLoading();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getEventJoinedError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }
}
