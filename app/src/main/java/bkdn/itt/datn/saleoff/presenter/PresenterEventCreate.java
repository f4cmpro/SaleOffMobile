package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.model.model.Event;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryEventCreate;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.view.iface.IActivityEventCreate;

/**
 * Created by TuDLT on 5/28/2018.
 */
public class PresenterEventCreate extends PresenterBase<IActivityEventCreate> {
    private RepositoryEventCreate mRepositoryEventCreate;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryEventCreate = RepositoryEventCreate.getInstance();
    }

    public void createEvent(Event createdEvent) {
        getIFace().showLoading();
        mRepositoryEventCreate.createEvent(getContext(), createdEvent, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {
                getIFace().createEventSuccess();
                getIFace().hideLoading();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().createEventError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }
}
