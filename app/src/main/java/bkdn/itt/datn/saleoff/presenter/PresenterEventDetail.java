package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryEventDetail;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.model.response.EventResponse;
import bkdn.itt.datn.saleoff.model.response.JoinEventResponse;
import bkdn.itt.datn.saleoff.model.response.ParticipantsResponse;
import bkdn.itt.datn.saleoff.view.iface.IActivityEventDetail;

/**
 * Created by TuDLT on 5/31/2018.
 */
public class PresenterEventDetail extends PresenterBase<IActivityEventDetail> {
    private RepositoryEventDetail mRepositoryEventDetail;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryEventDetail = RepositoryEventDetail.getInstance();
    }

    public void getEventDetail(boolean isFirst, int eventId) {
        if (isFirst) {
            getIFace().showLoading();
        }
        mRepositoryEventDetail.getEventDetail(getContext(), eventId, new DataCallBack<EventResponse>() {
            @Override
            public void onSuccess(EventResponse result) {
                if (result.getData() != null && result.getData().getEvent() != null) {
                    getIFace().getEventDetailSuccess(result.getData().getEvent());
                } else {
                    getIFace().getEventDetailError(getContext().getString(R.string.error_data_is_null));
                }
                getIFace().hideLoading();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getEventDetailError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }

    public void getParticipants(int eventId) {
        mRepositoryEventDetail.getParticipants(getContext(), eventId, new DataCallBack<ParticipantsResponse>() {
            @Override
            public void onSuccess(ParticipantsResponse result) {
                if(result.getData() != null && result.getData().getParticipants() != null)
                getIFace().getParticipantsSuccess(result.getData().getParticipants());
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getParticipantsError(errorMessage);
            }
        });
    }

    public void joinEvent(int eventId, int presenterCode, String shareDescription) {
        getIFace().showLoading();
        mRepositoryEventDetail.joinEvent(getContext(), eventId, presenterCode, shareDescription,
                new DataCallBack<JoinEventResponse>() {
            @Override
            public void onSuccess(JoinEventResponse result) {
                getIFace().joinEventSuccess(result.getData().getShareCode());
                getIFace().hideLoading();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().joinEventError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }
}
