package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryEvent;
import bkdn.itt.datn.saleoff.model.response.EventListResponse;
import bkdn.itt.datn.saleoff.view.iface.IFragmentEventHot;

/**
 * Created by TuDLT on 5/29/2018.
 */
public class PresenterEventHot extends PresenterBase<IFragmentEventHot>{
    private RepositoryEvent mRepositoryEvent;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryEvent = RepositoryEvent.getInstance();
    }

    public void getAllHotEvents(boolean isFirst, int currentPage) {
        if(isFirst){
            getIFace().showLoading();
        }
        mRepositoryEvent.getHappeningEvents(getContext(), currentPage, new DataCallBack<EventListResponse>() {
            @Override
            public void onSuccess(EventListResponse result) {
                if(result.getData() != null){
                    getIFace().getAllHotEvents(result.getData().getEvents());
                }
                getIFace().hideLoading();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }
}
