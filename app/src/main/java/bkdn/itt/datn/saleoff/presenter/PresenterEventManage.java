package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryEventManage;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.model.response.EventResponse;
import bkdn.itt.datn.saleoff.model.response.ParticipantsResponse;
import bkdn.itt.datn.saleoff.view.iface.IActivityEventManage;

/**
 * Created by TuDLT on 5/31/2018.
 */
public class PresenterEventManage extends PresenterBase<IActivityEventManage> {
    private RepositoryEventManage mRepositoryEventManage;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryEventManage = RepositoryEventManage.getInstance();
    }

    public void getEventDetail(boolean isFirst, int eventId) {
        if (isFirst) {
            getIFace().showLoading();
        }
        mRepositoryEventManage.getEventDetail(getContext(), eventId, new DataCallBack<EventResponse>() {
            @Override
            public void onSuccess(EventResponse result) {
                if (result.getData() != null && result.getData().getEvent() != null) {
                    getIFace().getEventDetailSuccess(result.getData().getEvent());
                } else {
                    getIFace().getEventDetailError(getContext().getString(R.string.error_data_is_null));
                }
                getIFace().hideLoading();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getEventDetailError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }

    public void getParticipants(int eventId) {
        mRepositoryEventManage.getParticipants(getContext(), eventId, new DataCallBack<ParticipantsResponse>() {
            @Override
            public void onSuccess(ParticipantsResponse result) {
                if (result.getData() != null && result.getData().getParticipants() != null)
                    getIFace().getParticipantsSuccess(result.getData().getParticipants());
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getParticipantsError(errorMessage);
            }
        });
    }

    public void useEventPoint(int eventId, int participantId) {
        getIFace().showLoading();
        mRepositoryEventManage.useEventPoint(getContext(), eventId, participantId, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {
                getIFace().useEventPointSuccess();
                getIFace().hideLoading();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().useEventPointError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }
}
