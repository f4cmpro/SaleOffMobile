package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryEventMe;
import bkdn.itt.datn.saleoff.model.response.EventListResponse;
import bkdn.itt.datn.saleoff.view.iface.IFragmentEventMe;

/**
 * Created by TuDLT on 5/29/2018.
 */
public class PresenterEventMe extends PresenterBase<IFragmentEventMe>{

    private RepositoryEventMe mRepositoryEventMe;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryEventMe = RepositoryEventMe.getInstance();
    }

    public void getAllMyEvents(boolean isFirst, int currentPage) {
        if(isFirst){
            getIFace().showLoading();
        }
        mRepositoryEventMe.getAllMyEvents(getContext(), new DataCallBack<EventListResponse>() {
            @Override
            public void onSuccess(EventListResponse result) {
                if(result.getData() != null){
                    getIFace().getAllMyEvents(result.getData().getEvents());
                }
                getIFace().hideLoading();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }
}
