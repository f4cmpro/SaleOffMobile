package bkdn.itt.datn.saleoff.presenter;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Shop;
import bkdn.itt.datn.saleoff.model.model.ShopInfo;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryFollowShop;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.model.response.ShopInfoResponse;
import bkdn.itt.datn.saleoff.model.response.ShopResponse;
import bkdn.itt.datn.saleoff.view.iface.IFragmentFollowShop;

/**
 * Created by TuDLT on 4/15/2018.
 */
public class PresenterFollowShop extends PresenterBase<IFragmentFollowShop> {
    private RepositoryFollowShop mRepositoryFollowShop;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryFollowShop = RepositoryFollowShop.getInstance();
    }

    public void getFollowedShops() {
        getIFace().showLoading();
        mRepositoryFollowShop.getFollowedShops(getContext(), new DataCallBack<ShopInfoResponse>() {
            @Override
            public void onSuccess(ShopInfoResponse result) {
                getIFace().hideLoading();
                if (result.getData() != null) {
                    getIFace().getFollowShops(result.getData().getShopInfoList());
                }
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }

    public void getSuggestedShops() {
        getIFace().showLoading();
        mRepositoryFollowShop.getSuggestedShops(getContext(), new DataCallBack<ShopResponse>() {
            @Override
            public void onSuccess(ShopResponse result) {
                getIFace().hideLoading();
                if (result.getData() != null) {
                    if (result.getData().getShops() != null) {
                        List<ShopInfo> shopInfoList = new ArrayList<>();
                        for (Shop item : result.getData().getShops()) {
                            ShopInfo shopInfo = new ShopInfo();
                            shopInfo.setId(item.getShopId());
                            shopInfo.setAvatar(item.getAvatar());
                            shopInfo.setName(item.getName());
                            shopInfo.setFollowsNumber(item.getFollowers());
                            shopInfoList.add(shopInfo);
                        }
                        getIFace().getSuggestionShop(shopInfoList);
                    }
                }
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }

    public void followShop(int shopId, int bonus) {
        getIFace().showLoading();
        mRepositoryFollowShop.followShop(getContext(), shopId, bonus, new DataCallBack<BaseResponse>() {

            @Override
            public void onSuccess(BaseResponse result) {
                getIFace().hideLoading();
                getIFace().followShopSuccess();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().hideLoading();
                getIFace().followShopFail(errorMessage);
            }
        });
    }

    public void unFollowShop(int shopId) {
        getIFace().showLoading();
        mRepositoryFollowShop.unFollowShop(getContext(), shopId, new DataCallBack<BaseResponse>() {

            @Override
            public void onSuccess(BaseResponse result) {
                getIFace().hideLoading();
                getIFace().unFollowShopSuccess();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().hideLoading();
                getIFace().getError(errorMessage);
            }
        });
    }
}
