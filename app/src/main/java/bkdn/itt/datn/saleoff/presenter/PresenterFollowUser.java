package bkdn.itt.datn.saleoff.presenter;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.model.model.User;
import bkdn.itt.datn.saleoff.model.model.UserInfo;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryFollowUser;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.model.response.UserInfoResponse;
import bkdn.itt.datn.saleoff.model.response.UserResponse;
import bkdn.itt.datn.saleoff.view.iface.IFragmentFollowUser;

/**
 * Created by TuDLT on 4/15/2018.
 */
public class PresenterFollowUser extends PresenterBase<IFragmentFollowUser> {
    private RepositoryFollowUser mRepositoryFollowUser;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryFollowUser = RepositoryFollowUser.getInstance();
    }

    public void getFollowedUser() {
        getIFace().showLoading();
        mRepositoryFollowUser.getFollowedUsers(getContext(), new DataCallBack<UserInfoResponse>() {
            @Override
            public void onSuccess(UserInfoResponse result) {
                getIFace().hideLoading();
                if (result.getData() != null) {
                    getIFace().getFollowUser(result.getData().getUsers());
                }
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }

    public void getSuggestedUser() {
        getIFace().showLoading();
        mRepositoryFollowUser.getSuggestedUsers(getContext(), new DataCallBack<UserResponse>() {
            @Override
            public void onSuccess(UserResponse result) {
                getIFace().hideLoading();
                if (result.getData() != null) {
                    if (result.getData().getListUser() != null) {
                        List<UserInfo> userInfoList = new ArrayList<>();
                        for (User item : result.getData().getListUser()) {
                            UserInfo userInfo = new UserInfo();
                            userInfo.setId(item.getId());
                            userInfo.setAvatar(item.getAvatar());
                            userInfo.setUserName(item.getUsername());
                            userInfo.setFollowsNumber(item.getFollowNumber());
                            userInfoList.add(userInfo);
                        }
                        getIFace().getSuggestionUser(userInfoList);
                    }
                }
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }

    public void followUser(int userId) {
        getIFace().showLoading();
        mRepositoryFollowUser.followUser(getContext(), userId, new DataCallBack<BaseResponse>() {

            @Override
            public void onSuccess(BaseResponse result) {
                getIFace().hideLoading();
                getIFace().followUserSuccess();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().hideLoading();
                getIFace().getError(errorMessage);
            }
        });
    }

    public void unFollowUser(int userId) {
        getIFace().showLoading();
        mRepositoryFollowUser.unFollowUser(getContext(), userId, new DataCallBack<BaseResponse>() {

            @Override
            public void onSuccess(BaseResponse result) {
                getIFace().hideLoading();
                getIFace().unFollowUserSuccess();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().hideLoading();
                getIFace().getError(errorMessage);
            }
        });
    }
}
