package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryCategorySub;
import bkdn.itt.datn.saleoff.model.repository.RepositoryInterested;
import bkdn.itt.datn.saleoff.model.response.CategoryResponse;
import bkdn.itt.datn.saleoff.model.response.SubCategoryResponse;
import bkdn.itt.datn.saleoff.view.iface.IActivityInterested;

/**
 * Created by TuDLT on 5/14/2018.
 */
public class PresenterInterested extends PresenterBase<IActivityInterested> {
    private RepositoryInterested mRepositoryInterested;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryInterested = RepositoryInterested.getInstance();
    }

    public void getAllCategory() {
        getIFace().showLoading();
        mRepositoryInterested.getAllCategory(getContext(), new DataCallBack<CategoryResponse>() {
            @Override
            public void onSuccess(CategoryResponse result) {
                if (result.getCategories() != null && !result.getCategories().isEmpty()) {
                    getIFace().getCategories(result.getCategories());
                }
                getIFace().hideLoading();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }

    public void getAllSubCategory() {
        getIFace().showLoading();
        mRepositoryInterested.getAllSubCategory(getContext(), new DataCallBack<SubCategoryResponse>() {
            @Override
            public void onSuccess(SubCategoryResponse result) {
                if (result.getListSubCategory() != null && !result.getListSubCategory().isEmpty()) {
                    getIFace().getSubCategories(result.getListSubCategory());
                } else {
                    getIFace().getError(getContext().getResources().getString(R.string.error_data_is_null));
                }
                getIFace().hideLoading();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }


}
