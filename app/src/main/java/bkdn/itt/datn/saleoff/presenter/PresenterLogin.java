package bkdn.itt.datn.saleoff.presenter;

import android.content.Intent;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import java.util.Arrays;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryLogin;
import bkdn.itt.datn.saleoff.model.response.LoginResponse;
import bkdn.itt.datn.saleoff.model.response.ProfileResponse;
import bkdn.itt.datn.saleoff.utils.SharedPreferencesUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.iface.IActivityLogin;

/**
 * Created by TuDLT on 3/3/2018.
 */
public class PresenterLogin extends PresenterBase<IActivityLogin> {

    private static final String TAG = "TAG";
    private CallbackManager mCallbackManager;
    private FirebaseAuth mAuth;
    private GoogleSignInClient mGoogleSignInClient;
    private RepositoryLogin mRepositoryLogin;


    @Override
    public void onInit() {
        super.onInit();
        mRepositoryLogin = RepositoryLogin.getInstance();
    }

    public CallbackManager getCallbackManager() {
        return mCallbackManager;
    }

    public void sigUpWithFacebook() {
        Utils.showLoadingDialog(getContext());
        mAuth = FirebaseAuth.getInstance();
        mCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(getIFace().getActivity(),
                Arrays.asList("email", "public_profile"));
        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult.getAccessToken());
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
                getIFace().loginFacebookFail("Login attempt cancelled.");
                getIFace().hideLoading();
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
                getIFace().loginFacebookFail(error.getMessage());
                getIFace().hideLoading();
            }
        });
    }

    public void sigUpWithGoogle() {
        Utils.showLoadingDialog(getContext());
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getContext().getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(getContext(), gso);
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        getIFace().getActivity().startActivityForResult(signInIntent, ConstantDefine.RC_SIGN_IN);
    }

    public void setReturnResultFromGGSignIn(Intent data) {
        Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
        try {
            // Google Sign In was successful, authenticate with Firebase
            GoogleSignInAccount account = task.getResult(ApiException.class);
            fireBaseAuthWithGoogle(account);
        } catch (ApiException e) {
            // Google Sign In failed, update UI appropriately
            Log.w(TAG, "Google sign in failed", e);
            // [START_EXCLUDE]
            getIFace().loginGoogleFail(e.getMessage());
            // [END_EXCLUDE]
        }
    }

    private void fireBaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "fireBaseAuthWithGoogle:" + acct.getId());
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth = FirebaseAuth.getInstance();
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(getIFace().getActivity(), task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        FirebaseUser user = mAuth.getCurrentUser();
                        user.getIdToken(true).addOnSuccessListener(getTokenResult -> {
                            Log.d(TAG, "Google Token: " + getTokenResult.getToken());
                            mRepositoryLogin.doLogin(getContext(), getTokenResult.getToken(),
                                    new DataCallBack<LoginResponse>() {
                                        @Override
                                        public void onSuccess(LoginResponse result) {
                                            SharedPreferencesUtils.getInstance(getContext()).
                                                    setKeySaveToken(result.getToken());
                                            SharedPreferencesUtils.getInstance(getContext())
                                                    .setBool(getContext().getString(R.string.key_is_first_login),
                                                            result.getIsFistLogin() == 1 ? true : false);
                                            getIFace().loginGoogleSuccess();
                                            getIFace().hideLoading();
                                        }

                                        @Override
                                        public void onError(String errorMessage) {
                                            getIFace().loginGoogleFail(errorMessage);
                                            getIFace().hideLoading();
                                        }
                                    });

                        });
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithCredential:failure", task.getException());
                        getIFace().loginGoogleFail("Authentication Failed.");
                        getIFace().hideLoading();
                    }
                });
    }

    private void handleFacebookAccessToken(AccessToken accessToken) {
        AuthCredential credential = FacebookAuthProvider.getCredential(accessToken.getToken());
        mAuth.signInWithCredential(credential).addOnCompleteListener(getIFace().getActivity(), task -> {
            if (task.isSuccessful()) {
                // Sign in success, update UI with the signed-in user's information
                FirebaseUser user = mAuth.getCurrentUser();
                user.getIdToken(true).addOnSuccessListener(getTokenResult -> {
                    Log.d(TAG, "Facebook token: " + getTokenResult.getToken());
                    mRepositoryLogin.doLogin(getContext(), getTokenResult.getToken(), new DataCallBack<LoginResponse>() {
                        @Override
                        public void onSuccess(LoginResponse result) {
                            SharedPreferencesUtils.getInstance(getContext()).
                                    setKeySaveToken(result.getToken());
                            SharedPreferencesUtils.getInstance(getContext())
                                    .setBool(getContext().getString(R.string.key_is_first_login),
                                            result.getIsFistLogin() == 1 ? true : false);
                            getIFace().loginFacebookSuccess();
                            getIFace().hideLoading();
                        }

                        @Override
                        public void onError(String errorMessage) {
                            getIFace().loginFacebookFail(errorMessage);
                            getIFace().hideLoading();
                        }
                    });

                });
            } else {
                // If sign in fails, display a message to the user.
                Log.w(TAG, "signInWithCredential:failure", task.getException());
                getIFace().loginFacebookFail(getContext().getResources().getString(R.string.error_authentication_failed));
                getIFace().hideLoading();
            }
        });
    }

    public void getProfile() {
        getIFace().showLoading();
        mRepositoryLogin.getProfile(getContext(), new DataCallBack<ProfileResponse>() {
            @Override
            public void onSuccess(ProfileResponse result) {
                getIFace().getProfileSuccess(result.getUser());
                getIFace().hideLoading();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().requestFail(errorMessage);
                getIFace().hideLoading();
            }
        });
    }


}
