package bkdn.itt.datn.saleoff.presenter;

import android.app.Activity;
import android.util.Log;

import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryMe;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.utils.SharedPreferencesUtils;
import bkdn.itt.datn.saleoff.view.iface.IFragmentMe;

/**
 * Created by TuDLT on 5/8/2018.
 */
public class PresenterMe extends PresenterBase<IFragmentMe> {
    private GoogleSignInClient mGoogleSignInClient;
    private RepositoryMe mRepositoryMe;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryMe = RepositoryMe.getInstance();
    }

    public void logOut() {
        getIFace().showLoading();
        try {

            mRepositoryMe.doLogOut(getContext(), new DataCallBack<BaseResponse>() {
                @Override
                public void onSuccess(BaseResponse result) {
                    SharedPreferencesUtils.getInstance(getContext()).resetDataWhenLogout();
                    GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                            .requestIdToken(getContext().getString(R.string.default_web_client_id))
                            .requestEmail()
                            .build();
                    mGoogleSignInClient = GoogleSignIn.getClient(getContext(), gso);
                    mGoogleSignInClient.signOut().addOnCompleteListener((Activity) getContext(), task -> {
                        Log.e("TAG1", "Sign out success");

                    });
                    LoginManager.getInstance().logOut();
                    FirebaseAuth.getInstance().signOut();
                    getIFace().logOutSuccess();
                    getIFace().hideLoading();
                }

                @Override
                public void onError(String errorMessage) {
                    getIFace().logOutFail(errorMessage);
                    getIFace().hideLoading();
                }
            });



        } catch (Exception e) {
            getIFace().logOutFail(e.getMessage());
            getIFace().hideLoading();
        }

    }

}
