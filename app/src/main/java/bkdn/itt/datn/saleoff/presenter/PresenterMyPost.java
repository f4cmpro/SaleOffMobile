package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryMyPost;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.model.response.PostResponse;
import bkdn.itt.datn.saleoff.view.iface.IActivityMyPost;

/**
 * Created by TuDLT on 3/19/2018.
 */
public class PresenterMyPost extends PresenterBase<IActivityMyPost> {
    private RepositoryMyPost mRepositoryMyPost;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryMyPost = RepositoryMyPost.getInstance();
    }


    public void getMyPost(boolean isFirst, int page) {
        if(isFirst){
            getIFace().showLoading();
        }
        mRepositoryMyPost.getMyPost(getContext(), page, new DataCallBack<PostResponse>() {
            @Override
            public void onSuccess(PostResponse result) {
                if (result.getData() != null) {
                    getIFace().getMyPostSuccess(result.getData().getPosts());
                }
                getIFace().hideLoading();

            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }

    public void deleteUserPost(int postId) {
        getIFace().showLoading();
        mRepositoryMyPost.deleteMyPost(getContext(), postId, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {
                getIFace().deleteUserPostSuccess();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().hideLoading();
                getIFace().getError(errorMessage);
            }
        });
    }

    public void likePost(int postId) {
        mRepositoryMyPost.likeMyPost(getContext(), postId, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {
                getIFace().likePostSuccess();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().likePostFail(errorMessage);
            }
        });
    }

    public void unLikePost(int postId) {
        mRepositoryMyPost.unLikeMyPost(getContext(), postId, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {
                getIFace().unLikePostSuccess();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().likePostFail(errorMessage);
            }
        });
    }
}
