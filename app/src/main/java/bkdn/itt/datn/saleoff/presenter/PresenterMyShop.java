package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryMyShop;
import bkdn.itt.datn.saleoff.model.response.ShopInfoResponse;
import bkdn.itt.datn.saleoff.view.iface.IFragmentMyShop;

/**
 * Created by TuDLT on 3/19/2018.
 */
public class PresenterMyShop extends PresenterBase<IFragmentMyShop> {
    private RepositoryMyShop mRepositoryMyShop;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryMyShop = RepositoryMyShop.getInstance();
    }

    public void getOwnerShop() {
        getIFace().showLoading();
        mRepositoryMyShop.getOwnedShop(getContext(), new DataCallBack<ShopInfoResponse>() {
            @Override
            public void onSuccess(ShopInfoResponse result) {
                getIFace().hideLoading();
                if (result.getData() != null) {
                    getIFace().getOwnerShopSuccess(result.getData().getShopInfoList());
                }
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getErrorMessage(errorMessage);
                getIFace().hideLoading();
            }
        });
    }

    public void getWorkingShop() {
        getIFace().showLoading();
        mRepositoryMyShop.getWorkingShop(getContext(), new DataCallBack<ShopInfoResponse>() {
            @Override
            public void onSuccess(ShopInfoResponse result) {
                getIFace().hideLoading();
                if (result.getData() != null) {
                    getIFace().getWorkingShopSuccess(result.getData().getShopInfoList());
                }
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().hideLoading();
                getIFace().getErrorMessage(errorMessage);
            }
        });
    }
}
