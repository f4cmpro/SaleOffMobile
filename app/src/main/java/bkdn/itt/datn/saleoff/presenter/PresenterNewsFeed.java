package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryNewsFeed;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.model.response.CategoryResponse;
import bkdn.itt.datn.saleoff.model.response.PostResponse;
import bkdn.itt.datn.saleoff.view.iface.IFragmentNewsFeed;

/**
 * Created by TuDLT on 3/3/2018.
 */
public class PresenterNewsFeed extends PresenterBase<IFragmentNewsFeed> {
    private RepositoryNewsFeed mRepositoryNewsFeed;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryNewsFeed = RepositoryNewsFeed.getInstance();
    }

    public void getAllCategory() {
        getIFace().showLoading();
        mRepositoryNewsFeed.getAllCategory(getContext(), new DataCallBack<CategoryResponse>() {
            @Override
            public void onSuccess(CategoryResponse result) {
                if (result.getCategories() != null && !result.getCategories().isEmpty()) {
                    getIFace().getCategories(result.getCategories());
                }
                getIFace().hideLoading();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }


    public void getNewsFeedFollowing(boolean isFirst, int categoryId, int lastId) {
        if(isFirst){
            getIFace().showLoading();
        }
        mRepositoryNewsFeed.getNewsFeedFollow(getContext(), categoryId, lastId, new DataCallBack<PostResponse>() {
            @Override
            public void onSuccess(PostResponse result) {
                if (result.getData().getPosts() != null && !result.getData().getPosts().isEmpty()) {
                    getIFace().getFollowingNewsFeed(result.getData().getPosts(), result.getData().getLastId());
                    getIFace().hideLoading();
                } else {
                    getIFace().getFollowingNewsFeedEmpty();
                }
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }

    public void getNewsFeedNotFollowing(int categoryId, int lastId) {
        mRepositoryNewsFeed.getNewsFeedNotFollow(getContext(), categoryId, lastId, new DataCallBack<PostResponse>() {
            @Override
            public void onSuccess(PostResponse result) {
                if (result.getData().getPosts() != null && !result.getData().getPosts().isEmpty()) {
                    getIFace().getNotFollowingNewsFeed(result.getData().getPosts(), result.getData().getLastId());
                    getIFace().hideLoading();
                } else {
                    getIFace().getNotFollowingNewsFeedEmpty();
                    getIFace().hideLoading();
                }
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }

    public void getCareFollow(boolean isFirst, int lastId) {
        if(isFirst){
            getIFace().showLoading();
        }
        mRepositoryNewsFeed.getYourCareFollow(getContext(), lastId, new DataCallBack<PostResponse>() {
            @Override
            public void onSuccess(PostResponse result) {
                if (result.getData().getPosts() != null && !result.getData().getPosts().isEmpty()) {
                    getIFace().getCareFollow(result.getData().getPosts(), result.getData().getLastId());
                    getIFace().hideLoading();
                } else {
                    getIFace().getCareFollowEmpty();
                }
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }

    public void getCareNotFollow(int lastId) {
        mRepositoryNewsFeed.getYourCareNotFollow(getContext(), lastId, new DataCallBack<PostResponse>() {
            @Override
            public void onSuccess(PostResponse result) {
                if (result.getData().getPosts() != null && !result.getData().getPosts().isEmpty()) {
                    getIFace().getCareNotFollow(result.getData().getPosts(), result.getData().getLastId());
                    getIFace().hideLoading();
                } else {
                    getIFace().getCareNotFollowEmpty();
                    getIFace().hideLoading();
                }
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }

    public void likeNewsPost(int postId) {
        mRepositoryNewsFeed.likeFeedPost(getContext(), postId, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {
                getIFace().likePostSuccess();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().likePostFail(errorMessage);
            }
        });
    }

    public void unLikeNewsPost(int postId) {
        mRepositoryNewsFeed.unLikeFeedPost(getContext(), postId, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {
                getIFace().unLikePostSuccess();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
            }
        });
    }

    public void reportPost(int postId, int reasonId){
        mRepositoryNewsFeed.sendReportPost(getContext(), postId, reasonId, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {
                getIFace().reportSuccess();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
            }
        });
    }
}
