package bkdn.itt.datn.saleoff.presenter;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.model.local.DBHelper;
import bkdn.itt.datn.saleoff.model.model.Notification;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryNotification;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.model.response.NotificationResponse;
import bkdn.itt.datn.saleoff.view.iface.IFragmentNotification;

/**
 * Created by TuDLT on 3/3/2018.
 */
public class PresenterNotification extends PresenterBase<IFragmentNotification> {
    private RepositoryNotification mRepositoryNotification;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryNotification = RepositoryNotification.getInstance();
    }

    public void getNotifications(boolean isFirst) {
        if(isFirst){
            getIFace().showLoading();
        }
        mRepositoryNotification.getNotifications(getContext(), new DataCallBack<NotificationResponse>() {
            @Override
            public void onSuccess(NotificationResponse result) {
                if (result.getData() != null) {
                    getIFace().hideLoading();
                    getIFace().getNotificationsSuccess(result.getData().getNotifications());
                }
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().hideLoading();
                getIFace().getError(errorMessage);
            }
        });
    }

    public void deleteNotification(String noteId) {
        getIFace().showLoading();
        mRepositoryNotification.deleteNotification(getContext(), noteId, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {
                getIFace().hideLoading();
                getIFace().deleteNotificationSuccess();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().hideLoading();
                getIFace().getError(errorMessage);
            }
        });
    }
}
