package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.Post;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryPostCreate;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.view.iface.IActivityPostCreate;

/**
 * Created by TuDLT on 4/7/2018.
 */
public class PresenterPostCreate extends PresenterBase<IActivityPostCreate>{
    private RepositoryPostCreate mRepositoryPost;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryPost = RepositoryPostCreate.getInstance();
    }

    public void createPost(Post createdPost) {
        getIFace().showLoading();
        mRepositoryPost.createPost(getContext(), createdPost, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {
                getIFace().createPostSuccess(getContext().getString(R.string.post_waitting_active));
                getIFace().hideLoading();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().createPostFail(errorMessage);
                getIFace().hideLoading();
            }
        });
    }

}
