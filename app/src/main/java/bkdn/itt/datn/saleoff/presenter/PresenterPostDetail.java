package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryPostDetail;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.model.response.PostDetailResponse;
import bkdn.itt.datn.saleoff.model.response.PostResponse;
import bkdn.itt.datn.saleoff.view.iface.IActivityPostDetail;

/**
 * Created by TuDLT on 3/3/2018.
 */
public class PresenterPostDetail extends PresenterBase<IActivityPostDetail> {
    private RepositoryPostDetail mRepositoryPostDetail;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryPostDetail = RepositoryPostDetail.getInstance();
    }

    public void getPostDetail(int postId){
        getIFace().showLoading();
        mRepositoryPostDetail.getPostDetail(getContext(), postId, new DataCallBack<PostDetailResponse>() {
            @Override
            public void onSuccess(PostDetailResponse result) {
                getIFace().hideLoading();
                if(result.getData() != null && result.getData().getPost() != null){
                    getIFace().getPostDetailSuccess(result.getData().getPost());
                }
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().hideLoading();
                getIFace().getError(errorMessage);
            }
        });
    }

    public void likePost(int postId) {
        mRepositoryPostDetail.likePostDetail(getContext(), postId, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {
                getIFace().likePostSuccess();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
            }
        });
    }

    public void unLikePost(int postId) {
        mRepositoryPostDetail.unLikePostDetail(getContext(), postId, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {
                getIFace().unLikePostSuccess();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
            }
        });
    }

    public void getRecentPosts(int id){
        getIFace().showLoading();
        mRepositoryPostDetail.getPostRecent(getContext(), id, new DataCallBack<PostResponse>() {
            @Override
            public void onSuccess(PostResponse result) {
                if(result.getData().getPosts() != null){
                    getIFace().hideLoading();
                    getIFace().getRecentPosts(result.getData().getPosts());
                }
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().hideLoading();
                getIFace().getError(errorMessage);
            }
        });
    }
}
