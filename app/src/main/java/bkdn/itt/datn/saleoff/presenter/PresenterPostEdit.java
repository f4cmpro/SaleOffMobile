package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.model.model.Post;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryPostEdit;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.view.iface.IActivityPostEdit;

/**
 * Created by TuDLT on 4/18/2018.
 */
public class PresenterPostEdit extends PresenterBase<IActivityPostEdit>{
    private RepositoryPostEdit mRepositoryPostEdit;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryPostEdit = RepositoryPostEdit.getInstance();
    }

    public void editPost(Post post){
        getIFace().showLoading();
        if(post.getShopId() != 0){
            mRepositoryPostEdit.editShopPost(getContext(), post, new DataCallBack<BaseResponse>() {
                @Override
                public void onSuccess(BaseResponse result) {
                    getIFace().editPostSuccess();
                    getIFace().hideLoading();
                }

                @Override
                public void onError(String errorMessage) {
                    getIFace().hideLoading();
                    getIFace().editPostError(errorMessage);
                }
            });
        }else{
            mRepositoryPostEdit.editUserPost(getContext(), post, new DataCallBack<BaseResponse>() {
                @Override
                public void onSuccess(BaseResponse result) {
                    getIFace().editPostSuccess();
                    getIFace().hideLoading();
                }

                @Override
                public void onError(String errorMessage) {
                    getIFace().hideLoading();
                    getIFace().editPostError(errorMessage);
                }
            });
        }
    }
}
