package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositorySharePost;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.view.iface.IActivityShare;

/**
 * Created by TuDLT on 5/16/2018.
 */
public class PresenterPostShare extends PresenterBase<IActivityShare> {
    private RepositorySharePost mRepositorySharePost;

    @Override
    public void onInit() {
        super.onInit();
        mRepositorySharePost = RepositorySharePost.getInstance();
    }

    public void share(String description, int postId) {
        getIFace().showLoading();
        mRepositorySharePost.sharePost(getContext(), description, postId, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {
                getIFace().shareSuccess();
                getIFace().hideLoading();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().shareError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }
}
