package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.User;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryProfile;
import bkdn.itt.datn.saleoff.model.response.ProfileResponse;
import bkdn.itt.datn.saleoff.view.iface.IActivityProfile;

/**
 * Created by TuDLT on 3/3/2018.
 */
public class PresenterProfile extends PresenterBase<IActivityProfile> {
    private RepositoryProfile mRepositoryProfile;
    @Override
    public void onInit() {
        super.onInit();
        mRepositoryProfile = RepositoryProfile.getInstance();
    }
    public void editProfile(User user){
        getIFace().showLoading();
        mRepositoryProfile.editProfile(getContext(), user, new DataCallBack<ProfileResponse>() {
            @Override
            public void onSuccess(ProfileResponse result) {
                if(result.getUser() != null){
                    getIFace().editProfileSuccess(result.getUser());
                    getIFace().hideLoading();
                }else{
                    getIFace().editProfileFail(getContext().getString(R.string.error_edit_fail));
                    getIFace().hideLoading();
                }
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().editProfileFail(errorMessage);
                getIFace().hideLoading();
            }
        });
    }



}
