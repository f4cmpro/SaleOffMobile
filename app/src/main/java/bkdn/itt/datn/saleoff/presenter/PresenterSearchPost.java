package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositorySearchPost;
import bkdn.itt.datn.saleoff.model.response.CategoryResponse;
import bkdn.itt.datn.saleoff.model.response.PostResponse;
import bkdn.itt.datn.saleoff.model.response.SubCategoryResponse;
import bkdn.itt.datn.saleoff.view.iface.IFragmentSearchPost;

/**
 * Created by TuDLT on 4/26/2018.
 */
public class PresenterSearchPost extends PresenterBase<IFragmentSearchPost> {
    private RepositorySearchPost mRepositorySearchPost;

    @Override
    public void onInit() {
        super.onInit();
        mRepositorySearchPost = RepositorySearchPost.getInstance();
    }

    public void searchPost(int page, String textKey, String type, boolean isFollow, int catId,
                           int subCateId) {
        getIFace().showLoading();
        mRepositorySearchPost.searchPost(getContext(), page, type, textKey, isFollow, catId, subCateId,
                new DataCallBack<PostResponse>() {
                    @Override
                    public void onSuccess(PostResponse result) {
                        if (result.getData() != null) {
                            getIFace().hideLoading();
                            getIFace().searchPostSuccess(result.getData().getPosts());
                        }
                    }

                    @Override
                    public void onError(String errorMessage) {
                        getIFace().hideLoading();
                        getIFace().getError(errorMessage);
                    }
                });

    }

    public void getCategories() {
        getIFace().showLoading();
        mRepositorySearchPost.getAllCategory(getContext(), new DataCallBack<CategoryResponse>() {
            @Override
            public void onSuccess(CategoryResponse result) {
                if (result.getCategories() != null) {
                    getIFace().getCategoriesSuccess(result.getCategories());
                    getSubCategories();
                }
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().hideLoading();
                getIFace().getError(errorMessage);
            }
        });
    }

    public void getSubCategories() {
        mRepositorySearchPost.getAllSubCategory(getContext(), new DataCallBack<SubCategoryResponse>() {
            @Override
            public void onSuccess(SubCategoryResponse result) {
                getIFace().hideLoading();
                if (result.getListSubCategory() != null) {
                    getIFace().getSubCategoriesSuccess(result.getListSubCategory());
                }
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().hideLoading();
                getIFace().getError(errorMessage);
            }
        });
    }

}
