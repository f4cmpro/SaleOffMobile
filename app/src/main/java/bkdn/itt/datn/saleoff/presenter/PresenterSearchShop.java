package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositorySearchShop;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.model.response.CategoryResponse;
import bkdn.itt.datn.saleoff.model.response.ShopResponse;
import bkdn.itt.datn.saleoff.view.iface.IFragmentSearchShop;

/**
 * Created by TuDLT on 4/26/2018.
 */
public class PresenterSearchShop extends PresenterBase<IFragmentSearchShop> {
    private RepositorySearchShop mRepositorySearchShop;

    @Override
    public void onInit() {
        super.onInit();
        mRepositorySearchShop = RepositorySearchShop.getInstance();
    }

    public void searchShop(int currentPage, String textKey, boolean isFollow, int catId) {
        getIFace().showLoading();
        mRepositorySearchShop.searchShop(getContext(), currentPage, textKey, isFollow, catId,
                new DataCallBack<ShopResponse>() {
                    @Override
                    public void onSuccess(ShopResponse result) {
                        if (result.getData() != null) {
                            getIFace().hideLoading();
                            getIFace().getSearchShopSuccess(result.getData().getShops());
                        }
                    }

                    @Override
                    public void onError(String errorMessage) {
                        getIFace().hideLoading();
                        getIFace().getError(errorMessage);
                    }
                });
    }

    public void getCategories() {
        getIFace().showLoading();
        mRepositorySearchShop.getAllCategory(getContext(), new DataCallBack<CategoryResponse>() {
            @Override
            public void onSuccess(CategoryResponse result) {
                if (result.getCategories() != null) {
                    getIFace().getCategoriesSuccess(result.getCategories());
                }
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().hideLoading();
                getIFace().getError(errorMessage);
            }
        });

    }

    public void followShop(int shopId, int bonusCode) {
        mRepositorySearchShop.followShop(getContext(), shopId, bonusCode, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
                getIFace().followShopFail();
            }
        });
    }

    public void unFollowShop(int shopId) {
        mRepositorySearchShop.unFollowShop(getContext(), shopId, new DataCallBack<BaseResponse>() {

            @Override
            public void onSuccess(BaseResponse result) {
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
                getIFace().unFollowShopFail();
            }
        });
    }
}
