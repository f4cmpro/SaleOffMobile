package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositorySearchUser;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.model.response.UserResponse;
import bkdn.itt.datn.saleoff.view.iface.IFragmentSearchUser;

/**
 * Created by TuDLT on 4/26/2018.
 */
public class PresenterSearchUser extends PresenterBase<IFragmentSearchUser> {
    private RepositorySearchUser mRepositorySearchUser;

    @Override
    public void onInit() {
        super.onInit();
        mRepositorySearchUser = RepositorySearchUser.getInstance();
    }

    public void searchUser(int currentPage, String textKey, boolean isFollow) {
        getIFace().showLoading();
        mRepositorySearchUser.searchUser(getContext(), currentPage, textKey, isFollow,
                new DataCallBack<UserResponse>() {
                    @Override
                    public void onSuccess(UserResponse result) {
                        getIFace().hideLoading();
                        if (result.getData() != null) {
                            getIFace().getSearchUsersSuccess(result.getData().getListUser()
                            );
                        }
                    }

                    @Override
                    public void onError(String errorMessage) {
                        getIFace().hideLoading();
                        getIFace().getError(errorMessage);
                    }
                });
    }

    public void followUser(int userId) {
        mRepositorySearchUser.followUser(getContext(), userId, new DataCallBack<BaseResponse>() {

            @Override
            public void onSuccess(BaseResponse result) {
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
                getIFace().FollowUserFail();
            }
        });
    }

    public void unFollowUser(int userId) {
        mRepositorySearchUser.unFollowUser(getContext(), userId, new DataCallBack<BaseResponse>() {

            @Override
            public void onSuccess(BaseResponse result) {
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
                getIFace().unFollowUserFail();
            }
        });
    }
}
