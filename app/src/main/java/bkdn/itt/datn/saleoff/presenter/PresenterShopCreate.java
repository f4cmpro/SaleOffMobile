package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.model.model.Shop;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryShopCreate;
import bkdn.itt.datn.saleoff.model.response.CreateShopResponse;
import bkdn.itt.datn.saleoff.view.iface.IActivityShopCreate;

/**
 * Created by TuDLT on 3/18/2018.
 */
public class PresenterShopCreate extends PresenterBase<IActivityShopCreate> {
    private RepositoryShopCreate mRepositoryShopCreate;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryShopCreate = RepositoryShopCreate.getInstance();
    }

    public void createShop(Shop createdShop) {
        getIFace().showLoading();
        mRepositoryShopCreate.createShop(getContext(), createdShop, new DataCallBack<CreateShopResponse>() {
            @Override
            public void onSuccess(CreateShopResponse result) {
                getIFace().createShopSuccess(result.getShop());
                getIFace().hideLoading();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getErrorMessage(errorMessage);
                getIFace().hideLoading();
            }
        });
    }
}
