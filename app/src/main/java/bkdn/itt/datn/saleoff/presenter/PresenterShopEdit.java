package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.model.model.Shop;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryShopEdit;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.view.iface.IActivityShopEdit;

/**
 * Created by TuDLT on 5/7/2018.
 */
public class PresenterShopEdit extends PresenterBase<IActivityShopEdit> {
    private RepositoryShopEdit mRepositoryShopEdit;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryShopEdit = RepositoryShopEdit.getInstance();
    }

    public void editShop(Shop editedShop) {
        getIFace().showLoading();
        mRepositoryShopEdit.editShop(getContext(), editedShop, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {
                getIFace().editShopSuccess();
                getIFace().hideLoading();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }
}
