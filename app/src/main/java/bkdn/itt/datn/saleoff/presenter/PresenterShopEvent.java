package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryShopEvent;
import bkdn.itt.datn.saleoff.model.response.EventListResponse;
import bkdn.itt.datn.saleoff.view.iface.IFragmentShopEvent;

/**
 * Created by TuDLT on 5/28/2018.
 */
public class PresenterShopEvent extends PresenterBase<IFragmentShopEvent> {

    private RepositoryShopEvent mRepositoryShopEvent;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryShopEvent = RepositoryShopEvent.getInstance();
    }

    public void getAllShopEvents(boolean isFirst, int shopId) {
        if (isFirst) {
            getIFace().showLoading();
        }
        mRepositoryShopEvent.getAllShopEvents(getContext(), shopId, new DataCallBack<EventListResponse>() {
            @Override
            public void onSuccess(EventListResponse result) {
                if (result.getData() != null) {
                    getIFace().getAllEventsSuccess(result.getData().getEvents());
                }
                getIFace().hideLoading();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getAllEventsError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }
}
