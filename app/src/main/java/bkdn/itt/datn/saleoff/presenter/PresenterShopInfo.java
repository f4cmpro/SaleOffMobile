package bkdn.itt.datn.saleoff.presenter;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryShopInfo;
import bkdn.itt.datn.saleoff.model.response.ShopDetailResponse;
import bkdn.itt.datn.saleoff.view.iface.IFragmentShopInfo;

/**
 * Created by TuDLT on 3/25/2018.
 */
public class PresenterShopInfo extends PresenterBase<IFragmentShopInfo> {
    private RepositoryShopInfo mRepositoryShopInfo;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryShopInfo = RepositoryShopInfo.getInstance();
    }

    public void getShopDetail(int shopId) {
        getIFace().showLoading();
        mRepositoryShopInfo.getShopDetailById(getContext(), shopId, new DataCallBack<ShopDetailResponse>() {
            @Override
            public void onSuccess(ShopDetailResponse result) {
                getIFace().hideLoading();
                if (result.getData() != null) {
                    getIFace().getShopDetailSuccess(result.getData().getShop());
                }
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }
}
