package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryShopMembers;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.model.response.UserResponse;
import bkdn.itt.datn.saleoff.view.iface.IFragmentShopMembers;

/**
 * Created by TuDLT on 5/18/2018.
 */
public class PresenterShopMembers extends PresenterBase<IFragmentShopMembers> {
    RepositoryShopMembers mRepositoryShopMembers;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryShopMembers = RepositoryShopMembers.getInstance();
    }

    public void getShopMembers(int shopId) {
        getIFace().showLoading();
        mRepositoryShopMembers.getShopMembers(getContext(), shopId, new DataCallBack<UserResponse>() {
            @Override
            public void onSuccess(UserResponse result) {
                if (result.getData() != null) {
                    getIFace().getShopMembersSuccess(result.getData().getListUser());
                }
                getIFace().hideLoading();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getShopMembersFail(errorMessage);
                getIFace().hideLoading();
            }
        });
    }

    public void minusBonus(int shopId, int userId, int minusPoint) {
        getIFace().showLoading();
        mRepositoryShopMembers.minusBonus(getContext(), shopId, userId, minusPoint, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {
                getIFace().minusBonusSuccess();
                getIFace().hideLoading();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().minusBonusFail(errorMessage);
                getIFace().hideLoading();
            }
        });
    }
}
