package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryShopPost;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.model.response.PostResponse;
import bkdn.itt.datn.saleoff.view.iface.IFragmentShopPost;

/**
 * Created by TuDLT on 3/25/2018.
 */
public class PresenterShopPost extends PresenterBase<IFragmentShopPost> {
    private RepositoryShopPost mRepositoryShopPost;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryShopPost = RepositoryShopPost.getInstance();
    }

    public void getShopPost(int page, int shopId) {
        mRepositoryShopPost.getShopPost(getContext(), page, shopId, new DataCallBack<PostResponse>() {
            @Override
            public void onSuccess(PostResponse result) {
                if (result.getData() != null && result.getData().getPosts() != null) {
                    getIFace().getListShopPostSuccess(result.getData().getPosts());
                    getIFace().hideLoading();
                }
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }

    public void deleteShopPost(int postId, int shopId) {
        getIFace().showLoading();
        mRepositoryShopPost.deleteShopPost(getContext(), postId, shopId, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {
                getIFace().deleteShopPostSuccess(result.getMessage());
                getIFace().hideLoading();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }

    public void likePost(int postId) {
        mRepositoryShopPost.likeShopPost(getContext(), postId, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {
                getIFace().likePostSuccess();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
            }
        });
    }

    public void unLikePost(int postId) {
        mRepositoryShopPost.unLikeShopPost(getContext(), postId, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {
                getIFace().unLikePostSuccess();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
            }
        });
    }

}
