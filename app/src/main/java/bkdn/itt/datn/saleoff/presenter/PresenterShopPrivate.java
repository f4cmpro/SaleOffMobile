package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryShopPrivate;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.model.response.ShopDetailResponse;
import bkdn.itt.datn.saleoff.view.iface.IActivityShopPrivate;

/**
 * Created by TuDLT on 5/6/2018.
 */
public class PresenterShopPrivate extends PresenterBase<IActivityShopPrivate>{
    private RepositoryShopPrivate mRepositoryShopPrivate;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryShopPrivate = RepositoryShopPrivate.getInstance();
    }

    public void getShopDetail(int shopId) {
        getIFace().showLoading();
        mRepositoryShopPrivate.getShopDetailById(getContext(), shopId, new DataCallBack<ShopDetailResponse>() {
            @Override
            public void onSuccess(ShopDetailResponse result) {
                getIFace().hideLoading();
                if (result.getData() != null) {
                    getIFace().getShopDetailSuccess(result.getData().getShop());
                }
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }

    public void deleteShop(int shopId) {
        getIFace().showLoading();
        mRepositoryShopPrivate.deleteShop(getContext(), shopId, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {
                getIFace().hideLoading();
                getIFace().requestDeleteShopSuccess();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().hideLoading();
                getIFace().getError(errorMessage);
            }
        });
    }
}
