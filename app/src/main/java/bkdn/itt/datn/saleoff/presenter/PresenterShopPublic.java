package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryShopPublic;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.model.response.ShopDetailResponse;
import bkdn.itt.datn.saleoff.view.iface.IActivityShop;

/**
 * Created by TuDLT on 3/3/2018.
 */
public class PresenterShopPublic extends PresenterBase<IActivityShop> {
    private RepositoryShopPublic mRepositoryShopPublic;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryShopPublic = RepositoryShopPublic.getInstance();
    }

    public void getShopDetail(int shopId) {
        getIFace().showLoading();
        mRepositoryShopPublic.getShopDetailById(getContext(), shopId, new DataCallBack<ShopDetailResponse>() {
            @Override
            public void onSuccess(ShopDetailResponse result) {
                getIFace().hideLoading();
                if (result.getData() != null) {
                    getIFace().getShopDetailSuccess(result.getData().getShop());
                }
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
                getIFace().hideLoading();
            }
        });
    }

    public void ratingShop(int shopId, int rate) {
        mRepositoryShopPublic.ratingShop(getContext(), shopId, rate, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {
                getIFace().ratingShopSuccess();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
            }
        });
    }

    public void reportShop(int shopId, int reasonId){
        mRepositoryShopPublic.sendReportShop(getContext(), shopId, reasonId, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {
                getIFace().reportSuccess();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
            }
        });
    }
}
