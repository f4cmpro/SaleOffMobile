package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryCategorySub;
import bkdn.itt.datn.saleoff.model.response.SubCategoryResponse;
import bkdn.itt.datn.saleoff.view.iface.IAcvititySubCategory;

/**
 * Created by TuDLT on 4/7/2018.
 */
public class PresenterSubCategory extends PresenterBase<IAcvititySubCategory> {
    private RepositoryCategorySub mRepositoryCategorySub;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryCategorySub = RepositoryCategorySub.getInstance();
    }

    public void getAllSubCategory() {
        getIFace().showLoading();
        mRepositoryCategorySub.getAllSubCategory(getContext(), new DataCallBack<SubCategoryResponse>() {
            @Override
            public void onSuccess(SubCategoryResponse result) {
                getIFace().getSubCategorySuccess(result.getListSubCategory());
                getIFace().hideLoading();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getSubCategoryFail(errorMessage);
                getIFace().hideLoading();
            }
        });
    }
}
