package bkdn.itt.datn.saleoff.presenter;

import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.repository.RepositoryUserPublic;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.model.response.PostResponse;
import bkdn.itt.datn.saleoff.model.response.UserDetailResponse;
import bkdn.itt.datn.saleoff.view.iface.IActivityUserPublic;

/**
 * Created by TuDLT on 5/3/2018.
 */
public class PresenterUserPublic extends PresenterBase<IActivityUserPublic> {
    private RepositoryUserPublic mRepositoryUserPublic;

    @Override
    public void onInit() {
        super.onInit();
        mRepositoryUserPublic = RepositoryUserPublic.getInstance();
    }

    public void getUserById(int userId) {
        getIFace().showLoading();
        mRepositoryUserPublic.getUserById(getContext(), userId, new DataCallBack<UserDetailResponse>() {
            @Override
            public void onSuccess(UserDetailResponse result) {
                getIFace().hideLoading();
                if (result.getData() != null) {
                    getIFace().getUserByIdSuccess(result.getData().getUser());
                }
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().hideLoading();
                getIFace().getError(errorMessage);
            }
        });
    }

    public void getUserPosts(int userId, int lastId) {
        mRepositoryUserPublic.getUserPost(getContext(), userId, lastId, new DataCallBack<PostResponse>() {
            @Override
            public void onSuccess(PostResponse result) {
                if (result.getData() != null) {
                    getIFace().getUserPostSuccess(result.getData().getPosts(), result.getData().getLastId());
                }
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
            }
        });
    }

    public void reportUser(int userId, int reasonId){
        mRepositoryUserPublic.sendReportUser(getContext(), userId, reasonId, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {
                getIFace().reportSuccess();
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
            }
        });
    }


    public void followUser(int userId) {
        mRepositoryUserPublic.followUser(getContext(), userId, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {

            }
            @Override
            public void onError(String errorMessage) {
                getIFace().hideLoading();
                getIFace().followUserFail();
                getIFace().getError(errorMessage);
            }
        });
    }

    public void unFollowUser(int userId) {
        mRepositoryUserPublic.unFollowUser(getContext(), userId, new DataCallBack<BaseResponse>() {

            @Override
            public void onSuccess(BaseResponse result) {
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().hideLoading();
                getIFace().unFollowUserFail();
                getIFace().getError(errorMessage);
            }
        });
    }

    public void likePost(int postId) {
        mRepositoryUserPublic.likePost(getContext(), postId, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
                getIFace().likePostFail();
            }
        });
    }

    public void unLikePost(int postId) {
        mRepositoryUserPublic.unLikePost(getContext(), postId, new DataCallBack<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse result) {
            }

            @Override
            public void onError(String errorMessage) {
                getIFace().getError(errorMessage);
                getIFace().unLikeFail();
            }
        });
    }
}
