package bkdn.itt.datn.saleoff.utils;


import android.os.Handler;
import android.util.Log;

/**
 * Created by TiepNguyen on 28-12-17.
 * TiepNguyen8197@gmail.com.
 */

public class ClickUtils {
    private static final int CLICK_LOCK_INTERVAL = 500;

    private static boolean sIsLocked;

    private static Runnable mClickLockRunnable = new Runnable() {
        @Override
        public void run() {
            sIsLocked = false;
        }
    };

    private static Handler mHandler = new Handler();

    public synchronized static boolean isLocked() {
        Log.d("Dulieu", sIsLocked+"");
        if (sIsLocked) return true;
        mHandler.postDelayed(mClickLockRunnable, CLICK_LOCK_INTERVAL);
        sIsLocked = true;
        return false;
    }
}
