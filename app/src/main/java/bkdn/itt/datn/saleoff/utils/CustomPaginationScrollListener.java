package bkdn.itt.datn.saleoff.utils;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

public abstract class CustomPaginationScrollListener extends RecyclerView.OnScrollListener {

    private LinearLayoutManager layoutManager;

    /**
     * Supporting GridLayoutManager  now.
     *
     * @param layoutManager
     */
    public CustomPaginationScrollListener(LinearLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        Log.d("dy", "" + dy);
        if (!isLoading() && dy >= 0) {
            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
            Log.d("onScrolled  ", "visibleItemCount : " + visibleItemCount + " totalItemCount : " + totalItemCount + " firstVisibleItemPosition : " + firstVisibleItemPosition);
            if (firstVisibleItemPosition > 0 && (visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
                loadMoreItems();
            }
        } else if (dy < 0) {
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
            if (firstVisibleItemPosition > 0) {
                enableRefreshLayout(false);
            } else {
                enableRefreshLayout(true);
            }
        }
    }

    protected abstract void loadMoreItems();

    protected abstract boolean isLoading();

    protected abstract void enableRefreshLayout(boolean isEnable);

}


