package bkdn.itt.datn.saleoff.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;


public class DialogUtils {
    private DialogUtils() {
        //no-op
    }

    public static void createMultiChoiceDialog(Context context, String title, String[] arrs, boolean[] bools, DialogAlertCallback callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMultiChoiceItems(arrs, bools, (dialog, which, isChecked) -> bools[which] = isChecked);
        builder.setPositiveButton("yes", (dialog, which) -> callback.onClickPositive());
        builder.show();
    }

    public static void createSingleDialog(Context context, String title, String[] arrs, int positionChoosen, DialogInterface.OnClickListener listener, DialogAlertCallback callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setSingleChoiceItems(arrs, positionChoosen, listener);
        builder.setPositiveButton("yes", (dialog, which) -> callback.onClickPositive());
        builder.show();
    }

    public static void createAlertDialog(Context context, String title, String message) {
        createAlertDialog(context, title, message, null);
    }

    public static void createAlertDialog(Context context, String title, String message, DialogAlertCallback callback) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(title);
        alert.setMessage(message);
        alert.setPositiveButton("Ok", (dialog, which) -> {
            if (null != callback) {
                callback.onClickPositive();
            }
            dialog.dismiss();
        });
        alert.show();
    }

    public static void createConfirmDialog(Context context, String title, String message, DialogConfirmCallback callback) {
        createConfirmDialog(context, title, message,
                "Đồng ý",
                "Hủy bỏ",
                callback);
    }

    public static void createConfirmDialog(Context context, String title, String message, String textPositive, String textNegative, DialogConfirmCallback callback) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(title);
        alert.setMessage(message);
        alert.setPositiveButton(textPositive, (dialog, which) -> {
            if (null != callback) {
                callback.onClickPositiveButton();
            }
            dialog.dismiss();
        });
        alert.setNegativeButton(textNegative, (dialog, which) -> {
            if (null != callback) {
                callback.onClickNegativeButton();
            }
            dialog.dismiss();
        });
        alert.show();
    }

    public interface DialogConfirmCallback {
        void onClickPositiveButton();

        void onClickNegativeButton();
    }

    public interface DialogAlertCallback {
        void onClickPositive();
    }
}