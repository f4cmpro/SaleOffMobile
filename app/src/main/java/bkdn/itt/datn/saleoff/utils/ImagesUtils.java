package bkdn.itt.datn.saleoff.utils;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.view.activity.ActivityBase;
import es.dmoral.toasty.Toasty;
import id.zelory.compressor.Compressor;

/**
 * Created by TuDLT on 5/4/2018.
 */
public class ImagesUtils {
    public static File getRealFileFromUri(Context context, Uri uri) {
        if (uri == null) {
            return null;
        }
        Cursor cursor = null;
        try {
            String[] strings = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(uri, strings, null, null, null);
            int column_index = cursor != null ? cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA) : 0;
            if (cursor != null) {
                cursor.moveToFirst();
            }
            File file = new File(cursor != null ? cursor.getString(column_index) : "");
            return file;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static List<String> loadAllImagesFromSdCard(Context context) {
        List<String> imvFiles = new ArrayList<>();
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        final String[] columns = {MediaStore.Images.Media.DATA};
        final String orderBy = MediaStore.Images.Media.DATE_TAKEN;
        if (uri == null) {
            return null;
        }
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(uri, columns, null, null,
                    orderBy + " DESC");
            int column_index = cursor != null ? cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA) : 0;
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    File file = new File(cursor != null ? cursor.getString(column_index) : "");
                    imvFiles.add(file.getPath());
                }
            }
            return imvFiles;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static Bitmap decodeScaledBitmapFromSdCard(String filePath, int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = false;
        BitmapFactory.decodeFile(filePath, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(filePath, options);
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        return inSampleSize;
    }

    public static File saveImage(Bitmap finalBitmap) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/saved_images");
        myDir.mkdirs();
        Random generator = new Random();
        int n = generator.nextInt();
        String fname = "Image-" + n + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }

    public static void cropImage(Context context, Uri uri) {
        try {
            Intent CropIntent = new Intent("com.android.camera.action.CROP");
            CropIntent.setDataAndType(uri, "image/*");
            CropIntent.putExtra("crop", "true");
            CropIntent.putExtra("outputX", 90);
            CropIntent.putExtra("outputY", 90);
            CropIntent.putExtra("aspectX", 10);
            CropIntent.putExtra("aspectY", 10);
            CropIntent.putExtra("scaleUpIfNeeded", true);
            CropIntent.putExtra("return-data", true);
            ((Activity) context).startActivityForResult(CropIntent, ConstantDefine.RESULT_CROP_IMG);
        } catch (ActivityNotFoundException ex) {
            Toasty.error(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public static void openGallery(Context context) {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        ((Activity) context).startActivityForResult(photoPickerIntent, ConstantDefine.PICK_IMAGE_REQUEST);
    }

    public static void openCamera(Context context) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
            ((ActivityBase) context).startActivityForResult(takePictureIntent, ConstantDefine.IMAGE_CAPTURE_REQUEST);
        }
    }


    public static File compressPostImage(Context context, File actualImageFile) {
        try {
            File compressedImage = new Compressor(context)
                    .setMaxWidth(600)
                    .setMaxHeight(400)
                    .setQuality(100)
                    .setCompressFormat(Bitmap.CompressFormat.JPEG)
                    .compressToFile(actualImageFile);
            return compressedImage;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static File compressCoverImage(Context context, File actualImageFile) {
        try {
            File compressedImage = new Compressor(context)
                    .setMaxWidth(600)
                    .setMaxHeight(400)
                    .setQuality(100)
                    .setCompressFormat(Bitmap.CompressFormat.JPEG)
                    .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES).getAbsolutePath())
                    .compressToFile(actualImageFile);
            return compressedImage;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static File compressAvatarImage(Context context, File actualImageFile) {
        try {
            File compressedImage = new Compressor(context)
                    .setMaxWidth(96)
                    .setMaxHeight(96)
                    .setQuality(100)
                    .setCompressFormat(Bitmap.CompressFormat.JPEG)
                    .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES).getAbsolutePath())
                    .compressToFile(actualImageFile);
            return compressedImage;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


    public static Bitmap getRoundedCornerBitmap(Bitmap raw, float round) {
        int width = raw.getWidth();
        int height = raw.getHeight();
        Bitmap result = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(result);
        canvas.drawARGB(0, 0, 0, 0);

        final Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.parseColor("#000000"));

        final Rect rect = new Rect(0, 0, width, height);
        final RectF rectF = new RectF(rect);

        canvas.drawRoundRect(rectF, round, round, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(raw, rect, rect, paint);

        return result;
    }


}
