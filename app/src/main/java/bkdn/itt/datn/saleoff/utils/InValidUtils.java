package bkdn.itt.datn.saleoff.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by TuDLT on 5/4/2018.
 */
public class InValidUtils {
    public static boolean isValidPhone(String phoneNumber) {
        if (!Pattern.matches("[a-zA-Z]+", phoneNumber)) {

            if (phoneNumber.length() < 10 || phoneNumber.length() > 11) {
                return false;
            } else if (phoneNumber.charAt(0) != '0') {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public static boolean isValidEmail(String email) {
        boolean check;
        Pattern p;
        Matcher m;

        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        p = Pattern.compile(EMAIL_STRING);

        m = p.matcher(email);
        check = m.matches();

        return check;
    }

    public static boolean isValidUserName(String userName) {
        String charactersIDontWant = "!@#$%^&*()-+=|\\/|?<>:;~`\",.'[]{}";
        if (userName.length() > 50 || userName.length() < 6) {
            return false;
        }
        for (char c : charactersIDontWant.toCharArray()) {
            if (userName.indexOf(c) != -1) return false;
        }

        return true;
    }
}
