package bkdn.itt.datn.saleoff.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.User;
import bkdn.itt.datn.saleoff.model.model.UserFacebook;

/**
 * Created by PhuocDH on 12/26/17.
 */
public class SharedPreferencesUtils {
    public static final String KEY_SAVE_GCM_REGISTRATION_DEVICE_TOKEN = "key_save_gcm_registration_device_token";
    public static final String KEY_SAVE_USER_PROFILE = "key_save_user_facebook";
    public static final String KEY_SAVE_ACCESS_TOKEN = "key_save_access_token";
    public static final String KEY_IS_FIRST_LOGIN = "is_first_login";
    private static SharedPreferencesUtils mInstance = null;
    private final String SHARE_PREFERENCE = "SaleOffSharePreference";
    private SharedPreferences sharedPreferences;
    private Context mContext;

    public SharedPreferencesUtils() {
    }

    public static SharedPreferencesUtils getInstance(Context context) {
//        Class var1 = SharedPreferencesUtils.class;
        synchronized (SharedPreferencesUtils.class) {
            if (mInstance == null) {
                mInstance = new SharedPreferencesUtils();
                mInstance.setContext(context);
                mInstance.setSharedPreferences();
            }
            return mInstance;
        }
    }

    public static synchronized SharedPreferencesUtils getInstance() {
        if (mInstance == null) {
            try {
                throw new Exception("The newInstance() function have to call after newInstance(Context context) function.");
            } catch (Exception var1) {
                var1.printStackTrace();
            }
        }

        return mInstance;
    }

    private void setSharedPreferences() {
        this.sharedPreferences = this.mContext.getSharedPreferences(SHARE_PREFERENCE, Context.MODE_PRIVATE);
    }

    private void setContext(Context context) {
        this.mContext = context;
    }

    public boolean getBool(String key) {
        return this.sharedPreferences.getBoolean(key, false);
    }

    public float getFloat(String key) {
        return this.sharedPreferences.getFloat(key, -1.0F);
    }

    public int getInt(String key) {
        return this.sharedPreferences.getInt(key, -1);
    }

    public long getLong(String key) {
        return this.sharedPreferences.getLong(key, -1L);
    }

    public String getString(String key) {
        return this.sharedPreferences.getString(key, (String) null);
    }

    public void setBool(String key, Boolean value) {
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putBoolean(key, value.booleanValue());
        editor.commit();
    }

    public void setFloat(String key, float value) {
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putFloat(key, value);
        editor.commit();
    }

    public void setInt(String key, int value) {
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public void setLong(String key, long value) {
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putLong(key, value);
        editor.commit();
    }

    public void setString(String key, String value) {
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void setObject(String key, Object object) {
        Gson gson = new Gson();
        String json = gson.toJson(object);
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putString(key, json);
        editor.commit();
    }

    public Object getObject(String key, Class<?> object) {
        try {
            Gson e = new Gson();
            String json = this.sharedPreferences.getString(key, null);
            Object obj = e.fromJson(json, object);
            return obj;
        } catch (Exception var6) {
            return null;
        }
    }

    public boolean isExists(String key) {
        try {
            return this.sharedPreferences.getString(key, (String) null) != null;
        } catch (Exception var3) {
            return true;
        }
    }

    public void remove(String key) {
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.remove(key);
        editor.commit();
    }

    public void clear() {
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    /**
     * get device token
     *
     * @return
     */
    public String getKeySaveGcmRegistrationDeviceToken() {
        return this.sharedPreferences.getString(KEY_SAVE_GCM_REGISTRATION_DEVICE_TOKEN, "");
    }

    /**
     * save device token
     *
     * @param deviceToken
     */
    public void setKeySaveGcmRegistrationDeviceToken(String deviceToken) {
        this.sharedPreferences.edit().putString(KEY_SAVE_GCM_REGISTRATION_DEVICE_TOKEN, deviceToken).apply();
    }

    /**
     * get token
     *
     * @return
     */
    public String getKeySaveToken() {
        return this.sharedPreferences.getString(KEY_SAVE_ACCESS_TOKEN, null);
    }

    /**
     * save token
     *
     * @param token
     */
    public void setKeySaveToken(String token) {
        this.sharedPreferences.edit().putString(KEY_SAVE_ACCESS_TOKEN, token).apply();
    }


    public void setUserProfile(User user) {
        mInstance.setObject(KEY_SAVE_USER_PROFILE, user);
    }

    public User getUserProfile() {
        return (User) mInstance.getObject(KEY_SAVE_USER_PROFILE, User.class);
    }


    public void resetDataWhenLogout() {
        String deviceToken = getKeySaveGcmRegistrationDeviceToken();
        clear();
        setBool(mContext.getString(R.string.intro_key_save_first_open_app), true);
        setKeySaveGcmRegistrationDeviceToken(deviceToken);
    }
}
