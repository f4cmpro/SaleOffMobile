package bkdn.itt.datn.saleoff.utils;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import bkdn.itt.datn.saleoff.global.GlobalDefine;

/**
 * Created by QuangTD
 * on 1/2/2018.
 */

public class TimeUtils {
    /**
     * @param time: input time
     * @return HH:mm aa format
     */
    public static String formatTime(float time, boolean format12) {
        int h = (int) Math.floor(time);
        int m = (int) ((time - h) * 60);
        h = h % 24;
        boolean AM = (h < 12);
        if (format12) {
            h = h % 12;
        }
        return ((h < 10) ? "0" : "") + h + ":" + ((m < 10) ? "0" : "") + m + ((format12) ? ((AM) ? "AM" : "PM") : "");
    }

    public static String parseTimestampToString(long timeStamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeStamp);
        DateFormat dateFormat = new SimpleDateFormat(GlobalDefine.FORMAT_DATE_DEFAULT, Locale.getDefault());
        return dateFormat.format(calendar.getTime());
    }

    public static String parseTimestampToString(long timeStamp, String format) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeStamp);
        DateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(calendar.getTime());
    }

    /**
     * ex: 8:00 AM at local will be convert to 8:00 at UTC in order to map with time in server
     *
     * @param timestamp 8:00 AM timestamp
     * @return timestamp at 8:00 AM with UTC time zone.
     */
    public static long convertLocalTimestampToUTCTimestamp(long timestamp) {
        TimeZone timezone = TimeZone.getTimeZone(Calendar.getInstance().getTimeZone().getID());
        return timestamp + timezone.getOffset(Calendar.ZONE_OFFSET);
    }

    public static long convertUTCTimestampToLocalTimestamp(long timestamp) {
        TimeZone timeZone = TimeZone.getTimeZone(Calendar.getInstance().getTimeZone().getID());
        return timestamp - timeZone.getOffset(Calendar.ZONE_OFFSET);
    }

    public static float getTimeFromTimestamp(long timestamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        float time = 0;
        time += calendar.get(Calendar.HOUR_OF_DAY);
        time += calendar.get(Calendar.MINUTE) * 1.0f / 60;
        return time;
    }


    private static boolean isSameDay(Date date1, Date date2) {
        return date1.getDate() == date2.getDate() && date1.getMonth() == date2.getMonth() && date1.getYear() == date2.getYear();
    }

    public static String formatDate(String format, String date) {
        try {
            Date dat = new SimpleDateFormat(format).parse(date);
            DateFormat dateFormat = new SimpleDateFormat(format);
            return dateFormat.format(dat);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }

    }
}
