package bkdn.itt.datn.saleoff.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.model.net.DataCallBack;
import bkdn.itt.datn.saleoff.model.response.BaseResponse;
import bkdn.itt.datn.saleoff.view.activity.ActivityLogin;
import es.dmoral.toasty.Toasty;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

/**
 * Created by PhuocDH on 12/26/17.
 */

public class Utils {

    private static Dialog mDialog;
    private static int screenWidth = -1;

    //hide keyboard on activity
    public static void hideSoftKeyboard(Activity activity) {
        if (activity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
            }
        }
    }

    //show keyboard
    public static void showKeyboard(View v) {
        InputMethodManager inputMethodManager = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (v.requestFocus()) {
            if (inputMethodManager != null) {
                inputMethodManager.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
            }
        }
    }

    //hide keyboard
    public static void hideKeyboard(View v) {
        InputMethodManager inputMethodManager = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

    /*
    * show dialog when call API
    *
    * @param context app context
    */
    public static void showLoadingDialog(Context context) {
        if (context == null) return;
        try {
            if (mDialog != null) {
                if (mDialog.isShowing()) {
                    mDialog.dismiss();
                }
                mDialog = null;
            }
            mDialog = new Dialog(context, android.R.style.Theme_Translucent);
            mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mDialog.setCancelable(false);
            //TODO: add layout progress dilog
            mDialog.setContentView(R.layout.custom_progress_dialog);
            mDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void hideLoadingDialog() {
        if (mDialog != null) {
            if (mDialog.isShowing()) {
                mDialog.dismiss();
            }
        }
    }

    @SuppressWarnings("deprecation")
    public static int getScreenWidth(Activity activity) {

        if (screenWidth != -1)
            return screenWidth;
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();

        int width;
        int currentApiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentApiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB_MR2) {
            display.getSize(size);
            width = size.x;
        } else {
            width = display.getWidth();

        }
        screenWidth = width;

        return width;
    }

    public static int getScreenWidth(final Context context) {
        if (context == null) {
            return 0;
        }
        return getDisplayMetrics(context).widthPixels;
    }

    /**
     * Returns a valid DisplayMetrics object
     *
     * @param context valid context
     * @return DisplayMetrics object
     */
    static DisplayMetrics getDisplayMetrics(final Context context) {
        final WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        final DisplayMetrics metrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(metrics);
        return metrics;
    }

    public static void resizeView(View view, int width, int height) {
        ViewGroup.LayoutParams layout = view.getLayoutParams();
        layout.width = width;
        layout.height = height;
        view.setLayoutParams(layout);
    }

    public static int convertDpToPixel(Context context, int dp) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return (int) (dp * (metrics.densityDpi / 160f));
    }


    public static void invalidToken(Context context) {
        Toast.makeText(context, "Login expired", Toast.LENGTH_SHORT).show();
        logOut(context);
    }

    public static <T extends BaseResponse> void checkAndReceiveResponse(Context context, Response<T> response, DataCallBack<T> callBack) {
        if (response.errorBody() != null) {
            JSONObject jsonObject = createJSONError(response);
            if (jsonObject == null) {
                callBack.onError("Undefined Error");
                return;
            }
            try {
                if (jsonObject.getInt("error") == ConstantDefine.ERROR_INVALID_TOKEN) {
                    Utils.invalidToken(context);
                } else {
                    callBack.onError(jsonObject.getString("message"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (null != response.body() && response.body().getStatus() == ConstantDefine.RESPONSE_SUCCESS_CODE) {
            callBack.onSuccess(response.body());
        } else {
            callBack.onError(response.raw().message());
        }
    }

    public static <T extends BaseResponse> JSONObject createJSONError(Response<T> response) {
        try {
            JSONObject jsonObject = new JSONObject(response.errorBody().string());
            return jsonObject;
        } catch (Exception e) {
            return null;
        }
    }

    public static void logOut(Context context) {
        try {
            SharedPreferencesUtils.getInstance(context).resetDataWhenLogout();
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(context.getString(R.string.default_web_client_id))
                    .requestEmail()
                    .build();
            GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(context, gso);
            mGoogleSignInClient.signOut().addOnCompleteListener((Activity) context, task -> {
            });
            LoginManager.getInstance().logOut();
            FirebaseAuth.getInstance().signOut();
            Intent intent = new Intent(context, ActivityLogin.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);
            ((Activity) context).finish();

        } catch (Exception e) {
            Log.e("TAG1", e.getMessage());
        }
    }


}
