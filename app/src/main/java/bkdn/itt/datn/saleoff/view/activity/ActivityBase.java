package bkdn.itt.datn.saleoff.view.activity;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import java.lang.reflect.ParameterizedType;
import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.presenter.PresenterBase;
import bkdn.itt.datn.saleoff.view.fragment.FragmentBase;
import bkdn.itt.datn.saleoff.view.iface.IViewBase;

/**
 * Created by TuDLT on 12/26/17.
 */

public abstract class ActivityBase<P extends PresenterBase> extends AppCompatActivity {

    private P viewPresenter;

    public P getPresenter(IViewBase iViewBase) {
        try {
            if (this.viewPresenter == null) {
                String e = ((Class) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0]).getName();
                Class classDefinition = Class.forName(e);
                this.viewPresenter = (P) classDefinition.newInstance();
                this.viewPresenter.setIFace(iViewBase);
                this.viewPresenter.onInit();
                return this.viewPresenter;
            }
        } catch (InstantiationException var4) {
            var4.printStackTrace();
        } catch (IllegalAccessException var5) {
            var5.printStackTrace();
        } catch (ClassNotFoundException var6) {
            var6.printStackTrace();
        }
        return this.viewPresenter;
    }

    public void replaceFragment(FragmentBase fragment, int containViewId, boolean addToBackStack) {
        String TAB = fragment.getClass().getName();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.view_enter_from_left, R.anim.view_exit_to_right,
                R.anim.view_enter_from_right, R.anim.view_exit_to_left);
        if (addToBackStack) {
            transaction.addToBackStack(TAB);
        }
        transaction.replace(containViewId, fragment, TAB);
        transaction.commit();
        getSupportFragmentManager().executePendingTransactions();
    }

    public void startActivity(Class<?> cls) {
        Intent intent = new Intent(this, cls);
        startActivity(intent);
    }
}
