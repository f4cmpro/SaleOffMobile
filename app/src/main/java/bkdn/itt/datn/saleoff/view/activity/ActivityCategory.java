package bkdn.itt.datn.saleoff.view.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.local.DBHelper;
import bkdn.itt.datn.saleoff.model.model.Category;
import bkdn.itt.datn.saleoff.presenter.PresenterCategory;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.adapter.AdapterShopCategory;
import bkdn.itt.datn.saleoff.view.iface.IActivityCategory;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class ActivityCategory extends ActivityBase<PresenterCategory> implements IActivityCategory {
    /*
    * Widgets
    * */
    @BindView(R.id.recyclerShopCategory)
    RecyclerView mRecyclerCategory;
    @BindView(R.id.imbCategoryBack)
    ImageButton mImbBack;
    /*
    * Fields
    * */
    private AdapterShopCategory mAdapterShopCategory;
    private DBHelper mDBHelper;
    private List<Category> mCategories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        ButterKnife.bind(this);
        mDBHelper = new DBHelper(this);
        initViews();
        mImbBack.setOnClickListener(v -> onBackPressed());
    }

    private void initViews() {
        if (mDBHelper.isCategoryExist()) {
            mCategories = mDBHelper.getNotAllCategory();
            mAdapterShopCategory = new AdapterShopCategory(getContext(), mCategories);
            RecyclerViewUtils.Create().setUpVertical(this, mRecyclerCategory).setAdapter(mAdapterShopCategory);
        } else {
            getPresenter(this).getAllCategories();
        }
    }


    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(this);
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
    }

    @Override
    public void getAllCategorySuccess(List<Category> categories) {
        if (categories != null && !categories.isEmpty()) {
            mDBHelper.setCategoryList(categories);
            initViews();
        }
    }

    @Override
    public void getAllCategoryFail(String errorMessage) {
        Toasty.error(this, errorMessage, Toast.LENGTH_SHORT).show();
    }
}
