package bkdn.itt.datn.saleoff.view.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.local.DBHelper;
import bkdn.itt.datn.saleoff.model.model.SubCategory;
import bkdn.itt.datn.saleoff.presenter.PresenterSubCategory;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.adapter.AdapterSubCategory;
import bkdn.itt.datn.saleoff.view.iface.IAcvititySubCategory;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class ActivityCategorySub extends ActivityBase<PresenterSubCategory> implements IAcvititySubCategory {

    /*
       * Widgets
       * */
    @BindView(R.id.recyclerShopCategory)
    RecyclerView mRecyclerCategory;
    @BindView(R.id.imbCategoryBack)
    ImageButton mImbBack;
    /*
    * Fields
    * */
    private DBHelper mDBHelper;
    private AdapterSubCategory mAdapterSubCategory;
    private List<SubCategory> mSubCategories;
    private int mCateId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        ButterKnife.bind(this);
        mCateId = getIntent().getIntExtra(GlobalDefine.KEY_INTENT_SELECT_SUB_CATEGORY, 0);
        mDBHelper = new DBHelper(this);
        initViews();
        mImbBack.setOnClickListener(v -> onBackPressed());
    }

    private void initViews() {
        if (mDBHelper.isSubCategoryExist()) {
            mSubCategories = mDBHelper.getListSubCatByCat(mCateId);
            mAdapterSubCategory = new AdapterSubCategory(getContext(), mSubCategories);
            RecyclerViewUtils.Create().setUpVertical(this, mRecyclerCategory)
                    .setAdapter(mAdapterSubCategory);
        } else {
            getPresenter(this).getAllSubCategory();
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(this);
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
    }

    @Override
    public void getSubCategorySuccess(List<SubCategory> subCats) {
        if (subCats != null && !subCats.isEmpty()) {
            mDBHelper.setSubCategoryList(subCats);
            initViews();
        }
    }

    @Override
    public void getSubCategoryFail(String errorMessage) {
        Toasty.error(this, errorMessage, Toast.LENGTH_SHORT).show();
    }
}
