package bkdn.itt.datn.saleoff.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.local.DBHelper;
import bkdn.itt.datn.saleoff.model.model.Category;
import bkdn.itt.datn.saleoff.model.model.SubCategory;
import bkdn.itt.datn.saleoff.model.model.User;
import bkdn.itt.datn.saleoff.presenter.PresenterInterested;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.SharedPreferencesUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.adapter.AdapterInterested;
import bkdn.itt.datn.saleoff.view.iface.IActivityInterested;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class ActivityChangeInterested extends ActivityBase<PresenterInterested> implements IActivityInterested {
    @BindView(R.id.imbChangeInterestedBack)
    ImageButton mImbBack;
    @BindView(R.id.imbChangeInterestedDone)
    ImageButton mImbDone;
    @BindView(R.id.imbChangeInterestedNotDone)
    ImageButton mImbNotDone;
    @BindView(R.id.recyclerChangeInterested)
    RecyclerView mRecyclerView;

    private List<Category> mCategories;
    private List<Integer> mChosenSubjectList;
    private AdapterInterested mAdapterInterested;
    private DBHelper mDBHelper;
    private User me;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_interested);
        ButterKnife.bind(this);
        initValues();
        initViews();
        initActions();

    }

    private void initValues() {
        me = SharedPreferencesUtils.getInstance(getContext()).getUserProfile();
        mChosenSubjectList = me.getInterestedSubjects();
        mDBHelper = new DBHelper(getContext());
        if (mDBHelper.isCategoryExist()) {
            mCategories = mDBHelper.getNotAllCategory();
        } else {
            getPresenter(this).getAllCategory();
        }
    }

    private void initViews() {
        if (mCategories != null) {
            if (mChosenSubjectList != null && !mChosenSubjectList.isEmpty()) {
                for (int cateId : mChosenSubjectList) {
                    mCategories.get(cateId - 2).setSelected(true);
                }
            }
            mAdapterInterested = new AdapterInterested(getContext(), mCategories);
            mAdapterInterested.setChoiceSubjectListener(new AdapterInterested.OnChoiceSubjectListener() {
                @Override
                public void choiceSubject(int catId) {
                    if(mChosenSubjectList == null){
                        mChosenSubjectList = new ArrayList<>();
                    }
                    mChosenSubjectList.add(catId);
                    checkSubjectNumber();
                }

                @Override
                public void unChoiceSubject(int catId) {
                    if(mChosenSubjectList == null){
                        mChosenSubjectList = new ArrayList<>();
                    }
                    for (Integer item : mChosenSubjectList) {
                        if (item.equals(catId)) {
                            mChosenSubjectList.remove(item);
                            break;
                        }
                    }
                    checkSubjectNumber();
                }
            });
            RecyclerViewUtils.Create().setUpVertical(getContext(), mRecyclerView)
                    .setAdapter(mAdapterInterested);

        }
    }

    private void initActions() {
        mImbDone.setOnClickListener(v -> {
            User user = SharedPreferencesUtils.getInstance(getContext()).getUserProfile();
            user.setInterestedSubjects(mChosenSubjectList);
            SharedPreferencesUtils.getInstance(getContext()).setUserProfile(user);
            finish();
        });
        mImbBack.setOnClickListener(v -> onBackPressed());
    }

    private void checkSubjectNumber() {
        if (!mChosenSubjectList.isEmpty()) {
            mImbDone.setVisibility(View.VISIBLE);
            mImbNotDone.setVisibility(View.GONE);
        } else {
            mImbDone.setVisibility(View.GONE);
            mImbNotDone.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
    }

    @Override
    public void getCategories(List<Category> categories) {
        if (categories != null) {
            mDBHelper.setCategoryList(categories);
            mCategories = mDBHelper.getNotAllCategory();
            initViews();
        }
    }

    @Override
    public void getError(String messageError) {
        Toasty.error(getContext(), messageError, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getSubCategories(List<SubCategory> listSubCategory) {
        if (listSubCategory != null && !listSubCategory.isEmpty()) {
            mDBHelper.setSubCategoryList(listSubCategory);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                setResult(RESULT_OK);
                finish();
            }
        }
    }
}
