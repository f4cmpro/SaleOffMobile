package bkdn.itt.datn.saleoff.view.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.model.Comment;
import bkdn.itt.datn.saleoff.model.model.ShopInfo;
import bkdn.itt.datn.saleoff.model.model.UserInfo;
import bkdn.itt.datn.saleoff.presenter.PresenterComment;
import bkdn.itt.datn.saleoff.utils.CustomPaginationScrollListener;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.adapter.AdapterComment;
import bkdn.itt.datn.saleoff.view.iface.IActivityComment;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

/**
 * Created by TuDLT on 4/20/2018.
 */
public class ActivityComment extends ActivityBase<PresenterComment>
        implements IActivityComment, AdapterComment.OnEditCommentListener {
    @BindView(R.id.imbCommentBack)
    ImageButton mImbBack;
    @BindView(R.id.recyclerComment)
    RecyclerView mRecyclerComment;
    @BindView(R.id.imvCommentAvatar)
    ImageView mImvCommentAvatar;
    @BindView(R.id.edtComment)
    EditText mEdtCommentAdd;
    @BindView(R.id.tvCommentPost)
    TextView mTvCommentPost;
    @BindView(R.id.tvCommentEdit)
    TextView mTvCommentEdit;
    @BindView(R.id.tvCommentCancel)
    TextView mTvCommentCancel;
    @BindView(R.id.llCommentAdd)
    LinearLayout mLlCommentAdd;
    @BindView(R.id.llCommentEdit)
    LinearLayout mLlCommentEdit;
    @BindView(R.id.edtCommentEdit)
    EditText mEdtCommentEdit;
    @BindView(R.id.swipeRefreshComment)
    SwipeRefreshLayout mRefreshLayout;


    private AdapterComment mAdapterComment;
    private int mLastId;
    private int mPostId;
    private boolean mIsLoadMore;
    private boolean mIsRefresh;
    private Comment mAddedComment;
    private String mCommentText;
    private ShopInfo mShopInfo;
    private UserInfo mUserInfo;
    private String mEditedComment;
    private int mEdtCommentPosition;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        ButterKnife.bind(this);
        initValues();
        initViews();
        initActions();
    }

    private void initValues() {
        mEdtCommentAdd.setRawInputType(InputType.TYPE_CLASS_TEXT);
        mEdtCommentEdit.setRawInputType(InputType.TYPE_CLASS_TEXT);

        if (getIntent().getExtras().getString(GlobalDefine.KEY_BUNDLE_NOTIFICATION) != null) {
            String data = getIntent().getExtras().getString(GlobalDefine.KEY_BUNDLE_NOTIFICATION);
            Comment comment = new Gson().fromJson(data, Comment.class);
            mPostId = comment.getPostId();
        } else {
            mPostId = getIntent().getIntExtra(GlobalDefine.KEY_INTENT_POST_ID, 0);
        }
        Utils.showLoadingDialog(getContext());
        mIsRefresh = true;
        getPresenter(this).getAllComment(mPostId, mLastId);
        mShopInfo = getIntent().getParcelableExtra(GlobalDefine.KEY_INTENT_SHOP_INFO);
        mUserInfo = getIntent().getParcelableExtra(GlobalDefine.KEY_INTENT_USER_INFO);
    }

    private void initViews() {
        mAdapterComment = new AdapterComment(getContext(), this);
        RecyclerViewUtils.Create().setUpVertical(getContext(), mRecyclerComment)
                .setAdapter(mAdapterComment);
    }

    private void initActions() {
        mRecyclerComment.addOnScrollListener(new CustomPaginationScrollListener((LinearLayoutManager)
                mRecyclerComment.getLayoutManager()) {
            @Override
            protected void loadMoreItems() {
                mIsLoadMore = true;
                getPresenter(ActivityComment.this).getAllComment(mPostId, mLastId);
            }

            @Override
            public boolean isLoading() {
                return mIsLoadMore;
            }

            @Override
            protected void enableRefreshLayout(boolean isEnable) {

            }

        });
        mEdtCommentAdd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()) {
                    setEnablePost(mTvCommentPost, false);
                } else {
                    setEnablePost(mTvCommentPost, true);
                    mCommentText = s.toString();
                }
            }
        });
        mEdtCommentEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()) {
                    setEnablePost(mTvCommentEdit, false);
                } else {
                    setEnablePost(mTvCommentEdit, true);
                    mEditedComment = s.toString();
                }
            }
        });
        mTvCommentPost.setOnClickListener(v -> getPresenter(ActivityComment.this)
                .addComment(mPostId, mCommentText));
        mTvCommentEdit.setOnClickListener(v -> getPresenter(ActivityComment.this)
                .editComment(mPostId, mEditedComment));
        mTvCommentCancel.setOnClickListener(v -> {
            Utils.hideKeyboard(getCurrentFocus());
            mLlCommentAdd.setVisibility(View.VISIBLE);
            mLlCommentEdit.setVisibility(View.GONE);
        });

        mImbBack.setOnClickListener(v -> onBackPressed());

        mRefreshLayout.setOnRefreshListener(() -> {
            mIsRefresh = true;
            getPresenter(ActivityComment.this).getAllComment(mPostId, mLastId);
        });


    }

    private void setEnablePost(TextView editText, boolean haveText) {
        editText.setEnabled(haveText);
        if (haveText) {
            editText.setTextColor(getResources().getColor(R.color.colorAccent));
        } else {
            editText.setTextColor(getResources().getColor(R.color.colorAccent50));
        }
    }


    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
        Utils.hideKeyboard(this.getCurrentFocus());
        mRefreshLayout.setRefreshing(false);
    }

    @Override
    public void addCommentSuccess(Comment comment) {
        mAddedComment = comment;
        mAdapterComment.addMyComment(mAddedComment);
        mEdtCommentAdd.setText("");
    }

    @Override
    public void editCommentSuccess() {
        mAdapterComment.editCommentItem(mEdtCommentPosition, mEditedComment);
        mRecyclerComment.smoothScrollToPosition(mEdtCommentPosition);
    }

    @Override
    public void getError(String errorMessage) {
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getCommentsSuccess(List<Comment> comments, int lastId) {
        mLastId = lastId;
        if(mIsRefresh){
            mAdapterComment.refreshComments(comments);
        }else{
            mIsLoadMore = false;
            mAdapterComment.loadComments(comments);
        }
    }

    @Override
    public void onEditComment(int position, String comment) {
        mEditedComment = comment;
        mEdtCommentPosition = position;
        mLlCommentAdd.setVisibility(View.INVISIBLE);
        mLlCommentEdit.setVisibility(View.VISIBLE);
        mEdtCommentEdit.setEnabled(true);
        mEdtCommentEdit.setText(comment);
        Utils.showKeyboard(this.getCurrentFocus());
    }
}
