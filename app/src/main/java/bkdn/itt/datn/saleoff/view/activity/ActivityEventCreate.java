package bkdn.itt.datn.saleoff.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBufferResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.Calendar;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.model.Event;
import bkdn.itt.datn.saleoff.model.model.Shop;
import bkdn.itt.datn.saleoff.presenter.PresenterEventCreate;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.TimeUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.adapter.AdapterSelectedImage;
import bkdn.itt.datn.saleoff.view.adapter.PlaceAutocompleteAdapter;
import bkdn.itt.datn.saleoff.view.dialog.DialogDatePicker;
import bkdn.itt.datn.saleoff.view.dialog.DialogTimePicker;
import bkdn.itt.datn.saleoff.view.iface.IActivityEventCreate;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class ActivityEventCreate extends ActivityBase<PresenterEventCreate>
        implements IActivityEventCreate, DialogDatePicker.OnPickDateListener,
        AdapterSelectedImage.OnSelectedImageListener, DialogTimePicker.OnPickTimeListener {

    private static final String TAG = ActivityEventCreate.class.getSimpleName();
    /*
        * Widgets
        * */
    @BindView(R.id.recyclerCreateEventImages)
    RecyclerView mRecyclerEventImages;
    @BindView(R.id.edtCreateEventTitle)
    EditText mEdtEventTitle;
    @BindView(R.id.tvCreateEventFrom)
    TextView mTvEventStartAt;
    @BindView(R.id.tvCreateEventTo)
    TextView mTvEventEndAt;
    @BindView(R.id.edtCreateEventAddress)
    AutoCompleteTextView mEdtEventAddress;
    @BindView(R.id.tvCreateEventAddress)
    TextView mTvEventAddress;
    @BindView(R.id.edtCreateEventDes)
    EditText mEdtEventDes;
    @BindView(R.id.imbCreateEventDone)
    ImageButton mImbDone;
    @BindView(R.id.imbCreateEventBack)
    ImageButton mImbBack;
    /*
    * Fields
    * */
    private PlaceAutocompleteAdapter mPlaceAutocompleteAdapter;
    private AdapterSelectedImage mAdapterSelectedImage;
    private AutocompleteFilter mAutocompleteFilter;
    private GeoDataClient mGeoDataClient;
    private ArrayList<String> mImgUrls;
    private Event mCreatedEvent;
    private Shop mShop;
    private boolean isFrom;
    private Calendar mStartDate = Calendar.getInstance();
    private Calendar mEndDate = Calendar.getInstance();


    private class GenericTextWatcher implements TextWatcher {
        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String text = s.toString();
            switch (view.getId()) {
                case R.id.edtCreateEventTitle:
                    if (!text.isEmpty()) {
                        mCreatedEvent.setTitle(text);
                    } else {
                        Toasty.warning(getContext(), getString(R.string.warning_empty),
                                Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.edtCreateEventDes:
                    if (!text.isEmpty()) {
                        mCreatedEvent.setDetail(text);
                    } else {
                        Toasty.warning(getContext(), getString(R.string.warning_empty),
                                Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_create);
        ButterKnife.bind(this);
        initValues();
        initViews();
        initActions();
    }

    private void initViews() {
        mAutocompleteFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS)
                .setCountry("VN")
                .build();
        mGeoDataClient = Places.getGeoDataClient(this, null);
        mPlaceAutocompleteAdapter = new PlaceAutocompleteAdapter(this, mGeoDataClient,
                null, mAutocompleteFilter);
        mEdtEventAddress.setAdapter(mPlaceAutocompleteAdapter);
        mEdtEventAddress.setRawInputType(InputType.TYPE_CLASS_TEXT);
        mEdtEventTitle.setRawInputType(InputType.TYPE_CLASS_TEXT);
        mEdtEventAddress.setRawInputType(InputType.TYPE_CLASS_TEXT);
        if (mShop != null) {
            mEdtEventAddress.setText(mShop.getAddress());
            mCreatedEvent.setAddress(mShop.getAddress());
        }
        mAdapterSelectedImage = new AdapterSelectedImage(this, false);
        RecyclerViewUtils.Create().setUpReverseHorizontal(this, mRecyclerEventImages)
                .setAdapter(mAdapterSelectedImage);
    }

    private void initValues() {
        mCreatedEvent = new Event();
        mShop = getIntent().getParcelableExtra(GlobalDefine.KEY_INTENT_SHOP);
        if (mShop != null) {
            mCreatedEvent.setShopId(mShop.getShopId());
        }
    }

    private void initActions() {
        mEdtEventAddress.setOnItemClickListener((parent, view, position, id) -> {
            Utils.hideSoftKeyboard(ActivityEventCreate.this);
            final AutocompletePrediction item = mPlaceAutocompleteAdapter.getItem(position);
            Task<PlaceBufferResponse> placeResult = mGeoDataClient.getPlaceById(item.getPlaceId());
            placeResult.addOnCompleteListener(task -> {
                PlaceBufferResponse places = task.getResult();
                Place place = places.get(0);
                Log.i(TAG, place.getAddress().toString());
                mCreatedEvent.setAddress(place.getAddress().toString());
            });
        });
        mEdtEventAddress.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH
                    || actionId == EditorInfo.IME_ACTION_DONE
                    || actionId == EditorInfo.IME_ACTION_NEXT) {
                // execute searching method
                Utils.hideSoftKeyboard(this);
                mCreatedEvent.setAddress(mEdtEventAddress.getText().toString());
            }
            return false;
        });
        mEdtEventTitle.addTextChangedListener(new ActivityEventCreate.GenericTextWatcher(mEdtEventTitle));
        mEdtEventDes.addTextChangedListener(new ActivityEventCreate.GenericTextWatcher(mEdtEventDes));
        mTvEventStartAt.setOnClickListener(v -> {
            isFrom = true;
            DialogDatePicker.newInstance(ActivityEventCreate.this, getSupportFragmentManager(),
                    Calendar.getInstance().getTimeInMillis(), "Ngày bắt đầu");
        });
        mTvEventEndAt.setOnClickListener(v -> {
            if (isFrom) {
                isFrom = false;
                DialogDatePicker.newInstance(ActivityEventCreate.this, getSupportFragmentManager(),
                        mStartDate.getTimeInMillis(), "Ngày kết thúc");
            } else {
                Toasty.warning(getContext(), "Chưa chọn ngày bắt đầu!", Toast.LENGTH_SHORT).show();
            }
        });
        mImbDone.setOnClickListener((View v) -> {
            if (mImgUrls != null && !mImgUrls.isEmpty()) {
                mCreatedEvent.setImages(mImgUrls.toArray(new String[mImgUrls.size()]));
                mCreatedEvent.setCover(mImgUrls.get(0));
            }
            getPresenter(ActivityEventCreate.this).createEvent(mCreatedEvent);
        });
        mImbBack.setOnClickListener(v -> onBackPressed());
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ConstantDefine.PICK_IMAGES_REQUEST) {
                ArrayList<String> imgUrls = data.getStringArrayListExtra(GlobalDefine.KEY_INTENT_PICK_IMAGES);
                mAdapterSelectedImage.addMoreImages(imgUrls);
                mRecyclerEventImages.smoothScrollToPosition(mAdapterSelectedImage.getItemCount() - 1);
            }
        }
    }

    @Override
    public void onPickDate(int day, int month, int year) {
        if (isFrom) {
            mStartDate.set(Calendar.YEAR, year);
            mStartDate.set(Calendar.MONTH, month);
            mStartDate.set(Calendar.DAY_OF_MONTH, day);
            DialogTimePicker.newInstance(this, getSupportFragmentManager());
        } else {
            mEndDate.set(Calendar.YEAR, year);
            mEndDate.set(Calendar.MONTH, month);
            mEndDate.set(Calendar.DAY_OF_MONTH, day);
            DialogTimePicker.newInstance(this, getSupportFragmentManager());

        }

    }

    @Override
    public void onPickTime(int hour, int minutes) {
        if (isFrom) {
            mStartDate.set(Calendar.HOUR_OF_DAY, hour);
            mStartDate.set(Calendar.MINUTE, minutes);
            mTvEventStartAt.setText(TimeUtils.parseTimestampToString(mStartDate.getTimeInMillis(), GlobalDefine.FORMAT_DATE_FOUR_DEFAULT));
            mCreatedEvent.setStartDate(TimeUtils.parseTimestampToString(mStartDate.getTimeInMillis(), GlobalDefine.FORMAT_DATE_DEFAULT));

        } else {
            mEndDate.set(Calendar.HOUR_OF_DAY, hour);
            mEndDate.set(Calendar.MINUTE, minutes);
            mTvEventEndAt.setText(TimeUtils.parseTimestampToString(mEndDate.getTimeInMillis(), GlobalDefine.FORMAT_DATE_FOUR_DEFAULT));
            mCreatedEvent.setEndDate(TimeUtils.parseTimestampToString(mEndDate.getTimeInMillis(), GlobalDefine.FORMAT_DATE_DEFAULT));
        }
    }


    @Override
    public void onSelectedImage(String imgUrl) {
        if (mImgUrls == null) {
            mImgUrls = new ArrayList<>();
        }
        mImgUrls.add(imgUrl);
    }

    @Override
    public void createEventSuccess() {
        Toasty.success(getContext(), "Tạo sự kiện thành công!", Toast.LENGTH_SHORT).show();
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void createEventError(String errorMessage) {
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
    }
}
