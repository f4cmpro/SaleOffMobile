package bkdn.itt.datn.saleoff.view.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.GoogleMap;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.model.Event;
import bkdn.itt.datn.saleoff.model.model.Participant;
import bkdn.itt.datn.saleoff.model.model.User;
import bkdn.itt.datn.saleoff.presenter.PresenterEventDetail;
import bkdn.itt.datn.saleoff.utils.MapUtils;
import bkdn.itt.datn.saleoff.utils.TimeUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.adapter.AdapterSlider;
import bkdn.itt.datn.saleoff.view.dialog.DialogPresenter;
import bkdn.itt.datn.saleoff.view.dialog.DialogShareEvent;
import bkdn.itt.datn.saleoff.view.fragment.MySupportMapFragment;
import bkdn.itt.datn.saleoff.view.iface.IActivityEventDetail;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class ActivityEventDetail extends ActivityBase<PresenterEventDetail>
        implements IActivityEventDetail {
    @BindView(R.id.swipeRefreshEventDetail)
    SwipeRefreshLayout mRefreshDetail;
    @BindView(R.id.imbEventDetailBack)
    ImageButton mImbBack;
    @BindView(R.id.nestedScrollViewEventDetail)
    NestedScrollView mNestedScrollDetail;
    @BindView(R.id.viewPagerEventDetailSlider)
    ViewPager mViewPagerImages;
    @BindView(R.id.tabLayoutIndicatorEventDetail)
    TabLayout mTabLayoutIndicator;
    @BindView(R.id.tvEventDetailTitle)
    TextView mTvTitle;
    @BindView(R.id.tvEventDetailStartDay)
    TextView mTvStartDate;
    @BindView(R.id.tvEventDetailEndDay)
    TextView mTvEndDate;
    @BindView(R.id.tvEventDetailAddress)
    TextView mTvAddress;
    @BindView(R.id.imvEventDetailAvatar)
    ImageView mImvShopAvatar;
    @BindView(R.id.tvEventDetailShopName)
    TextView mTvShopName;
    @BindView(R.id.tvEventDetailCreatedAt)
    TextView mTvCreatedAt;
    @BindView(R.id.imbEventDetailWatchShop)
    ImageButton mImbWatchShop;
    @BindView(R.id.tvEventDetailDescription)
    TextView mTvDescription;
    @BindView(R.id.btnEventDetailJoin)
    Button mBtnJoin;
    @BindView(R.id.llEventDetailShareCode)
    LinearLayout mLlShareCode;
    @BindView(R.id.tvEventDetailShareCode)
    TextView mTvShareCode;

    private MySupportMapFragment mMapFragment;
    private AdapterSlider mAdapterSlider;
    private Event mEvent;
    private ArrayList<Participant> mParticipants;
    private int mPresenterCode;
    private String mShareDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);
        ButterKnife.bind(this);
        initValues();
        intActions();
    }

    private void initValues() {
        if (getIntent().getExtras().getString(GlobalDefine.KEY_BUNDLE_NOTIFICATION) != null) {
            String data = getIntent().getExtras().getString(GlobalDefine.KEY_BUNDLE_NOTIFICATION);
            mEvent = new Gson().fromJson(data, Event.class);
        } else if (getIntent().getParcelableExtra(GlobalDefine.KEY_INTENT_EVENT) != null) {
            mEvent = getIntent().getParcelableExtra(GlobalDefine.KEY_INTENT_EVENT);
        } else {
            mEvent = new Event();
            mEvent.setEventId(getIntent().getIntExtra(GlobalDefine.KEY_INTENT_EVENT_ID, 0));
        }
        getPresenter(this).getEventDetail(true, mEvent.getEventId());
    }

    private void initViews() {
        if (mEvent != null) {
            //set images
            if (mEvent.getImages() != null && mEvent.getImages().length > 0) {
                mAdapterSlider = new AdapterSlider(getSupportFragmentManager(), mEvent.getImages());
                mViewPagerImages.setAdapter(mAdapterSlider);
                mTabLayoutIndicator.setupWithViewPager(mViewPagerImages);
            }
            //set title
            mTvTitle.setText(mEvent.getTitle());
            //set start date
            if (mEvent.getStartDate() != null && !mEvent.getStartDate().isEmpty()) {
                mTvStartDate.setText(mEvent.getStartDate());
            }
            //set end date
            if (mEvent.getEndDate() != null && !mEvent.getEndDate().isEmpty()) {
                mTvEndDate.setText(mEvent.getEndDate());
            }
            //set address
            mTvAddress.setText(mEvent.getAddress());

            //set description
            mTvDescription.setText(mEvent.getDetail());
            //set map
            mMapFragment = (MySupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.mapEventDetailAddress);
            mMapFragment.setListener(() -> mNestedScrollDetail.requestDisallowInterceptTouchEvent(true));
            mMapFragment.getMapAsync(googleMap -> {
                if (googleMap == null) {
                    return;
                }
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                String shopName = mEvent.getShopInfo() != null ? mEvent.getShopInfo().getName() : "Cửa hàng";
                MapUtils.getInstance(googleMap).markPlaceOnMap(getContext(), googleMap, shopName, mEvent.getAddress());
            });
            //set shop info
            if (mEvent.getShopInfo() != null) {
                //set avatar
                Glide.with(getContext()).load(mEvent.getShopInfo().getAvatar()).into(mImvShopAvatar);
                //set name
                mTvShopName.setText(mEvent.getShopInfo().getName());
                //set created event at
                mTvCreatedAt.setText(TimeUtils.formatDate(GlobalDefine.FORMAT_DATE_FOUR_DEFAULT,
                        mEvent.getCreatedAt()));
            }
            //set view button join
            if (mEvent.getIsJoin() == 1) {
                mLlShareCode.setVisibility(View.VISIBLE);
                mBtnJoin.setVisibility(View.GONE);
            } else {
                mBtnJoin.setVisibility(View.VISIBLE);
                mLlShareCode.setVisibility(View.GONE);
            }

        }
    }

    private void intActions() {
        mRefreshDetail.setOnRefreshListener(() -> getPresenter(ActivityEventDetail.this)
                .getEventDetail(true, mEvent.getEventId()));
        mBtnJoin.setOnClickListener(v -> {
            DialogPresenter dialogPresenter = DialogPresenter.newInstance(mParticipants);
            dialogPresenter.setListener(shareCode -> {
                mPresenterCode = shareCode;
                showShareEventDialog();
            });
            dialogPresenter.show(getSupportFragmentManager(), null);

        });
    }
    private void showShareEventDialog() {
        DialogShareEvent dialogShareEvent = DialogShareEvent.newInstance(mEvent);
        dialogShareEvent.setListener(description -> {
            mShareDescription = description;
            getPresenter(ActivityEventDetail.this).joinEvent(mEvent.getEventId(),
                    mPresenterCode, mShareDescription);
        });
        dialogShareEvent.show(getSupportFragmentManager(), null);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
        mRefreshDetail.setRefreshing(false);
    }

    @Override
    public void getEventDetailSuccess(Event event) {
        if (event != null) {
            mEvent = event;
            initViews();
            getPresenter(this).getParticipants(mEvent.getEventId());
        }
    }

    @Override
    public void getEventDetailError(String errorMessage) {
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getParticipantsSuccess(List<Participant> participants) {
        if (participants != null) {
            mParticipants = new ArrayList<>();
            mParticipants.addAll(participants);
        }
    }

    @Override
    public void getParticipantsError(String errorMessage) {
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void joinEventSuccess(int shareCode) {
        Toasty.success(getContext(), getString(R.string.join_event_success), Toast.LENGTH_SHORT).show();
        mTvShareCode.setText(String.valueOf(shareCode));
        mLlShareCode.setVisibility(View.VISIBLE);
        mBtnJoin.setVisibility(View.GONE);
        mEvent.setIsJoin(1);
    }

    @Override
    public void joinEventError(String errorMessage) {
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }
}
