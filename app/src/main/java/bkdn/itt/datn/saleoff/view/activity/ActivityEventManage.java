package bkdn.itt.datn.saleoff.view.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.model.Event;
import bkdn.itt.datn.saleoff.model.model.Participant;
import bkdn.itt.datn.saleoff.presenter.PresenterEventManage;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.adapter.AdapterParticipant;
import bkdn.itt.datn.saleoff.view.adapter.AdapterSlider;
import bkdn.itt.datn.saleoff.view.iface.IActivityEventManage;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class ActivityEventManage extends ActivityBase<PresenterEventManage>
        implements IActivityEventManage, AdapterParticipant.OnUseEventPointListener {
    @BindView(R.id.swipeRefreshEventDetail)
    SwipeRefreshLayout mRefreshDetail;
    @BindView(R.id.imbEventDetailBack)
    ImageButton mImbBack;
    @BindView(R.id.viewPagerEventDetailSlider)
    ViewPager mViewPagerImages;
    @BindView(R.id.tabLayoutIndicatorEventDetail)
    TabLayout mTabLayoutIndicator;
    @BindView(R.id.tvEventDetailTitle)
    TextView mTvTitle;
    @BindView(R.id.tvEventDetailStartDay)
    TextView mTvStartDate;
    @BindView(R.id.tvEventDetailEndDay)
    TextView mTvEndDate;
    @BindView(R.id.tvEventDetailAddress)
    TextView mTvAddress;
    @BindView(R.id.tvEventDetailDescription)
    TextView mTvDescription;
    @BindView(R.id.recyclerEventManageParticipants)
    RecyclerView mRecyclerParticipants;
    @BindView(R.id.tvLayoutNotFoundItem)
    TextView mTvNotFoundParticipant;
    @BindView(R.id.layoutNotFoundParicipants)
    View mLayoutNotFoundParticipants;

    private AdapterSlider mAdapterSlider;
    private ArrayList<Participant> mParticipants;
    private AdapterParticipant mAdapterParticipant;
    private Event mEvent;
    private OnUseEvenPointCallBack mUseEvenPointCallBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_manage);
        ButterKnife.bind(this);
        initValues();
        intActions();
    }

    private void initValues() {
        if (getIntent().getExtras().getString(GlobalDefine.KEY_BUNDLE_NOTIFICATION) != null) {
            String data = getIntent().getExtras().getString(GlobalDefine.KEY_BUNDLE_NOTIFICATION);
            mEvent = new Gson().fromJson(data, Event.class);
        } else if (getIntent().getParcelableExtra(GlobalDefine.KEY_INTENT_EVENT) != null) {
            mEvent = getIntent().getParcelableExtra(GlobalDefine.KEY_INTENT_EVENT);
        } else {
            mEvent = new Event();
            mEvent.setEventId(getIntent().getIntExtra(GlobalDefine.KEY_INTENT_EVENT_ID, 0));
        }
        getPresenter(this).getEventDetail(true, mEvent.getEventId());
    }

    private void initViews() {
        if (mEvent != null) {
            //set images
            if (mEvent.getImages() != null && mEvent.getImages().length > 0) {
                mAdapterSlider = new AdapterSlider(getSupportFragmentManager(), mEvent.getImages());
                mViewPagerImages.setAdapter(mAdapterSlider);
                mTabLayoutIndicator.setupWithViewPager(mViewPagerImages);
            }
            //set title
            mTvTitle.setText(mEvent.getTitle());
            //set start date
            if (mEvent.getStartDate() != null && !mEvent.getStartDate().isEmpty()) {
                mTvStartDate.setText(mEvent.getStartDate());
            }
            //set end date
            if (mEvent.getEndDate() != null && !mEvent.getEndDate().isEmpty()) {
                mTvEndDate.setText(mEvent.getEndDate());
            }
            //set address
            mTvAddress.setText(mEvent.getAddress());

            //set description
            mTvDescription.setText(mEvent.getDetail());
        }
    }

    private void setListParticipantsView() {
        if (mParticipants != null && !mParticipants.isEmpty()) {
            mAdapterParticipant = new AdapterParticipant(getContext(), mParticipants);
            mAdapterParticipant.setListener(this);
            RecyclerViewUtils.Create().setUpVertical(getContext(), mRecyclerParticipants)
                    .setAdapter(mAdapterParticipant);
            mRecyclerParticipants.setVisibility(View.VISIBLE);
            mLayoutNotFoundParticipants.setVisibility(View.GONE);
        } else {
            mRecyclerParticipants.setVisibility(View.GONE);
            mLayoutNotFoundParticipants.setVisibility(View.VISIBLE);
            mTvNotFoundParticipant.setText(R.string.not_event_participants);
        }
    }

    private void intActions() {
        mRefreshDetail.setOnRefreshListener(() -> getPresenter(ActivityEventManage.this)
                .getEventDetail(true, mEvent.getEventId()));
    }
    @Override
    public void useEventPoint(int participantId, OnUseEvenPointCallBack callBack) {
        mUseEvenPointCallBack = callBack;
        getPresenter(this).useEventPoint(mEvent.getEventId(), participantId);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
        mRefreshDetail.setRefreshing(false);
    }

    @Override
    public void getEventDetailSuccess(Event event) {
        if (event != null) {
            mEvent = event;
            initViews();
            getPresenter(this).getParticipants(mEvent.getEventId());
        }
    }

    @Override
    public void getEventDetailError(String errorMessage) {
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getParticipantsSuccess(List<Participant> participants) {
        if (participants != null) {
            mParticipants = new ArrayList<>();
            mParticipants.addAll(participants);
            setListParticipantsView();
        }
    }

    @Override
    public void getParticipantsError(String errorMessage) {
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void useEventPointSuccess() {
        mUseEvenPointCallBack.onSuccess();
    }

    @Override
    public void useEventPointError(String errorMessage) {
        mUseEvenPointCallBack.onError();
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    public interface OnUseEvenPointCallBack {
        void onSuccess();
        void onError();
    }
}
