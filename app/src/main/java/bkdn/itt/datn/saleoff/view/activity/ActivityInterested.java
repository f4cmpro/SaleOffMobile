package bkdn.itt.datn.saleoff.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.local.DBHelper;
import bkdn.itt.datn.saleoff.model.model.Category;
import bkdn.itt.datn.saleoff.model.model.SubCategory;
import bkdn.itt.datn.saleoff.model.model.User;
import bkdn.itt.datn.saleoff.presenter.PresenterInterested;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.SharedPreferencesUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.adapter.AdapterInterested;
import bkdn.itt.datn.saleoff.view.iface.IActivityInterested;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class ActivityInterested extends ActivityBase<PresenterInterested> implements IActivityInterested {
    @BindView(R.id.btnInterestedContinue)
    Button mBtnContinue;
    @BindView(R.id.btnInterestedUnContinue)
    Button mBtnUnContinue;
    @BindView(R.id.recyclerInterested)
    RecyclerView mRecyclerView;

    private List<Category> mCategories;
    private List<Integer> mChosenSubject;
    private AdapterInterested mAdapterInterested;
    private DBHelper mDBHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interested);
        ButterKnife.bind(this);
        initValues();
        initViews();
        initActions();

    }

    private void initValues() {
        mChosenSubject = new ArrayList<>();
        mDBHelper = new DBHelper(getContext());
        if (mDBHelper.isCategoryExist()) {
            mCategories = mDBHelper.getNotAllCategory();
        } else {
            getPresenter(this).getAllCategory();
        }
    }

    private void initViews() {
        if (mCategories != null) {
            mAdapterInterested = new AdapterInterested(getContext(), mCategories);
            mAdapterInterested.setChoiceSubjectListener(new AdapterInterested.OnChoiceSubjectListener() {
                @Override
                public void choiceSubject(int catId) {
                    mChosenSubject.add(catId);
                    checkSubjectNumber();
                }

                @Override
                public void unChoiceSubject(int catId) {
                    for (Integer item : mChosenSubject) {
                        if (item.equals(catId)) {
                            mChosenSubject.remove(item);
                            break;
                        }
                    }
                    checkSubjectNumber();
                }
            });
            RecyclerViewUtils.Create().setUpVertical(getContext(), mRecyclerView)
                    .setAdapter(mAdapterInterested);

        }
    }

    private void initActions() {
        mBtnContinue.setOnClickListener(v -> {
            User user = SharedPreferencesUtils.getInstance(getContext()).getUserProfile();
            user.setInterestedSubjects(mChosenSubject);
            SharedPreferencesUtils.getInstance(getContext()).setUserProfile(user);
            startActivityForResult(new Intent(this, ActivityProfile.class), 1);
        });
    }

    private void checkSubjectNumber() {
        if (!mChosenSubject.isEmpty()) {
            mBtnContinue.setVisibility(View.VISIBLE);
            mBtnUnContinue.setVisibility(View.GONE);
        } else {
            mBtnContinue.setVisibility(View.GONE);
            mBtnUnContinue.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
    }

    @Override
    public void getCategories(List<Category> categories) {
        if (categories != null) {
            mDBHelper.setCategoryList(categories);
            mCategories = mDBHelper.getNotAllCategory();
            initViews();
        }
    }

    @Override
    public void getError(String messageError) {
        Toasty.error(getContext(), messageError, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getSubCategories(List<SubCategory> listSubCategory) {
        if (listSubCategory != null && !listSubCategory.isEmpty()) {
            mDBHelper.setSubCategoryList(listSubCategory);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                setResult(RESULT_OK);
                finish();
            }
        }
    }
}
