package bkdn.itt.datn.saleoff.view.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.utils.SharedPreferencesUtils;
import bkdn.itt.datn.saleoff.view.adapter.AdapterPagerIntro;

/**
 * Created by TuDLT on 3/6/2018.
 */
public class ActivityIntro extends ActivityBase implements View.OnClickListener, ViewPager.OnPageChangeListener {

    private ViewPager mPagerIntro;
    private AdapterPagerIntro mAdapter;
    private Button mBtnLogin;
    private TextView[] mDots;
    private LinearLayout mDotsLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        initView();
        intValue();
    }

    private void initView() {
        mPagerIntro = findViewById(R.id.viewPagerIntro);
        mBtnLogin = findViewById(R.id.btnSkip);
        mDotsLayout = findViewById(R.id.llDots);
        // adding bottom dots
        addBottomDots(0);
        // making notification bar transparent
        changeStatusBarColor();
    }

    private void intValue() {
        mAdapter = new AdapterPagerIntro(this);
        mPagerIntro.setAdapter(mAdapter);
        mBtnLogin.setOnClickListener(this);
        mPagerIntro.addOnPageChangeListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSkip:
                finishIntro();
                break;
        }
    }

    private void addBottomDots(int currentPage) {
        mDots = new TextView[2];
        mDotsLayout.removeAllViews();
        for (int i = 0; i < mDots.length; i++) {
            mDots[i] = new TextView(this);
            mDots[i].setText(Html.fromHtml("&#8226;"));
            mDots[i].setTextSize(35);
            mDots[i].setTextColor(getResources().getColor(R.color.colorAccent));
            mDotsLayout.addView(mDots[i]);
        }

        if (mDots.length > 0)
            mDots[currentPage].setTextColor(getResources().getColor(R.color.colorWhite));
    }

    private void finishIntro() {
        SharedPreferencesUtils.getInstance(this).setBool(getString(R.string.intro_key_save_first_open_app), true);
        Intent intent = new Intent(this, ActivityLogin.class);
        startActivity(intent);
        finish();
    }

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        addBottomDots(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
