package bkdn.itt.datn.saleoff.view.activity;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.User;
import bkdn.itt.datn.saleoff.utils.SharedPreferencesUtils;
import bkdn.itt.datn.saleoff.view.dialog.DialogSelectLocation;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityLocation extends ActivityBase {
    @BindView(R.id.tvLocation)
    TextView mTvLocation;
    @BindView(R.id.tvContinueLocation)
    TextView mTvContinue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        ButterKnife.bind(this);
        changeStatusBarColor();
        mTvContinue.setEnabled(false);
        mTvContinue.setTextColor(getResources().getColor(R.color.colorWhite80));
        mTvLocation.setOnClickListener(v -> {
            DialogSelectLocation dialog = DialogSelectLocation.newInstance();

            dialog.setListener(city -> {
                User user = SharedPreferencesUtils.getInstance(ActivityLocation.this).getUserProfile();
                if (user != null) {
                    mTvLocation.setText(city);
                    user.setAddress(city);
                    SharedPreferencesUtils.getInstance(ActivityLocation.this).setUserProfile(user);
                    mTvContinue.setEnabled(true);
                    mTvContinue.setTextColor(getResources().getColor(R.color.colorWhite));
                }
            });
            dialog.show(ActivityLocation.this.getSupportFragmentManager(), null);
        });
        mTvContinue.setOnClickListener(v -> {
            startActivityForResult(new Intent(this, ActivityInterested.class), 1);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                SharedPreferencesUtils.getInstance().setBool(getString(R.string.key_is_first_login),
                        false);
                startActivity(ActivityMain.class);
                finish();
            }
        }

    }

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }
    }

}
