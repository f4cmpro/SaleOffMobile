package bkdn.itt.datn.saleoff.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.User;
import bkdn.itt.datn.saleoff.model.net.NetworkUtils;
import bkdn.itt.datn.saleoff.presenter.PresenterLogin;
import bkdn.itt.datn.saleoff.utils.SharedPreferencesUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.dialog.DialogErrorMessage;
import bkdn.itt.datn.saleoff.view.iface.IActivityLogin;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

import static bkdn.itt.datn.saleoff.global.ConstantDefine.RC_SIGN_IN;

public class ActivityLogin extends ActivityBase<PresenterLogin> implements IActivityLogin {
    /*
    * Widgets
    * */
    @BindView(R.id.btnLoginWithFacebook)
    Button mBtnLoginWithFacebook;
    @BindView(R.id.btnLoginWithGoogle)
    Button mBtnLoginWithGoogle;
    /*
    * Field
    * */
    private boolean isLoginFb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        initActions();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (isLoginFb) {
            CallbackManager callbackManager = getPresenter(this).getCallbackManager();
            if (callbackManager != null) {
                callbackManager.onActivityResult(requestCode, resultCode, data);
            }
        } else {
            // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
            if (requestCode == RC_SIGN_IN) {
                getPresenter(this).setReturnResultFromGGSignIn(data);
            }
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(this);
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
    }

    @Override
    public ActivityLogin getActivity() {
        return this;
    }

    @Override
    public void loginFacebookSuccess() {
        getPresenter(this).getProfile();
    }

    @Override
    public void loginFacebookFail(String failMessage) {
        Toasty.error(this, failMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loginGoogleSuccess() {
        getPresenter(this).getProfile();
    }

    @Override
    public void loginGoogleFail(String failMessage) {
        Toasty.error(this, failMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getProfileSuccess(User user) {
        SharedPreferencesUtils.getInstance(getContext()).setUserProfile(user);
        startActivity(ActivitySplash.class);
        finish();
    }

    @Override
    public void requestFail(String errorMessage) {
        Toasty.warning(this, "Login Fail!", Toast.LENGTH_SHORT).show();

    }


    private void initActions() {
        mBtnLoginWithFacebook.setOnClickListener(v -> {
            isLoginFb = true;
            if (NetworkUtils.isConnected(ActivityLogin.this)) {
                getPresenter(this).sigUpWithFacebook();
            } else {
                DialogErrorMessage.newInstance(getString(R.string.error_not_connect))
                        .show(getSupportFragmentManager(), getString(R.string.dialog));
            }
        });
        mBtnLoginWithGoogle.setOnClickListener(v -> {
            isLoginFb = false;
            if (NetworkUtils.isConnected(ActivityLogin.this)) {
                getPresenter(this).sigUpWithGoogle();
            } else {
                DialogErrorMessage.newInstance(getString(R.string.error_not_connect))
                        .show(getSupportFragmentManager(), getString(R.string.dialog));
            }
        });
    }
}
