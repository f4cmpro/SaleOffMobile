package bkdn.itt.datn.saleoff.view.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GoogleApiAvailability;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.fcm.MyFireBaseMessagingService;
import bkdn.itt.datn.saleoff.fcm.NotificationBody;
import bkdn.itt.datn.saleoff.fcm.NotificationType;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.global.SaleOffApplication;
import bkdn.itt.datn.saleoff.mCloud.UploadImageCloud;
import bkdn.itt.datn.saleoff.presenter.PresenterMain;
import bkdn.itt.datn.saleoff.utils.ImagesUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.fragment.FragmentBase;
import bkdn.itt.datn.saleoff.view.fragment.FragmentEvent;
import bkdn.itt.datn.saleoff.view.fragment.FragmentFollow;
import bkdn.itt.datn.saleoff.view.fragment.FragmentMe;
import bkdn.itt.datn.saleoff.view.fragment.FragmentNewsFeed;
import bkdn.itt.datn.saleoff.view.fragment.FragmentNotification;
import bkdn.itt.datn.saleoff.view.iface.IActivityMain;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class ActivityMain extends ActivityBase<PresenterMain> implements IActivityMain {
    private static final int FRAGMENT_NEWSFEED = 0;
    private static final int FRAGMENT_FOLLOW = 1;
    private static final int FRAGMENT_EVENT = 2;
    private static final int FRAGMENT_NOTIFICATION = 3;
    private static final int FRAGMENT_MY_PAGE = 4;
    /*
     * Widgets
     * */
    @BindView(R.id.flContain)
    FrameLayout mFlContain;
    @BindView(R.id.tabLayoutActivityMain)
    TabLayout mTabLayoutMain;
    @BindView(R.id.llMainTop)
    LinearLayout mLlMainTop;

    private TextView mTvNotification;
    private int mNewNotification;


    /*
    * Fields
    * */
    int[] mUnselectedTabImages = {
            R.drawable.ic_home_default,
            R.drawable.ic_follow_tab_default,
            R.drawable.ic_event_default,
            R.drawable.ic_notification_default,
            R.drawable.ic_my_page_default};
    int[] mSelectedTabImages = {
            R.drawable.ic_home,
            R.drawable.ic_follow_tab,
            R.drawable.ic_event,
            R.drawable.ic_notifi,
            R.drawable.ic_my_page};
    String[] mTabNames = {"Trang chủ", "Theo dõi", "Sự kiện", "Thông báo", "Tôi"};
    private boolean doubleBackToExitPressedOnce;

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(context, "You have a new notification", Toast.LENGTH_SHORT).show();
            mNewNotification++;
            setNotificationIcon();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GoogleApiAvailability.getInstance().makeGooglePlayServicesAvailable(this);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        checkOpenFromNotification();
        initViews();
        setActions();
    }


    private void initViews() {

        for (int i = 0; i < mUnselectedTabImages.length; i++) {
            View tabView = LayoutInflater.from(getContext()).inflate(R.layout.tab_item, null, false);
            ((ImageView) tabView.findViewById(R.id.imvTabItem)).setImageResource(mUnselectedTabImages[i]);
            ((TextView) tabView.findViewById(R.id.tvTabItem)).setText(mTabNames[i]);
            mTabLayoutMain.addTab(mTabLayoutMain.newTab().setCustomView(tabView));
        }
        mTvNotification = mTabLayoutMain.getTabAt(FRAGMENT_NOTIFICATION).getCustomView()
                .findViewById(R.id.tvTabNotification);
        switchTab(FRAGMENT_NEWSFEED);
        replaceFragment(FragmentNewsFeed.newInstance(), mFlContain.getId(), false);
    }


    private void setNotificationIcon() {
        mTvNotification.setText(String.valueOf(mNewNotification));
        mTvNotification.setVisibility(View.VISIBLE);
    }

    private void switchTab(int selectedTab) {
        mTabLayoutMain.getTabAt(selectedTab).select();
        View tabView = mTabLayoutMain.getTabAt(selectedTab).getCustomView();
        ((ImageView) tabView.findViewById(R.id.imvTabItem)).setImageResource(mSelectedTabImages[selectedTab]);
        for (int i = 0; i < mTabLayoutMain.getTabCount(); i++) {
            if (i != selectedTab) {
                tabView = mTabLayoutMain.getTabAt(i).getCustomView();
                ((ImageView) tabView.findViewById(R.id.imvTabItem)).
                        setImageResource(mUnselectedTabImages[i]);
            }
        }
    }


    private void setActions() {
        mTabLayoutMain.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switchTab(tab.getPosition());
                switch (tab.getPosition()) {
                    case FRAGMENT_NEWSFEED:
                        ActivityMain.this.replaceFragment(FragmentNewsFeed.newInstance(), mFlContain.getId(),
                                false);
                        break;
                    case FRAGMENT_FOLLOW:
                        ActivityMain.this.replaceFragment(FragmentFollow.newInstance(), mFlContain.getId(),
                                false);
                        break;
                    case FRAGMENT_EVENT:
                        ActivityMain.this.replaceFragment(FragmentEvent.newInstance(), mFlContain.getId(),
                                false);
                        break;
                    case FRAGMENT_NOTIFICATION:
                        mTvNotification.setVisibility(View.GONE);
                        mNewNotification = 0;
                        ActivityMain.this.replaceFragment(FragmentNotification.newInstance(), mFlContain.getId(),
                                false);
                        break;
                    case FRAGMENT_MY_PAGE:
                        ActivityMain.this.replaceFragment(FragmentMe.newInstance(), mFlContain.getId(),
                                false);
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        mLlMainTop.setOnClickListener(v -> startActivity(ActivitySearch.class));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ConstantDefine.CREATE_HOME_POST_REQUEST) {
            } else if (requestCode == ConstantDefine.CREATE_MY_POST_REQUEST) {
                startActivity(new Intent(getContext(), ActivityMyPost.class));
            } else if (requestCode == ConstantDefine.EDIT_USER_POST_REQUEST) {
            } else if (requestCode == ConstantDefine.CREATE_SHOP_REQUEST) {
            } else if (requestCode == ConstantDefine.EDIT_PROFILE_REQUEST) {
                switchTab(FRAGMENT_MY_PAGE);
                replaceFragment(FragmentMe.newInstance(), mFlContain.getId(), false);
            } else if (requestCode == ConstantDefine.IMAGE_CAPTURE_REQUEST) {
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                File imageFile = ImagesUtils.saveImage(imageBitmap);
                Uri imageUri = Uri.fromFile(imageFile);
                // start cropping activity for pre-acquired image saved on the device
                int aspectRatioX = getResources().getDimensionPixelSize(R.dimen._600sdp);
                int aspectRatioY = getResources().getDimensionPixelSize(R.dimen._600sdp);
                CropImage.activity(imageUri)
                        .setAutoZoomEnabled(false)
                        .setActivityTitle("Tạo Ảnh Bìa")
                        .setAspectRatio(aspectRatioX, aspectRatioY)
                        .setMinCropWindowSize(aspectRatioX, aspectRatioY)
                        .setAllowRotation(false)
                        .setOutputCompressFormat(Bitmap.CompressFormat.JPEG)
                        .setOutputCompressQuality(100)
                        .setOutputUri(imageUri)
                        .setFixAspectRatio(true)
                        .setAllowFlipping(false)
                        .start(this);
            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();
                    UploadImageCloud.getInstance().uploadPostCover(getContext(), resultUri,
                            new UploadImageCloud.OnUpLoadPostPhotosListener() {
                                @Override
                                public void onUpLoadPostPhotos(String imgUrl) {
                                    Toasty.success(getContext(), "Success!", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onUpLoadFail(String errorMessage) {
                                    Toasty.success(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
                                }
                            });
                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                }
            }
        }
    }

    private void checkOpenFromNotification() {
        Bundle bundle = getIntent().getExtras();
        if (null != bundle) {
            NotificationBody notificationBody = bundle.getParcelable(GlobalDefine.KEY_BUNDLE_NOTIFICATION);
            if (null != notificationBody) {
                switch (notificationBody.getType()) {
                    case NotificationType.USER_FOLLOW_USER:
                        startActivityForNotification(ActivityUserPublic.class, notificationBody);
                        break;
                    case NotificationType.USER_NEW_POST:
                        startActivityForNotification(ActivityPostDetail.class, notificationBody);
                        break;
                    case NotificationType.SHOP_NEW_POST:
                        startActivityForNotification(ActivityPostDetail.class, notificationBody);
                        break;
                    case NotificationType.COMMENT:
                        startActivityForNotification(ActivityComment.class, notificationBody);
                        break;
                    case NotificationType.LIKE_POST:
                        startActivityForNotification(ActivityPostDetail.class, notificationBody);
                        break;
                    case NotificationType.ADDED_EMPLOYEE:
                        startActivityForNotification(ActivityShopPublic.class, notificationBody);
                        break;
                    case NotificationType.DELETED_EMPLOYEE:
                        startActivityForNotification(ActivityShopPublic.class, notificationBody);
                        break;
                    case NotificationType.SHOP_ACTIVED:
                        startActivityForNotification(ActivityShopPrivate.class, notificationBody);
                        break;
                    case NotificationType.SHOP_UN_ACTIVED:
                        startActivityForNotification(ActivityShopPrivate.class, notificationBody);
                        break;
                    case NotificationType.POST_ACTIVED:
                        startActivityForNotification(ActivityPostDetail.class, notificationBody);
                        break;
                    case NotificationType.POST_UN_ACTIVED:
                        startActivityForNotification(ActivityPostDetail.class, notificationBody);
                        break;
                    case NotificationType.DELETED_SHOP:
                        startActivityForNotification(ActivityShopList.class, notificationBody);
                        break;
                    case NotificationType.EVENT:
                        startActivityForNotification(ActivityEventDetail.class, notificationBody);
                        break;
                    case NotificationType.EVENT_ALARM:
                        startActivityForNotification(ActivityEventDetail.class, notificationBody);
                        break;
                    default:
                        break;
                }
            }
        }

    }

    @Override
    public void onBackPressed() {
        boolean isAtFragmentNewsFeed = getSupportFragmentManager()
                .findFragmentByTag(FragmentNewsFeed.class.getName()) instanceof FragmentNewsFeed;
        if (!isAtFragmentNewsFeed) {
            ActivityMain.this.replaceFragment(FragmentNewsFeed.newInstance(), mFlContain.getId(), false);
            mTabLayoutMain.getTabAt(FRAGMENT_NEWSFEED).setIcon(R.drawable.ic_home).select();
            return;
        }
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
    }

    private void startActivityForNotification(Class<?> cls,
                                              NotificationBody notificationBody) {
        Intent intent = new Intent(this, cls);
        Bundle bundle = new Bundle();
        bundle.putString(GlobalDefine.KEY_BUNDLE_NOTIFICATION, notificationBody.getData());
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SaleOffApplication.activityResumed();
        LocalBroadcastManager.getInstance(this).registerReceiver((mMessageReceiver),
                new IntentFilter("Notification")
        );
    }

    @Override
    protected void onPause() {
        super.onPause();
        SaleOffApplication.activityPaused();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    @Override
    public void replaceFragment(FragmentBase fragment, int containViewId, boolean addToBackStack) {
        String TAB = fragment.getClass().getName();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        //transaction.setCustomAnimations(R.anim.view_enter_from_left, R.anim.view_exit_to_right, R.anim.view_enter_from_right, R.anim.view_exit_to_left);
        if (addToBackStack) {
            transaction.addToBackStack(TAB);
        }
        transaction.replace(containViewId, fragment, TAB);
        transaction.commit();
        getSupportFragmentManager().executePendingTransactions();
    }
}
