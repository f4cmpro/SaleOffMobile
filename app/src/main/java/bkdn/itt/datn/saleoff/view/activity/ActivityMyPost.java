package bkdn.itt.datn.saleoff.view.activity;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.model.Event;
import bkdn.itt.datn.saleoff.model.model.Post;
import bkdn.itt.datn.saleoff.model.model.User;
import bkdn.itt.datn.saleoff.presenter.PresenterMyPost;
import bkdn.itt.datn.saleoff.utils.CustomPaginationScrollListener;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.SharedPreferencesUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.adapter.AdapterUserNewsFeed;
import bkdn.itt.datn.saleoff.view.iface.IActivityMyPost;
import bkdn.itt.datn.saleoff.view.iface.OnClickLikeListener;
import bkdn.itt.datn.saleoff.view.iface.OnClickUnLikeListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class ActivityMyPost extends ActivityBase<PresenterMyPost> implements
        IActivityMyPost, AdapterUserNewsFeed.OnClickPostItemListener,
        AdapterUserNewsFeed.OnEditPostItemListener {

    /*
    * Widgets
    * */
    @BindView(R.id.recyclerMyPost)
    RecyclerView mRecyclerMyPost;
    @BindView(R.id.swipeRefreshMyPost)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.llMyPostCreatePost)
    LinearLayout mLlCreatePost;
    @BindView(R.id.viewMyPostNotFoundPost)
    View mViewNotFoundPost;
    @BindView(R.id.imvMyPostAvatar)
    ImageView mImvMyAvatar;
    @BindView(R.id.imvMyPostCover)
    ImageView mImvMyCover;
    @BindView(R.id.tvMyPostName)
    TextView mTvMyName;
    @BindView(R.id.tvMyPostFollow)
    TextView mTvFollowerNumbers;
    @BindView(R.id.toolbarMyPost)
    Toolbar mToolbar;

    /*
    * Fields
    * */
    private AdapterUserNewsFeed mAdapterPost;
    private OnClickLikeListener mOnClickLikeListener;
    private OnClickUnLikeListener mOnClickUnLikeListener;
    private int mCurrentPage;
    private int mLikedPosition;
    private int mUnLikedPosition;
    private boolean mIsRefresh;
    private boolean mIsLoadMore;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_post);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Utils.showLoadingDialog(getContext());
        mCurrentPage = ConstantDefine.FIRST_PAGE;
        getPresenter(this).getMyPost(true, ConstantDefine.FIRST_PAGE);
        initViews(SharedPreferencesUtils.getInstance(getContext()).getUserProfile());
        initActions();

    }

    private void initViews(User me) {
        mAdapterPost = new AdapterUserNewsFeed(getContext(), this, ConstantDefine.PRIVATE);
        mAdapterPost.setEditPostItemListener(this);
        if (me != null) {
            //set my avatar
            Glide.with(getContext()).load(me.getAvatar()).into(mImvMyAvatar);
            //set my cover
            Glide.with(getContext()).load(me.getCover()).into(mImvMyCover);
            //set user name
            mTvMyName.setText(me.getUsername());
            //set followers numbers
            mTvFollowerNumbers.setText(getString(R.string.followers) + " " + me.getFollowNumber());
            //set avatar and name for post item
            mAdapterPost.setUserInfo(me.getAvatar(), me.getUsername());
        }
        RecyclerViewUtils.Create().setUpVertical(getContext(), mRecyclerMyPost).setAdapter(mAdapterPost);
        mRecyclerMyPost.setNestedScrollingEnabled(true);
    }

    private void initActions() {
        mLlCreatePost.setOnClickListener(v -> startActivityForResult(new Intent(getContext(),
                ActivityPostCreate.class), ConstantDefine.CREATE_MY_POST_REQUEST));
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mIsRefresh = true;
            getPresenter(ActivityMyPost.this)
                    .getMyPost(false, ConstantDefine.FIRST_PAGE);
        });
        mRecyclerMyPost.addOnScrollListener(new CustomPaginationScrollListener(
                (LinearLayoutManager) mRecyclerMyPost.getLayoutManager()) {
            @Override
            protected void loadMoreItems() {
                mIsLoadMore = true;
                mCurrentPage++;
                getPresenter(ActivityMyPost.this).getMyPost(false, mCurrentPage);
            }

            @Override
            public boolean isLoading() {
                return mIsLoadMore;
            }

            @Override
            protected void enableRefreshLayout(boolean isEnable) {
                mSwipeRefreshLayout.setEnabled(isEnable);
                if (isEnable) {
                    if (mRecyclerMyPost.getScrollState() == 1) {
                        if (mSwipeRefreshLayout.isRefreshing()) {
                            mRecyclerMyPost.stopScroll();
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ConstantDefine.CREATE_MY_POST_REQUEST) {
                Toasty.success(getContext(), getString(R.string.post_waitting_active),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onClickPostItem(Post post) {
        Intent intent = new Intent(getContext(), ActivityPostDetail.class);
        intent.putExtra(GlobalDefine.KEY_INTENT_POST_ID, post.getPostId());
        startActivity(intent);
    }

    @Override
    public void onClickLike(int postId, OnClickLikeListener onClickLikeListener) {
        mOnClickLikeListener = onClickLikeListener;
        getPresenter(this).likePost(postId);
    }

    @Override
    public void onClickUnLike(int postId, OnClickUnLikeListener onClickUnLikeListener) {
        mOnClickUnLikeListener = onClickUnLikeListener;
        getPresenter(this).unLikePost(postId);
    }

    @Override
    public void onClickDeletePostItem(int postId) {
        getPresenter(this).deleteUserPost(postId);

    }

    @Override
    public void onClickEditPostItem(Post post) {
        Intent intent = new Intent(getContext(), ActivityPostEdit.class);
        intent.putExtra(GlobalDefine.KEY_INTENT_POST, post);
        ((Activity) getContext()).startActivityForResult(intent, ConstantDefine.EDIT_USER_POST_REQUEST);
    }


    @Override
    public void onClickComment(int postId) {
        Intent intent = new Intent(getContext(), ActivityComment.class);
        intent.putExtra(GlobalDefine.KEY_INTENT_POST_ID, postId);
        startActivity(intent);
    }

    @Override
    public void onCLickShare(Post post) {
        Intent intent = new Intent(getContext(), ActivityPostDetail.class);
        intent.putExtra(GlobalDefine.KEY_INTENT_POST, post);
        startActivity(intent);
    }

    @Override
    public void onClickEventDetail(Event event) {
        Intent intent = new Intent(getContext(), ActivityEventDetail.class);
        intent.putExtra(GlobalDefine.KEY_INTENT_EVENT, event);
        startActivity(intent);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void getMyPostSuccess(List<Post> posts) {
        if (mIsRefresh) {
            mCurrentPage = ConstantDefine.FIRST_PAGE;
            mIsRefresh = false;
            mAdapterPost.refreshPosts(posts);
        } else {
            mIsLoadMore = false;
            mAdapterPost.setPosts(posts);
        }
        if (mAdapterPost.getItemCount() == 0) {
            mRecyclerMyPost.setVisibility(View.GONE);
            mViewNotFoundPost.setVisibility(View.VISIBLE);
            ((TextView) mViewNotFoundPost.findViewById(R.id.tvLayoutNotFoundItem))
                    .setText(getString(R.string.warning_not_post_yet));
        } else {
            mRecyclerMyPost.setVisibility(View.VISIBLE);
            mViewNotFoundPost.setVisibility(View.GONE);

        }
    }

    @Override
    public void getError(String errorMessage) {
        if (mIsRefresh) {
            mIsRefresh = false;
        } else if (mIsLoadMore) {
            mIsLoadMore = false;
            mCurrentPage--;
        }
        Toasty.warning(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void deleteUserPostSuccess() {
        mIsRefresh = true;
        getPresenter(this).getMyPost(true, ConstantDefine.FIRST_PAGE);
    }

    @Override
    public void likePostFail(String errorMessage) {
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
        mOnClickLikeListener.error();
    }

    @Override
    public void likePostSuccess() {
        mOnClickLikeListener.success();
    }

    @Override
    public void unLikePostFail(String errorMessage) {
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
        mOnClickUnLikeListener.error();
    }

    @Override
    public void unLikePostSuccess() {
        mOnClickUnLikeListener.success();
    }

}
