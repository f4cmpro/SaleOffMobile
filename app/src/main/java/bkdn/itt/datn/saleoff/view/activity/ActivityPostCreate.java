package bkdn.itt.datn.saleoff.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBufferResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.Calendar;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.model.Post;
import bkdn.itt.datn.saleoff.model.model.SubCategory;
import bkdn.itt.datn.saleoff.model.model.Shop;
import bkdn.itt.datn.saleoff.model.model.Category;
import bkdn.itt.datn.saleoff.presenter.PresenterPostCreate;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.TimeUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.adapter.AdapterSelectedImage;
import bkdn.itt.datn.saleoff.view.adapter.PlaceAutocompleteAdapter;
import bkdn.itt.datn.saleoff.view.dialog.DialogDatePicker;
import bkdn.itt.datn.saleoff.view.dialog.DialogSalePercent;
import bkdn.itt.datn.saleoff.view.iface.IActivityPostCreate;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class ActivityPostCreate extends ActivityBase<PresenterPostCreate> implements
        DialogDatePicker.OnPickDateListener, DialogSalePercent.OnChooseSalePercentListener,
        IActivityPostCreate, AdapterSelectedImage.OnSelectedImageListener, GoogleApiClient.OnConnectionFailedListener {
    private static final int REQUEST_CODE_CATEGORY = 1;
    private static final int REQUEST_CODE_SUB_CATEGORY = 2;
    private static final String TAG = "ActivityPostCreate";
    /*
    * Widgets
    * */
    @BindView(R.id.recyclerCreatePostImages)
    RecyclerView mRecyclerAddImages;
    @BindView(R.id.edtCreatePostTitle)
    EditText mEdtTitle;
    @BindView(R.id.edtCreatePostAddress)
    AutoCompleteTextView mEdtSearch;
    @BindView(R.id.tvCreatePostAddress)
    TextView mTvAddress;
    @BindView(R.id.edtCreatePostDes)
    EditText mEdtDes;
    @BindView(R.id.tvCreatePostCategory)
    TextView mTvCate;
    @BindView(R.id.tvCreatePostSubCategory)
    TextView mTvSubCate;
    @BindView(R.id.tvCreatePostSalePercent)
    TextView mTvSalePercent;
    @BindView(R.id.tvCreatePostFrom)
    TextView mTvFrom;
    @BindView(R.id.tvCreatePostTo)
    TextView mTvTo;
    @BindView(R.id.imbCreatePostBack)
    ImageButton mImbBack;
    @BindView(R.id.imbCreatePostDone)
    ImageButton mImbDone;
    @BindView(R.id.llPostCreateSubCat)
    LinearLayout mLlSubCat;

    /*
    * Fields
    * */
    private static final LatLngBounds LAT_LNG_VN_BOUNDS = new LatLngBounds(new LatLng(8.6, 104.6),
            new LatLng(23.4, 105.3));
    private PlaceAutocompleteAdapter mPlaceAutocompleteAdapter;
    private AdapterSelectedImage mAdapterSelectedImage;
    private AutocompleteFilter mAutocompleteFilter;
    private GeoDataClient mGeoDataClient;
    private boolean isFrom;
    private String[] mValuePercents;
    private Post mCreatedPost;
    private Category mCategory;
    private ArrayList<String> mImgUrls;
    private Shop mShop;
    private Calendar mStartDate = Calendar.getInstance();
    private Calendar mEndDate = Calendar.getInstance();


    private class GenericTextWatcher implements TextWatcher {
        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String text = s.toString();
            switch (view.getId()) {
                case R.id.edtCreatePostTitle:
                    if (!text.isEmpty()) {
                        mCreatedPost.setPostTitle(text);
                    } else {
                        Toasty.warning(getContext(), getString(R.string.warning_empty),
                                Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.edtCreatePostAddress:
                    if (!text.isEmpty()) {
                        mCreatedPost.setShopAddress(text);
                    } else {
                        Toasty.warning(getContext(), getString(R.string.warning_empty),
                                Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.edtCreatePostDes:
                    if (!text.isEmpty()) {
                        mCreatedPost.setPostDescribe(text);
                    } else {
                        Toasty.warning(getContext(), getString(R.string.warning_empty),
                                Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_create);
        ButterKnife.bind(this);
        initValues();
        initViews();
        initActions();
    }


    private void initViews() {
        mAutocompleteFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS)
                .setCountry("VN")
                .build();
        mGeoDataClient = Places.getGeoDataClient(this, null);
        mPlaceAutocompleteAdapter = new PlaceAutocompleteAdapter(this, mGeoDataClient,
                null, mAutocompleteFilter);
        mEdtSearch.setAdapter(mPlaceAutocompleteAdapter);
        mEdtDes.setRawInputType(InputType.TYPE_CLASS_TEXT);
        mEdtTitle.setRawInputType(InputType.TYPE_CLASS_TEXT);
        mEdtSearch.setRawInputType(InputType.TYPE_CLASS_TEXT);
        if (mShop != null) {
            mTvCate.setText(mShop.getCateName());
            mTvCate.setEnabled(false);
            mLlSubCat.setVisibility(View.VISIBLE);
            mEdtSearch.setText(mShop.getAddress());
        }
        mAdapterSelectedImage = new AdapterSelectedImage(this, false);
        RecyclerViewUtils.Create().setUpReverseHorizontal(this, mRecyclerAddImages)
                .setAdapter(mAdapterSelectedImage);
    }

    private void initValues() {
        mCreatedPost = new Post();
        mShop = getIntent().getParcelableExtra(GlobalDefine.KEY_INTENT_SHOP);
        if (mShop != null) {
            mCreatedPost.setShopId(mShop.getShopId());
            mCategory = new Category();
            mCategory.setId(mShop.getCateId());
            mCategory.setName(mShop.getCateName());
        }
        mValuePercents = getResources().getStringArray(R.array.sale_percent);
    }

    private void initActions() {
        mEdtSearch.setOnItemClickListener((parent, view, position, id) -> {
            Utils.hideSoftKeyboard(ActivityPostCreate.this);
            final AutocompletePrediction item = mPlaceAutocompleteAdapter.getItem(position);
            Task<PlaceBufferResponse> placeResult = mGeoDataClient.getPlaceById(item.getPlaceId());
            placeResult.addOnCompleteListener(task -> {
                PlaceBufferResponse places = task.getResult();
                Place place = places.get(0);
                Log.i(TAG, place.getAddress().toString());
                mCreatedPost.setShopAddress(place.getAddress().toString());
            });
        });
        mEdtSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH
                    || actionId == EditorInfo.IME_ACTION_DONE
                    || actionId == EditorInfo.IME_ACTION_NEXT) {
                // execute searching method
                Utils.hideSoftKeyboard(this);
                mCreatedPost.setShopAddress(mEdtSearch.getText().toString());
            }
            return false;
        });
        mEdtTitle.addTextChangedListener(new GenericTextWatcher(mEdtTitle));
        mEdtDes.addTextChangedListener(new GenericTextWatcher(mEdtDes));
        mTvCate.setOnClickListener(v -> {
            Intent cateIntent = new Intent(ActivityPostCreate.this, ActivityCategory.class);
            startActivityForResult(cateIntent, REQUEST_CODE_CATEGORY);
        });
        mTvSubCate.setOnClickListener(v -> {
            Intent cateIntent = new Intent(ActivityPostCreate.this, ActivityCategorySub.class);
            cateIntent.putExtra(GlobalDefine.KEY_INTENT_SELECT_SUB_CATEGORY, mCategory.getId());
            startActivityForResult(cateIntent, REQUEST_CODE_SUB_CATEGORY);
        });
        mTvFrom.setOnClickListener(v -> {
            isFrom = true;
            DialogDatePicker.newInstance(ActivityPostCreate.this, getSupportFragmentManager(),
                    Calendar.getInstance().getTimeInMillis(), "Ngày bắt đầu");
        });
        mTvTo.setOnClickListener(v -> {
            if (isFrom) {
                isFrom = false;
                DialogDatePicker.newInstance(ActivityPostCreate.this, getSupportFragmentManager(),
                        mStartDate.getTimeInMillis(), "Ngày kết thúc");
            } else {
                Toasty.warning(getContext(), "Chưa chọn ngày bắt đầu!", Toast.LENGTH_SHORT).show();
            }

        });
        mTvSalePercent.setOnClickListener(v -> DialogSalePercent.newInstance(this, getSupportFragmentManager()));
        mImbDone.setOnClickListener(v -> {
            getPresenter(ActivityPostCreate.this).createPost(mCreatedPost);
        });
        mImbBack.setOnClickListener(v -> onBackPressed());
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ConstantDefine.PICK_IMAGES_REQUEST) {
                ArrayList<String> imgUrls = data.getStringArrayListExtra(GlobalDefine.KEY_INTENT_PICK_IMAGES);
                mAdapterSelectedImage.addMoreImages(imgUrls);
                mRecyclerAddImages.smoothScrollToPosition(mAdapterSelectedImage.getItemCount() - 1);
            } else if (requestCode == REQUEST_CODE_CATEGORY) {
                mCategory = data.getParcelableExtra(GlobalDefine.KEY_INTENT_CATEGORY);
                mTvCate.setText(mCategory.getName());
                mLlSubCat.setVisibility(View.VISIBLE);
            } else if (requestCode == REQUEST_CODE_SUB_CATEGORY) {
                SubCategory subCategory = data.getParcelableExtra(GlobalDefine.KEY_INTENT_SELECT_SUB_CATEGORY);
                mTvSubCate.setText(subCategory.getName());
                mCreatedPost.setProductId(subCategory.getId());
            }
        }
    }

    @Override
    public void onPickDate(int day, int month, int year) {
        if (isFrom) {
            mStartDate.set(Calendar.YEAR, year);
            mStartDate.set(Calendar.MONTH, month);
            mStartDate.set(Calendar.DAY_OF_MONTH, day);
            mTvFrom.setText(TimeUtils.parseTimestampToString(mStartDate.getTimeInMillis(), GlobalDefine.FORMAT_DATE_THIRD_DEFAULT));
            mCreatedPost.setFromDate(TimeUtils.parseTimestampToString(mStartDate.getTimeInMillis(), GlobalDefine.FORMAT_DATE_SECOND_DEFAULT));
        } else {
            isFrom = true;
            mEndDate.set(Calendar.YEAR, year);
            mEndDate.set(Calendar.MONTH, month);
            mEndDate.set(Calendar.DAY_OF_MONTH, day);
            mTvTo.setText(TimeUtils.parseTimestampToString(mEndDate.getTimeInMillis(), GlobalDefine.FORMAT_DATE_THIRD_DEFAULT));
            mCreatedPost.setToDate(TimeUtils.parseTimestampToString(mEndDate.getTimeInMillis(), GlobalDefine.FORMAT_DATE_SECOND_DEFAULT));

        }

    }

    @Override
    public void onChooseSalePercent(int value) {
        mTvSalePercent.setText(mValuePercents[value]);
        mTvSalePercent.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen._18sdp));
        mCreatedPost.setSalePercent(5 * (value + 1));
    }


    @Override
    public void createPostSuccess(String message) {
        Toasty.success(getContext(), message, Toast.LENGTH_SHORT).show();
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void createPostFail(String errorMessage) {
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onSelectedImage(String imgUrl) {
        if (mImgUrls == null) {
            mImgUrls = new ArrayList<>();
        }
        mImgUrls.add(imgUrl);
        mCreatedPost.setPostImages(mImgUrls.toArray(new String[mImgUrls.size()]));
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(this);
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
