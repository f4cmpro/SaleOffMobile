package bkdn.itt.datn.saleoff.view.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.ggmap.DirectionFinder;
import bkdn.itt.datn.saleoff.ggmap.Route;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.model.Post;
import bkdn.itt.datn.saleoff.presenter.PresenterPostDetail;
import bkdn.itt.datn.saleoff.utils.MapUtils;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.adapter.AdapterRecentPosts;
import bkdn.itt.datn.saleoff.view.adapter.AdapterSlider;
import bkdn.itt.datn.saleoff.view.fragment.MySupportMapFragment;
import bkdn.itt.datn.saleoff.view.iface.IActivityPostDetail;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class ActivityPostDetail extends ActivityBase<PresenterPostDetail> implements IActivityPostDetail,
        AdapterRecentPosts.OnClickRecentPostItemListener, OnMapReadyCallback, DirectionFinder.DirectionFinderListener {
    /*
    * Widgets
    * */
    @BindView(R.id.viewPagerDetailSlider)
    ViewPager mViewPager;
    @BindView(R.id.btnPostDetailWatchShop)
    Button mBtnWatchShop;
    @BindView(R.id.tvPostDetailOf)
    TextView mTvPostOf;
    @BindView(R.id.tvPostDetailTitle)
    TextView mTvTitle;
    @BindView(R.id.tvDetailCreatedAt)
    TextView mTvCreatedAt;
    @BindView(R.id.tvPostDetailDescription)
    TextView mTvDes;
    @BindView(R.id.tvPostDetailSalePercent)
    TextView mTvSalePercent;
    @BindView(R.id.tvDetailUserName)
    TextView mTvUserName;
    @BindView(R.id.tvPostDetailSaleStart)
    TextView mTvSaleStart;
    @BindView(R.id.tvPostDetailSaleEnd)
    TextView mTvSaleEnd;
    @BindView(R.id.tvPostDetailAddress)
    TextView mTvAddress;
    @BindView(R.id.tvPostDetailLike)
    TextView mTvLike;
    @BindView(R.id.llPostDetailLike)
    LinearLayout mLlLike;
    @BindView(R.id.imvPostDetailLike)
    ImageView mImbLike;
    @BindView(R.id.llPostDetailComment)
    LinearLayout mLlComment;
    @BindView(R.id.tvPostDetailComment)
    TextView mTvComment;
    @BindView(R.id.llPostDetailRate)
    LinearLayout mLlRate;
    @BindView(R.id.tvDetailPostNumber)
    TextView mTvPostNumber;
    @BindView(R.id.tvDetailRateNumber)
    TextView mTvRateNumber;
    @BindView(R.id.tvDetailFollowNumber)
    TextView mTvFollowNumber;
    @BindView(R.id.imvPostDetailAvatar)
    ImageView mImvAvatar;
    @BindView(R.id.swipeRefreshPostDetail)
    SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.imbPostDetailBack)
    ImageButton mImbBack;
    @BindView(R.id.recyclerPostRecent)
    RecyclerView mRecyclerRecent;
    @BindView(R.id.tabLayoutIndicatorPostDetail)
    TabLayout mTabIndicator;
    @BindView(R.id.nestedScrollViewPostDetail)
    NestedScrollView mNestedScrollView;
    /*
    * Fields
    * */
    private MySupportMapFragment mMapFragment;
    private AdapterSlider mAdapterSlider;
    private Post mPost;
    private AdapterRecentPosts mAdapterRecentPosts;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        ButterKnife.bind(this);
        initValues();
        initViews();
        intActions();
    }

    private void initValues() {
        if (getIntent().getExtras().getString(GlobalDefine.KEY_BUNDLE_NOTIFICATION) != null) {
            String data = getIntent().getExtras().getString(GlobalDefine.KEY_BUNDLE_NOTIFICATION);
            mPost = new Gson().fromJson(data, Post.class);
        } else if (getIntent().getParcelableExtra(GlobalDefine.KEY_INTENT_POST) != null) {
            mPost = getIntent().getParcelableExtra(GlobalDefine.KEY_INTENT_POST);
        } else {
            mPost = new Post();
            mPost.setPostId(getIntent().getIntExtra(GlobalDefine.KEY_INTENT_POST_ID, 0));
        }
        getPresenter(this).getPostDetail(mPost.getPostId());
        getPresenter(this).getRecentPosts(mPost.getPostId());
    }

    private void initViews() {
        if (mPost != null) {
            //set Images
            if (mPost.getPostImages() != null && mPost.getPostImages().length > 0) {
                mAdapterSlider = new AdapterSlider(getSupportFragmentManager(), mPost.getPostImages());
                mViewPager.setAdapter(mAdapterSlider);
                mTabIndicator.setupWithViewPager(mViewPager);
            }
            //set post title
            if (mPost.getPostTitle() != null && !mPost.getPostTitle().isEmpty()) {
                mTvTitle.setText(mPost.getPostTitle());
            }
            //set sale percent
            if (mPost.getSalePercent() != 0) {
                mTvSalePercent.setText(String.valueOf(mPost.getSalePercent()) + "%");
            }
            //set start date
            if (mPost.getFromDate() != null && !mPost.getFromDate().isEmpty()) {
                mTvSaleStart.setText(mPost.getFromDate());
            }
            //set to date
            if (mPost.getToDate() != null && !mPost.getToDate().isEmpty()) {
                mTvSaleEnd.setText(mPost.getToDate());
            }
            //set address
            if (mPost.getShopAddress() != null && !mPost.getShopAddress().isEmpty()) {
                mTvAddress.setText(mPost.getShopAddress());
            }
            //set map
            if (mPost.getShopAddress() != null) {
                mMapFragment = (MySupportMapFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.mapPostDetailAddress);
                mMapFragment.setListener(() -> mNestedScrollView.requestDisallowInterceptTouchEvent(true));
                mMapFragment.getMapAsync(this);
            }

            //set post description
            if (mPost.getPostDescribe() != null && !mPost.getPostDescribe().isEmpty()) {
                mTvDes.setText(mPost.getPostDescribe());
            }
            //set like image
            if (mPost.getIsLiked() == 1) {
                mImbLike.setImageResource(R.drawable.ic_liked);
            } else {
                mImbLike.setImageResource(R.drawable.ic_like_default);
            }
            //set post likes
            mTvLike.setText(String.valueOf(mPost.getLikeNumber()) + " " + getString(R.string.like));

            //set post comments
            mTvComment.setText(String.valueOf(mPost.getCommentNumber()) + " " +
                    getString(R.string.comments));
            //set created at
            if (mPost.getCreatedDate() != null) {
                mTvCreatedAt.setText(mPost.getCreatedDate());
            }
            //set shop info
            if (mPost.getShop() != null) {
                mTvPostOf.setText("Bài viết của " + mPost.getShop().getName());
                mLlRate.setVisibility(View.VISIBLE);
                if (mPost.getShop().getName() != null) {
                    mTvUserName.setText(mPost.getShop().getName());
                }
                if (mPost.getShop().getAvatar() != null) {
                    Glide.with(getContext()).load(mPost.getShop().getAvatar()).into(mImvAvatar);
                }
                mTvPostNumber.setText(String.valueOf(mPost.getShop().getPostsNumber()));
                mTvRateNumber.setText(String.valueOf(mPost.getShop().getRate()));
                mTvFollowNumber.setText(String.valueOf(mPost.getShop().getFollowsNumber()));
            } else if (mPost.getUser() != null) { // set user info
                mTvPostOf.setText("Bài viết của " + mPost.getUser().getUserName());
                mLlRate.setVisibility(View.GONE);
                if (mPost.getUser().getUserName() != null) {
                    mTvUserName.setText(mPost.getUser().getUserName());
                }
                if (mPost.getUser().getAvatar() != null) {
                    Glide.with(getContext()).load(mPost.getUser().getAvatar()).into(mImvAvatar);
                }
                mTvPostNumber.setText(String.valueOf(mPost.getUser().getPostsNumber()));
                mTvFollowNumber.setText(String.valueOf(mPost.getUser().getFollowsNumber()));
            }

        }
    }

    private void setDirectionMap() {
        String myAddress = MapUtils.getInstance().getMyLocation(getContext());
        MapUtils.getInstance().findTheRoadToShop(this, myAddress, mPost.getShopAddress());
    }

    private void intActions() {
        mImbBack.setOnClickListener(v -> onBackPressed());
        mBtnWatchShop.setOnClickListener(v -> {
            if (mPost.getShop() != null) {
                Intent intent = new Intent(ActivityPostDetail.this, ActivityShopPublic.class);
                intent.putExtra(GlobalDefine.KEY_INTENT_SHOP_ID, mPost.getShopId());
                startActivity(intent);
            } else {
                Intent intent = new Intent(ActivityPostDetail.this, ActivityUserPublic.class);
                intent.putExtra(GlobalDefine.KEY_INTENT_USER_ID, mPost.getUserId());
                startActivity(intent);
            }
        });
        mRefreshLayout.setOnRefreshListener(() -> {
            getPresenter(this).getPostDetail(mPost.getPostId());
            getPresenter(this).getRecentPosts(mPost.getPostId());
        });
        mLlLike.setOnClickListener(v -> {
            if (mPost.isLiked() == 1) {
                getPresenter(this).unLikePost(mPost.getPostId());
                mPost.setLikeNumber(mPost.getLikeNumber() - 1);
                mImbLike.setImageResource(R.drawable.ic_like_default);
                mPost.setLiked(0);
            } else {
                getPresenter(this).likePost(mPost.getPostId());
                mPost.setLikeNumber(mPost.getLikeNumber() + 1);
                mImbLike.setImageResource(R.drawable.ic_liked);
                mPost.setLiked(1);
            }
            mTvLike.setText(String.valueOf(mPost.getLikeNumber()) + " " +
                    getString(R.string.like));
        });

        mLlComment.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), ActivityComment.class);
            intent.putExtra(GlobalDefine.KEY_INTENT_POST_ID, mPost.getPostId());
            startActivity(intent);
        });

    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
        mRefreshLayout.setRefreshing(false);
    }

    @Override
    public void getPostDetailSuccess(Post post) {
        mPost = post;
        initViews();
    }

    @Override
    public void getError(String errorMessage) {
        Toasty.warning(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void likePostSuccess() {

    }

    @Override
    public void unLikePostSuccess() {

    }

    @Override
    public void getRecentPosts(List<Post> posts) {
        if (mAdapterRecentPosts == null) {
            mAdapterRecentPosts = new AdapterRecentPosts(getContext());
            RecyclerViewUtils.Create().setUpHorizontal(getContext(), mRecyclerRecent)
                    .setAdapter(mAdapterRecentPosts);
        }
        mAdapterRecentPosts.setList(posts);
    }

    @Override
    public void onClickRecentPostItem(int postId) {
        Intent intent = new Intent(this, ActivityPostDetail.class);
        intent.putExtra(GlobalDefine.KEY_INTENT_POST_ID, postId);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (googleMap == null) {
            return;
        }
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        String shopName = mPost.getShop() != null ? mPost.getShop().getName() : "Cửa hàng";
        MapUtils.getInstance(googleMap).markPlaceOnMap(getContext(), googleMap, shopName, mPost.getShopAddress());
//        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
//                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(),
//                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            requestRuntimePermission();
//        } else {
//            //setDirectionMap();
//        }
    }


    @Override
    public void onDirectionFinderStart() {
        MapUtils.getInstance().findRoadStart();
    }

    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {
        if (mPost.getShop() != null) {
            MapUtils.getInstance().findRoadSuccess(routes, mPost.getShop().getName());
        } else {
            MapUtils.getInstance().findRoadSuccess(routes, "Shop");

        }
    }


    private void requestRuntimePermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION}, ConstantDefine.MY_LOCATION_REQUEST);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == ConstantDefine.MY_LOCATION_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                setDirectionMap();
            } else {
                Toasty.warning(getContext(), "Please grant me permission to find way to shop!");
            }
        }
    }
}
