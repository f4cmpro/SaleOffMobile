package bkdn.itt.datn.saleoff.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.model.Post;
import bkdn.itt.datn.saleoff.model.model.SubCategory;
import bkdn.itt.datn.saleoff.model.model.Category;
import bkdn.itt.datn.saleoff.presenter.PresenterPostEdit;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.TimeUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.adapter.AdapterSelectedImage;
import bkdn.itt.datn.saleoff.view.dialog.DialogDatePicker;
import bkdn.itt.datn.saleoff.view.dialog.DialogSalePercent;
import bkdn.itt.datn.saleoff.view.dialog.DialogTimePicker;
import bkdn.itt.datn.saleoff.view.iface.IActivityPostEdit;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

/**
 * Editd by TuDLT on 4/18/2018.
 */
public class ActivityPostEdit extends ActivityBase<PresenterPostEdit>
        implements IActivityPostEdit, AdapterSelectedImage.OnSelectedImageListener, DialogDatePicker.OnPickDateListener, DialogSalePercent.OnChooseSalePercentListener {
    private static final int REQUEST_CODE_CATEGORY = 1;
    private static final int REQUEST_CODE_SUB_CATEGORY = 2;
    /*
    * Widgets
    * */
    @BindView(R.id.recyclerEditPostImages)
    RecyclerView mRecyclerAddImages;
    @BindView(R.id.edtEditPostTitle)
    EditText mEdtTitle;
    @BindView(R.id.edtEditPostAddress)
    EditText mEdtAddress;
    @BindView(R.id.edtEditPostDes)
    EditText mEdtDes;
    @BindView(R.id.tvEditPostCategory)
    TextView mTvCate;
    @BindView(R.id.tvEditPostSubCategory)
    TextView mTvSubCate;
    @BindView(R.id.tvEditPostSalePercent)
    TextView mTvSalePercent;
    @BindView(R.id.tvEditPostFrom)
    TextView mTvFrom;
    @BindView(R.id.tvEditPostTo)
    TextView mTvTo;
    @BindView(R.id.imbEditPostBack)
    ImageButton mImbBack;
    @BindView(R.id.imbEditPostDone)
    ImageButton mImbDone;
    @BindView(R.id.llEditPostSubCat)
    LinearLayout mLlSubCat;
    /*
    * Fields
    * */
    private Post mEditedPost;
    private AdapterSelectedImage mAdapterSelectedImage;
    private Category mCategory;
    private boolean isFrom;
    private String[] mValuePercents;
    private Calendar mStartDate = Calendar.getInstance();
    private Calendar mEndDate = Calendar.getInstance();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_edit);
        ButterKnife.bind(this);
        initValues();
        initViews();
        initActions();
    }

    private void initValues() {
        if (mEditedPost == null) {
            mEditedPost = getIntent().getParcelableExtra(GlobalDefine.KEY_INTENT_POST);
        }
        if (mCategory == null) {
            mCategory = getIntent().getParcelableExtra(GlobalDefine.KEY_INTENT_CATEGORY);
        }

    }

    private void initViews() {
        if (mEditedPost != null) {
            //init Images
            mAdapterSelectedImage = new AdapterSelectedImage(this, true);
            RecyclerViewUtils.Create().setUpReverseHorizontal(getContext(), mRecyclerAddImages)
                    .setAdapter(mAdapterSelectedImage);
            mAdapterSelectedImage.addMoreImages(Arrays.asList(mEditedPost.getPostImages()));
            mRecyclerAddImages.smoothScrollToPosition(mAdapterSelectedImage.getItemCount() - 1);

            //init Post Title
            mEdtTitle.setText(mEditedPost.getPostTitle());

            //init Post Address
            mEdtAddress.setText(mEditedPost.getShopAddress());

            //init Post Description
            mEdtDes.setText(mEditedPost.getPostDescribe());

            //init Category
            if (mCategory != null) {
                mTvCate.setText(mCategory.getName());
                if (mEditedPost.getShopId() != 0) {
                    mTvCate.setEnabled(false);
                }
                mLlSubCat.setVisibility(View.VISIBLE);
            }

            //init Sub Category
            mTvSubCate.setText(String.valueOf(mEditedPost.getProductId()));

            //init SalePercent
            mTvSalePercent.setText(String.valueOf(mEditedPost.getSalePercent()) + "%");

            //init From date
            mTvFrom.setText(mEditedPost.getFromDate());

            //init To date
            mTvTo.setText(mEditedPost.getToDate());
        }
    }

    private void initActions() {
        mEdtTitle.addTextChangedListener(new ActivityPostEdit.GenericTextWatcher(mEdtTitle));
        mEdtAddress.addTextChangedListener(new ActivityPostEdit.GenericTextWatcher(mEdtAddress));
        mEdtDes.addTextChangedListener(new ActivityPostEdit.GenericTextWatcher(mEdtDes));
        mTvCate.setOnClickListener(v -> {
            Intent cateIntent = new Intent(ActivityPostEdit.this, ActivityCategory.class);
            startActivityForResult(cateIntent, REQUEST_CODE_CATEGORY);
        });
        mTvSubCate.setOnClickListener(v -> {
            Intent cateIntent = new Intent(ActivityPostEdit.this, ActivityCategorySub.class);
            cateIntent.putExtra(GlobalDefine.KEY_INTENT_SELECT_SUB_CATEGORY, mCategory.getId());
            startActivityForResult(cateIntent, REQUEST_CODE_SUB_CATEGORY);
        });
        mTvFrom.setOnClickListener(v -> {
            isFrom = true;
            DialogDatePicker.newInstance(ActivityPostEdit.this, getSupportFragmentManager(),
                    Calendar.getInstance().getTimeInMillis(), "Ngày bắt đầu");
        });
        mTvTo.setOnClickListener(v -> {
            if (isFrom) {
                isFrom = false;
                DialogDatePicker.newInstance(ActivityPostEdit.this, getSupportFragmentManager(),
                        mStartDate.getTimeInMillis(), "Ngày kết thúc");
            } else {
                Toasty.warning(getContext(), "Chưa chọn ngày bắt đầu!");
            }

        });
        mTvSalePercent.setOnClickListener(v -> DialogSalePercent.newInstance(this, getSupportFragmentManager()));
        mImbDone.setOnClickListener(v -> getPresenter(ActivityPostEdit.this).editPost(mEditedPost));
        mImbBack.setOnClickListener(v -> onBackPressed());
    }


    @Override
    public void editPostSuccess() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void editPostError(String errorMessage) {
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
    }

    @Override
    public void onSelectedImage(String imgUrl) {

    }


    @Override
    public void onPickDate(int day, int month, int year) {
        if (isFrom) {
            mStartDate.set(Calendar.YEAR, year);
            mStartDate.set(Calendar.MONTH, month);
            mStartDate.set(Calendar.DAY_OF_MONTH, day);
            mTvFrom.setText(TimeUtils.parseTimestampToString(mStartDate.getTimeInMillis(), GlobalDefine.FORMAT_DATE_THIRD_DEFAULT));
            mEditedPost.setFromDate(TimeUtils.parseTimestampToString(mStartDate.getTimeInMillis(), GlobalDefine.FORMAT_DATE_SECOND_DEFAULT));
        } else {
            isFrom = true;
            mEndDate.set(Calendar.YEAR, year);
            mEndDate.set(Calendar.MONTH, month);
            mEndDate.set(Calendar.DAY_OF_MONTH, day);
            mTvTo.setText(TimeUtils.parseTimestampToString(mEndDate.getTimeInMillis(), GlobalDefine.FORMAT_DATE_THIRD_DEFAULT));
            mEditedPost.setToDate(TimeUtils.parseTimestampToString(mEndDate.getTimeInMillis(), GlobalDefine.FORMAT_DATE_SECOND_DEFAULT));
        }
    }

    @Override
    public void onChooseSalePercent(int value) {
        mTvSalePercent.setText(mValuePercents[value]);
        mTvSalePercent.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen._18sdp));
        mEditedPost.setSalePercent(5 * (value + 1));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ConstantDefine.PICK_IMAGES_REQUEST) {
                ArrayList<String> imgUrls = data.getStringArrayListExtra(GlobalDefine.KEY_INTENT_PICK_IMAGES);
                mAdapterSelectedImage.addMoreImages(imgUrls);
                mRecyclerAddImages.smoothScrollToPosition(mAdapterSelectedImage.getItemCount() - 1);
            } else if (requestCode == REQUEST_CODE_CATEGORY) {
                mCategory = data.getParcelableExtra(GlobalDefine.KEY_INTENT_CATEGORY);
                mTvCate.setText(mCategory.getName());
                mLlSubCat.setVisibility(View.VISIBLE);
            } else if (requestCode == REQUEST_CODE_SUB_CATEGORY) {
                SubCategory subCategory = data.getParcelableExtra(GlobalDefine.KEY_INTENT_SELECT_SUB_CATEGORY);
                mTvSubCate.setText(subCategory.getName());
                mEditedPost.setProductId(subCategory.getId());
            }
        }
    }

    private class GenericTextWatcher implements TextWatcher {
        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String text = s.toString();
            switch (view.getId()) {
                case R.id.edtEditPostTitle:
                    if (!text.isEmpty()) {
                        mEditedPost.setPostTitle(text);
                    } else {
                        Toasty.warning(getContext(), "You must to enter Post's name!",
                                Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.edtEditPostAddress:
                    if (!text.isEmpty()) {
                        mEditedPost.setShopAddress(text);
                    } else {
                        Toasty.warning(getContext(), "You must to enter Shop's address!",
                                Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.edtEditPostDes:
                    if (!text.isEmpty()) {
                        mEditedPost.setPostDescribe(text);
                    } else {
                        Toasty.warning(getContext(), "You must to enter post description!",
                                Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    }
}
