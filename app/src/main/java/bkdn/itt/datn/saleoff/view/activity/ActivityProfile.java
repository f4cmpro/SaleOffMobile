package bkdn.itt.datn.saleoff.view.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.util.Calendar;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.mCloud.UploadImageCloud;
import bkdn.itt.datn.saleoff.model.model.User;
import bkdn.itt.datn.saleoff.model.net.NetworkUtils;
import bkdn.itt.datn.saleoff.presenter.PresenterProfile;
import bkdn.itt.datn.saleoff.utils.ImagesUtils;
import bkdn.itt.datn.saleoff.utils.InValidUtils;
import bkdn.itt.datn.saleoff.utils.SharedPreferencesUtils;
import bkdn.itt.datn.saleoff.utils.TimeUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.dialog.DialogChooseGender;
import bkdn.itt.datn.saleoff.view.dialog.DialogDatePicker;
import bkdn.itt.datn.saleoff.view.dialog.DialogErrorMessage;
import bkdn.itt.datn.saleoff.view.dialog.DialogSelectLocation;
import bkdn.itt.datn.saleoff.view.dialog.DialogSetEmail;
import bkdn.itt.datn.saleoff.view.dialog.DialogSetPhone;
import bkdn.itt.datn.saleoff.view.dialog.DialogSetUserName;
import bkdn.itt.datn.saleoff.view.dialog.DialogTimePicker;
import bkdn.itt.datn.saleoff.view.iface.IActivityProfile;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class ActivityProfile extends ActivityBase<PresenterProfile> implements IActivityProfile,
        DialogChooseGender.OnChooseGenderListener, DialogDatePicker.OnPickDateListener,
        DialogSetPhone.OnSetPhoneListener, DialogSetEmail.OnSetEmailListener,
        DialogSetUserName.OnSetUserNameListener {
    @BindView(R.id.imvProfileCover)
    ImageView mImvCover;
    @BindView(R.id.imvProfileAvatar)
    ImageView mImvAvatar;
    @BindView(R.id.tvProfileBirth)
    TextView mTvBirth;
    @BindView(R.id.tvProfileName)
    TextView mTvName;
    @BindView(R.id.tvProfileChangePass)
    TextView mTvChangePass;
    @BindView(R.id.tvProfileEmail)
    TextView mTvEmail;
    @BindView(R.id.tvProfileGender)
    TextView mTvGender;
    @BindView(R.id.tvProfilePhone)
    TextView mTvPhone;
    @BindView(R.id.tvProfileAddress)
    TextView mTvAddress;
    @BindView(R.id.btnProfileSave)
    Button mBtnSave;
    @BindView(R.id.imbProfileBack)
    ImageButton mImbBack;
    @BindView(R.id.llProfileUserName)
    LinearLayout mLlUserName;
    @BindView(R.id.llProfileGender)
    LinearLayout mLlGender;
    @BindView(R.id.llProfileBirthday)
    LinearLayout mLlBirthday;
    @BindView(R.id.llProfileAddress)
    LinearLayout mLlAddress;
    @BindView(R.id.llProfilePhoneNumber)
    LinearLayout mLlPhoneNumber;
    @BindView(R.id.llProfileEmail)
    LinearLayout mLlEmail;
    @BindView(R.id.prbProfileAvatar)
    ProgressBar mPrbAvatar;
    @BindView(R.id.prbProfileCover)
    ProgressBar mPrbCover;
    @BindView(R.id.flProfileInterested)
    FrameLayout mLlInterested;
    /*
    * Fields
    * */
    private User mUser;
    private boolean isSetAvatar;
    private UploadImageCloud mImageCloud;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        initValues();
        initActions();
    }


    private void initValues() {
        mImageCloud = UploadImageCloud.getInstance();
        mUser = SharedPreferencesUtils.getInstance(getContext()).getUserProfile();
        if (null != mUser) {
            if (mUser.getAvatar() != null) {
                Glide.with(this).load(mUser.getAvatar()).into(mImvAvatar);
            }
            if (mUser.getCover() != null) {
                Glide.with(this).load(mUser.getCover()).into(mImvCover);
            }
            String fullName = mUser.getUsername();
            mTvName.setText(fullName != null && !fullName.isEmpty() ? fullName : getString(R.string.error_not_setup_yet));
            String birthday = mUser.getBirthday();
            mTvBirth.setText(birthday != null && !birthday.isEmpty() ? birthday : getString(R.string.error_not_setup_yet));
            int gender = mUser.getGender();
            mTvGender.setText(gender == ConstantDefine.MALE ? getString(R.string.male) : getString(R.string.female));
            String email = mUser.getEmail();
            mTvEmail.setText(email != null && !email.isEmpty() ? email : getString(R.string.error_not_setup_yet));
            String phone = mUser.getPhone();
            mTvPhone.setText(phone != null && !phone.isEmpty() ? phone : getString(R.string.error_not_setup_yet));
            String address = mUser.getAddress();
            mTvAddress.setText(address != null && !address.isEmpty() ? address : getString(R.string.error_not_setup_yet));
        }
    }

    private void initActions() {
        mImvCover.setOnClickListener(v -> {
            isSetAvatar = false;
            requestRuntimePermission();
        });
        mImvAvatar.setOnClickListener(v -> {
            isSetAvatar = true;
            requestRuntimePermission();
        });
        mLlUserName.setOnClickListener(v -> DialogSetUserName.newInstance(mTvName.getText().toString(),
                ActivityProfile.this, getSupportFragmentManager()));
        mLlGender.setOnClickListener(v -> DialogChooseGender.newInstance(ActivityProfile.this,
                getSupportFragmentManager()));
        mLlBirthday.setOnClickListener(v -> DialogDatePicker.newInstance(this, getSupportFragmentManager()
                , 0, ""));
        mLlAddress.setOnClickListener(v -> {
            DialogSelectLocation dialogSelectLocation = DialogSelectLocation.newInstance();
            dialogSelectLocation.setListener(city -> {
                mUser.setAddress(city);
                mTvAddress.setText(city);
            });
            dialogSelectLocation.show(getSupportFragmentManager(), null);


        });
        mLlPhoneNumber.setOnClickListener(v -> DialogSetPhone.newInstance(mTvPhone.getText().toString(),
                this, getSupportFragmentManager()));
        mLlEmail.setOnClickListener(v -> DialogSetEmail.newInstance(mTvEmail.getText().toString(),
                this, getSupportFragmentManager()));
        mLlInterested.setOnClickListener(v -> startActivity(ActivityChangeInterested.class));
        mBtnSave.setOnClickListener((View v) -> getPresenter(this).editProfile(mUser));
        mImbBack.setOnClickListener(v -> onBackPressed());
    }


    private void requestRuntimePermission() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE}, ConstantDefine.READ_EXTERNAL_SDCARD_REQUEST);

        } else {
            ImagesUtils.openGallery(this);
        }
    }


    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
    }


    @Override
    public void editProfileSuccess(User user) {
        SharedPreferencesUtils.getInstance(getContext()).setUserProfile(user);
        Toasty.success(getContext(), getString(R.string.edit_success), Toast.LENGTH_SHORT).show();
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void editProfileFail(String string) {
        Toasty.error(getContext(), string, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onChooseGender(int gender) {
        mTvGender.setText(gender == ConstantDefine.MALE ? getString(R.string.male) : getString(R.string.female));
        mUser.setGender(gender);
    }


    @Override
    public void onPickDate(int day, int month, int year) {
        Calendar birthDay = Calendar.getInstance();
        birthDay.set(Calendar.YEAR, year);
        birthDay.set(Calendar.MONTH, month);
        birthDay.set(Calendar.DAY_OF_MONTH, day);
        mTvBirth.setText(TimeUtils.parseTimestampToString(birthDay.getTimeInMillis(), GlobalDefine.FORMAT_DATE_THIRD_DEFAULT));
        mUser.setBirthday(TimeUtils.parseTimestampToString(birthDay.getTimeInMillis(), GlobalDefine.FORMAT_DATE_SECOND_DEFAULT));
    }

    @Override
    public void onSetPhone(String phoneNumber) {
        if (InValidUtils.isValidPhone(phoneNumber)) {
            mTvPhone.setText(phoneNumber);
            mUser.setPhone(phoneNumber);
        } else {
            if (mUser.getPhone() != null && !mUser.getPhone().isEmpty()) {
                mTvPhone.setText(mUser.getPhone());
            } else {
                mTvPhone.setText(getString(R.string.error_not_setup_yet));
            }
            Toasty.warning(getContext(), getString(R.string.error_not_valid_phone_number)).show();
        }

    }

    @Override
    public void onSetEmail(String email) {
        if (InValidUtils.isValidEmail(email)) {
            mTvEmail.setText(email);
            mUser.setEmail(email);
        } else {
            if (mUser.getEmail() != null && !mUser.getEmail().isEmpty()) {
                mTvEmail.setText(mUser.getEmail());
            } else {
                mTvEmail.setText(getString(R.string.error_not_setup_yet));
            }
            Toasty.warning(getContext(), getString(R.string.error_not_valid_email)).show();
        }
    }

    @Override
    public void onSetUserName(String userName) {
        if (InValidUtils.isValidUserName(userName)) {
            mTvName.setText(userName);
            mUser.setUsername(userName);
        } else {
            if (mUser.getUsername() != null && !mUser.getUsername().isEmpty()) {
                mTvName.setText(mUser.getUsername());
            } else {
                mTvName.setText(getString(R.string.error_not_setup_yet));
            }
            Toasty.warning(getContext(), getString(R.string.error_not_valid_user_name)).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == ConstantDefine.READ_EXTERNAL_SDCARD_REQUEST) {
            ImagesUtils.openGallery(this);
        } else {
            Toasty.warning(getContext(), "Please grant me permission to upload your avatar!!!");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ConstantDefine.PICK_IMAGE_REQUEST) {
                File realFile = ImagesUtils.getRealFileFromUri(this, data.getData());
                if (realFile.exists()) {
                    if (isSetAvatar) {
                        mPrbAvatar.setVisibility(View.VISIBLE);
                        File compressImage = ImagesUtils.compressAvatarImage(getContext(), realFile);
                        mImageCloud.uploadUserAvatar(getContext(), compressImage, new UploadImageCloud.OnUpLoadAvatarListener() {
                            @Override
                            public void onUploadAvatar(String avatarUrl) {
                                Glide.with(getContext()).load(realFile.getPath()).into(mImvAvatar);
                                mPrbAvatar.setVisibility(View.GONE);
                                mUser.setAvatar(avatarUrl);
                            }

                            @Override
                            public void onUpLoadFail(String errorMessage) {
                                Toasty.warning(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
                                mPrbAvatar.setVisibility(View.GONE);
                            }
                        });
                    } else {
                        mPrbCover.setVisibility(View.VISIBLE);
                        File compressImage = ImagesUtils.compressCoverImage(getContext(), realFile);
                        mImageCloud.uploadUserCover(getContext(), compressImage, new UploadImageCloud.OnUploadCoverListener() {
                            @Override
                            public void onUploadCover(String coverUrl) {
                                Glide.with(getContext()).load(realFile.getPath()).into(mImvCover);
                                mPrbCover.setVisibility(View.GONE);
                                mUser.setCover(coverUrl);
                            }

                            @Override
                            public void onUpLoadFail(String errorMessage) {
                                Toasty.warning(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
                                mPrbCover.setVisibility(View.GONE);
                            }
                        });
                    }
                }
            }
        } else {
            Toast.makeText(getContext(), "You haven't picked Image", Toast.LENGTH_LONG).show();
        }
    }


}
