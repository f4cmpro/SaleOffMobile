package bkdn.itt.datn.saleoff.view.activity;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.presenter.PresenterSearch;
import bkdn.itt.datn.saleoff.view.fragment.FragmentSearchPost;
import bkdn.itt.datn.saleoff.view.fragment.FragmentSearchShop;
import bkdn.itt.datn.saleoff.view.fragment.FragmentSearchUser;
import bkdn.itt.datn.saleoff.view.iface.IActivitySearch;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivitySearch extends ActivityBase<PresenterSearch> implements IActivitySearch {
    @BindView(R.id.imbSearchBack)
    ImageButton mImbBack;
    @BindView(R.id.edtSearchInput)
    EditText mEdtInputText;
    @BindView(R.id.tvSearchPost)
    TextView mTvSearchPost;
    @BindView(R.id.tvSearchShop)
    TextView mTvSearchShop;
    @BindView(R.id.tvSearchUser)
    TextView mTvSearchUser;
    @BindView(R.id.underViewSearchPost)
    View mUnderViewPost;
    @BindView(R.id.underViewSearchShop)
    View mUnderViewShop;
    @BindView(R.id.underViewSearchUser)
    View mUnderViewUser;
    @BindView(R.id.flSearchContain)
    FrameLayout mFlContain;

    private static final int SEARCH_POST = 0;
    private static final int SEARCH_SHOP = 1;
    private static final int SEARCH_USER = 2;
    private String mInputKey;
    private int mStatus; //search post|shop|user
    private OnSearchPostListener mOnSearchPostListener;
    private OnSearchShopListener mOnSearchShopListener;
    private OnSearchUserListener mOnSearchUserListener;
    private FragmentSearchPost mFragmentSearchPost;
    private FragmentSearchShop mFragmentSearchShop;
    private FragmentSearchUser mFragmentSearchUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        initViews();
        initActions();
    }

    private void initViews() {
        mEdtInputText.setRawInputType(InputType.TYPE_CLASS_TEXT);
        mFragmentSearchPost = FragmentSearchPost.newInstance();
        mFragmentSearchShop = FragmentSearchShop.newInstance();
        mFragmentSearchUser = FragmentSearchUser.newInstance();
        replaceFragment(mFragmentSearchPost, mFlContain.getId(), false);
    }

    private void initActions() {
        mImbBack.setOnClickListener(v -> onBackPressed());
        mEdtInputText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mInputKey = s.toString().trim();
            }
        });
        mEdtInputText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                switch (mStatus) {
                    case SEARCH_POST:
                        mFragmentSearchPost.searchPost(mInputKey);
                        break;
                    case SEARCH_SHOP:
                        mFragmentSearchShop.searchShop(mInputKey);
                        break;
                    case SEARCH_USER:
                        mFragmentSearchUser.searchUser(mInputKey);
                        break;
                }
                return true;
            }
            return false;
        });
        mTvSearchPost.setOnClickListener(v -> {
            mTvSearchPost.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            mUnderViewPost.setVisibility(View.VISIBLE);
            mTvSearchShop.setTextColor(getResources().getColor(R.color.colorGray600));
            mUnderViewShop.setVisibility(View.GONE);
            mTvSearchUser.setTextColor(getResources().getColor(R.color.colorGray600));
            mUnderViewUser.setVisibility(View.GONE);
            mStatus = SEARCH_POST;
            replaceFragment(mFragmentSearchPost, mFlContain.getId(), false);
        });
        mTvSearchShop.setOnClickListener(v -> {
            mTvSearchPost.setTextColor(getResources().getColor(R.color.colorGray600));
            mUnderViewPost.setVisibility(View.GONE);
            mTvSearchShop.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            mUnderViewShop.setVisibility(View.VISIBLE);
            mTvSearchUser.setTextColor(getResources().getColor(R.color.colorGray600));
            mUnderViewUser.setVisibility(View.GONE);
            mStatus = SEARCH_SHOP;
            replaceFragment(mFragmentSearchShop, mFlContain.getId(), false);

        });
        mTvSearchUser.setOnClickListener(v -> {
            mTvSearchPost.setTextColor(getResources().getColor(R.color.colorGray600));
            mUnderViewPost.setVisibility(View.GONE);
            mTvSearchShop.setTextColor(getResources().getColor(R.color.colorGray600));
            mUnderViewShop.setVisibility(View.GONE);
            mTvSearchUser.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            mUnderViewUser.setVisibility(View.VISIBLE);
            mStatus = SEARCH_USER;
            replaceFragment(mFragmentSearchUser, mFlContain.getId(), false);
        });
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    public interface OnSearchPostListener {
        void onSearchPost();
    }

    public interface OnSearchShopListener {
        void onSearchShop();
    }

    public interface OnSearchUserListener {
        void onSearchUser();
    }
}
