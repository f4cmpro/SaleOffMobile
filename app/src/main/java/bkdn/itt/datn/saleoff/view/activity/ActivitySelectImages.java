package bkdn.itt.datn.saleoff.view.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageButton;

import java.util.ArrayList;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.view.adapter.AdapterSelectImage;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivitySelectImages extends AppCompatActivity implements AdapterSelectImage.OnSelectedImageListener {

    /*
    * Widgets
    * */
    @BindView(R.id.recyclerSelectImage)
    RecyclerView mRecyclerSelectImage;
    @BindView(R.id.imbSelectImageDone)
    ImageButton mImbDone;
    @BindView(R.id.imbSelectImageBack)
    ImageButton mImbBack;
    /*
    * Fields
    * */
    private AdapterSelectImage mAdapterSelectImage;
    private ArrayList<String> mImgUrls;
    private OnSelectedImagesListener mListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_images);
        ButterKnife.bind(this);
        initViews();
        initValues();
        initActions();
    }

    private void initViews() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE}, ConstantDefine.READ_EXTERNAL_SDCARD_REQUEST);
        }else {
            mAdapterSelectImage = new AdapterSelectImage(this);
            RecyclerViewUtils.Create().setUpGrid(this, mRecyclerSelectImage, 3).setAdapter(mAdapterSelectImage);
        }

    }

    private void initValues() {
        mImgUrls = new ArrayList<>();
    }

    private void initActions() {
        mImbBack.setOnClickListener(v -> onBackPressed());
        mImbDone.setOnClickListener(v -> {
            Intent pickImagesIntent = getIntent();
            pickImagesIntent.putStringArrayListExtra(GlobalDefine.KEY_INTENT_PICK_IMAGES, mImgUrls);
            setResult(RESULT_OK, pickImagesIntent);
            this.finish();
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == ConstantDefine.READ_EXTERNAL_SDCARD_REQUEST) {
            mAdapterSelectImage = new AdapterSelectImage(this);
            RecyclerViewUtils.Create().setUpGrid(this, mRecyclerSelectImage, 3).setAdapter(mAdapterSelectImage);
        } else {
            onBackPressed();
        }
    }

    @Override
    public void onSelectedImage(String url, boolean isSelected) {
        if (isSelected) {
            mImgUrls.add(url);
        } else {
            mImgUrls.remove(url);
        }
    }

    public interface OnSelectedImagesListener {
        void onSelectedImagesListener(String[] urls);
    }
}
