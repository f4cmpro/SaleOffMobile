package bkdn.itt.datn.saleoff.view.activity;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.model.Post;
import bkdn.itt.datn.saleoff.model.model.User;
import bkdn.itt.datn.saleoff.presenter.PresenterPostShare;
import bkdn.itt.datn.saleoff.utils.SharedPreferencesUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.iface.IActivityShare;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class ActivityShare extends ActivityBase<PresenterPostShare>
        implements IActivityShare {
    /*
    * Widgets
    * */
    @BindView(R.id.imvShareAvatar)
    ImageView mImvShareUserAvatar;
    @BindView(R.id.tvShareName)
    TextView mTvShareUserName;
    @BindView(R.id.edtShareDescription)
    EditText mEdtShareDes;
    @BindView(R.id.imvItemShareAvatar)
    ImageView mImvSharedAuthorAvatar;
    @BindView(R.id.imvItemShareCover)
    ImageView mImvShareAuthorCover;
    @BindView(R.id.tvItemShareUserName)
    TextView mTvSharedUserName;
    @BindView(R.id.tvItemShareCreatedAt)
    TextView mTvSharedCreatedAt;
    @BindView(R.id.tvItemShareTitle)
    TextView mTvSharedTitle;
    @BindView(R.id.tvItemShareSalePercent)
    TextView mTvSharedPercent;
    @BindView(R.id.tvItemShareStartDate)
    TextView mTvSharedStart;
    @BindView(R.id.tvItemShareEndDate)
    TextView mTvSharedEnd;
    @BindView(R.id.tvItemShareAddress)
    TextView mTvSharedAddress;
    @BindView(R.id.tvShareDone)
    TextView mTvShareDone;
    @BindView(R.id.imbShareBack)
    ImageButton mImbBack;
    private Post mSharedPost;
    private User me;
    private boolean isSharedShop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);
        ButterKnife.bind(this);
        me = SharedPreferencesUtils.getInstance(getContext()).getUserProfile();
        mSharedPost = getIntent().getParcelableExtra(GlobalDefine.KEY_INTENT_POST);
        if (mSharedPost != null) {
            isSharedShop = mSharedPost.getShop() != null ? true : false;
            initViews();
            initActions();
        }
    }

    private void initViews() {
        //set avatar of me
        Glide.with(getContext()).load(me.getAvatar()).into(mImvShareUserAvatar);
        //set name of me
        mTvShareUserName.setText(me.getUsername());
        if (isSharedShop) {
            //set name of me and author name
            mTvSharedUserName.setText(Html.fromHtml(mSharedPost.getShop().getName()));
            //set author avatar for shared post
            Glide.with(getContext()).load(mSharedPost.getShop().getAvatar()).into(mImvSharedAuthorAvatar);
            //set author name for shared post
            Glide.with(getContext()).load(mSharedPost.getUser().getUserName());
        } else {
            //set name of me and author name
            mTvSharedUserName.setText(Html.fromHtml(mSharedPost.getUser().getUserName()));
            //set author avatar for shared post
            Glide.with(getContext()).load(mSharedPost.getUser().getAvatar()).into(mImvSharedAuthorAvatar);
            //set author name for shared post
            Glide.with(getContext()).load(mSharedPost.getUser().getUserName());
        }
        //set created at of share post
        mTvSharedCreatedAt.setText(mSharedPost.getCreatedDate());
        //set title of share post
        mTvSharedTitle.setText(mSharedPost.getPostTitle());
        //set sale percent of share post
        mTvSharedPercent.setText(mSharedPost.getSalePercent() + " %");
        //set sale date
        mTvSharedStart.setText(mSharedPost.getFromDate());
        mTvSharedEnd.setText(mSharedPost.getToDate());
        //set address
        mTvSharedAddress.setText(mSharedPost.getShopAddress());
        //set cover of shared post
        Glide.with(getContext()).load(mSharedPost.getCover()).into(mImvShareAuthorCover);
    }

    private void initActions() {
        mTvShareDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String desription = mEdtShareDes.getText().toString().trim();
                getPresenter(ActivityShare.this).share(desription, mSharedPost.getPostId());
            }
        });

        mImbBack.setOnClickListener(v -> onBackPressed());

    }


    @Override
    public void shareSuccess() {
        Toasty.info(getContext(), getString(R.string.share_success), Toast.LENGTH_SHORT).show();
        onBackPressed();
    }

    @Override
    public void shareError(String errorMessage) {
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
    }
}
