package bkdn.itt.datn.saleoff.view.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBufferResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.tasks.Task;

import java.io.File;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.mCloud.UploadImageCloud;
import bkdn.itt.datn.saleoff.model.model.Shop;
import bkdn.itt.datn.saleoff.model.model.Category;
import bkdn.itt.datn.saleoff.presenter.PresenterShopCreate;
import bkdn.itt.datn.saleoff.utils.ImagesUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.adapter.AdapterSelectedImage;
import bkdn.itt.datn.saleoff.view.adapter.PlaceAutocompleteAdapter;
import bkdn.itt.datn.saleoff.view.iface.IActivityShopCreate;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

/**
 * Created by TuDLT on 3/18/2018.
 */
public class ActivityShopCreate extends ActivityBase<PresenterShopCreate> implements
        IActivityShopCreate {
    private static final int REQUEST_READ_EXTERNAL_SDCARD_CODE = 1;
    private static final String TAG = "ActivityShopCreate";
    /*
    * Widgets
    * */
    @BindView(R.id.imvCreateShopCover)
    ImageView mImvCover;
    @BindView(R.id.imvCreateShopAvatar)
    ImageView mImvAvatar;
    @BindView(R.id.edtCreateShopName)
    EditText mEdtShopName;
    @BindView(R.id.tvCreateShopCategory)
    TextView mTvCategory;
    @BindView(R.id.edtCreateShopAddress)
    AutoCompleteTextView mEdtAddress;
    @BindView(R.id.edtCreateShopWeb)
    EditText mEdtWeb;
    @BindView(R.id.edtCreateShopPhone)
    EditText mEdtPhone;
    @BindView(R.id.edtCreateShopDes)
    EditText mEdtDes;
    @BindView(R.id.imbCreateShopBack)
    ImageButton mImbBack;
    @BindView(R.id.imbCreateShopDone)
    ImageButton mImbDone;
    @BindView(R.id.prbCreateShopAvatar)
    ProgressBar mPrbLoadAvatar;
    @BindView(R.id.prbCreateShopCover)
    ProgressBar mPrbLoadCover;

    /*
    * Fields
    * */
    private PlaceAutocompleteAdapter mPlaceAutocompleteAdapter;
    private AdapterSelectedImage mAdapterSelectedImage;
    private AutocompleteFilter mAutocompleteFilter;
    private GeoDataClient mGeoDataClient;
    private Shop mCreatedShop;
    private UploadImageCloud mImageCloud;
    private boolean isSetAvatar;


    private class GenericTextWatcher implements TextWatcher {
        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String text = s.toString();
            switch (view.getId()) {
                case R.id.edtCreateShopName:
                    if (text != null && !text.isEmpty()) {
                        mCreatedShop.setName(text);
                    } else {
                        Toasty.warning(getContext(), getString(R.string.warning_empty), Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.edtCreateShopPhone:
                    if (text != null && !text.isEmpty()) {
                        mCreatedShop.setPhone(text);
                    } else {
                        Toasty.warning(getContext(), getString(R.string.warning_empty), Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.edtCreateShopWeb:
                    mCreatedShop.setWeb(text);
                    break;
                case R.id.edtCreateShopDes:
                    mCreatedShop.setDescription(text);
                    break;
            }
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_create);
        ButterKnife.bind(this);
        initValues();
        initViews();
        initActions();
    }

    private void initViews() {
        mAutocompleteFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS)
                .setCountry("VN")
                .build();
        mGeoDataClient = Places.getGeoDataClient(this, null);
        mPlaceAutocompleteAdapter = new PlaceAutocompleteAdapter(this, mGeoDataClient,
                null, mAutocompleteFilter);
        mEdtAddress.setAdapter(mPlaceAutocompleteAdapter);
        mEdtShopName.setRawInputType(InputType.TYPE_CLASS_TEXT);
        mEdtPhone.setRawInputType(InputType.TYPE_CLASS_PHONE);
        mEdtWeb.setRawInputType(InputType.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS);
        mEdtDes.setRawInputType(InputType.TYPE_CLASS_TEXT);

    }

    private void initValues() {
        mImageCloud = UploadImageCloud.getInstance();
        mCreatedShop = new Shop();
    }

    private void initActions() {
        //Created value for shop
        mEdtShopName.addTextChangedListener(new GenericTextWatcher(mEdtShopName));
        mEdtPhone.addTextChangedListener(new GenericTextWatcher(mEdtPhone));
        mEdtWeb.addTextChangedListener(new GenericTextWatcher(mEdtWeb));
        mEdtDes.addTextChangedListener(new GenericTextWatcher(mEdtDes));
        mEdtAddress.setOnItemClickListener((parent, view, position, id) -> {
            Utils.hideSoftKeyboard(ActivityShopCreate.this);
            final AutocompletePrediction item = mPlaceAutocompleteAdapter.getItem(position);
            Task<PlaceBufferResponse> placeResult = mGeoDataClient.getPlaceById(item.getPlaceId());
            placeResult.addOnCompleteListener(task -> {
                PlaceBufferResponse places = task.getResult();
                Place place = places.get(0);
                Log.i(TAG, place.getAddress().toString());
                mCreatedShop.setAddress(place.getAddress().toString());
            });
        });

        mTvCategory.setOnClickListener(v ->
                startActivityForResult(new Intent(this, ActivityCategory.class),
                        ConstantDefine.SELECT_CATEGORY_REQUEST));
        mImvCover.setOnClickListener(v -> {
            isSetAvatar = false;
            requestRuntimePermission();
        });
        mImvAvatar.setOnClickListener(v -> {
            isSetAvatar = true;
            requestRuntimePermission();
        });
        mImbDone.setOnClickListener(v -> getPresenter(ActivityShopCreate.this).createShop(mCreatedShop));
        mImbBack.setOnClickListener(v -> onBackPressed());
    }

    private void requestRuntimePermission() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_READ_EXTERNAL_SDCARD_CODE);
        } else {
            ImagesUtils.openGallery(this);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_READ_EXTERNAL_SDCARD_CODE) {
            ImagesUtils.openGallery(this);
        } else {
            Toasty.warning(getContext(), "Please grant me permission to add Avatar!!!");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ConstantDefine.SELECT_CATEGORY_REQUEST) {
                Category category = data.getParcelableExtra(GlobalDefine.KEY_INTENT_CATEGORY);
                if (category != null) {
                    mTvCategory.setText(category.getName());
                    mCreatedShop.setCateId(category.getId());
                }
            } else if (requestCode == ConstantDefine.PICK_IMAGE_REQUEST) {
                File realFile = ImagesUtils.getRealFileFromUri(this, data.getData());
                if (realFile.exists()) {
                    if (isSetAvatar) {
                        mPrbLoadAvatar.setVisibility(View.VISIBLE);
                        File compressAvatar = ImagesUtils.compressAvatarImage(getContext(), realFile);
                        mImageCloud.uploadUserAvatar(getContext(), compressAvatar, new UploadImageCloud.OnUpLoadAvatarListener() {
                            @Override
                            public void onUploadAvatar(String avatarUrl) {
                                Glide.with(getContext()).load(realFile.getPath()).into(mImvAvatar);
                                mPrbLoadAvatar.setVisibility(View.GONE);
                                mCreatedShop.setAvatar(avatarUrl);
                            }

                            @Override
                            public void onUpLoadFail(String errorMessage) {
                                Toasty.warning(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
                                mPrbLoadAvatar.setVisibility(View.GONE);
                            }
                        });
                    } else {
                        mPrbLoadCover.setVisibility(View.VISIBLE);
                        File compressCover = ImagesUtils.compressCoverImage(getContext(), realFile);
                        mImageCloud.uploadUserCover(getContext(), compressCover, new UploadImageCloud.OnUploadCoverListener() {
                            @Override
                            public void onUploadCover(String coverUrl) {
                                Glide.with(getContext()).load(realFile.getPath()).into(mImvCover);
                                mPrbLoadCover.setVisibility(View.GONE);
                                mCreatedShop.setCover(coverUrl);
                            }

                            @Override
                            public void onUpLoadFail(String errorMessage) {
                                Toasty.warning(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
                                mPrbLoadCover.setVisibility(View.GONE);
                            }
                        });
                    }
                }
            }
        }
    }

    @Override
    public void createShopSuccess(Shop shop) {
        Intent intent = getIntent().putExtra(GlobalDefine.KEY_INTENT_CREATE_SHOP, shop);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void getErrorMessage(String errorMessage) {
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(this);
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
    }

}
