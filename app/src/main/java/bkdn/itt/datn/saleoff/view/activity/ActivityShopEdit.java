package bkdn.itt.datn.saleoff.view.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.util.Arrays;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.mCloud.UploadImageCloud;
import bkdn.itt.datn.saleoff.model.model.Category;
import bkdn.itt.datn.saleoff.model.model.Shop;
import bkdn.itt.datn.saleoff.presenter.PresenterShopEdit;
import bkdn.itt.datn.saleoff.utils.ImagesUtils;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.adapter.AdapterSelectedImage;
import bkdn.itt.datn.saleoff.view.iface.IActivityShopEdit;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class ActivityShopEdit extends ActivityBase<PresenterShopEdit> implements IActivityShopEdit {
    private static final int REQUEST_READ_EXTERNAL_SDCARD_CODE = 1;
    /*
    * Widgets
    * */
    @BindView(R.id.imvEditShopCover)
    ImageView mImvCover;
    @BindView(R.id.imvEditShopAvatar)
    ImageView mImvAvatar;
    @BindView(R.id.edtEditShopName)
    EditText mEdtShopName;
    @BindView(R.id.edtEditShopAddress)
    EditText mEdtAddress;
    @BindView(R.id.edtEditShopWeb)
    EditText mEdtWeb;
    @BindView(R.id.edtEditShopPhone)
    EditText mEdtPhone;
    @BindView(R.id.edtEditShopDes)
    EditText mEdtDes;
    @BindView(R.id.imbEditShopBack)
    ImageButton mImbBack;
    @BindView(R.id.imbEditShopDone)
    ImageButton mImbDone;
    @BindView(R.id.prbEditShopAvatar)
    ProgressBar mPrbLoadAvatar;
    @BindView(R.id.prbEditShopCover)
    ProgressBar mPrbLoadCover;

    /*
    * Fields
    * */
    private Shop mEditedShop;
    private UploadImageCloud mImageCloud;
    private boolean isSetAvatar;


    private class GenericTextWatcher implements TextWatcher {
        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String text = s.toString();
            switch (view.getId()) {
                case R.id.edtEditShopName:
                    if (text != null && !text.isEmpty()) {
                        mEditedShop.setName(text);
                    } else {
                        Toasty.warning(getContext(), getString(R.string.warning_empty), Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.edtEditShopAddress:
                    if (text != null && !text.isEmpty()) {
                        mEditedShop.setAddress(text);
                    } else {
                        Toasty.warning(getContext(), getString(R.string.warning_empty), Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.edtEditShopPhone:
                    if (text != null && !text.isEmpty()) {
                        mEditedShop.setPhone(text);
                    } else {
                        Toasty.warning(getContext(), getString(R.string.warning_empty), Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.edtEditShopWeb:
                    mEditedShop.setWeb(text);
                    break;
                case R.id.edtEditShopDes:
                    mEditedShop.setDescription(text);
                    break;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_edit);
        ButterKnife.bind(this);
        mImageCloud = UploadImageCloud.getInstance();
        initViews();
        initActions();
    }

    private void initViews() {
        mEditedShop = getIntent().getParcelableExtra(GlobalDefine.KEY_INTENT_SHOP);
        if (mEditedShop != null) {
            //init cover
            if(mEditedShop.getCover() != null) {
                Glide.with(getContext()).load(mEditedShop.getCover()).into(mImvCover);
            }

            //init avatar
            if(mEditedShop.getAvatar() != null) {
                Glide.with(getContext()).load(mEditedShop.getAvatar()).into(mImvAvatar);
            }

            //init Shop name
            mEdtShopName.setText(mEditedShop.getName());

            //init Shop Address
            mEdtAddress.setText(mEditedShop.getAddress());

            //init Shop Description
            mEdtDes.setText(mEditedShop.getDescription());

            //init Shop Phone
            mEdtPhone.setText(mEditedShop.getPhone());

        }
    }

    private void initActions() {
        //Edited value for shop
        mEdtShopName.addTextChangedListener(new ActivityShopEdit.GenericTextWatcher(mEdtShopName));
        mEdtAddress.addTextChangedListener(new ActivityShopEdit.GenericTextWatcher(mEdtAddress));
        mEdtPhone.addTextChangedListener(new ActivityShopEdit.GenericTextWatcher(mEdtPhone));
        mEdtWeb.addTextChangedListener(new ActivityShopEdit.GenericTextWatcher(mEdtWeb));
        mEdtDes.addTextChangedListener(new ActivityShopEdit.GenericTextWatcher(mEdtDes));

        mImvCover.setOnClickListener(v -> {
            isSetAvatar = false;
            requestRuntimePermission();
        });
        mImvAvatar.setOnClickListener(v -> {
            isSetAvatar = true;
            requestRuntimePermission();
        });
        mImbDone.setOnClickListener(v -> getPresenter(ActivityShopEdit.this).editShop(mEditedShop));
        mImbBack.setOnClickListener(v -> onBackPressed());
    }

    private void requestRuntimePermission() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_READ_EXTERNAL_SDCARD_CODE);
        } else {
            ImagesUtils.openGallery(this);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_READ_EXTERNAL_SDCARD_CODE) {
            ImagesUtils.openGallery(this);
        } else {
            Toasty.warning(getContext(), "Please grant me permission to add Avatar!!!");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ConstantDefine.PICK_IMAGE_REQUEST) {
                File realFile = ImagesUtils.getRealFileFromUri(this, data.getData());
                if (realFile.exists()) {
                    if (isSetAvatar) {
                        mPrbLoadAvatar.setVisibility(View.VISIBLE);
                        File compressAvatar = ImagesUtils.compressAvatarImage(getContext(), realFile);
                        mImageCloud.uploadUserAvatar(getContext(), compressAvatar, new UploadImageCloud.OnUpLoadAvatarListener() {
                            @Override
                            public void onUploadAvatar(String avatarUrl) {
                                Glide.with(getContext()).load(realFile.getPath()).into(mImvAvatar);
                                mPrbLoadAvatar.setVisibility(View.GONE);
                                mEditedShop.setAvatar(avatarUrl);
                            }

                            @Override
                            public void onUpLoadFail(String errorMessage) {
                                Toasty.warning(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
                                mPrbLoadAvatar.setVisibility(View.GONE);
                            }
                        });
                    } else {
                        mPrbLoadCover.setVisibility(View.VISIBLE);
                        File compressCover = ImagesUtils.compressCoverImage(getContext(), realFile);
                        mImageCloud.uploadUserCover(getContext(), compressCover, new UploadImageCloud.OnUploadCoverListener() {
                            @Override
                            public void onUploadCover(String coverUrl) {
                                Glide.with(getContext()).load(realFile.getPath()).into(mImvCover);
                                mPrbLoadCover.setVisibility(View.GONE);
                                mEditedShop.setCover(coverUrl);
                            }

                            @Override
                            public void onUpLoadFail(String errorMessage) {
                                Toasty.warning(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
                                mPrbLoadCover.setVisibility(View.GONE);
                            }
                        });
                    }
                }
            }
        }
    }

    @Override
    public void editShopSuccess() {
        Intent intent = getIntent().putExtra(GlobalDefine.KEY_INTENT_SHOP, mEditedShop);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void getError(String errorMessage) {
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
    }
}
