package bkdn.itt.datn.saleoff.view.activity;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.fcm.NotificationBody;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.view.adapter.AdapterMyPagePager;
import bkdn.itt.datn.saleoff.view.fragment.FragmentBase;
import bkdn.itt.datn.saleoff.view.fragment.FragmentMyShop;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class ActivityShopList extends ActivityBase {
    /*
    * Widgets
    * */
    @BindView(R.id.tabLayoutMyPage)
    TabLayout mTabLayoutMyPage;
    @BindView(R.id.viewPagerMyPage)
    ViewPager mViewPagerMyPage;
    @BindView(R.id.imbShopListBack)
    ImageButton mImbBack;
    /*
    * Fields
    * */
    private static final int MY_SHOP = 0;
    private static final int WORKING_SHOP = 1;
    private AdapterMyPagePager mAdapterMyPagePager;
    private List<FragmentBase> mFragmentBases;
    private boolean mIsMyShop;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_list);
        ButterKnife.bind(this);
        initValues();
        initViews();
    }

    private void initValues() {
        String data = null;
        if (getIntent().getStringExtra(GlobalDefine.KEY_BUNDLE_NOTIFICATION) != null) {
            data = getIntent().getStringExtra(GlobalDefine.KEY_BUNDLE_NOTIFICATION);
        } else if (getIntent().getStringExtra(GlobalDefine.ROLE) != null) {
            data = getIntent().getStringExtra(GlobalDefine.ROLE);
        }
        mIsMyShop = data.equals(GlobalDefine.OWNER_SHOP) ? true : false;
    }

    private void initViews() {
        mFragmentBases = new ArrayList<>();
        mFragmentBases.add(FragmentMyShop.newInstance(true));
        mFragmentBases.add(FragmentMyShop.newInstance(false));
        mAdapterMyPagePager = new AdapterMyPagePager(this, getSupportFragmentManager(), mFragmentBases);
        mViewPagerMyPage.setAdapter(mAdapterMyPagePager);
        mTabLayoutMyPage.setupWithViewPager(mViewPagerMyPage);
        if (mIsMyShop) {
            mViewPagerMyPage.setCurrentItem(MY_SHOP);
        } else {
            mViewPagerMyPage.setCurrentItem(WORKING_SHOP);
        }
        mImbBack.setOnClickListener(v -> onBackPressed());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == ConstantDefine.CREATE_SHOP_REQUEST){
                Toasty.success(this, "Cửa hàng đang được phê duyệt!", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
