package bkdn.itt.datn.saleoff.view.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.model.UserInfo;
import bkdn.itt.datn.saleoff.presenter.PresenterEmployeeAdd;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.adapter.AdapterShopEmployeeAdd;
import bkdn.itt.datn.saleoff.view.iface.IActivityAddMember;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

/**
 * Created by TuDLT on 3/23/2018.
 */
public class ActivityShopMemberAdd extends ActivityBase<PresenterEmployeeAdd> implements IActivityAddMember,
        AdapterShopEmployeeAdd.OnChosenMemberListener {
    /*
    * Widgets
    * */
    @BindView(R.id.recyclerAddMember)
    RecyclerView mRecyclerAddMember;
    @BindView(R.id.edtAddMemberSearch)
    EditText mEdtSearch;
    @BindView(R.id.imbAddMemberDone)
    ImageButton mImbDone;
    @BindView(R.id.imbAddMemberBack)
    ImageButton mImbBack;
    @BindView(R.id.llAddMemberListAdd)
    LinearLayout mLlListAdd;

    /*
    * Fields
    * */
    private AdapterShopEmployeeAdd mAdapterShopEmployeeAdd;
    private int mShopId;
    private List<UserInfo> mUserInfoList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_member_add);
        ButterKnife.bind(this);
        initValues();
        initViews();
        initActions();
    }

    private void initValues() {
        mShopId = getIntent().getIntExtra(GlobalDefine.KEY_INTENT_SHOP_ID, 0);
        mAdapterShopEmployeeAdd = new AdapterShopEmployeeAdd(this);
    }

    private void initViews() {
        RecyclerViewUtils.Create().setUpVertical(this, mRecyclerAddMember)
                .setAdapter(mAdapterShopEmployeeAdd);
    }

    private void initActions() {
        mEdtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                getPresenter(ActivityShopMemberAdd.this).searchUsers(mShopId,
                        s.toString().trim());
            }
        });
        mImbBack.setOnClickListener(v -> onBackPressed());
        mImbDone.setOnClickListener(v -> {
            List<Integer> userIds = new ArrayList<>();
            for (UserInfo item : mUserInfoList) {
                userIds.add(item.getId());
            }
            getPresenter(ActivityShopMemberAdd.this)
                    .addShopMembers(mShopId, userIds);
        });
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
    }

    @Override
    public void getUsersToAddMemberSuccess(List<UserInfo> listUserInfo) {
        if (listUserInfo != null) {
            if (mUserInfoList != null) {
                for (UserInfo item : listUserInfo) {
                    for (UserInfo item2 : mUserInfoList) {
                        if (item.getId() == item2.getId()) {
                            item.setChecked(true);
                            break;
                        }
                    }
                }
            }
            mAdapterShopEmployeeAdd.setListUserInfo(listUserInfo);
        }
    }

    @Override
    public void getError(String errorMessage) {
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void addMembersSuccess() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onChosenMember(UserInfo userInfo) {
        if (mUserInfoList == null) {
            mUserInfoList = new ArrayList<>();
        }
        mUserInfoList.add(userInfo);
        addListView(userInfo.getAvatar());
    }

    @Override
    public void onUnChosenMember(UserInfo userInfo) {
        for (UserInfo item : mUserInfoList) {
            if (item.getId() == userInfo.getId()) {
                mLlListAdd.removeViewAt(mUserInfoList.indexOf(item));
                mUserInfoList.remove(item);
                break;
            }
        }
    }

    private void addListView(String avatar) {
        if (mUserInfoList == null || mUserInfoList.isEmpty()) {
            mLlListAdd.setVisibility(View.GONE);
            return;
        }
        mLlListAdd.setVisibility(View.VISIBLE);
        View itemView = LayoutInflater.from(this).inflate(R.layout.item_shop_member, mLlListAdd,
                false);
        if (avatar != null) {
            Glide.with(getContext()).load(avatar).into((ImageView) itemView.findViewById(R.id.imvShopMember));
        }
        itemView.setOnClickListener(v -> {
        });
        mLlListAdd.addView(itemView);
    }


}
