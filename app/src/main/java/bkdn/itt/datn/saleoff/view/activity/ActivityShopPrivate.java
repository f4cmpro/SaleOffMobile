package bkdn.itt.datn.saleoff.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.model.Shop;
import bkdn.itt.datn.saleoff.presenter.PresenterShopPrivate;
import bkdn.itt.datn.saleoff.utils.DialogUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.fragment.FragmentBase;
import bkdn.itt.datn.saleoff.view.fragment.FragmentEvent;
import bkdn.itt.datn.saleoff.view.fragment.FragmentShopEmployee;
import bkdn.itt.datn.saleoff.view.fragment.FragmentShopEvent;
import bkdn.itt.datn.saleoff.view.fragment.FragmentShopInfo;
import bkdn.itt.datn.saleoff.view.fragment.FragmentShopMembers;
import bkdn.itt.datn.saleoff.view.fragment.FragmentShopPost;
import bkdn.itt.datn.saleoff.view.iface.IActivityShopPrivate;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class ActivityShopPrivate extends ActivityBase<PresenterShopPrivate> implements IActivityShopPrivate {
    private static final int TAB_INFO = 0;
    private static final int TAB_POST = 1;
    private static final int TAB_EVENT = 2;
    private static final int TAB_EMPLOYEE = 3;
    private static final int TAB_MEMBER = 4;
    /*
    * Widgets
    * */

    @BindView(R.id.toolbarShopPrivate)
    Toolbar mToolbar;
    @BindView(R.id.tvToolBarSearch)
    TextView mTvToolBarSearch;
    @BindView(R.id.imvShopPrivateCover)
    ImageView mImvCover;
    @BindView(R.id.imvShopPrivateAvatar)
    ImageView mImvAvatar;
    @BindView(R.id.tvShopPrivateName)
    TextView mTvName;
    @BindView(R.id.tabLayoutShopPrivate)
    TabLayout mTabLayoutShop;
    @BindView(R.id.flShopPrivateContainer)
    FrameLayout mFlContainer;
    @BindView(R.id.appBarShopPrivate)
    AppBarLayout mAppBarLayout;
    @BindView(R.id.tvShopPrivateCreatePost)
    TextView mTvCreatePost;
    @BindView(R.id.tvShopPrivateCreateEvent)
    TextView mTvCreateEvent;
    @BindView(R.id.tvShopPrivateEditShop)
    TextView mTvEditShop;
    @BindView(R.id.tvShopPrivateAddEmployee)
    TextView mTvAddEmployee;
    @BindView(R.id.imbToolBarMenu)
    ImageButton mImbMenu;
    PopupMenu mPopupMenu;
    /*
    * Fields
    * */
    private Shop mShop;
    private FragmentShopInfo mFragmentShopInfo;
    private FragmentShopPost mFragmentShopPost;
    private FragmentShopEvent mFragmentShopEvent;
    private FragmentShopEmployee mFragmentShopEmployee;
    private FragmentShopMembers mFragmentShopMembers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_private);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mPopupMenu = new PopupMenu(getContext(), mImbMenu);
        mPopupMenu.getMenuInflater().inflate(R.menu.menu_detail_shop, mPopupMenu.getMenu());
        initValues();
        initActions();
    }


    private void initValues() {
        if (getIntent().getExtras().getString(GlobalDefine.KEY_BUNDLE_NOTIFICATION) != null) {
            String data = getIntent().getExtras().getString(GlobalDefine.KEY_BUNDLE_NOTIFICATION);
            mShop = new Gson().fromJson(data, Shop.class);
            initViews();
        } else if (getIntent().getParcelableExtra(GlobalDefine.KEY_INTENT_SHOP) != null) {
            mShop = getIntent().getParcelableExtra(GlobalDefine.KEY_INTENT_SHOP);
            initViews();
        } else {
            mShop = new Shop();
            mShop.setShopId(getIntent().getIntExtra(GlobalDefine.KEY_INTENT_SHOP_ID, 0));
            getPresenter(this).getShopDetail(mShop.getShopId());
        }
    }

    private void initViews() {
        if (mShop != null) {
            mFragmentShopInfo = FragmentShopInfo.newInstance(mShop);
            mFragmentShopPost = FragmentShopPost.newInstance(mShop, ConstantDefine.PRIVATE);
            mFragmentShopEvent = FragmentShopEvent.newInstance(mShop, ConstantDefine.PRIVATE );
            mFragmentShopEmployee = FragmentShopEmployee.newInstance(mShop.getShopId());
            mFragmentShopMembers = FragmentShopMembers.newInstance(mShop.getShopId());
            //set cover
            if (mShop.getCover() != null) {
                Glide.with(getContext()).load(mShop.getCover()).into(mImvCover);
            }
            //set avatar
            if (mShop.getAvatar() != null) {
                Glide.with(getContext()).load(mShop.getAvatar()).into(mImvAvatar);
            }
            //set name
            if (mShop.getName() != null) {
                mTvName.setText(mShop.getName());
            }
            replaceFragment(mFragmentShopInfo, mFlContainer.getId(), false);
        }
    }

    private void initActions() {
        mToolbar.setNavigationOnClickListener(v -> onBackPressed());
        mTabLayoutShop.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mAppBarLayout.setExpanded(false);
                switch (tab.getPosition()) {
                    case TAB_INFO:
                        replaceFragment(mFragmentShopInfo, mFlContainer.getId(), false);
                        break;
                    case TAB_POST:
                        replaceFragment(mFragmentShopPost, mFlContainer.getId(), false);
                        break;
                    case TAB_EVENT:
                        replaceFragment(mFragmentShopEvent, mFlContainer.getId(), false);
                        break;
                    case TAB_EMPLOYEE:
                        replaceFragment(mFragmentShopEmployee, mFlContainer.getId(), false);
                        break;
                    case TAB_MEMBER:
                        replaceFragment(mFragmentShopMembers, mFlContainer.getId(), false);
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        mTvCreatePost.setOnClickListener(v -> {
            Intent intent = new Intent(ActivityShopPrivate.this, ActivityPostCreate.class);
            intent.putExtra(GlobalDefine.KEY_INTENT_SHOP, mShop);
            startActivityForResult(intent, ConstantDefine.CREATE_SHOP_POST_REQUEST);
        });
        mTvCreateEvent.setOnClickListener(v -> {
            Intent intent = new Intent(ActivityShopPrivate.this, ActivityEventCreate.class);
            intent.putExtra(GlobalDefine.KEY_INTENT_SHOP, mShop);
            startActivityForResult(intent, ConstantDefine.CREATE_EVENT_REQUEST);
        });
        mTvAddEmployee.setOnClickListener(v -> {
            Intent intent = new Intent(ActivityShopPrivate.this, ActivityShopMemberAdd.class);
            intent.putExtra(GlobalDefine.KEY_INTENT_SHOP_ID, mShop.getShopId());
            startActivityForResult(intent, ConstantDefine.ADD_SHOP_MEMBERS_REQUEST);
        });
        mTvEditShop.setOnClickListener(v -> {
            Intent intent = new Intent(ActivityShopPrivate.this, ActivityShopEdit.class);
            intent.putExtra(GlobalDefine.KEY_INTENT_SHOP, mShop);
            startActivityForResult(intent, ConstantDefine.EDIT_SHOP_INFO_REQUEST);
        });
        mImbMenu.setOnClickListener(v -> mPopupMenu.show());
        mPopupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.item_delete_shop:
                    DialogUtils.createConfirmDialog(getContext(), "Bạn muốn xóa cửa hàng:",
                            mShop.getName(), new DialogUtils.DialogConfirmCallback() {
                                @Override
                                public void onClickPositiveButton() {
                                    getPresenter(ActivityShopPrivate.this)
                                            .deleteShop(mShop.getShopId());
                                }

                                @Override
                                public void onClickNegativeButton() {
                                }
                            });
                    break;
                case R.id.item_back_home:
                    break;
            }
            return false;
        });
    }

    @Override
    public void replaceFragment(FragmentBase fragment, int containViewId, boolean addToBackStack) {
        String TAB = fragment.getClass().getName();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (addToBackStack) {
            transaction.addToBackStack(TAB);
        }
        transaction.replace(containViewId, fragment, TAB);
        transaction.commit();
        getSupportFragmentManager().executePendingTransactions();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
    }


    @Override
    public void getError(String errorMessage) {
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requestDeleteShopSuccess() {
        Toasty.success(getContext(), "Gửi yêu cầu thành công", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getShopDetailSuccess(Shop shop) {
        if (shop != null) {
            mShop = shop;
            initViews();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ConstantDefine.ADD_SHOP_MEMBERS_REQUEST) {
                Toasty.success(getContext(), getString(R.string.add_employee_success), Toast.LENGTH_SHORT).show();
                mFragmentShopEmployee = FragmentShopEmployee.newInstance(mShop.getShopId());
                replaceFragment(mFragmentShopEmployee, mFlContainer.getId(), false);
                mTabLayoutShop.getTabAt(TAB_EMPLOYEE).select();
                mAppBarLayout.setExpanded(false);
            } else if (requestCode == ConstantDefine.CREATE_SHOP_POST_REQUEST) {
                mFragmentShopPost = FragmentShopPost.newInstance(mShop, ConstantDefine.PRIVATE);
                replaceFragment(mFragmentShopPost, mFlContainer.getId(), false);
                mTabLayoutShop.getTabAt(TAB_POST).select();
                mAppBarLayout.setExpanded(false);
            } else if (requestCode == ConstantDefine.EDIT_SHOP_INFO_REQUEST) {
                Toasty.success(getContext(), getString(R.string.edit_success), Toast.LENGTH_SHORT).show();
                mShop = data.getParcelableExtra(GlobalDefine.KEY_INTENT_SHOP);
                mFragmentShopInfo = FragmentShopInfo.newInstance(mShop);
                replaceFragment(mFragmentShopInfo, mFlContainer.getId(), false);
                mTabLayoutShop.getTabAt(TAB_INFO).select();
                mAppBarLayout.setExpanded(false);
            }else if(requestCode == ConstantDefine.CREATE_EVENT_REQUEST){
                mFragmentShopEvent = FragmentShopEvent.newInstance(mShop, ConstantDefine.PRIVATE);
                replaceFragment(mFragmentShopPost, mFlContainer.getId(), false);
                mTabLayoutShop.getTabAt(TAB_EVENT).select();
                mAppBarLayout.setExpanded(false);
            }
        }
    }
}
