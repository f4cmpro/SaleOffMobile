package bkdn.itt.datn.saleoff.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.model.Shop;
import bkdn.itt.datn.saleoff.presenter.PresenterShopPublic;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.dialog.DialogRateShop;
import bkdn.itt.datn.saleoff.view.dialog.DialogReport;
import bkdn.itt.datn.saleoff.view.fragment.FragmentBase;
import bkdn.itt.datn.saleoff.view.fragment.FragmentShopInfo;
import bkdn.itt.datn.saleoff.view.fragment.FragmentShopPost;
import bkdn.itt.datn.saleoff.view.iface.IActivityShop;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class ActivityShopPublic extends ActivityBase<PresenterShopPublic> implements
        IActivityShop, DialogRateShop.OnChooseRateListener {

    /*
     * Widgets
     * */
    @BindView(R.id.tabLayoutShopPublic)
    TabLayout mTabLayoutShop;
    @BindView(R.id.imvShopPublicCover)
    ImageView mImvCover;
    @BindView(R.id.imvShopPublicAvatar)
    ImageView mImvAvatar;
    @BindView(R.id.tvShopPublicName)
    TextView mTvShopName;
    @BindView(R.id.btnShopPublicFollow)
    Button mBtnFollow;
    @BindView(R.id.btnShopPublicUnFollow)
    Button mBtnUnFollow;
    @BindView(R.id.btnShopPublicRate)
    Button mBtnRate;
    @BindView(R.id.toolbarShopPublic)
    Toolbar mToolbar;
    @BindView(R.id.appBarShopPublic)
    AppBarLayout mAppBarLayout;
    @BindView(R.id.flShopPublicContainer)
    FrameLayout mFlContainer;
    @BindView(R.id.imbToolBarMenu)
    ImageButton mImbMenu;
    PopupMenu mPopupMenu;
    /*
    * Fields
    * */
    private static final int PAGE_INFO = 0;
    private static final int PAGE_POST = 1;
    private Shop mShop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_public);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mPopupMenu = new PopupMenu(getContext(), mImbMenu);
        mPopupMenu.getMenuInflater().inflate(R.menu.menu_report_shop, mPopupMenu.getMenu());
        initValues();
        initActions();
    }

    private void initValues() {
        if (getIntent().getExtras().getString(GlobalDefine.KEY_BUNDLE_NOTIFICATION) != null) {
            String data = getIntent().getExtras().getString(GlobalDefine.KEY_BUNDLE_NOTIFICATION);
            mShop = new Gson().fromJson(data, Shop.class);
            initViews();
        } else if (getIntent().getParcelableExtra(GlobalDefine.KEY_INTENT_SHOP) != null) {
            mShop = getIntent().getParcelableExtra(GlobalDefine.KEY_INTENT_SHOP);
            initViews();
        } else {
            mShop = new Shop();
            mShop.setShopId(getIntent().getIntExtra(GlobalDefine.KEY_INTENT_SHOP_ID, 0));
            getPresenter(this).getShopDetail(mShop.getShopId());
        }
    }

    private void initViews() {
        if (mShop != null) {
            if (mShop.getCover() != null) {
                Glide.with(getContext()).load(mShop.getCover()).into(mImvCover);
            }
            if (mShop.getAvatar() != null) {
                Glide.with(getContext()).load(mShop.getAvatar()).into(mImvAvatar);
            }
            if (mShop.getName() != null) {
                mTvShopName.setText(mShop.getName());
            }
            if (mShop.getIsFollow() == 1) {
                mBtnUnFollow.setVisibility(View.VISIBLE);
                mBtnFollow.setVisibility(View.GONE);
            } else {
                mBtnUnFollow.setVisibility(View.GONE);
                mBtnFollow.setVisibility(View.VISIBLE);
            }
            replaceFragment(FragmentShopInfo.newInstance(mShop), mFlContainer.getId(), false);
        }
    }

    private void initActions() {
        mToolbar.setNavigationOnClickListener(v -> onBackPressed());
        mBtnRate.setOnClickListener(v -> DialogRateShop.newInstance(this, getSupportFragmentManager()));
        mTabLayoutShop.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mAppBarLayout.setExpanded(false, true);

                switch (tab.getPosition()) {
                    case PAGE_INFO:
                        replaceFragment(FragmentShopInfo.newInstance(mShop), mFlContainer.getId(), false);
                        break;
                    case PAGE_POST:
                        replaceFragment(FragmentShopPost.newInstance(mShop, ConstantDefine.PUBLIC),
                                mFlContainer.getId(), false);
                        break;

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        mImbMenu.setOnClickListener(v -> mPopupMenu.show());
        mPopupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.item_report_shop:
                    DialogReport dialogReport = DialogReport.newInstance();
                    dialogReport.setListener(ConstantDefine.REPORT_SHOP, reason -> {
                        getPresenter(this).reportShop(mShop.getShopId(), reason);
                    });
                    dialogReport.show(getSupportFragmentManager(), null);
                    break;
                case R.id.item_back_home:
                    break;
            }
            return false;
        });
    }

    @Override
    public void replaceFragment(FragmentBase fragment, int containViewId, boolean addToBackStack) {
        String TAB = fragment.getClass().getName();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (addToBackStack) {
            transaction.addToBackStack(TAB);
        }
        transaction.replace(containViewId, fragment, TAB);
        transaction.commit();
        getSupportFragmentManager().executePendingTransactions();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
    }


    @Override
    public void getError(String errorMessage) {
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getShopDetailSuccess(Shop shop) {
        if (shop != null) {
            mShop = shop;
            initViews();
        }
    }

    @Override
    public void ratingShopSuccess() {
        Toasty.success(getContext(), getString(R.string.rate_success), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void reportSuccess() {
        Toasty.success(getContext(), getString(R.string.report_success), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

        }
    }

    @Override
    public void onChooseRate(int rate) {
        getPresenter(this).ratingShop(mShop.getShopId(), rate);
    }
}
