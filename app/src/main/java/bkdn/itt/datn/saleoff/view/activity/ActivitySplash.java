package bkdn.itt.datn.saleoff.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import java.util.HashMap;
import java.util.Set;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.User;
import bkdn.itt.datn.saleoff.presenter.PresenterSplash;
import bkdn.itt.datn.saleoff.utils.SharedPreferencesUtils;

public class ActivitySplash extends ActivityBase<PresenterSplash> {

    private boolean mIsFirstOpenApp;
    private boolean mIsFirstLogin;
    private int DELAY_TIME = 1000;
    private String mToken;
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        bundle = getIntent().getExtras();
        mIsFirstOpenApp = SharedPreferencesUtils.getInstance(this).getBool(getString(R.string.intro_key_save_first_open_app));
        mIsFirstLogin = SharedPreferencesUtils.getInstance(this).getBool(getString(R.string.key_is_first_login));
        mToken = SharedPreferencesUtils.getInstance(this).getKeySaveToken();
        Log.e("TAG", "mIsFirstOpenApp: " + mIsFirstOpenApp);
        Log.e("TAG", "mIsFirstLogin: " + mIsFirstLogin);
        Log.e("TAG", "token: " + mToken);
        Handler handler = new Handler();
        handler.postDelayed(this::gotoIntroOrMainActivity, DELAY_TIME);
    }

    private void gotoIntroOrMainActivity() {
        if (mIsFirstOpenApp) {
            if (!TextUtils.isEmpty(mToken)) {
                if (mIsFirstLogin) {
                    startActivity(ActivityLocation.class);
                } else {
                    goToMainActivity();
                }

            } else {
                startActivity(ActivityLogin.class);
            }

        } else {
            startActivity(ActivityIntro.class);
        }
        finish();
    }


    private void goToMainActivity() {
        if (null != bundle) {
            Intent intent = new Intent(this, ActivityMain.class);
            intent.putExtras(bundle);
            startActivity(intent);
        } else {
            startActivity(ActivityMain.class);
        }
    }

    private void handleNotification(){
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            Set<String> keySet = getIntent().getExtras().keySet();
            String data = bundle.getString("data");
            Log.e("TAG", "data: " + data);
            for(String key : keySet){
                if(key.equals("post")){
                    String post = getIntent().getExtras().getString(key);
                }
                if(key.equals("type")){
                    String type = getIntent().getExtras().getString(key);
                }
                if(key.equals("id")){
                    String id = getIntent().getExtras().getString(key);
                }
            }
        }
    }


}
