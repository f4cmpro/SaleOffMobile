package bkdn.itt.datn.saleoff.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.model.Event;
import bkdn.itt.datn.saleoff.model.model.Post;
import bkdn.itt.datn.saleoff.model.model.User;
import bkdn.itt.datn.saleoff.presenter.PresenterUserPublic;
import bkdn.itt.datn.saleoff.utils.CustomPaginationScrollListener;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.adapter.AdapterUserNewsFeed;
import bkdn.itt.datn.saleoff.view.dialog.DialogReport;
import bkdn.itt.datn.saleoff.view.iface.IActivityUserPublic;
import bkdn.itt.datn.saleoff.view.iface.OnClickLikeListener;
import bkdn.itt.datn.saleoff.view.iface.OnClickUnLikeListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

/**
 * Created by TuDLT on 5/3/2018.
 */
public class ActivityUserPublic extends ActivityBase<PresenterUserPublic> implements
        IActivityUserPublic, AdapterUserNewsFeed.OnClickPostItemListener {
    /*
    * Widgets
    * */
    @BindView(R.id.imvUserPublicCover)
    ImageView mImvCover;
    @BindView(R.id.imvUserPublicAvatar)
    ImageView mImvAvatar;
    @BindView(R.id.tvUserPublicName)
    TextView mTvUserName;
    @BindView(R.id.recyclerUserPublicPost)
    RecyclerView mRecyclerPost;
    @BindView(R.id.tvUserPublicFollow)
    TextView mTvFollow;
    @BindView(R.id.btnUserPublicFollow)
    Button mBtnFollow;
    @BindView(R.id.btnUserPublicUnFollow)
    Button mBtnUnFollow;
    @BindView(R.id.toolbarUserPublic)
    Toolbar mToolbar;
    @BindView(R.id.swipeRefreshLayoutUserPublic)
    SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.viewUserPublicNotFoundPost)
    View mViewNotFoundPost;
    @BindView(R.id.imbUserPublicMenu)
    ImageButton mImbMenu;
    private PopupMenu mPopupMenu;


    /*
    * Fields
    * */
    private User mUser;
    private AdapterUserNewsFeed mAdapterUserNewsFeed;
    private int mLastId;
    private boolean mIsRefresh;
    private boolean mIsLoadMore;
    private OnClickLikeListener mOnClickLikeListener;
    private OnClickUnLikeListener mOnClickUnLikeListener;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_public);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mPopupMenu = new PopupMenu(getContext(), mImbMenu);
        mPopupMenu.getMenuInflater().inflate(R.menu.menu_report_user, mPopupMenu.getMenu());
        initValues();
        initViews();
        initActions();
    }

    private void initValues() {
        mIsRefresh = true;
        if (getIntent().getExtras().getString(GlobalDefine.KEY_BUNDLE_NOTIFICATION) != null) {
            String data = getIntent().getExtras().getString(GlobalDefine.KEY_BUNDLE_NOTIFICATION);
            mUser = new Gson().fromJson(data, User.class);
        } else if (getIntent().getParcelableExtra(GlobalDefine.KEY_INTENT_USER) != null) {
            mUser = getIntent().getParcelableExtra(GlobalDefine.KEY_INTENT_USER);
        } else {
            mUser = new User();
            mUser.setId(getIntent().getIntExtra(GlobalDefine.KEY_INTENT_USER_ID, 0));
            getPresenter(this).getUserById(mUser.getId());
        }
        getPresenter(this).getUserPosts(mUser.getId(), mLastId);
    }

    private void initActions() {
        mToolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
        });
        mBtnFollow.setOnClickListener(v -> {
            getPresenter(this).followUser(mUser.getId());
            mBtnFollow.setVisibility(View.GONE);
            mBtnUnFollow.setVisibility(View.VISIBLE);
        });
        mBtnUnFollow.setOnClickListener(v -> {
            getPresenter(this).unFollowUser(mUser.getId());
            mBtnFollow.setVisibility(View.VISIBLE);
            mBtnUnFollow.setVisibility(View.GONE);
        });
        mRefreshLayout.setOnRefreshListener(() -> {
            mIsRefresh = true;
            getPresenter(this).getUserPosts(mUser.getId(), 0);
        });
        mRecyclerPost.addOnScrollListener(new CustomPaginationScrollListener(RecyclerViewUtils.Create()
                .getLinearLayoutManager(mRecyclerPost)) {
            @Override
            protected void loadMoreItems() {
                mIsLoadMore = true;
                getPresenter(ActivityUserPublic.this).getUserPosts(mUser.getId(), mLastId);
            }

            @Override
            protected boolean isLoading() {
                return mIsLoadMore;
            }

            @Override
            protected void enableRefreshLayout(boolean isEnable) {
            }
        });
        mImbMenu.setOnClickListener(v -> mPopupMenu.show());
        mPopupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.item_report_user:
                    DialogReport dialogReport = DialogReport.newInstance();
                    dialogReport.setListener(ConstantDefine.REPORT_USER, reasonId -> {
                        getPresenter(ActivityUserPublic.this).reportUser(mUser.getId(), reasonId);
                    });
                    dialogReport.show(getSupportFragmentManager(), null);
                    break;
                case R.id.item_back_home:
                    break;
            }
            return false;
        });
    }

    private void initViews() {
        if (mUser != null) {
            //set cover
            if (mUser.getCover() != null) {
                Glide.with(getContext()).load(mUser.getCover()).into(mImvCover);
            }
            //set avatar
            if (mUser.getAvatar() != null) {
                Glide.with(getContext()).load(mUser.getAvatar()).into(mImvAvatar);
            }
            //set name
            if (mUser.getUsername() != null) {
                mTvUserName.setText(mUser.getUsername());
            }
            //set follow
            if (mUser.getIsFollow() == 1) {
                mBtnUnFollow.setVisibility(View.VISIBLE);
                mBtnFollow.setVisibility(View.GONE);
            } else {
                mBtnUnFollow.setVisibility(View.GONE);
                mBtnFollow.setVisibility(View.VISIBLE);
            }
            //set followers
            mTvFollow.setText(getString(R.string.followers) + " " + String.valueOf(mUser.getFollowNumber()));
            if (mAdapterUserNewsFeed == null) {
                mAdapterUserNewsFeed = new AdapterUserNewsFeed(getContext(), this, ConstantDefine.PUBLIC);
            }
            mAdapterUserNewsFeed.setUserInfo(mUser.getAvatar(), mUser.getUsername());
            RecyclerViewUtils.Create().setUpVertical(getContext(), mRecyclerPost).setAdapter(mAdapterUserNewsFeed);
            mRecyclerPost.setNestedScrollingEnabled(true);
        }

    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
    }

    @Override
    public void getUserPostSuccess(List<Post> posts, int lastId) {
        if (mIsRefresh) {
            mIsRefresh = false;
            mRefreshLayout.setRefreshing(false);
            if (mAdapterUserNewsFeed == null) {
                mAdapterUserNewsFeed = new AdapterUserNewsFeed(getContext(), this, ConstantDefine.PUBLIC);
            }
            mAdapterUserNewsFeed.refreshPosts(posts);
        } else if (mIsLoadMore) {
            mIsLoadMore = false;
            mAdapterUserNewsFeed.setPosts(posts);
        }
        mLastId = lastId;
        if (mAdapterUserNewsFeed.getItemCount() == 0) {
            mRecyclerPost.setVisibility(View.GONE);
            mViewNotFoundPost.setVisibility(View.VISIBLE);
            ((TextView) mViewNotFoundPost.findViewById(R.id.tvLayoutNotFoundItem))
                    .setText(getString(R.string.warning_not_post_yet));
        } else {
            mRecyclerPost.setVisibility(View.VISIBLE);
            mViewNotFoundPost.setVisibility(View.GONE);
        }
    }

    @Override
    public void getError(String errorMessage) {
        if (mIsRefresh) {
            mIsRefresh = false;
            mRefreshLayout.setRefreshing(false);
        } else {
            mIsLoadMore = false;
        }
        Toasty.warning(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getUserByIdSuccess(User user) {
        mUser = user;
        initViews();
    }

    @Override
    public void followUserFail() {
        mBtnFollow.setVisibility(View.VISIBLE);
        mBtnUnFollow.setVisibility(View.GONE);
    }

    @Override
    public void unFollowUserFail() {
        mBtnFollow.setVisibility(View.GONE);
        mBtnUnFollow.setVisibility(View.VISIBLE);
    }

    @Override
    public void likePostFail() {
    }

    @Override
    public void unLikeFail() {
    }

    @Override
    public void reportSuccess() {
        Toasty.success(getContext(), getString(R.string.report_success), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClickPostItem(Post post) {
        Intent intent = new Intent(getContext(), ActivityPostDetail.class);
        intent.putExtra(GlobalDefine.KEY_INTENT_POST, post);
        startActivity(intent);
    }


    @Override
    public void onClickUnLike(int postId, OnClickUnLikeListener onClickUnLikeListener) {
        getPresenter(this).unLikePost(postId);
        mOnClickUnLikeListener = onClickUnLikeListener;
    }

    @Override
    public void onClickLike(int postId, OnClickLikeListener onClickLikeListener) {
        getPresenter(this).likePost(postId);
        mOnClickLikeListener = onClickLikeListener;

    }

    @Override
    public void onClickComment(int postId) {
        Intent intent = new Intent(getContext(), ActivityComment.class);
        intent.putExtra(GlobalDefine.KEY_INTENT_POST_ID, postId);
        startActivity(intent);
    }

    @Override
    public void onCLickShare(Post post) {
        Intent intent = new Intent(getContext(), ActivityPostDetail.class);
        intent.putExtra(GlobalDefine.KEY_INTENT_POST, post);
        startActivity(intent);
    }

    @Override
    public void onClickEventDetail(Event event) {
        Intent intent = new Intent(getContext(), ActivityEventDetail.class);
        intent.putExtra(GlobalDefine.KEY_INTENT_EVENT, event);
        startActivity(intent);
    }
}
