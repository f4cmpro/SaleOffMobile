package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.Category;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 5/25/2018.
 */
public class AdapterCategory extends AdapterBase<AdapterCategory.CategoryHolder> {
    private List<Category> mCategories;
    private OnClickCatItemListener mListener;
    private int mSelectedCatId;

    public AdapterCategory(Context context, List<Category> categories) {
        super(context);
        mCategories = categories;
        mSelectedCatId = mCategories.get(1).getId();
    }

    public void setListener(OnClickCatItemListener listener) {
        mListener = listener;
    }

    @NonNull
    @Override
    public CategoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_category_tab, parent, false);
        return new CategoryHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryHolder holder, int position) {
        holder.bindData(mCategories.get(position));
    }

    @Override
    public int getItemCount() {
        return mCategories == null ? 0 : mCategories.size();
    }

    protected class CategoryHolder extends ViewHolderBase<Category> {
        @BindView(R.id.imvCategoryTab)
        ImageView mImvAvatar;
        @BindView(R.id.imvCategoryTabCover)
        ImageView mImvCover;
        @BindView(R.id.tvCategoryTab)
        TextView mTvName;

        public CategoryHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindData(Category category) {
            super.bindData(category);
            mImvAvatar.setImageResource(category.getImvRes());
            mTvName.setText(category.getName());
            mImvCover.setVisibility(View.GONE);
            if(category.getId() == mSelectedCatId){
                mTvName.setTextColor(getResources().getColor(R.color.colorAccent));
                mImvCover.setVisibility(View.VISIBLE);
            }else {
                mTvName.setTextColor(getResources().getColor(R.color.colorBlack));
                mImvCover.setVisibility(View.GONE);
            }
            itemView.setOnClickListener(v -> {
                mListener.onClickCatItem(category.getId());
                mSelectedCatId = category.getId();
                notifyDataSetChanged();
            });
        }
    }

    public interface OnClickCatItemListener {
        void onClickCatItem(int catId);
    }
}
