package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.Category;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 5/14/2018.
 */
public class AdapterChangeInterested extends AdapterBase<AdapterChangeInterested.InterestedHolder> {
    List<Category> mCategories;
    List<Integer> mChosenList;
    private OnChoiceSubjectListener mListener;


    public AdapterChangeInterested(Context context, List<Category> categories, List<Integer> chosenList) {
        super(context);
        mCategories = categories;
        mChosenList = chosenList;
    }


    @Override
    public InterestedHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_subject, parent, false);
        return new InterestedHolder(itemView);
    }

    @Override
    public void onBindViewHolder(InterestedHolder holder, int position) {
        holder.bindData(mCategories.get(position));
    }

    @Override
    public int getItemCount() {
        return mCategories != null ? mCategories.size() : 0;
    }

    public void setChoiceSubjectListener(OnChoiceSubjectListener listener) {
        mListener = listener;
    }


    protected class InterestedHolder extends ViewHolderBase<Category> {
        @BindView(R.id.imvSubjectItem)
        ImageView mImvSubjectItem;
        @BindView(R.id.imvSubjectCover)
        ImageView mImvCover;
        @BindView(R.id.imvSubjectChecked)
        ImageView mImvChecked;
        @BindView(R.id.tvSubjectName)
        TextView mTvSubjectName;

        public InterestedHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindData(Category category) {
            super.bindData(category);
            if (category.getImvRes() != 0) {
                mImvSubjectItem.setImageResource(category.getImvRes());
            }
            if (category.isSelected()) {
                mImvChecked.setVisibility(View.VISIBLE);
                mImvCover.setVisibility(View.VISIBLE);
            } else {
                mImvChecked.setVisibility(View.GONE);
                mImvCover.setVisibility(View.GONE);
            }
            mTvSubjectName.setText(category.getName());
            itemView.setOnClickListener(v -> {
                if (category.isSelected()) {
                    category.setSelected(false);
                    mListener.unChoiceSubject(category.getId());
                } else {
                    category.setSelected(true);
                    mListener.choiceSubject(category.getId());
                }
                notifyDataSetChanged();
            });
        }
    }

    public interface OnChoiceSubjectListener {
        void choiceSubject(int catId);

        void unChoiceSubject(int catId);
    }
}
