package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.model.Comment;
import bkdn.itt.datn.saleoff.model.model.User;
import bkdn.itt.datn.saleoff.utils.SharedPreferencesUtils;
import bkdn.itt.datn.saleoff.utils.TimeUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 4/20/2018.
 */
public class AdapterComment extends AdapterBase<AdapterComment.CommentHolder> {
    private List<Comment> mComments;
    private OnEditCommentListener mListener;

    public AdapterComment(Context context, OnEditCommentListener listener) {
        super(context);
        mComments = new ArrayList<>();
        mListener = listener;
    }

    @Override
    public CommentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_comment, parent, false);
        return new CommentHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CommentHolder holder, int position) {
        holder.bindData(mComments.get(position));
    }

    @Override
    public int getItemCount() {
        return mComments.size();
    }

    public void loadComments(List<Comment> comments) {
        mComments.addAll(comments);
        notifyDataSetChanged();
    }

    public void addMyComment(Comment addedComment) {
        if (mComments.isEmpty()) {
            mComments.add(addedComment);
        } else {
            mComments.add(0, addedComment);
        }
        notifyDataSetChanged();
    }

    public void editCommentItem(int position, String comment) {
        String updatedAt = TimeUtils.parseTimestampToString(Calendar.getInstance().getTimeInMillis());
        mComments.get(position).setComment(comment);
        mComments.get(position).setUpdateAt(updatedAt);
        notifyDataSetChanged();
    }

    public void refreshComments(List<Comment> comments) {
        mComments.clear();
        mComments.addAll(comments);
        notifyDataSetChanged();
    }

    protected class CommentHolder extends ViewHolderBase<Comment> {
        @BindView(R.id.imvItemCommentAvatar)
        ImageView imvAvatar;
        @BindView(R.id.tvItemComment)
        TextView tvComment;
        @BindView(R.id.tvItemCommentCreatedAt)
        TextView tvAgoTime;
        @BindView(R.id.tvItemCommentUserName)
        TextView tvUserName;
        PopupMenu mPopupMenu;

        public CommentHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mPopupMenu = new PopupMenu(getContext(), itemView);
            mPopupMenu.getMenuInflater().inflate(R.menu.menu_post_edit, mPopupMenu.getMenu());
        }

        @Override
        public void bindData(Comment comment) {
            super.bindData(comment);
            if (comment.getAuthor() != null) {
                //Avatar
                if (comment.getAuthor().getAvatar() != null && !comment.getAuthor().getAvatar().isEmpty()) {
                    Glide.with(getContext()).load(comment.getAuthor().getAvatar())
                            .into(imvAvatar);
                }
                //Lam gi cung that binh tinh chu tu, roi tu tu moi chuyen cung sẽ ổn thôi
                if (comment.getAuthor().getName() != null && !comment.getAuthor().getName().isEmpty()) {
                    tvUserName.setText(comment.getAuthor().getName());
                }
                if (comment.getComment() != null && !comment.getComment().isEmpty()) {
                    tvComment.setText(comment.getComment());
                }
                //AgoTime
                tvAgoTime.setText(comment.getUpdateAt() != null
                        ? comment.getUpdateAt() : comment.getCreatedAt());

                //set click edit or delete
                itemView.setOnLongClickListener(v -> {
                    User me = SharedPreferencesUtils.getInstance(getContext()).getUserProfile();
                    if (comment.getAuthor().getId() == me.getId()) {
                        mPopupMenu.show();
                    }
                    return false;
                });

                mPopupMenu.setOnMenuItemClickListener(item -> {
                    switch (item.getItemId()) {
                        case R.id.itemPostEdit: {
                            mListener.onEditComment(mComments.indexOf(comment), comment.getComment());
                            break;
                        }
                        case R.id.itemPostDel: {
                            break;
                        }
                    }
                    return false;
                });


            }
        }
    }

    public interface OnEditCommentListener {
        void onEditComment(int position, String comment);
    }
}
