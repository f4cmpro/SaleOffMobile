package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.model.model.Event;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 5/27/2018.
 */
public class AdapterEvent extends AdapterBase<AdapterEvent.EventHolder> {

    private List<Event> mEvents;
    private OnBaseItemClickListener mListener;
    //joined, going_happen, happening, happened, event_of_shop
    private int mEventStatus;

    public AdapterEvent(Context context, int eventStatus) {
        super(context);
        mEvents = new ArrayList<>();
        mEventStatus = eventStatus;
    }

    @NonNull
    @Override
    public EventHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_event, parent, false);
        return new EventHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull EventHolder holder, int position) {
        holder.bindData(mEvents.get(position));
    }

    @Override
    public int getItemCount() {
        return mEvents.size();
    }

    public void addData(List<Event> events) {
        mEvents.addAll(events);
        notifyDataSetChanged();
    }

    public void refreshData(List<Event> events) {
        if (mEvents != null && !mEvents.isEmpty()) {
            mEvents.clear();
        }
        addData(events);
    }

    public void setListener(OnBaseItemClickListener<Event> listener) {
        mListener = listener;
    }

    public void clearData() {
        mEvents.clear();
        notifyDataSetChanged();
    }

    public void changeEventStatus(int eventStatus){
        mEventStatus = eventStatus;
    }

    protected class EventHolder extends ViewHolderBase<Event> {
        @BindView(R.id.imvEventCover)
        ImageView mImvCover;
        @BindView(R.id.imvEventAvatar)
        ImageView mImvAvatar;
        @BindView(R.id.tvEventName)
        TextView mTvShopName;
        @BindView(R.id.tvEventCreatedAt)
        TextView mTvCreatedAt;
        @BindView(R.id.tvEventTitle)
        TextView mTvTitle;
        @BindView(R.id.tvEventStartDate)
        TextView mTvStartDate;
        @BindView(R.id.tvEventEndDate)
        TextView mTvEndDate;
        @BindView(R.id.tvEventDes)
        TextView mTvDescription;
        @BindView(R.id.btnEventJoin)
        Button mBtnJoin;

        public EventHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindData(Event event) {
            super.bindData(event);
            //set cover
            Glide.with(getContext()).load(event.getCover()).into(mImvCover);
            //set avatar
            Glide.with(getContext()).load(event.getShopInfo().getAvatar()).into(mImvAvatar);
            //set name
            mTvShopName.setText(event.getShopInfo().getName());
            //set createdAt
            mTvCreatedAt.setText(event.getCreatedAt());
            //set Title
            mTvTitle.setText(event.getTitle());
            //set start date
            mTvStartDate.setText(event.getStartDate());
            //st end date
            mTvEndDate.setText(event.getEndDate());
            //set description
            mTvDescription.setText(event.getDetail());
            //set visible btnJoin
            if(mEventStatus == ConstantDefine.EVENT_HAPPENING
                    || mEventStatus == ConstantDefine.EVENT_GOING_HAPPEN){
                mBtnJoin.setVisibility(View.VISIBLE);
                mBtnJoin.setOnClickListener(v -> mListener.onItemClick(event, mEvents.indexOf(event)));
            }else if(mEventStatus == ConstantDefine.EVENT_HAPPENED){
                mBtnJoin.setVisibility(View.GONE);
            }else if(mEventStatus == ConstantDefine.EVENT_JOINED){
                mBtnJoin.setVisibility(View.GONE);
                itemView.setOnClickListener(v -> mListener.onItemClick(event, mEvents.indexOf(event)));
            }else{ // event of shop
                mBtnJoin.setVisibility(View.GONE);
                itemView.setOnClickListener(v -> mListener.onItemClick(event, mEvents.indexOf(event)));
            }
        }


    }
}
