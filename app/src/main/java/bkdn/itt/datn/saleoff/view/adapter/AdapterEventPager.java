package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.view.fragment.FragmentBase;

/**
 * Created by TuDLT on 5/29/2018.
 */
public class AdapterEventPager extends FragmentPagerAdapter {
    private List<FragmentBase> mFragmentBases;
    private Context mContext;

    public AdapterEventPager(Context context, FragmentManager fm, List<FragmentBase> fragmentBases) {
        super(fm);
        mContext = context;
        mFragmentBases = fragmentBases;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentBases.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentBases == null ? 0 : mFragmentBases.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getString(R.string.hot_events);
            case 1:
                return mContext.getString(R.string.my_events);
        }
        return null;
    }
}
