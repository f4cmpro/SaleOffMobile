package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.model.model.ModelBase;
import bkdn.itt.datn.saleoff.model.model.SubCategory;
import bkdn.itt.datn.saleoff.model.model.Shop;
import bkdn.itt.datn.saleoff.model.model.User;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 3/13/2018.
 */
public class AdapterFollow extends AdapterBase<AdapterFollow.FollowHolder> {
    private List<Shop> mShops;
    private List<User> mUsers;
    private List<SubCategory> mSubCategories;
    private int mStatus;

    public AdapterFollow(Context context) {
        super(context);
        mShops = new ArrayList<>();
        mShops.add(new Shop());
        mShops.add(new Shop());
        mShops.add(new Shop());
        mUsers = new ArrayList<>();
        mUsers.add(new User());
        mUsers.add(new User());
        mUsers.add(new User());
        mSubCategories = new ArrayList<>();
        mSubCategories.add(new SubCategory());
        mSubCategories.add(new SubCategory());
        mSubCategories.add(new SubCategory());
    }

    @Override
    public FollowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_follow, parent, false);
        return new FollowHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FollowHolder holder, int position) {
        switch (mStatus) {
            case ConstantDefine.SHOP:
                holder.bindData(mShops.get(position));
                break;
            case ConstantDefine.USER:
                holder.bindData(mUsers.get(position));
                break;
            case ConstantDefine.PRODUCT:
                holder.bindData(mSubCategories.get(position));
                break;
        }

    }

    @Override
    public int getItemCount() {
        switch (mStatus) {
            default:
                return mShops.size();
            case ConstantDefine.USER:
                return mUsers.size();
            case ConstantDefine.PRODUCT:
                return mSubCategories.size();
        }
    }

    public void setStatus(int status) {
        mStatus = status;
        notifyDataSetChanged();
    }

    public void addShops(List<Shop> shops) {
        if (mShops == null) {
            mShops = new ArrayList<>();
        }
        mShops.addAll(shops);
        notifyDataSetChanged();
    }

    public void addUsers(List<User> users) {
        if (mUsers == null) {
            mUsers = new ArrayList<>();
        }
        mUsers.addAll(users);
        notifyDataSetChanged();
    }

    public void addProducts(List<SubCategory> subCategories) {
        if (mSubCategories == null) {
            mSubCategories = new ArrayList<>();
        }
        mSubCategories.addAll(subCategories);
        notifyDataSetChanged();
    }

    public void refreshAdapterFollow(){

    }


    protected class FollowHolder extends ViewHolderBase<ModelBase> {


        public FollowHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindData(ModelBase modelBase) {
            super.bindData(modelBase);
        }
    }
}
