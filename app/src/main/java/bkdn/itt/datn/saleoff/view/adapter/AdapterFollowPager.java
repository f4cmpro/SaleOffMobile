package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.view.fragment.FragmentBase;

/**
 * Created by TuDLT on 4/15/2018.
 */
public class AdapterFollowPager extends FragmentPagerAdapter {
    private List<FragmentBase> mFragments;
    private Context mContext;

    public AdapterFollowPager(Context context, FragmentManager fm, List<FragmentBase> fragments) {
        super(fm);
        mFragments = fragments;
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments != null ? mFragments.size() : 0;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getString(R.string.shop_title);
            case 1:
                return mContext.getString(R.string.user_title);
        }
        return null;
    }
}
