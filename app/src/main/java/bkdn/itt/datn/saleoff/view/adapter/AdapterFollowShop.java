package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.Shop;
import bkdn.itt.datn.saleoff.model.model.ShopInfo;
import bkdn.itt.datn.saleoff.presenter.PresenterFollowShop;
import bkdn.itt.datn.saleoff.view.activity.ActivityBase;
import bkdn.itt.datn.saleoff.view.dialog.DialogBonusCode;
import bkdn.itt.datn.saleoff.view.iface.OnClickFollowListener;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 4/15/2018.
 */
public class AdapterFollowShop extends AdapterBase<AdapterFollowShop.FollowShopHolder> {
    private List<ShopInfo> mShopInfoList;
    private OnClickFollowShopListener mListener;
    private PresenterFollowShop mPresenterFollowShop;

    public AdapterFollowShop(Context context, OnClickFollowShopListener listener) {
        super(context);
        mShopInfoList = new ArrayList<>();
        mListener = listener;
    }

    @Override
    public FollowShopHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_follow, parent, false);
        return new FollowShopHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FollowShopHolder holder, int position) {
        holder.bindData(mShopInfoList.get(position));
    }

    @Override
    public int getItemCount() {
        return mShopInfoList.size();
    }

    public void loadMoreShopList(List<ShopInfo> shopInfoList) {
        mShopInfoList.addAll(shopInfoList);
        notifyDataSetChanged();
    }

    public void refreshShopList(List<ShopInfo> shopInfoList) {
        mShopInfoList.clear();
        mShopInfoList.addAll(shopInfoList);
        notifyDataSetChanged();
    }


    protected class FollowShopHolder extends ViewHolderBase<ShopInfo> {
        @BindView(R.id.imvFollowAvatar)
        ImageView mImvAvatar;
        @BindView(R.id.tvFollowName)
        TextView mTvName;
        @BindView(R.id.btnFollow)
        Button mBtnFollow;
        @BindView(R.id.btnUnFollow)
        Button mBtnUnFollow;

        public FollowShopHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindData(ShopInfo shopInfo) {
            super.bindData(shopInfo);
            if (shopInfo != null) {
                if (shopInfo.getAvatar() != null && !shopInfo.getAvatar().isEmpty()) {
                    Glide.with(getContext()).load(shopInfo.getAvatar()).into(mImvAvatar);
                }
                if (shopInfo.getName() != null && !shopInfo.getName().isEmpty()) {
                    mTvName.setText(shopInfo.getName().toString());
                }
                mBtnFollow.setVisibility(View.GONE);
                mBtnUnFollow.setVisibility(View.VISIBLE);
                mBtnFollow.setOnClickListener(v -> {
                    DialogBonusCode dialogBonusCode = DialogBonusCode.newInstance();
                    dialogBonusCode.addOnInputBonusCodeListener(new DialogBonusCode.OnInputBonusCodeListener() {
                        @Override
                        public void onInputBonusCode(int bonusCode) {
                            mListener.onClickFollow(shopInfo.getId(), bonusCode, new OnClickFollowListener() {
                                @Override
                                public void success() {
                                    mBtnFollow.setVisibility(View.GONE);
                                    mBtnUnFollow.setVisibility(View.VISIBLE);
                                }

                                @Override
                                public void fail() {
                                    mBtnFollow.setVisibility(View.VISIBLE);
                                    mBtnUnFollow.setVisibility(View.GONE);
                                }
                            });
                        }

                        @Override
                        public void onCancelBodeCode() {
                            mListener.onClickFollow(shopInfo.getId(), 0, new OnClickFollowListener() {
                                @Override
                                public void success() {
                                    mBtnFollow.setVisibility(View.GONE);
                                    mBtnUnFollow.setVisibility(View.VISIBLE);
                                }

                                @Override
                                public void fail() {
                                    mBtnFollow.setVisibility(View.VISIBLE);
                                    mBtnUnFollow.setVisibility(View.GONE);
                                }
                            });

                        }
                    });
                    dialogBonusCode.show(((ActivityBase) getContext()).getSupportFragmentManager(), null);

                });
                mBtnUnFollow.setOnClickListener(v -> {
                    mListener.onClickUnFollow(shopInfo.getId());
                    mBtnFollow.setVisibility(View.VISIBLE);
                    mBtnUnFollow.setVisibility(View.GONE);
                });
                itemView.setOnClickListener(v -> mListener.onWatchDetailShop(shopInfo.getId()));
            }
        }
    }

    public interface OnClickFollowShopListener {

        void onClickFollow(int shopId, int bonusCode, OnClickFollowListener followListener);

        void onClickUnFollow(int shopId);

        void onWatchDetailShop(int shopId);
    }
}
