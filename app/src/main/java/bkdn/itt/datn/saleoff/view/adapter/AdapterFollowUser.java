package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.UserInfo;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 4/15/2018.
 */
public class AdapterFollowUser extends AdapterBase<AdapterFollowUser.FollowUserHolder> {
    private List<UserInfo> mUserInfoList;
    private OnClickFollowUserListener mListener;

    public AdapterFollowUser(Context context, OnClickFollowUserListener listener) {
        super(context);
        mUserInfoList = new ArrayList<>();
        mListener = listener;

    }

    @Override
    public FollowUserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_follow, parent, false);
        return new FollowUserHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FollowUserHolder holder, int position) {
        holder.bindData(mUserInfoList.get(position));
    }

    @Override
    public int getItemCount() {
        return mUserInfoList.size();
    }

    public void loadMoreUserList(List<UserInfo> userInfos) {
        mUserInfoList.addAll(userInfos);
        notifyDataSetChanged();
    }

    public void refreshUserList(List<UserInfo> userInfoList) {
        mUserInfoList.clear();
        mUserInfoList.addAll(userInfoList);
        notifyDataSetChanged();
    }

    protected class FollowUserHolder extends ViewHolderBase<UserInfo> {
        @BindView(R.id.imvFollowAvatar)
        ImageView mImvAvatar;
        @BindView(R.id.tvFollowName)
        TextView mTvName;
        @BindView(R.id.btnFollow)
        Button mBtnFollow;
        @BindView(R.id.btnUnFollow)
        Button mBtnUnFollow;

        public FollowUserHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindData(UserInfo user) {
            super.bindData(user);
            if (user != null) {
                if (user.getAvatar() != null && !user.getAvatar().isEmpty()) {
                    Glide.with(getContext()).load(user.getAvatar()).into(mImvAvatar);
                }
                if (user.getUserName() != null && !user.getUserName().isEmpty()) {
                    mTvName.setText(user.getUserName().toString());
                } else {
                    mTvName.setText(getString(R.string.unknown));
                }
                mBtnFollow.setVisibility(View.GONE);
                mBtnUnFollow.setVisibility(View.VISIBLE);
                mBtnFollow.setOnClickListener(v -> {
                    mListener.onClickFollow(user.getId());
                    mBtnFollow.setVisibility(View.GONE);
                    mBtnUnFollow.setVisibility(View.VISIBLE);
                });
                mBtnUnFollow.setOnClickListener(v -> {
                    mListener.onClickUnFollow(user.getId());
                    mBtnFollow.setVisibility(View.VISIBLE);
                    mBtnUnFollow.setVisibility(View.GONE);
                });
                itemView.setOnClickListener(v -> mListener.onWatchUserDetail(user.getId()));
            }
        }


    }

    public interface OnClickFollowUserListener {

        void onClickFollow(int userId);

        void onClickUnFollow(int userId);

        void onWatchUserDetail(int userId);
    }
}
