package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.History;

/**
 * Created by TuDLT on 3/29/2018.
 */
public class AdapterHistory extends AdapterBase<AdapterHistory.HistoryHolder> {
    private List<History> mHistoryList;

    public AdapterHistory(Context context) {
        super(context);
        mHistoryList = new ArrayList<>();
        mHistoryList.add(new History());
        mHistoryList.add(new History());
        mHistoryList.add(new History());
        mHistoryList.add(new History());
        mHistoryList.add(new History());
    }

    @Override
    public HistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_history, parent, false);
        return new HistoryHolder(itemView);
    }

    @Override
    public void onBindViewHolder(HistoryHolder holder, int position) {
        holder.bindData(mHistoryList.get(position));
    }

    @Override
    public int getItemCount() {
        return mHistoryList.size();
    }

    public void setHistoryList(List<History> histories){
        mHistoryList = histories;
    }

    protected class HistoryHolder extends ViewHolderBase<History> {

        public HistoryHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void bindData(History history) {
            super.bindData(history);
        }
    }
}
