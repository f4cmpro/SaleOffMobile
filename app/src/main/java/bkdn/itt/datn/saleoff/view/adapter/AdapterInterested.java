package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.Category;
import bkdn.itt.datn.saleoff.utils.ImagesUtils;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 5/14/2018.
 */
public class AdapterInterested extends AdapterBase<AdapterInterested.InterestedHolder> {
    List<Category> mCategories;
    private OnChoiceSubjectListener mListener;


    public AdapterInterested(Context context, List<Category> categories) {
        super(context);
        mCategories = categories;
    }


    @Override
    public InterestedHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_subject, parent, false);
        return new InterestedHolder(itemView);
    }

    @Override
    public void onBindViewHolder(InterestedHolder holder, int position) {
        holder.bindData(mCategories.get(position));
    }

    @Override
    public int getItemCount() {
        return mCategories != null ? mCategories.size() : 0;
    }

    public void setChoiceSubjectListener(OnChoiceSubjectListener listener) {
        mListener = listener;
    }


    protected class InterestedHolder extends ViewHolderBase<Category> {
        @BindView(R.id.imvSubjectItem)
        ImageView mImvSubjectItem;
        @BindView(R.id.imvSubjectCover)
        ImageView mImvCover;
        @BindView(R.id.imvSubjectChecked)
        ImageView mImvChecked;
        @BindView(R.id.tvSubjectName)
        TextView mTvSubjectName;

        public InterestedHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindData(Category category) {
            super.bindData(category);
            if (category.getImvRes() != 0) {
                mImvSubjectItem.setImageResource(category.getImvRes());
            }
            if(category.isSelected()){
                mImvChecked.setVisibility(View.VISIBLE);
                mImvCover.setVisibility(View.VISIBLE);
            }else {
                mImvChecked.setVisibility(View.GONE);
                mImvCover.setVisibility(View.GONE);
            }
            mTvSubjectName.setText(category.getName());
            itemView.setOnClickListener(v -> {
                if (category.isSelected()) {
                    category.setSelected(false);
                    mListener.unChoiceSubject(category.getId());
                } else {
                    category.setSelected(true);
                    mListener.choiceSubject(category.getId());
                }
                notifyDataSetChanged();
            });
        }
    }

    public interface OnChoiceSubjectListener {
        void choiceSubject(int catId);

        void unChoiceSubject(int catId);
    }
}
