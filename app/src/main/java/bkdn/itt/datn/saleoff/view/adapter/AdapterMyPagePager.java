package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.view.fragment.FragmentBase;

/**
 * Created by TuDLT on 4/23/2018.
 */
public class AdapterMyPagePager extends FragmentPagerAdapter {
    private List<FragmentBase> mFragmentBases;
    private Context mContext;
    public AdapterMyPagePager(Context context, FragmentManager fm, List<FragmentBase> fragmentBases) {
        super(fm);
        mFragmentBases = fragmentBases;
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentBases.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentBases.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getString(R.string.my_shops);
            case 1:
                return mContext.getString(R.string.my_work_shop);
        }
        return null;
    }
}
