package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.daimajia.swipe.SwipeLayout;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.model.ShopInfo;
import bkdn.itt.datn.saleoff.view.activity.ActivityShopPrivate;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 3/31/2018.
 */
public class AdapterMyShop extends AdapterBase<AdapterMyShop.MyShopHolder> {
    private List<ShopInfo> mShops;

    public AdapterMyShop(Context context) {
        super(context);
        mShops = new ArrayList<>();
    }

    @Override
    public MyShopHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_owned_shop, parent, false);
        return new MyShopHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyShopHolder holder, int position) {
        holder.bindData(mShops.get(position));
    }

    @Override
    public int getItemCount() {
        return mShops == null ? 0 : mShops.size();
    }

    public void refreshShopInfo(List<ShopInfo> shops) {
        mShops.clear();
        for (ShopInfo item : shops) {
            if (item != null && item.getName() != null && item.getAvatar() != null) {
                mShops.add(item);
            }
        }
        notifyDataSetChanged();
    }

    public void loadMoreShops(List<ShopInfo> shops) {
        for (ShopInfo item : shops) {
            if (item != null && item.getName() != null && item.getAvatar() != null) {
                mShops.add(item);
            }
        }
        notifyDataSetChanged();
    }

    protected class MyShopHolder extends ViewHolderBase<ShopInfo> {
        @BindView(R.id.imvOwnedShopAvatar)
        ImageView imvAvatar;
        @BindView(R.id.tvOwnedShopName)
        TextView tvName;
        @BindView(R.id.tvOwnedShopDelete)
        TextView tvDelete;
        @BindView(R.id.swipeLayoutOwnedShop)
        SwipeLayout swipeLayout;

        public MyShopHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindData(ShopInfo shop) {
            super.bindData(shop);
            Glide.with(getContext()).load(shop.getAvatar()).into(imvAvatar);
            tvName.setText(shop.getName());
            itemView.setOnClickListener(v -> {
                Intent intent = new Intent(getContext(), ActivityShopPrivate.class);
                intent.putExtra(GlobalDefine.KEY_INTENT_SHOP_ID, shop.getId());
                getContext().startActivity(intent);
            });
            swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
                @Override
                public void onStartOpen(SwipeLayout layout) {

                }

                @Override
                public void onOpen(SwipeLayout layout) {

                }

                @Override
                public void onStartClose(SwipeLayout layout) {

                }

                @Override
                public void onClose(SwipeLayout layout) {

                }

                @Override
                public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

                }

                @Override
                public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

                }
            });
        }
    }
}
