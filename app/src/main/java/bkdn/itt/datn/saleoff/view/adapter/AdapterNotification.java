package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.Notification;
import bkdn.itt.datn.saleoff.model.model.NotificationTarget;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 3/16/2018.
 */
public class AdapterNotification extends AdapterBase<AdapterNotification.NotificationHolder> {

    private List<Notification> mNotifications;
    private OnClickNotificationItemListener mListener;

    public AdapterNotification(Context context, OnClickNotificationItemListener listener) {
        super(context);
        mNotifications = new ArrayList<>();
        mListener = listener;
    }

    @Override
    public NotificationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_notification, parent, false);
        return new NotificationHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NotificationHolder holder, int position) {
        holder.bindData(mNotifications.get(position));
    }

    @Override
    public int getItemCount() {
        return mNotifications.size();
    }

    public void refreshListNotification(List<Notification> notifications) {
        mNotifications.clear();
        mNotifications.addAll(notifications);
        notifyDataSetChanged();
    }

    public void removeNotification(int position) {
        mNotifications.remove(position);
        notifyDataSetChanged();
    }

    protected class NotificationHolder extends ViewHolderBase<Notification> {
        @BindView(R.id.imvNotAvatar)
        ImageView imvAvatar;
        @BindView(R.id.tvNotContent)
        TextView tvContent;
        @BindView(R.id.tvNotCreatedAt)
        TextView tvCreatedAt;
        @BindView(R.id.imvNotCover)
        ImageView imvCover;

        public NotificationHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindData(Notification notification) {
            super.bindData(notification);
            itemView.setOnClickListener(v -> mListener.onClickNotificationItem(mNotifications
                    .indexOf(notification), notification.getId(), notification.getTarget()));
            if (notification.getBody() != null) {
                //set avatar
                if (notification.getBody().getAvatar() != null) {
                    Glide.with(getContext()).load(notification.getBody().getAvatar())
                            .into(imvAvatar);
                }
                //set content
                if (notification.getBody().getContent() != null) {
                    tvContent.setText(notification.getBody().getContent());
                }

                // time ago
                if (notification.getCreated_at() != null) {
                    tvCreatedAt.setText(notification.getCreated_at());
                }
            }
        }
    }

    public interface OnClickNotificationItemListener {
        void onClickNotificationItem(int position, String notificationId, NotificationTarget target);
    }
}
