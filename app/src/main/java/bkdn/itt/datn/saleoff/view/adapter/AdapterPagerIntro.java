package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import bkdn.itt.datn.saleoff.R;

/**
 * Created by Tu Doan Le on 12/27/2017.
 */

public class AdapterPagerIntro extends PagerAdapter {

    private int[] mImagesResource = {R.drawable.skip_one, R.drawable.skip_two};
    private Context mContext;

    public AdapterPagerIntro(Context context) {
        mContext = context;
    }

    @Override
    public int getCount() {
        return mImagesResource.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.item_intro, container, false);

        ImageView imageView = itemView.findViewById(R.id.imvIntro);
        imageView.setImageResource(mImagesResource[position]);
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
