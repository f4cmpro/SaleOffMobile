package bkdn.itt.datn.saleoff.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import bkdn.itt.datn.saleoff.view.fragment.FragmentBase;

/**
 * Created by TuDLT on 3/3/2018.
 */
public class AdapterPagerMain extends FragmentPagerAdapter {
    private List<FragmentBase> mFragments;

    public AdapterPagerMain(FragmentManager fm, List<FragmentBase> fragments) {
        super(fm);
        mFragments = fragments;
    }

    @Override
    public int getCount() {
        return mFragments != null ? mFragments.size() : 0;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }
}
