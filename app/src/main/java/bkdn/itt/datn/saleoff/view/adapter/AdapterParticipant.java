package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.Participant;
import bkdn.itt.datn.saleoff.view.activity.ActivityEventManage;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 6/1/2018.
 */
public class AdapterParticipant extends AdapterBase<AdapterParticipant.PresenterHolder> {

    private List<Participant> mParticipants;
    private OnUseEventPointListener mListener;

    public AdapterParticipant(Context context, List<Participant> participants) {
        super(context);
        mParticipants = new ArrayList<>();
        mParticipants.addAll(participants);

    }

    public void setListener(OnUseEventPointListener listener) {
        mListener = listener;
    }


    @Override
    public PresenterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_participants, parent, false);
        return new PresenterHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PresenterHolder holder, int position) {
        holder.bindData(mParticipants.get(position));
    }

    @Override
    public int getItemCount() {
        return mParticipants.size();
    }

    public void refreshData(List<Participant> participants) {
        mParticipants.clear();
        mParticipants.addAll(participants);
        notifyDataSetChanged();
    }

    protected class PresenterHolder extends ViewHolderBase<Participant> {
        @BindView(R.id.imvParticipantAvatar)
        ImageView mImvAvatar;
        @BindView(R.id.tvParticipantName)
        TextView mTvParticipantName;
        @BindView(R.id.tvParticipantEventPoint)
        TextView mTvEventPoint;
        @BindView(R.id.btnParticipantUsePoint)
        Button mBtnUsePoint;
        @BindView(R.id.btnParticipantUsedPoint)
        Button mBtnUsedPoint;
        @BindView(R.id.tvParticipantShareCode)
        TextView mTvShareCode;

        public PresenterHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindData(Participant participant) {
            super.bindData(participant);
            if (participant.getAvatar() != null) {
                Glide.with(getContext()).load(participant.getAvatar()).into(mImvAvatar);
            }
            mTvParticipantName.setText(participant.getParticipantName());
            mTvEventPoint.setText(String.valueOf(participant.getPoint()));
            mTvShareCode.setText(String.valueOf(participant.getShareCode()));
            if (participant.getIsUsed() == 1) {
                mBtnUsedPoint.setVisibility(View.VISIBLE);
                mBtnUsePoint.setVisibility(View.GONE);
            } else {
                mBtnUsedPoint.setVisibility(View.GONE);
                mBtnUsePoint.setVisibility(View.VISIBLE);
                mBtnUsePoint.setOnClickListener(v -> {
                    mListener.useEventPoint(participant.getParticipantId(), new ActivityEventManage
                            .OnUseEvenPointCallBack() {
                        @Override
                        public void onSuccess() {
                            mBtnUsedPoint.setVisibility(View.VISIBLE);
                            mBtnUsePoint.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            mBtnUsedPoint.setVisibility(View.GONE);
                            mBtnUsePoint.setVisibility(View.VISIBLE);
                        }
                    });
                });
            }


        }
    }

    public interface OnUseEventPointListener {
        void useEventPoint(int participantId, ActivityEventManage.OnUseEvenPointCallBack callBack);
    }
}
