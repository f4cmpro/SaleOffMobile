package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.Participant;
import bkdn.itt.datn.saleoff.model.model.User;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 6/1/2018.
 */
public class AdapterPresenter extends AdapterBase<AdapterPresenter.PresenterHolder> {

    private List<Participant> mPresenters;
    private OnSelectPresenterListener mListener;

    public AdapterPresenter(Context context, List<Participant> presenters) {
        super(context);
        mPresenters = new ArrayList<>();
        mPresenters.addAll(presenters);

    }

    public void setListener(OnSelectPresenterListener listener) {
        mListener = listener;
    }


    @Override
    public PresenterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_presenter, parent, false);
        return new PresenterHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PresenterHolder holder, int position) {
        holder.bindData(mPresenters.get(position));
    }

    @Override
    public int getItemCount() {
        return mPresenters.size();
    }

    public void refreshData(List<Participant> participants) {
        mPresenters.clear();
        mPresenters.addAll(participants);
        notifyDataSetChanged();
    }

    protected class PresenterHolder extends ViewHolderBase<Participant> {
        @BindView(R.id.imvPresenterAvatar)
        ImageView mImvAvatar;
        @BindView(R.id.tvPresenterName)
        TextView mTvPresenterName;
        @BindView(R.id.btnPresenterSelect)
        Button mBtnSelect;
        @BindView(R.id.btnPresenterUnSelect)
        Button mBtnUnSelect;

        public PresenterHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindData(Participant participant) {
            super.bindData(participant);
            if (participant.getAvatar() != null) {
                Glide.with(getContext()).load(participant.getAvatar()).into(mImvAvatar);
            }
            if (participant.isSelect()) {
                mBtnSelect.setVisibility(View.GONE);
                mBtnUnSelect.setVisibility(View.VISIBLE);
            } else {
                mBtnSelect.setVisibility(View.VISIBLE);
                mBtnUnSelect.setVisibility(View.GONE);
            }
            mTvPresenterName.setText(participant.getParticipantName());
            mBtnSelect.setOnClickListener(v -> {

                for (Participant item : mPresenters) {
                    if (participant.equals(item)) {
                        item.setSelect(true);
                    } else {
                        item.setSelect(false);
                    }
                }
                notifyDataSetChanged();
                if (participant.getShareCode() != 0) {
                    mBtnSelect.setVisibility(View.GONE);
                    mBtnUnSelect.setVisibility(View.VISIBLE);
                    mListener.onSelect(participant.getShareCode());
                } else {
                    mBtnSelect.setVisibility(View.VISIBLE);
                    mBtnUnSelect.setVisibility(View.GONE);
                    Toast.makeText(getContext(), "Mã người giới thiệu không hợp lệ!",
                            Toast.LENGTH_SHORT).show();
                }
            });
            mBtnUnSelect.setOnClickListener(v -> {
                mBtnSelect.setVisibility(View.VISIBLE);
                mBtnUnSelect.setVisibility(View.GONE);
                mListener.onUnSelect();
            });

        }
    }

    public interface OnSelectPresenterListener {
        void onSelect(int shareCode);

        void onUnSelect();
    }
}
