package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.Post;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 3/12/2018.
 */
public class AdapterRecentPosts extends AdapterBase<AdapterRecentPosts.RecentPostHolder> {
    private List<Post> mPosts;
    private OnClickRecentPostItemListener mListener;

    public AdapterRecentPosts(Context context) {
        super(context);
        mPosts = new ArrayList<>();
        mListener = (OnClickRecentPostItemListener) context;
    }

    @Override
    public RecentPostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_recent_posts, parent, false);
        return new RecentPostHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecentPostHolder holder, int position) {
        holder.bindData(mPosts.get(position));
    }

    @Override
    public int getItemCount() {
        return mPosts.size();
    }

    public void setList(List<Post> posts) {
        mPosts.addAll(posts);
        notifyDataSetChanged();
    }

    public void refreshList(List<Post> posts) {
        mPosts.clear();
        mPosts.addAll(posts);
        notifyDataSetChanged();
    }

    protected class RecentPostHolder extends ViewHolderBase<Post> {
        @BindView(R.id.tvRecentPostSale)
        TextView mTvSalePercent;
        @BindView(R.id.tvRecentPostFrom)
        TextView mTvFromDate;
        @BindView(R.id.tvRecentPostTo)
        TextView mTvToDate;
        @BindView(R.id.tvRecentPostAddress)
        TextView mTvAddress;
        @BindView(R.id.tvRecentPostTitle)
        TextView mTvPostTitle;
        @BindView(R.id.imvRecentPostCover)
        ImageView mImvPostCover;

        public RecentPostHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindData(Post post) {
            super.bindData(post);

            //Cover
            if (post.getCover() != null) {
                Glide.with(getContext()).load(post.getCover()).into(mImvPostCover);
            }

            //Title Post
            if (post.getPostTitle() != null && !post.getPostTitle().isEmpty()) {
                mTvPostTitle.setText(post.getPostTitle());
            }
            //Sale percent Post
            mTvSalePercent.setText("-" + String.valueOf(post.getSalePercent()) + "%");
            //Start sale
            if (post.getFromDate() != null && !post.getFromDate().isEmpty()) {
                mTvFromDate.setText(post.getFromDate());
            }

            //End sale
            if (post.getToDate() != null && !post.getToDate().isEmpty()) {
                mTvToDate.setText(post.getFromDate());
            }

            //set address
            if (post.getShopAddress() != null) {
                mTvAddress.setText(post.getShopAddress());
            }

            itemView.setOnClickListener(v -> {
                if (mListener != null) {
                    mListener.onClickRecentPostItem(post.getPostId());
                }
            });


        }
    }

    public interface OnClickRecentPostItemListener {
        void onClickRecentPostItem(int postId);
    }
}