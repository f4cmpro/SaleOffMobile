package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.Post;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 4/27/2018.
 */
public class AdapterSearchPost extends AdapterBase<AdapterSearchPost.SearchPostHolder> {

    private List<Post> mPosts;
    private OnClickItemSearchPostListener mListener;

    public AdapterSearchPost(Context context, OnClickItemSearchPostListener listener) {
        super(context);
        mPosts = new ArrayList<>();
        mListener = listener;
    }

    @Override
    public SearchPostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_search_post, parent,
                false);
        return new SearchPostHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SearchPostHolder holder, int position) {
        holder.bindData(mPosts.get(position));
    }

    @Override
    public int getItemCount() {
        return mPosts.size();
    }

    public void addPostList(List<Post> posts) {
        if (mPosts != null && posts != null) {
            mPosts.addAll(posts);
            notifyDataSetChanged();
        }
    }

    public void refreshPostList(List<Post> posts) {
        if (mPosts != null && !mPosts.isEmpty()) {
            mPosts.clear();
        }
        if (posts != null) {
            mPosts.addAll(posts);
            notifyDataSetChanged();
        }
    }

    public void clearPostList() {
        if (mPosts != null && !mPosts.isEmpty()) {
            mPosts.clear();
            notifyDataSetChanged();
        }
    }

    protected class SearchPostHolder extends ViewHolderBase<Post> {
        @BindView(R.id.imvSearchPostAvatar)
        ImageView mImvAvatar;
        @BindView(R.id.tvSearchPostName)
        TextView mTvName;
        @BindView(R.id.tvSearchPostCreatedAt)
        TextView mTvCreatedAt;
        @BindView(R.id.tvSearchPostSaleOff)
        TextView mTvSaleOff;
        @BindView(R.id.tvSearchPostFrom)
        TextView mTvFrom;
        @BindView(R.id.tvSearchPostTo)
        TextView mTvTo;
        @BindView(R.id.tvSearchPostAddress)
        TextView mTvAddress;
        @BindView(R.id.tvSearchPostTitle)
        TextView mTvTitle;
        @BindView(R.id.imvSearchPostCover)
        ImageView mImvCover;
        @BindView(R.id.tvSearchPostLikeNumber)
        TextView mTvLikeNumber;
        @BindView(R.id.tvSearchPostCommentNumber)
        TextView mTvCommentNumber;

        public SearchPostHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindData(Post post) {
            super.bindData(post);
            if (post != null) {
                if (post.getShop() != null) {
                    //set avatar
                    if (post.getShop().getAvatar() != null) {
                        Glide.with(getContext()).load(post.getShop().getAvatar()).into(mImvAvatar);
                    }
                    //set name
                    if (post.getShop().getName() != null) {
                        mTvName.setText(post.getShop().getName());
                    }
                } else {
                    //set avatar
                    if (post.getUser().getAvatar() != null) {
                        Glide.with(getContext()).load(post.getUser().getAvatar()).into(mImvAvatar);
                    }
                    //set name
                    if (post.getUser().getUserName() != null) {
                        mTvName.setText(post.getUser().getUserName());
                    }
                }
                //set created at
                if (post.getCreatedDate() != null) {
                    mTvCreatedAt.setText(post.getCreatedDate());
                }
                //ser percent
                if (post.getSalePercent() != 0) {
                    String saleOffText = String.valueOf(post.getSalePercent()) + "%";
                    mTvSaleOff.setText(saleOffText);
                }
                //set from
                if (post.getFromDate() != null) {
                    mTvFrom.setText(post.getFromDate());
                }
                //set to
                if (post.getToDate() != null) {
                    mTvTo.setText(post.getToDate());
                }
                //set address
                if (post.getShopAddress() != null) {
                    mTvAddress.setText(post.getShopAddress());
                }
                if (post.getPostTitle() != null) {
                    mTvTitle.setText(post.getPostTitle());
                }
                //set cover
                if (post.getCover() != null) {
                    Glide.with(getContext()).load(post.getCover()).into(mImvCover);
                }
                //set like number
                String likeNumberText = String.valueOf(post.getLikeNumber())
                        + " " + getContext().getResources().getString(R.string.like);
                mTvLikeNumber.setText(likeNumberText);

                //set comment number
                String commentNumberText = String.valueOf(post.getCommentNumber())
                        + " " + getContext().getResources().getString(R.string.comments);
                mTvCommentNumber.setText(commentNumberText);
                //set click item
                itemView.setOnClickListener(v -> mListener.onClickItem(post));
            }
        }
    }

    public interface OnClickItemSearchPostListener {
        void onClickItem(Post post);
    }
}
