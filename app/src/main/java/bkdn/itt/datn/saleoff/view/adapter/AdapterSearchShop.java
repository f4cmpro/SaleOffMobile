package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.Post;
import bkdn.itt.datn.saleoff.model.model.Shop;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 4/28/2018.
 */
public class AdapterSearchShop extends AdapterBase<AdapterSearchShop.SearchShopHolder> {

    private List<Shop> mShops;
    private OnClickItemSearchShopListener mListener;

    public AdapterSearchShop(Context context, OnClickItemSearchShopListener listener) {
        super(context);
        mShops = new ArrayList<>();
        mListener = listener;
    }

    @Override
    public SearchShopHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_search_shop, parent,
                false);
        return new SearchShopHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SearchShopHolder holder, int position) {
        holder.bindData(mShops.get(position));
    }

    @Override
    public int getItemCount() {
        return mShops.size();
    }

    public void addShopList(List<Shop> shops) {
        if (mShops != null && shops != null) {
            mShops.addAll(shops);
            notifyDataSetChanged();
        }
    }

    public void refreshShopList(List<Shop> shops) {
        if (mShops != null && !mShops.isEmpty()) {
            mShops.clear();
        }
        if (shops != null) {
            mShops.addAll(shops);
            notifyDataSetChanged();
        }
    }

    public void clearShopList() {
        if (mShops != null && !mShops.isEmpty()) {
            mShops.clear();
            notifyDataSetChanged();
        }
    }

    public void setFollowFail(int position) {
        mShops.get(position).setIsFollow(0);
        notifyDataSetChanged();
    }

    public void setUnFollowFail(int position) {
        mShops.get(position).setIsFollow(1);
    }

    protected class SearchShopHolder extends ViewHolderBase<Shop> {
        @BindView(R.id.imvSearchShopAvatar)
        ImageView mImvAvatar;
        @BindView(R.id.tvSearchShopName)
        TextView mTvName;
        @BindView(R.id.tvSearchShopAddress)
        TextView mTvAddress;
        @BindView(R.id.tvSearchShopFollow)
        TextView mTvFollows;
        @BindView(R.id.tvSearchShopRate)
        TextView mTvRate;
        @BindView(R.id.btnSearchShopFollow)
        Button mBtnFollow;
        @BindView(R.id.btnSearchShopUnFollow)
        Button mBtnUnFollow;

        public SearchShopHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindData(Shop shop) {
            super.bindData(shop);
            //set avatar
            if (shop.getAvatar() != null) {
                Glide.with(getContext()).load(shop.getAvatar()).into(mImvAvatar);
            }
            //set name
            if (shop.getName() != null) {
                mTvName.setText(shop.getName());
            }

            //set address
            if (shop.getAddress() != null) {
                mTvAddress.setText(shop.getAddress());
            }

            //set follows
            String likeNumberText = String.valueOf(shop.getFollowers())
                    + " " + getContext().getResources().getString(R.string.people);
            mTvFollows.setText(likeNumberText);

            //set rate number
            String commentNumberText = String.valueOf(shop.getRate())
                    + getContext().getResources().getString(R.string.ratio_rate);
            mTvRate.setText(commentNumberText);

            //set follow button view
            if (shop.getIsFollow() == 1) {
                mBtnFollow.setVisibility(View.GONE);
                mBtnUnFollow.setVisibility(View.VISIBLE);
            } else {
                mBtnFollow.setVisibility(View.VISIBLE);
                mBtnUnFollow.setVisibility(View.GONE);
            }
            //set click follow
            mBtnFollow.setOnClickListener(v -> {
                mBtnFollow.setVisibility(View.GONE);
                mBtnUnFollow.setVisibility(View.VISIBLE);
                mListener.onClickFollow(shop.getShopId(), mShops.indexOf(shop));
            });
            //set click UnFollow
            mBtnUnFollow.setOnClickListener(v -> {
                mBtnFollow.setVisibility(View.VISIBLE);
                mBtnUnFollow.setVisibility(View.GONE);
                mListener.onClickUnFollow(shop.getShopId(), mShops.indexOf(shop));
            });
            //set click item
            itemView.setOnClickListener(v -> mListener.onClickItem(shop));
        }
    }

    public interface OnClickItemSearchShopListener {
        void onClickItem(Shop shop);

        void onClickFollow(int shopId, int position);

        void onClickUnFollow(int shopId, int position);
    }
}