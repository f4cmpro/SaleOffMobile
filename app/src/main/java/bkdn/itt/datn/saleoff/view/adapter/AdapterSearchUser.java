package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.Post;
import bkdn.itt.datn.saleoff.model.model.Shop;
import bkdn.itt.datn.saleoff.model.model.User;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 4/29/2018.
 */
public class AdapterSearchUser extends AdapterBase<AdapterSearchUser.SearchUserHolder> {

    private List<User> mUsers;
    private OnClickItemSearchUserListener mListener;
    private boolean mIsFollow;

    public AdapterSearchUser(Context context, OnClickItemSearchUserListener listener) {
        super(context);
        mUsers = new ArrayList<>();
        mListener = listener;
        mIsFollow = true;
    }

    @Override
    public SearchUserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_search_user, parent,
                false);
        return new SearchUserHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SearchUserHolder holder, int position) {
        holder.bindData(mUsers.get(position));
    }

    @Override
    public int getItemCount() {
        return mUsers.size();
    }

    public void addUserList(List<User> users) {
        if (mUsers != null && users != null) {
            mUsers.addAll(users);
            notifyDataSetChanged();
        }
    }

    public void refreshUserList(List<User> users) {
        if (mUsers != null && !mUsers.isEmpty()) {
            mUsers.clear();
        }
        if (users != null) {
            mUsers.addAll(users);
            notifyDataSetChanged();
        }
    }

    public void clearUserList() {
        if (mUsers != null && !mUsers.isEmpty()) {
            mUsers.clear();
            notifyDataSetChanged();
        }
    }

    public void setFollow(boolean isFollow) {
        mIsFollow = isFollow;
    }

    public void setFollowFail(int position) {
        mUsers.get(position).setIsFollow(0);
    }

    public void setUnFollowFail(int position) {
        mUsers.get(position).setIsFollow(1);
    }

    protected class SearchUserHolder extends ViewHolderBase<User> {
        @BindView(R.id.imvSearchUserAvatar)
        ImageView mImvAvatar;
        @BindView(R.id.tvSearchUserName)
        TextView mTvName;
        @BindView(R.id.tvSearchUsUserFollow)
        TextView mTvFollows;
        @BindView(R.id.btnSearchUserFollow)
        Button mBtnFollow;
        @BindView(R.id.btnSearchUserUnFollow)
        Button mBtnUnFollow;

        public SearchUserHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindData(User user) {
            super.bindData(user);
            //set avatar
            if (user.getAvatar() != null) {
                Glide.with(getContext()).load(user.getAvatar()).into(mImvAvatar);
            }
            //set name
            if (user.getUsername() != null) {
                mTvName.setText(user.getUsername());
            }

            //set follows
            String likeNumberText = String.valueOf(user.getFollowNumber())
                    + " " + getContext().getResources().getString(R.string.people);
            mTvFollows.setText(likeNumberText);

            //set follow button view
            if (user.getIsFollow() == 1) {
                mBtnFollow.setVisibility(View.GONE);
                mBtnUnFollow.setVisibility(View.VISIBLE);
            } else {
                mBtnFollow.setVisibility(View.VISIBLE);
                mBtnUnFollow.setVisibility(View.GONE);
            }
            //set click follow
            mBtnFollow.setOnClickListener(v -> {
                mBtnFollow.setVisibility(View.GONE);
                mBtnUnFollow.setVisibility(View.VISIBLE);
                mListener.onClickFollow(user.getId(), mUsers.indexOf(user));
            });
            //set click UnFollow
            mBtnUnFollow.setOnClickListener(v -> {
                mBtnFollow.setVisibility(View.VISIBLE);
                mBtnUnFollow.setVisibility(View.GONE);
                mListener.onClickUnFollow(user.getId(), mUsers.indexOf(user));
            });

            //set click item
            itemView.setOnClickListener(v -> mListener.onClickItem(user));
        }
    }

    public interface OnClickItemSearchUserListener {
        void onClickItem(User user);

        void onClickFollow(int userId, int position);

        void onClickUnFollow(int userId, int position);
    }
}