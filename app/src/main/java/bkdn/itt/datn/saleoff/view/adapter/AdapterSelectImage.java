package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.utils.ImagesUtils;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 3/31/2018.
 */
public class AdapterSelectImage extends AdapterBase<AdapterSelectImage.SelectImageHolder> {
    private List<String> mImgUrls;
    private OnSelectedImageListener mListener;

    public AdapterSelectImage(Context context) {
        super(context);
        mImgUrls = ImagesUtils.loadAllImagesFromSdCard(context);
        mListener = (OnSelectedImageListener) context;
    }

    @Override
    public SelectImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_select_image, parent, false);
        return new SelectImageHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SelectImageHolder holder, int position) {
        holder.bindData(mImgUrls.get(position));
    }

    @Override
    public int getItemCount() {
        return mImgUrls != null ? mImgUrls.size() : 0;
    }

    protected class SelectImageHolder extends ViewHolderBase<String> {
        @BindView(R.id.imvSelectImg)
        ImageView mImvSelectImg;
        @BindView(R.id.cbxSelectImg)
        CheckBox mCbxSelectImg;

        public SelectImageHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindData(String url) {
            super.bindData(url);
            Glide.with(getContext()).load(url).into(mImvSelectImg);
            mCbxSelectImg.setOnClickListener(v -> {
                if (mCbxSelectImg.isChecked()) {
                    mListener.onSelectedImage(url, true);
                } else {
                    mListener.onSelectedImage(url, false);
                }
            });
        }
    }

    public interface OnSelectedImageListener {
        void onSelectedImage(String url, boolean isSelected);
    }
}
