package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.mCloud.UploadImageCloud;
import bkdn.itt.datn.saleoff.utils.ImagesUtils;
import bkdn.itt.datn.saleoff.view.activity.ActivityBase;
import bkdn.itt.datn.saleoff.view.activity.ActivityEventCreate;
import bkdn.itt.datn.saleoff.view.activity.ActivitySelectImages;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

/**
 * Created by TuDLT on 4/1/2018.
 */
public class AdapterSelectedImage extends AdapterBase<AdapterSelectedImage.SelectImageHolder> {
    private List<String> mImgUrls;
    private UploadImageCloud mCloud;
    private OnSelectedImageListener mListener;
    private boolean mIsEdited;

    public AdapterSelectedImage(Context context, boolean isEdited) {
        super(context);
        mImgUrls = new ArrayList<>();
        mImgUrls.add(new String());
        mCloud = UploadImageCloud.getInstance();
        mListener = (OnSelectedImageListener) context;
        mIsEdited = isEdited;
    }

    @Override
    public SelectImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_selected_image, parent, false);
        return new SelectImageHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SelectImageHolder holder, int position) {
        holder.bindData(mImgUrls.get(position));
    }

    @Override
    public int getItemCount() {
        return mImgUrls != null ? mImgUrls.size() : 0;
    }

    public void addMoreImages(List<String> imgUrls) {
        mImgUrls.addAll(imgUrls);
        notifyDataSetChanged();
    }

    protected class SelectImageHolder extends ViewHolderBase<String> {
        @BindView(R.id.imvSelectedImg)
        ImageView mImvSelectedImg;
        @BindView(R.id.tvAddImages)
        TextView mTvAddImages;
        @BindView(R.id.flSelectedImg)
        FrameLayout mFlSelectedImg;
        @BindView(R.id.prbSelectedImg)
        ProgressBar mProgressBar;

        public SelectImageHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindData(String url) {
            super.bindData(url);
            if (url == null || url.isEmpty()) {
                mFlSelectedImg.setVisibility(View.GONE);
                mTvAddImages.setVisibility(View.VISIBLE);
                mTvAddImages.setOnClickListener(v -> {
                    Intent pickImages = new Intent(getContext(), ActivitySelectImages.class);
                    ((ActivityBase) getContext()).startActivityForResult(pickImages,
                            ConstantDefine.PICK_IMAGES_REQUEST);
                });
            } else {
                mFlSelectedImg.setVisibility(View.VISIBLE);
                mTvAddImages.setVisibility(View.GONE);
                if (mIsEdited) {
                    itemView.setTag("Uploaded");
                }
                if (itemView.getTag() != null) {
                    Glide.with(getContext()).load(url)
                            .listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    mProgressBar.setVisibility(View.GONE);
                                    return false;
                                }
                            })
                            .into(mImvSelectedImg);
                } else {
                    File compressImage = ImagesUtils.compressPostImage(getContext(), new File(url));
                    mCloud.uploadPostPhotos(getContext(), compressImage,
                            new UploadImageCloud.OnUpLoadPostPhotosListener() {
                                @Override
                                public void onUpLoadPostPhotos(String imgUrl) {
                                    mListener.onSelectedImage(imgUrl);
                                    mProgressBar.setVisibility(View.GONE);
                                    Glide.with(getContext()).load(url).into(mImvSelectedImg);
                                    itemView.setTag("Uploaded");
                                }

                                @Override
                                public void onUpLoadFail(String errorMessage) {
                                    Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
                                }
                            });
                }

            }
        }
    }

    public interface OnSelectedImageListener {
        void onSelectedImage(String imgUrl);
    }
}
