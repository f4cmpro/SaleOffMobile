package bkdn.itt.datn.saleoff.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.model.Category;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 4/6/2018.
 */
public class AdapterShopCategory extends AdapterBase<AdapterShopCategory.ShopCategoryHolder> {
    private List<Category> mCategoryList;

    public AdapterShopCategory(Context context, List<Category> shopCategories) {
        super(context);
        mCategoryList = shopCategories;
    }

    @Override
    public ShopCategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_category, parent, false);
        return new ShopCategoryHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ShopCategoryHolder holder, int position) {
        holder.bindData(mCategoryList.get(position));
    }

    @Override
    public int getItemCount() {
        return mCategoryList.size();
    }

    public void setCategoryList(List<Category> categoryList) {
        mCategoryList = categoryList;
        notifyDataSetChanged();
    }

    protected class ShopCategoryHolder extends ViewHolderBase<Category> {
        @BindView(R.id.tvItemShopCategory)
        TextView mTvItem;

        public ShopCategoryHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindData(Category category) {
            super.bindData(category);
            mTvItem.setText(category.getName());
            itemView.setOnClickListener(v -> {
                Intent catIntent = ((Activity) getContext()).getIntent();
                catIntent.putExtra(GlobalDefine.KEY_INTENT_CATEGORY, category);
                ((Activity) getContext()).setResult(Activity.RESULT_OK, catIntent);
                ((Activity) getContext()).finish();
            });
        }
    }
}
