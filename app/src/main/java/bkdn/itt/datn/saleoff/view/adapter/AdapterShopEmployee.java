package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.User;
import bkdn.itt.datn.saleoff.utils.DialogUtils;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 3/25/2018.
 */
public class AdapterShopEmployee extends AdapterBase<AdapterShopEmployee.MemberHolder> {
    private List<User> mUsers;
    private OnDeletedMemberListener mListener;

    public AdapterShopEmployee(Context context, OnDeletedMemberListener listener) {
        super(context);
        mUsers = new ArrayList<>();
        mListener = listener;
    }


    @Override
    public MemberHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_shop_member_more, parent, false);
        return new MemberHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MemberHolder holder, int position) {
        holder.bindData(mUsers.get(position));
    }

    @Override
    public int getItemCount() {
        return mUsers.size();
    }


    public void setListUsers(List<User> users) {
        mUsers.clear();
        mUsers.addAll(users);
        notifyDataSetChanged();
    }

    public void deleteEmployeeSuccess(int position) {
        mUsers.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mUsers.size());
    }

    protected class MemberHolder extends ViewHolderBase<User> {
        @BindView(R.id.imvMoreAvatar)
        ImageView mImvAvatar;
        @BindView(R.id.tvMoreFullName)
        TextView mTvName;
        @BindView(R.id.imbMoreDelete)
        ImageButton mImbDelete;
        @BindView(R.id.expandedLayoutMore)
        ExpandableLayout mExpandableLayout;

        public MemberHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindData(User user) {
            super.bindData(user);
            if (user.getAvatar() != null) {
                Glide.with(getContext()).load(user.getAvatar()).into(mImvAvatar);
            }
            mTvName.setText(user.getUsername());
            mImbDelete.setOnClickListener(v -> {
                String title = getContext().getString(R.string.delete_employee_title);
                DialogUtils.createConfirmDialog(getContext(), title, user.getUsername(), new DialogUtils.DialogConfirmCallback() {
                    @Override
                    public void onClickPositiveButton() {
                        mListener.onDeletedEmployee(user.getId(), mUsers.indexOf(user));
                    }

                    @Override
                    public void onClickNegativeButton() {
                    }
                });
            });
        }
    }

    public interface OnDeletedMemberListener {
        void onDeletedEmployee(int userId, int position);
    }
}
