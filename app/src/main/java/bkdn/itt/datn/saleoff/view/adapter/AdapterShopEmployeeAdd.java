package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.UserInfo;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 3/25/2018.
 */
public class AdapterShopEmployeeAdd extends AdapterBase<AdapterShopEmployeeAdd.MemberHolder> {
    private List<UserInfo> mUserInfoList;
    private OnChosenMemberListener mListener;

    public AdapterShopEmployeeAdd(Context context) {
        super(context);
        mUserInfoList = new ArrayList<>();
        mListener = (OnChosenMemberListener) context;
    }


    @Override
    public MemberHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_shop_member_add, parent, false);
        return new MemberHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MemberHolder holder, int position) {
        holder.bindData(mUserInfoList.get(position));
    }

    @Override
    public int getItemCount() {
        return mUserInfoList.size();
    }

    public void setListUserInfo(List<UserInfo> infoList) {
        mUserInfoList.clear();
        mUserInfoList.addAll(infoList);
        notifyDataSetChanged();
    }

    protected class MemberHolder extends ViewHolderBase<UserInfo> {
        @BindView(R.id.imvAddMemberAvatar)
        ImageView mImvAddMemberAvatar;
        @BindView(R.id.tvAddMemberName)
        TextView mTvAddMemberName;
        @BindView(R.id.cbxAddMember)
        CheckBox mCbxAddMember;

        public MemberHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindData(UserInfo userInfo) {
            super.bindData(userInfo);
            if (userInfo.getAvatar() != null) {
                Glide.with(getContext()).load(userInfo.getAvatar()).into(mImvAddMemberAvatar);
            }
            mTvAddMemberName.setText(userInfo.getUserName());
            mCbxAddMember.setChecked(userInfo.isChecked());
            mCbxAddMember.setOnClickListener(v -> {
                if (mCbxAddMember.isChecked()) {
                    userInfo.setChecked(true);
                    mListener.onChosenMember(userInfo);
                } else {
                    userInfo.setChecked(false);
                    mListener.onUnChosenMember(userInfo);
                }
            });
        }
    }

    public interface OnChosenMemberListener {
        void onChosenMember(UserInfo userInfo);

        void onUnChosenMember(UserInfo userInfo);
    }
}
