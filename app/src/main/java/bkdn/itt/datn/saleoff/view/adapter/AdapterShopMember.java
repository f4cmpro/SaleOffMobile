package bkdn.itt.datn.saleoff.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.User;
import bkdn.itt.datn.saleoff.view.activity.ActivityBase;
import bkdn.itt.datn.saleoff.view.dialog.DialogMinusBonusPoint;
import bkdn.itt.datn.saleoff.view.fragment.FragmentShopMembers;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 5/19/2018.
 */
public class AdapterShopMember extends AdapterBase<AdapterShopMember.MemberHolder> {

    private List<User> mUsers;
    private OnMinusBonusListener mListener;

    public AdapterShopMember(Context context, OnMinusBonusListener listener) {
        super(context);
        mUsers = new ArrayList<>();
        mListener = listener;
    }


    @Override
    public MemberHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_shop_member_2, parent, false);
        return new MemberHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MemberHolder holder, int position) {
        holder.bindData(mUsers.get(position));
    }

    @Override
    public int getItemCount() {
        return mUsers.size();
    }

    public void searchUsers(String key){

    }



    public void refreshData(List<User> users) {
        mUsers.clear();
        mUsers.addAll(users);
        notifyDataSetChanged();
    }

    protected class MemberHolder extends ViewHolderBase<User> {
        @BindView(R.id.imvMemberAvatar)
        ImageView mImvAvatar;
        @BindView(R.id.tvMemberTotalPoint)
        TextView mTvTotalPoint;
        @BindView(R.id.tvMemberName)
        TextView mTvName;
        @BindView(R.id.tvMemberCode)
        TextView mTvCode;

        public MemberHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindData(User user) {
            super.bindData(user);
            if (user.getAvatar() != null) {
                Glide.with(getContext()).load(user.getAvatar()).into(mImvAvatar);
            }
            mTvTotalPoint.setText(String.valueOf(user.getTotal()));
            mTvName.setText(user.getUsername());
            mTvCode.setText(String.valueOf(user.getBonus()));
            itemView.setOnClickListener(v -> DialogMinusBonusPoint.newInstance(((ActivityBase) getContext())
                    .getSupportFragmentManager(), user.getTotal())
                    .setOnMinusBonusListener(minusPoint -> {
                        mListener.onMinusBonus(user.getId(), minusPoint, new FragmentShopMembers.OnResponseFromServerListener() {
                            @Override
                            public void onSuccess() {
                                user.setTotal(user.getTotal() - minusPoint);
                                mTvTotalPoint.setText(String.valueOf(user.getTotal()));
                            }

                            @Override
                            public void onError() {
                            }
                        });
                    }));
        }
    }

    public interface OnMinusBonusListener{
        void onMinusBonus(int userId, int minusPoint, FragmentShopMembers.OnResponseFromServerListener listener);
    }
}
