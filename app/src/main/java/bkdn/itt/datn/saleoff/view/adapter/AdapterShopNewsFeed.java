package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.model.model.Post;
import bkdn.itt.datn.saleoff.view.iface.OnClickLikeListener;
import bkdn.itt.datn.saleoff.view.iface.OnClickUnLikeListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by TuDLT on 3/12/2018.
 */
public class AdapterShopNewsFeed extends AdapterBase<AdapterShopNewsFeed.PostHolder> {
    private List<Post> mPosts;
    private OnClickPostItemListener mListener;
    //distinguish the different between public post and private post
    private int mStatus;
    private String mShopAvatar;
    private String mShopName;

    public AdapterShopNewsFeed(Context context, OnClickPostItemListener listener, int status) {
        super(context);
        mPosts = new ArrayList<>();
        mListener = listener;
        mStatus = status;
    }

    @Override
    public PostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_post, parent, false);
        return new PostHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PostHolder holder, int position) {
        holder.bindData(mPosts.get(position));
    }

    @Override
    public int getItemCount() {
        return mPosts.size();
    }

    public void setPosts(List<Post> posts) {
        mPosts.addAll(posts);
        notifyDataSetChanged();
    }


    public void setShopInfo(String shopAvatar, String shopName) {
        mShopAvatar = shopAvatar;
        mShopName = shopName;
    }

    public void setRefreshPosts(List<Post> posts) {
        mPosts.clear();
        mPosts.addAll(posts);
        notifyDataSetChanged();
    }

    protected class PostHolder extends ViewHolderBase<Post> {
        @BindView(R.id.imvPostAvatar)
        CircleImageView mImvAvatar;
        @BindView(R.id.tvPostUserName)
        TextView mTvShopName;
        @BindView(R.id.tvPostCreatedAt)
        TextView mTvCreateDate;
        @BindView(R.id.tvPostNormalSalePercent)
        TextView mTvSalePercent;
        @BindView(R.id.tvPosNormalStartDate)
        TextView mTvFromDate;
        @BindView(R.id.tvPostNormalEndDate)
        TextView mTvToDate;
        @BindView(R.id.tvPostNormalAddress)
        TextView mTvAddress;
        @BindView(R.id.tvPostNormalTitle)
        TextView mTvPostTitle;
        @BindView(R.id.imvPostNormalCover)
        ImageView mImvPostCover;
        @BindView(R.id.tvPostLike)
        TextView mTvPostLike;
        @BindView(R.id.tvPostComment)
        TextView mTvPostComment;
        @BindView(R.id.llPostLike)
        LinearLayout mLlLike;
        @BindView(R.id.llPostComment)
        LinearLayout mLlComment;
        @BindView(R.id.imbPostLike)
        ImageView mImbLike;
        @BindView(R.id.imbPostComment)
        ImageView mImbComment;
        @BindView(R.id.imbPostEdit)
        ImageButton mImbEdit;
        @BindView(R.id.layoutNormalContent)
        View mLayoutNormal;
        @BindView(R.id.layoutShareContent)
        View mLayoutShare;
        PopupMenu mPopupMenu;

        public PostHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mPopupMenu = new PopupMenu(itemView.getContext(), mImbEdit);
            mPopupMenu.getMenuInflater().inflate(R.menu.menu_post_edit, mPopupMenu.getMenu());
        }

        @Override
        public void bindData(Post post) {
            super.bindData(post);
            mLayoutNormal.setVisibility(View.VISIBLE);
            mLayoutShare.setVisibility(View.GONE);
            //Avatar
            if (mShopAvatar != null) {
                Glide.with(getContext()).load(mShopAvatar).into(mImvAvatar);
            }
            //Cover
            if (post.getCover() != null) {
                Glide.with(getContext()).load(post.getCover()).into(mImvPostCover);
            }
            //Shop Name
            if (mShopName != null && !mShopName.isEmpty()) {
                mTvShopName.setText(mShopName);
            }
            //CreatedAt
            if (post.getCreatedDate() != null && !post.getCreatedDate().isEmpty()) {
                mTvCreateDate.setText(post.getCreatedDate());
            }
            //Title Post
            if (post.getPostTitle() != null && !post.getPostTitle().isEmpty()) {
                mTvPostTitle.setText(post.getPostTitle());
            }
            //Sale percent Post
            mTvSalePercent.setText(String.valueOf(post.getSalePercent()) + "%");
            //From date Post
            if (post.getFromDate() != null && !post.getFromDate().isEmpty()) {
                mTvFromDate.setText(post.getFromDate());
            }

            //Address
            if (post.getShopAddress() != null && !post.getShopAddress().isEmpty()) {
                mTvAddress.setText(post.getShopAddress());
            }

            //To date Post
            if (post.getToDate() != null && !post.getToDate().isEmpty()) {
                mTvToDate.setText(post.getFromDate());
            }
            //set view like
            if (post.isLiked() == 1) {
                mImbLike.setImageResource(R.drawable.ic_heart_red);
            } else {
                mImbLike.setImageResource(R.drawable.ic_heart_black);
            }
            //Like numbers
            mTvPostLike.setText(String.valueOf(post.getLikeNumber()));
            //Comment numbers
            mTvPostComment.setText(String.valueOf(post.getLikeNumber()));
            if (mStatus == ConstantDefine.PUBLIC) {
                mImbEdit.setVisibility(View.GONE);
            } else {
                mImbEdit.setVisibility(View.VISIBLE);
                mImbEdit.setOnClickListener(v -> mPopupMenu.show());
                mPopupMenu.setOnMenuItemClickListener(item -> {
                    switch (item.getItemId()) {
                        case R.id.itemPostEdit: {
                            mListener.onClickEditPostItem(post);
                            break;
                        }
                        case R.id.itemPostDel: {
                            mListener.onClickDeletePostItem(post.getPostId());
                            break;
                        }
                    }
                    return false;
                });
            }
            itemView.setOnClickListener(v -> {
                if (mListener != null) {
                    mListener.onClickPostItem(post);
                }
            });
            mLlLike.setOnClickListener(v -> {
                if (post.isLiked() == 1) {
                    mListener.onClickUnLike(post.getPostId(), new OnClickUnLikeListener() {
                        @Override
                        public void success() {
                            post.setLiked(0);
                            post.setLikeNumber(post.getLikeNumber() - 1);
                            mImbLike.setImageResource(R.drawable.ic_heart_black);
                        }

                        @Override
                        public void error() {

                        }
                    });

                } else {
                    mListener.onClickLike(post.getPostId(), new OnClickLikeListener() {
                        @Override
                        public void success() {
                            post.setLiked(1);
                            post.setLikeNumber(post.getLikeNumber() + 1);
                            mImbLike.setImageResource(R.drawable.ic_heart_red);
                        }

                        @Override
                        public void error() {

                        }
                    });

                }
                mTvPostLike.setText(String.valueOf(post.getLikeNumber()));
            });

            mLlComment.setOnClickListener(v -> {
                if (mListener != null) {
                    mListener.onClickComment(post.getPostId());
                }
            });

        }
    }

    public interface OnClickPostItemListener {
        void onClickPostItem(Post post);

        void onClickDeletePostItem(int postId);

        void onClickEditPostItem(Post post);

        void onClickLike(int postId, OnClickLikeListener onClickLikeListener);

        void onClickUnLike(int postId, OnClickUnLikeListener onClickUnLikeListener);

        void onClickComment(int postId);
    }
}