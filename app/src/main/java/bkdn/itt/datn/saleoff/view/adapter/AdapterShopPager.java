package bkdn.itt.datn.saleoff.view.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Shop;
import bkdn.itt.datn.saleoff.view.fragment.FragmentBase;
import bkdn.itt.datn.saleoff.view.fragment.FragmentShopInfo;
import bkdn.itt.datn.saleoff.view.fragment.FragmentShopPost;

/**
 * Created by TuDLT on 4/16/2018.
 */
public class AdapterShopPager extends FragmentPagerAdapter {
    private List<FragmentBase> mFragmentBases;
    private Shop mShop;

    public AdapterShopPager(FragmentManager fm, List<FragmentBase> fragmentBases) {
        super(fm);
        mFragmentBases = fragmentBases;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentBases.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentBases.size();
    }

}
