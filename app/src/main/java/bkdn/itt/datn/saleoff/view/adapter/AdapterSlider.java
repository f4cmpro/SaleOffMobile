package bkdn.itt.datn.saleoff.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import bkdn.itt.datn.saleoff.view.fragment.FragmentSlider;

/**
 * Created by TuDLT on 3/24/2018.
 */
public class AdapterSlider extends FragmentStatePagerAdapter {
    String[] mImgUrls;

    public AdapterSlider(FragmentManager fm, String[] imgUrls) {
        super(fm);
        mImgUrls = imgUrls;
    }

    @Override
    public Fragment getItem(int position) {
        return FragmentSlider.newInstance(mImgUrls[position]);
    }

    @Override
    public int getCount() {
        return mImgUrls != null ? mImgUrls.length : 0;
    }
}
