package bkdn.itt.datn.saleoff.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.model.SubCategory;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 4/6/2018.
 */
public class AdapterSubCategory extends AdapterBase<AdapterSubCategory.SubCategoryHolder> {
    private List<SubCategory> mSubCats;

    public AdapterSubCategory(Context context, List<SubCategory> subCats) {
        super(context);
        mSubCats = subCats;
    }

    @Override
    public SubCategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_category, parent, false);
        return new SubCategoryHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SubCategoryHolder holder, int position) {
        holder.bindData(mSubCats.get(position));
    }

    @Override
    public int getItemCount() {
        return mSubCats.size();
    }

    public void setSubCats(List<SubCategory> subCats) {
        mSubCats = subCats;
        notifyDataSetChanged();
    }

    protected class SubCategoryHolder extends ViewHolderBase<SubCategory> {
        @BindView(R.id.tvItemShopCategory)
        TextView mTvItem;

        public SubCategoryHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindData(SubCategory subCat) {
            super.bindData(subCat);
            mTvItem.setText(subCat.getName());
            itemView.setOnClickListener(v -> {
                Intent subCatIntent = ((Activity) getContext()).getIntent();
                subCatIntent.putExtra(GlobalDefine.KEY_INTENT_SELECT_SUB_CATEGORY, subCat);
                ((Activity) getContext()).setResult(Activity.RESULT_OK, subCatIntent);
                ((Activity) getContext()).finish();
            });
        }
    }
}
