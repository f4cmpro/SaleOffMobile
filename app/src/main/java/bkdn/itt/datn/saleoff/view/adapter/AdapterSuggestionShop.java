package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.ShopInfo;
import bkdn.itt.datn.saleoff.view.activity.ActivityBase;
import bkdn.itt.datn.saleoff.view.dialog.DialogBonusCode;
import bkdn.itt.datn.saleoff.view.iface.OnClickFollowListener;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 3/13/2018.
 */
public class AdapterSuggestionShop extends AdapterBase<AdapterSuggestionShop.SuggestionHolder> {
    private List<ShopInfo> mShopInfoList;
    private AdapterFollowShop.OnClickFollowShopListener mListener;

    public AdapterSuggestionShop(Context context, AdapterFollowShop.OnClickFollowShopListener listener) {
        super(context);
        mShopInfoList = new ArrayList<>();
        mListener = listener;
    }

    @Override
    public SuggestionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_suggestion, parent,
                false);
        return new SuggestionHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SuggestionHolder holder, int position) {
        holder.bindData(mShopInfoList.get(position));
    }


    @Override
    public int getItemCount() {
        return mShopInfoList.size();
    }


    public void loadMoreSuggestedShops(List<ShopInfo> shopInfoList) {
        mShopInfoList.addAll(shopInfoList);
        notifyDataSetChanged();
    }


    public void refreshSuggestedShops(List<ShopInfo> shopInfoList) {
        mShopInfoList.clear();
        mShopInfoList.addAll(shopInfoList);
        notifyDataSetChanged();
    }

    protected class SuggestionHolder extends ViewHolderBase<ShopInfo> {
        @BindView(R.id.imvSuggestionAvatar)
        ImageView mImvAvatar;
        @BindView(R.id.tvSuggestionName)
        TextView mTvName;
        @BindView(R.id.tvSuggestionFollowNumber)
        TextView mTvFollows;
        @BindView(R.id.btnSuggestionFollow)
        TextView mBtnFollow;
        @BindView(R.id.btnSuggestionUnFollow)
        TextView mBtnUnFollow;

        public SuggestionHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindData(ShopInfo shopInfo) {
            super.bindData(shopInfo);

            if (shopInfo.getAvatar() != null) {
                Glide.with(getContext()).load(shopInfo.getAvatar()).into(mImvAvatar);
            }
            if (shopInfo.getName() != null) {
                mTvName.setText(shopInfo.getName());
            }
            mTvFollows.setText(String.valueOf(shopInfo.getFollowsNumber()) + " "
                    + getString(R.string.people_follow));
            mBtnFollow.setVisibility(View.VISIBLE);
            mBtnUnFollow.setVisibility(View.GONE);
            mBtnFollow.setOnClickListener(v -> {
                DialogBonusCode dialogBonusCode = DialogBonusCode.newInstance();
                dialogBonusCode.addOnInputBonusCodeListener(new DialogBonusCode.OnInputBonusCodeListener() {
                    @Override
                    public void onInputBonusCode(int bonusCode) {
                        mListener.onClickFollow(shopInfo.getId(), bonusCode, new OnClickFollowListener() {
                            @Override
                            public void success() {
                                mBtnFollow.setVisibility(View.GONE);
                                mBtnUnFollow.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void fail() {
                                mBtnFollow.setVisibility(View.VISIBLE);
                                mBtnUnFollow.setVisibility(View.GONE);
                            }
                        });

                    }

                    @Override
                    public void onCancelBodeCode() {
                        mListener.onClickFollow(shopInfo.getId(), 0, new OnClickFollowListener() {
                            @Override
                            public void success() {
                                mBtnFollow.setVisibility(View.GONE);
                                mBtnUnFollow.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void fail() {
                                mBtnFollow.setVisibility(View.VISIBLE);
                                mBtnUnFollow.setVisibility(View.GONE);

                            }
                        });
                    }
                });
                dialogBonusCode.show(((ActivityBase) getContext()).getSupportFragmentManager(), null);
            });
            mBtnUnFollow.setOnClickListener(v -> {
                mListener.onClickUnFollow(shopInfo.getId());
                mBtnFollow.setVisibility(View.VISIBLE);
                mBtnUnFollow.setVisibility(View.GONE);
            });
            itemView.setOnClickListener(v -> mListener.onWatchDetailShop(shopInfo.getId()));
        }
    }
}
