package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.ShopInfo;
import bkdn.itt.datn.saleoff.model.model.UserInfo;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 3/13/2018.
 */
public class AdapterSuggestionUser extends AdapterBase<AdapterSuggestionUser.SuggestionHolder> {
    private List<UserInfo> mUserInfoList;
    private AdapterFollowUser.OnClickFollowUserListener mListener;

    public AdapterSuggestionUser(Context context, AdapterFollowUser.OnClickFollowUserListener listener) {
        super(context);
        mUserInfoList = new ArrayList<>();
        mListener = listener;
    }

    @Override
    public SuggestionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_suggestion, parent,
                false);
        return new SuggestionHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SuggestionHolder holder, int position) {
        holder.bindData(mUserInfoList.get(position));
    }


    @Override
    public int getItemCount() {
        return mUserInfoList.size();
    }


    public void loadMoreSuggestedUsers(List<UserInfo> userInfoList) {
        mUserInfoList.addAll(userInfoList);
        notifyDataSetChanged();
    }


    public void refreshSuggestedUsers(List<UserInfo> userInfos) {
        mUserInfoList.clear();
        mUserInfoList.addAll(userInfos);
        notifyDataSetChanged();
    }

    protected class SuggestionHolder extends ViewHolderBase<UserInfo> {
        @BindView(R.id.imvSuggestionAvatar)
        ImageView mImvAvatar;
        @BindView(R.id.tvSuggestionName)
        TextView mTvName;
        @BindView(R.id.tvSuggestionFollowNumber)
        TextView mTvFollows;
        @BindView(R.id.btnSuggestionFollow)
        TextView mBtnFollow;
        @BindView(R.id.btnSuggestionUnFollow)
        TextView mBtnUnFollow;

        public SuggestionHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindData(UserInfo user) {
            super.bindData(user);

            if (user.getAvatar() != null) {
                Glide.with(getContext()).load(user.getAvatar()).into(mImvAvatar);
            }
            if (user.getUserName() != null) {
                mTvName.setText(user.getUserName());
            }
            mTvFollows.setText(String.valueOf(user.getFollowsNumber()) + " "
                    + getString(R.string.people_follow));

            mBtnFollow.setVisibility(View.VISIBLE);
            mBtnUnFollow.setVisibility(View.GONE);
            mBtnFollow.setOnClickListener(v -> {
                mListener.onClickFollow(user.getId());
                mBtnFollow.setVisibility(View.GONE);
                mBtnUnFollow.setVisibility(View.VISIBLE);
            });
            mBtnUnFollow.setOnClickListener(v -> {
                mListener.onClickUnFollow(user.getId());
                mBtnFollow.setVisibility(View.VISIBLE);
                mBtnUnFollow.setVisibility(View.GONE);
            });
            itemView.setOnClickListener(v -> mListener.onWatchUserDetail(user.getId()));
        }
    }
}
