package bkdn.itt.datn.saleoff.view.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.model.model.Event;
import bkdn.itt.datn.saleoff.model.model.Post;
import bkdn.itt.datn.saleoff.view.iface.OnClickLikeListener;
import bkdn.itt.datn.saleoff.view.iface.OnClickUnLikeListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by TuDLT on 3/12/2018.
 */
public class AdapterUserNewsFeed extends AdapterBase<AdapterUserNewsFeed.PostHolder> {
    private List<Post> mPosts;
    private OnClickPostItemListener mClickPostItemListener;
    private OnEditPostItemListener mEditPostItemListener;
    //distinguish the different between public post and private post
    private int mStatus;
    private String mUserAvatar;
    private String mUserName;

    public AdapterUserNewsFeed(Context context, OnClickPostItemListener clickPostItemListener, int status) {
        super(context);
        mPosts = new ArrayList<>();
        mStatus = status;
        mClickPostItemListener = clickPostItemListener;
    }

    @Override
    public PostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_post, parent, false);
        return new PostHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PostHolder holder, int position) {
        holder.bindData(mPosts.get(position));
    }

    @Override
    public int getItemCount() {
        return mPosts.size();
    }

    public void setPosts(List<Post> posts) {
        mPosts.addAll(posts);
        notifyDataSetChanged();
    }

    public void setUserInfo(String userAvatar, String userName) {
        mUserAvatar = userAvatar;
        mUserName = userName;
    }

    public void refreshPosts(List<Post> posts) {
        mPosts.clear();
        mPosts.addAll(posts);
        notifyDataSetChanged();
    }

    public void setLikeView(boolean isLikeFail, int position) {
        //if like fail return origin view like
        if (isLikeFail) {
            mPosts.get(position).setLiked(0);
        } else {
            mPosts.get(position).setLiked(1);
        }
        notifyDataSetChanged();
    }

    public void setEditPostItemListener(OnEditPostItemListener listener) {
        mEditPostItemListener = listener;
    }

    protected class PostHolder extends ViewHolderBase<Post> {
        @BindView(R.id.imvPostAvatar)
        CircleImageView mImvAvatar;
        @BindView(R.id.tvPostUserName)
        TextView mTvNameOfPoster;
        @BindView(R.id.tvPostCreatedAt)
        TextView mTvCreateDate;
        @BindView(R.id.llPostShare)
        LinearLayout mLlShare;
        @BindView(R.id.llPostLike)
        LinearLayout mLlLike;
        @BindView(R.id.llPostComment)
        LinearLayout mLlComment;
        @BindView(R.id.imbPostLike)
        ImageView mImvLike;
        @BindView(R.id.imbPostEdit)
        ImageButton mImbEdit;
        @BindView(R.id.tvPostLike)
        TextView mTvPostLike;
        @BindView(R.id.tvPostComment)
        TextView mTvPostComment;
        /*
        * Widgets of content_normal
        * */
        @BindView(R.id.layoutNormalContent)
        View mLayoutNormal;
        @BindView(R.id.tvPostNormalSalePercent)
        TextView mTvNormalSalePercent;
        @BindView(R.id.tvPosNormalStartDate)
        TextView mTvNormalStartDate;
        @BindView(R.id.tvPostNormalEndDate)
        TextView mTvNormalEndDate;
        @BindView(R.id.tvPostNormalAddress)
        TextView mTvNormalAddress;
        @BindView(R.id.tvPostNormalTitle)
        TextView mTvNormalTitle;
        @BindView(R.id.imvPostNormalCover)
        ImageView mImvNormalCover;
        /*
        * Widgets of content_share;
        * */
        @BindView(R.id.layoutShareContent)
        View mLayoutShare;
        @BindView(R.id.tvPostDesForShare)
        TextView mTvDesForShare;
        @BindView(R.id.imvShareContentAvatar)
        ImageView mImvShareAvatar;
        @BindView(R.id.tvShareContentName)
        TextView mTvShareName;
        @BindView(R.id.tvShareContentCreatedAt)
        TextView mTvShareCreatedAt;
        @BindView(R.id.tvShareContentSalePercent)
        TextView mTvShareSalePercent;
        @BindView(R.id.tvShareContentStartDate)
        TextView mTvShareStartDate;
        @BindView(R.id.tvShareContentEndDate)
        TextView mTvShareEndDate;
        @BindView(R.id.tvShareContentAddress)
        TextView mTvShareAddress;
        @BindView(R.id.tvShareContentTitle)
        TextView mTvShareTitle;
        @BindView(R.id.imvShareContentCover)
        ImageView mImvShareCover;

        /*
        * Widgets of share event
        * */
        @BindView(R.id.layoutShareEventContent)
        View mLayoutShareEvent;
        @BindView(R.id.imvShareEventCover)
        ImageView mImvEventCover;
        @BindView(R.id.tvShareEventTitle)
        TextView mTvEventTitle;
        @BindView(R.id.tvShareEventStartDate)
        TextView mTvEventStartAt;
        @BindView(R.id.tvShareEventEndDate)
        TextView mTvEventEndDate;

        public PostHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindData(Post post) {
            super.bindData(post);
            //Set Avatar
            Glide.with(getContext()).load(post.getUser().getAvatar()).into(mImvAvatar);
            //Set Name of Poster;
            if (post.getIsShare() == 1) {
                String postOf = post.getSharedPost().getShop() != null
                        ? post.getSharedPost().getShop().getName()
                        : post.getSharedPost().getUser().getUserName();
                SpannableStringBuilder sharer = new SpannableStringBuilder(post.getUser().getUserName()
                        + " đã chia sẻ tin giảm giá của " + postOf);
                sharer.setSpan(new StyleSpan(Typeface.BOLD), 0, post.getUser().getUserName().length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sharer.setSpan(new StyleSpan(Typeface.BOLD), sharer.length() - postOf.length(), sharer.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                mTvNameOfPoster.setText(sharer);
            } else if (post.getIsEvent() == 1) {
                String name = "<b>" + post.getUser().getUserName() + "</b>" + " đã tham gia một sự kiện của cửa hàng "
                        + "<b>" + post.getEvent().getShopInfo().getName() + "</b>";
                mTvNameOfPoster.setText(Html.fromHtml(name));
            } else {
                String name = "<b>" + post.getUser().getUserName() + "</b>";
                mTvNameOfPoster.setText(Html.fromHtml(name));
            }
            //Set created date
            if (post.getCreatedDate() != null && !post.getCreatedDate().isEmpty()) {
                mTvCreateDate.setText(post.getCreatedDate());
            }
            //Set like numbers
            mTvPostLike.setText(String.valueOf(post.getLikeNumber()));
            //Set comment numbers
            mTvPostComment.setText(String.valueOf(post.getCommentNumber()));
            if (post.isLiked() == 1) {
                mImvLike.setImageResource(R.drawable.ic_heart_red);
            } else {
                mImvLike.setImageResource(R.drawable.ic_heart_black);
            }
            //Mode public or private
            if (mStatus == ConstantDefine.PUBLIC) {
                mImbEdit.setVisibility(View.GONE);
            }

            itemView.setOnClickListener(v -> {
                if (post.getIsShare() == 1) {
                    mClickPostItemListener.onClickPostItem(post.getSharedPost());
                } else if (post.getIsEvent() == 1) {
                    mClickPostItemListener.onClickEventDetail(post.getEvent());
                } else {
                    mClickPostItemListener.onClickPostItem(post);
                }
            });
            mLlLike.setOnClickListener(v -> {
                if (post.isLiked() == 1) {
                    mClickPostItemListener.onClickUnLike(post.getPostId(), new OnClickUnLikeListener() {
                        @Override
                        public void success() {
                            post.setLiked(0);
                            post.setLikeNumber(post.getLikeNumber() - 1);
                            mImvLike.setImageResource(R.drawable.ic_heart_black);
                            mTvPostLike.setText(String.valueOf(post.getLikeNumber()));
                        }

                        @Override
                        public void error() {

                        }
                    });

                } else {
                    mClickPostItemListener.onClickLike(post.getPostId(), new OnClickLikeListener() {
                        @Override
                        public void success() {
                            post.setLiked(1);
                            post.setLikeNumber(post.getLikeNumber() + 1);
                            mImvLike.setImageResource(R.drawable.ic_heart_red);
                            mTvPostLike.setText(String.valueOf(post.getLikeNumber()));
                        }

                        @Override
                        public void error() {

                        }
                    });

                }
            });
            mLlComment.setOnClickListener(v -> mClickPostItemListener.onClickComment(post.getPostId()));
            mLlShare.setOnClickListener(v -> mClickPostItemListener.onCLickShare(post));
            //set view for sharedPost or normalPost
            if (post.getIsShare() == 1 && post.getSharedPost() != null) {
                mLayoutShare.setVisibility(View.VISIBLE);
                mLayoutNormal.setVisibility(View.GONE);
                mLayoutShareEvent.setVisibility(View.GONE);
                //set description share normal
                mTvDesForShare.setText(post.getPostDescribe());
                //set avatar and name of shared man
                if (post.getSharedPost().getShop() != null) {
                    mTvShareName.setText(post.getSharedPost().getShop().getName());
                    Glide.with(getContext()).load(post.getSharedPost().getShop().getAvatar()).into(mImvShareAvatar);
                } else {
                    mTvShareName.setText(post.getSharedPost().getUser().getUserName());
                    Glide.with(getContext()).load(post.getSharedPost().getUser().getAvatar()).into(mImvShareAvatar);
                }
                //set created date of shared post
                mTvShareCreatedAt.setText(post.getSharedPost().getCreatedDate());
                //set post share title
                mTvShareTitle.setText(post.getSharedPost().getPostTitle());
                //set share sale percent
                mTvShareSalePercent.setText(String.valueOf(post.getSharedPost().getSalePercent()) + "%");
                //set share start date
                mTvShareStartDate.setText(post.getSharedPost().getFromDate());
                //set share end date
                mTvShareEndDate.setText(post.getSharedPost().getFromDate());
                //set share address
                mTvShareAddress.setText(post.getSharedPost().getShopAddress());
                //set share cover
                Glide.with(getContext()).load(post.getSharedPost().getCover()).into(mImvShareCover);
            } else if (post.getIsEvent() == 1 && post.getEvent() != null) {
                mLayoutShare.setVisibility(View.GONE);
                mLayoutNormal.setVisibility(View.GONE);
                mLayoutShareEvent.setVisibility(View.VISIBLE);
                Event event = post.getEvent();
                //set cover
                Glide.with(getContext()).load(event.getCover()).into(mImvEventCover);
                mTvEventTitle.setText(event.getTitle());
                mTvEventStartAt.setText(event.getStartDate());
                mTvEventEndDate.setText(event.getEndDate());
            } else {
                mLayoutShare.setVisibility(View.GONE);
                mLayoutShareEvent.setVisibility(View.GONE);
                mLayoutNormal.setVisibility(View.VISIBLE);

                //set post normal title
                mTvNormalTitle.setText(post.getPostTitle());
                //set normal sale percent
                mTvNormalSalePercent.setText(String.valueOf(post.getSalePercent()) + "%");
                //set normal start date
                mTvNormalStartDate.setText(post.getFromDate());
                //set normal end date
                mTvNormalEndDate.setText(post.getFromDate());
                //set normal address
                mTvNormalAddress.setText(post.getShopAddress());
                //set normal cover
                Glide.with(getContext()).load(post.getCover()).into(mImvNormalCover);
            }
        }
    }


    public interface OnClickPostItemListener {
        void onClickPostItem(Post post);

        void onClickLike(int postId, OnClickLikeListener onClickLikeListener);

        void onClickUnLike(int postId, OnClickUnLikeListener onClickUnLikeListener);

        void onClickComment(int postId);

        void onCLickShare(Post post);

        void onClickEventDetail(Event event);
    }

    public interface OnEditPostItemListener {
        void onClickDeletePostItem(int postId);

        void onClickEditPostItem(Post post);
    }
}