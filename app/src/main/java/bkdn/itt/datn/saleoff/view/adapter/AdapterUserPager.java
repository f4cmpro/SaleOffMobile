package bkdn.itt.datn.saleoff.view.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Shop;
import bkdn.itt.datn.saleoff.model.model.User;
import bkdn.itt.datn.saleoff.view.fragment.FragmentBase;

/**
 * Created by TuDLT on 5/4/2018.
 */
public class AdapterUserPager extends FragmentPagerAdapter{
    private List<FragmentBase> mFragmentBases;
    private User mUser;

    public AdapterUserPager(FragmentManager fm, List<FragmentBase> fragmentBases) {
        super(fm);
        mFragmentBases = fragmentBases;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentBases.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentBases.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            default:
                return "Thông tin";
            case 1:
                return "Bài đăng";
        }
    }
}
