package bkdn.itt.datn.saleoff.view.component;

/**
 * Created by TuDLT on 3/5/2018.
 */
public class PagesLessException extends Exception {
    @Override
    public String getMessage() {
        return "Pages must equal or larger than 2";
    }
}