package bkdn.itt.datn.saleoff.view.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import bkdn.itt.datn.saleoff.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 5/18/2018.
 */
public class DialogBonusCode extends DialogFragment {
    @BindView(R.id.btnBonusCodeDone)
    Button mBtnDone;
    @BindView(R.id.btnBonusCodeCancel)
    Button mBtnCancel;
    @BindView(R.id.edtBonusCode)
    EditText mEdtSetBonusCode;
    private View mRootView;
    private OnInputBonusCodeListener mListener;

    public static DialogBonusCode newInstance() {
        DialogBonusCode dialogBonusCode = new DialogBonusCode();
        return dialogBonusCode;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.dialog_bonus_code, container, false);
            ButterKnife.bind(this, mRootView);
            initViews();
            initActions();
        }

        return mRootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(getResources().getDimensionPixelSize(R.dimen._240sdp),
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    private void initActions() {
        mBtnDone.setOnClickListener(v -> {
            mListener.onInputBonusCode(Integer.parseInt(mEdtSetBonusCode.getText().toString()));
            dismiss();
        });
        mBtnCancel.setOnClickListener(v -> {
            mListener.onCancelBodeCode();
            dismiss();
        });
    }

    private void initViews() {
        mEdtSetBonusCode.setRawInputType(InputType.TYPE_CLASS_TEXT);
    }

    public void addOnInputBonusCodeListener(OnInputBonusCodeListener listener) {
        mListener = listener;
    }


    public interface OnInputBonusCodeListener {
        void onInputBonusCode(int bonusCode);
        void onCancelBodeCode();
    }
}
