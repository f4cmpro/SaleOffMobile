package bkdn.itt.datn.saleoff.view.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 3/20/2018.
 */
public class DialogChooseGender extends DialogFragment {
    @BindView(R.id.tvChooseGenderMale)
    TextView mTvMale;
    @BindView(R.id.tvChooseGenderFemale)
    TextView mTvFemale;
    private View mRootView;
    private OnChooseGenderListener mListener;
    public static DialogChooseGender newInstance(OnChooseGenderListener listener, FragmentManager manager){
        DialogChooseGender dialogChooseGender = new DialogChooseGender();
        dialogChooseGender.setOnChooseGenderListener(listener);
        dialogChooseGender.show(manager, DialogChooseGender.class.getSimpleName());
        return dialogChooseGender;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if(mRootView == null){
            mRootView = inflater.inflate(R.layout.dialog_choose_gender, container, false);
            ButterKnife.bind(this, mRootView);
            mTvMale.setOnClickListener(v -> {
                dismiss();
                mListener.onChooseGender(ConstantDefine.MALE);
            });
            mTvFemale.setOnClickListener(v -> {
                dismiss();
                mListener.onChooseGender(ConstantDefine.FEMALE);
            });
        }
        return mRootView;
    }



    private void setOnChooseGenderListener(OnChooseGenderListener listener){
        mListener = listener;
    }

    public interface OnChooseGenderListener{
        void onChooseGender(int gender);
    }
}
