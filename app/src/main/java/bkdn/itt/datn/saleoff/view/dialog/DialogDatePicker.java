package bkdn.itt.datn.saleoff.view.dialog;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import java.util.Calendar;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.utils.TimeUtils;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 3/21/2018.
 */
public class DialogDatePicker extends DialogFragment {
    private static final String KEY_MIN_DATE = "KEY_MIN_DATE";
    private static final String KEY_TITLE = "KEY_TITLE";
    @BindView(R.id.datePickerSpinner)
    DatePicker mDatePicker;
    @BindView(R.id.btnDatePickerCancel)
    Button mBtnCancel;
    @BindView(R.id.btnDatePickerDone)
    Button mBtnDone;
    @BindView(R.id.tvDatePickerTitle)
    TextView mTvTitle;
    private View mRootView;
    private OnPickDateListener mListener;

    public static DialogDatePicker newInstance(OnPickDateListener listener, FragmentManager manager,
                                               long minDate, String title) {
        DialogDatePicker dialogDatePicker = new DialogDatePicker();
        dialogDatePicker.setListener(listener);
        Bundle bundle = new Bundle();
        bundle.putLong(KEY_MIN_DATE, minDate);
        bundle.putString(KEY_TITLE, title);
        dialogDatePicker.setArguments(bundle);
        dialogDatePicker.show(manager, DialogDatePicker.class.getSimpleName());
        return dialogDatePicker;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.dialog_date_picker, container, false);
            ButterKnife.bind(this, mRootView);
            mDatePicker.setMinDate(getArguments().getLong(KEY_MIN_DATE));
            mTvTitle.setText(getArguments().getString(KEY_TITLE));
            initActions();
        }
        return mRootView;
    }

    private void initActions() {
        mBtnDone.setOnClickListener(v -> selectDate());
        mBtnCancel.setOnClickListener(v -> dismiss());
    }


    private void setListener(OnPickDateListener listener) {
        mListener = listener;
    }

    private void selectDate() {
        int day = mDatePicker.getDayOfMonth();
        int month = mDatePicker.getMonth();
        int year = mDatePicker.getYear();
        mListener.onPickDate(day, month, year);
        dismiss();
    }

    public interface OnPickDateListener {
        void onPickDate(int day, int month, int year);
    }
}
