package bkdn.itt.datn.saleoff.view.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.global.GlobalDefine;

/**
 * Created by TuDLT on 3/9/2018.
 */
public class DialogErrorMessage extends DialogFragment {



    public static DialogErrorMessage newInstance(String errorMessage){
        Bundle bundle = new Bundle();
        bundle.putString(GlobalDefine.KEY_BUNDLE_ERROR_MESSAGE, errorMessage);
        DialogErrorMessage dialog = new DialogErrorMessage();
        dialog.setArguments(bundle);
        return dialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String errorMessage = getArguments().getString(GlobalDefine.KEY_BUNDLE_ERROR_MESSAGE);
        return new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.error))
                .setMessage(errorMessage)
                .setPositiveButton(R.string.alert_dialog_ok, (dialog, which) -> {
                })
                .create();
    }

}
