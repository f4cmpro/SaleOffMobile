package bkdn.itt.datn.saleoff.view.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.widget.NumberPicker;

import bkdn.itt.datn.saleoff.R;

/**
 * Created by TuDLT on 5/21/2018.
 */
public class DialogMinusBonusPoint extends DialogFragment {
    private static final String BONUS_POINT = "Bonus point";
    private OnMinusBonusListener mOnMinusBonusListener;
    public static DialogMinusBonusPoint newInstance(FragmentManager manager, int bonusPoint) {
        Bundle bundle = new Bundle();
        bundle.putInt(BONUS_POINT, bonusPoint);
        DialogMinusBonusPoint dialogMinusBonusPoint = new DialogMinusBonusPoint();
        dialogMinusBonusPoint.setArguments(bundle);
        dialogMinusBonusPoint.show(manager, DialogSalePercent.class.getSimpleName());
        return dialogMinusBonusPoint;
    }

    public void setOnMinusBonusListener(OnMinusBonusListener listener){
        mOnMinusBonusListener = listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int maxBonus = getArguments().getInt(BONUS_POINT);
        NumberPicker numberPicker = new NumberPicker(getContext());
        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(maxBonus);
        numberPicker.setDescendantFocusability(
                 NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Trừ điểm bonus: ");
        builder.setView(numberPicker);
        builder.setPositiveButton("OK", (dialog, which) -> mOnMinusBonusListener.onMinusBonus(numberPicker.getValue()));
        builder.setNegativeButton("CANCEL", (dialog, which) -> dismiss());

        return builder.create();
    }

    public interface OnMinusBonusListener{
        void onMinusBonus(int minusPoint);
    }

}
