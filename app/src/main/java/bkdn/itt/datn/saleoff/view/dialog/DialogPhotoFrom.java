package bkdn.itt.datn.saleoff.view.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import bkdn.itt.datn.saleoff.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 5/12/2018.
 */
public class DialogPhotoFrom extends DialogFragment {
    @BindView(R.id.tvPhotoFromCamera)
    TextView mTvFromCamera;
    @BindView(R.id.tvPhotoFromGallery)
    TextView mTvFromGallery;
    private View mRootView;
    private OnPhotoFromCallBack mCallBack;

    public static DialogPhotoFrom newInstance(FragmentManager manager, OnPhotoFromCallBack callBack) {
        DialogPhotoFrom dialogPhotoFrom = new DialogPhotoFrom();
        dialogPhotoFrom.setPhotoFrom(callBack);
        dialogPhotoFrom.show(manager, DialogChooseGender.class.getSimpleName());
        return dialogPhotoFrom;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.dialog_photo_from, container, false);
            ButterKnife.bind(this, mRootView);
            mTvFromCamera.setOnClickListener(v -> {
                mCallBack.fromCamera();
                dismiss();
            });
            mTvFromGallery.setOnClickListener(v -> {
                mCallBack.fromGallery();
                dismiss();
            });
        }
        return mRootView;
    }

    public void setPhotoFrom(OnPhotoFromCallBack callback) {
        mCallBack = callback;
    }

    public interface OnPhotoFromCallBack {
        void fromCamera();

        void fromGallery();
    }
}
