package bkdn.itt.datn.saleoff.view.dialog;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.Participant;
import bkdn.itt.datn.saleoff.model.model.User;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.view.adapter.AdapterPresenter;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 6/1/2018.
 */
public class DialogPresenter extends DialogFragment implements AdapterPresenter.OnSelectPresenterListener {
    private static final String KEY_BUNDLE_PARTICIPANTS = "KEY_BUNDLE_PARTICIPANTS";
    @BindView(R.id.edtSearchPresenter)
    EditText mEdtSearchPresenter;
    @BindView(R.id.recyclerPresenter)
    RecyclerView mRecyclerPresenter;
    @BindView(R.id.layoutNotFoundPresenter)
    View mViewNotFound;
    @BindView(R.id.tvLayoutNotFoundItem)
    TextView mTvNotFound;
    @BindView(R.id.btnPresenterConfirm)
    Button mBtnConfirm;
    @BindView(R.id.imbPresenterClose)
    ImageButton mBtnClose;

    private OnSelectPresenterListener mListener;
    private View mRootView;
    private AdapterPresenter mAdapterPresenter;
    private ArrayList<Participant> mParticipants;;
    private int mShareCode;

    public static DialogPresenter newInstance(ArrayList<Participant> participants) {
        DialogPresenter dialogPresenter = new DialogPresenter();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(KEY_BUNDLE_PARTICIPANTS, participants);
        dialogPresenter.setArguments(bundle);
        return dialogPresenter;
    }

    public void setListener(OnSelectPresenterListener listener) {
        mListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle(getString(R.string.who_do_present_for_you));
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.dialog_select_presenter, container, false);
            ButterKnife.bind(this, mRootView);
            setValues();
            setViews();
            setActions();
        }
        return mRootView;
    }

    private void setValues() {
        mParticipants = getArguments().getParcelableArrayList(KEY_BUNDLE_PARTICIPANTS);
    }

    private void setViews() {
        if (mParticipants != null && !mParticipants.isEmpty()) {
            mAdapterPresenter = new AdapterPresenter(getContext(), mParticipants);
            mAdapterPresenter.setListener(this);
            RecyclerViewUtils.Create().setUpVertical(getContext(), mRecyclerPresenter).setAdapter(mAdapterPresenter);
        } else {
            mRecyclerPresenter.setVisibility(View.GONE);
            mViewNotFound.setVisibility(View.VISIBLE);
            mTvNotFound.setText(R.string.not_found_participant);
        }
    }

    private void setActions() {
        mEdtSearchPresenter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                searchParticipants(s.toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        mBtnConfirm.setOnClickListener(v -> {
            mListener.onSelectPresenter(mShareCode);
            dismiss();
        });
        mBtnClose.setOnClickListener(v -> dismiss());
    }

    private void searchParticipants(String participantName) {
        if (participantName.isEmpty()) {
            mAdapterPresenter.refreshData(mParticipants);
            return;
        }
        List<Participant> participants = new ArrayList<>();
        for (Participant participant : mParticipants) {
            if (participant.getParticipantName().contains(participantName)) {
                participants.add(participant);
            }
        }
        mAdapterPresenter.refreshData(participants);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(getResources().getDimensionPixelSize(R.dimen._280sdp),
                    getResources().getDimensionPixelSize(R.dimen._380sdp));
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }


    @Override
    public void onSelect(int shareCode) {
        mShareCode = shareCode;
    }

    @Override
    public void onUnSelect() {
        mShareCode = 0;
    }

    public interface OnSelectPresenterListener {
        void onSelectPresenter(int shareCode);
    }
}
