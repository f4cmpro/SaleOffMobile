package bkdn.itt.datn.saleoff.view.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import bkdn.itt.datn.saleoff.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 5/2/2018.
 */
public class DialogRateShop extends DialogFragment {

    private static final int ONE_RATE = 1;
    private static final int TWO_RATE = 2;
    private static final int THREE_RATE = 3;
    private static final int FOUR_RATE = 4;
    private static final int FIVE_RATE = 5;
    /*
    * Widgets
    * */
    @BindView(R.id.btnRateShopDone)
    Button mBtnDone;
    @BindView(R.id.btnRateShopCancel)
    Button mBtnCancel;
    @BindView(R.id.llRateShopOne)
    LinearLayout mllRateOne;
    @BindView(R.id.llRateShopTwo)
    LinearLayout mllRateTwo;
    @BindView(R.id.llRateShopThree)
    LinearLayout mllRateThree;
    @BindView(R.id.llRateShopFour)
    LinearLayout mllRateFour;
    @BindView(R.id.llRateShopFive)
    LinearLayout mllRateFive;
    /*
   * Fields
   * */
    private OnChooseRateListener mListener;
    private View mRootView;
    private int mRate;

    public static DialogRateShop newInstance(OnChooseRateListener listener, FragmentManager manager) {
        DialogRateShop dialogRateShop = new DialogRateShop();
        dialogRateShop.setListener(listener);
        dialogRateShop.show(manager, DialogRateShop.class.getSimpleName());
        return dialogRateShop;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.dialog_rate_shop, container, false);
            ButterKnife.bind(this, mRootView);
            initActions();
        }
        return mRootView;
    }

    private void initActions() {
        mllRateOne.setOnClickListener(v -> {
            mRate = ONE_RATE;
            mllRateOne.setBackgroundColor(getResources().getColor(R.color.colorAccent20));
            mllRateTwo.setBackgroundColor(0x00000000);
            mllRateThree.setBackgroundColor(0x00000000);
            mllRateFour.setBackgroundColor(0x00000000);
            mllRateFive.setBackgroundColor(0x00000000);

        });
        mllRateTwo.setOnClickListener(v -> {
            mRate = TWO_RATE;
            mllRateOne.setBackgroundColor(0x00000000);
            mllRateTwo.setBackgroundColor(getResources().getColor(R.color.colorAccent20));
            mllRateThree.setBackgroundColor(0x00000000);
            mllRateFour.setBackgroundColor(0x00000000);
            mllRateFive.setBackgroundColor(0x00000000);

        });
        mllRateThree.setOnClickListener(v -> {
            mRate = THREE_RATE;
            mllRateOne.setBackgroundColor(0x00000000);
            mllRateTwo.setBackgroundColor(0x00000000);
            mllRateThree.setBackgroundColor(getResources().getColor(R.color.colorAccent20));
            mllRateFour.setBackgroundColor(0x00000000);
            mllRateFive.setBackgroundColor(0x00000000);

        });
        mllRateFour.setOnClickListener(v -> {
            mRate = FOUR_RATE;
            mllRateOne.setBackgroundColor(0x00000000);
            mllRateTwo.setBackgroundColor(0x00000000);
            mllRateThree.setBackgroundColor(0x00000000);
            mllRateFour.setBackgroundColor(getResources().getColor(R.color.colorAccent20));
            mllRateFive.setBackgroundColor(0x00000000);

        });
        mllRateFive.setOnClickListener(v -> {
            mRate = FIVE_RATE;
            mllRateOne.setBackgroundColor(0x00000000);
            mllRateTwo.setBackgroundColor(0x00000000);
            mllRateThree.setBackgroundColor(0x00000000);
            mllRateFour.setBackgroundColor(0x00000000);
            mllRateFive.setBackgroundColor(getResources().getColor(R.color.colorAccent20));

        });
        mBtnDone.setOnClickListener(v -> {
            mListener.onChooseRate(mRate);
            dismiss();
        });
        mBtnCancel.setOnClickListener(v -> dismiss());
    }


    private void setListener(OnChooseRateListener listener) {
        mListener = listener;
    }

    public interface OnChooseRateListener {
        void onChooseRate(int rate);
    }
}
