package bkdn.itt.datn.saleoff.view.dialog;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class DialogReport extends DialogFragment {
    @BindView(R.id.btnReportCancel)
    Button mBtnCancel;
    @BindView(R.id.btnReportSend)
    Button mBtnSend;
    @BindView(R.id.tvReportTitle)
    TextView mTvTitle;
    @BindView(R.id.reasonOne)
    TextView mTvReasonOne;
    @BindView(R.id.reasonTwo)
    TextView mTvReasonTwo;
    @BindView(R.id.reasonThree)
    TextView mTvReasonThree;
    @BindView(R.id.reasonFour)
    TextView mTvReasonFour;
    @BindView(R.id.reasonFive)
    TextView mTvReasonFive;


    private static final int REASON_ONE = 1;
    private static final int REASON_TWO = 2;
    private static final int REASON_THREE = 3;
    private static final int REASON_FOUR = 4;
    private static final int REASON_FIVE = 5;
    private View mRootView;
    private int mType;
    private String[] mReasonList;
    private OnReportListener mOnReportListener;
    private int mReasonId;

    public static DialogReport newInstance() {
        return new DialogReport();
    }

    public void setListener(int type, OnReportListener onReportListener) {
        mOnReportListener = onReportListener;
        this.mType = type;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mRootView != null) {
            return mRootView;
        }
        mRootView = inflater.inflate(R.layout.dialog_report, container, false);
        ButterKnife.bind(this, mRootView);
        initViews();
        initActions();
        return mRootView;
    }

    private void initViews() {
        if (mType == ConstantDefine.REPORT_POST) {
            mTvTitle.setText(getString(R.string.report_post));
            mReasonList = getResources().getStringArray(R.array.report_post_reason);
        } else if (mType == ConstantDefine.REPORT_SHOP) {
            mTvTitle.setText(getString(R.string.report_shop));
            mReasonList = getResources().getStringArray(R.array.report_shop_reason);
        } else {
            mTvTitle.setText(getString(R.string.report_user));
            mReasonList = getResources().getStringArray(R.array.report_user_reason);
        }
        mTvReasonOne.setText(mReasonList[0]);
        mTvReasonTwo.setText(mReasonList[1]);
        mTvReasonThree.setText(mReasonList[2]);
        mTvReasonFour.setText(mReasonList[3]);
        if (mReasonList.length == REASON_FIVE) {
            mTvReasonFive.setText(mReasonList[4]);
        } else {
            mTvReasonFive.setVisibility(View.GONE);
        }
    }

    private void initActions() {
        mTvReasonOne.setOnClickListener(v -> {
            mReasonId = REASON_ONE;
            mTvReasonOne.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight20));
            mTvReasonTwo.setBackgroundColor(0x00000000);
            mTvReasonThree.setBackgroundColor(0x00000000);
            mTvReasonFour.setBackgroundColor(0x00000000);
            mTvReasonFive.setBackgroundColor(0x00000000);
        });

        mTvReasonTwo.setOnClickListener(v -> {
            mReasonId = REASON_TWO;
            mTvReasonOne.setBackgroundColor(0x00000000);
            mTvReasonTwo.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight20));
            mTvReasonThree.setBackgroundColor(0x00000000);
            mTvReasonFour.setBackgroundColor(0x00000000);
            mTvReasonFive.setBackgroundColor(0x00000000);
        });

        mTvReasonThree.setOnClickListener(v -> {
            mReasonId = REASON_THREE;
            mTvReasonOne.setBackgroundColor(0x00000000);
            mTvReasonTwo.setBackgroundColor(0x00000000);
            mTvReasonThree.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight20));
            mTvReasonFour.setBackgroundColor(0x00000000);
            mTvReasonFive.setBackgroundColor(0x00000000);
        });

        mTvReasonFour.setOnClickListener(v -> {
            mReasonId = REASON_FOUR;
            mTvReasonOne.setBackgroundColor(0x00000000);
            mTvReasonTwo.setBackgroundColor(0x00000000);
            mTvReasonThree.setBackgroundColor(0x00000000);
            mTvReasonFour.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight20));
            mTvReasonFive.setBackgroundColor(0x00000000);
        });

        mTvReasonFive.setOnClickListener(v -> {
            mReasonId = REASON_FIVE;
            mTvReasonOne.setBackgroundColor(0x00000000);
            mTvReasonTwo.setBackgroundColor(0x00000000);
            mTvReasonThree.setBackgroundColor(0x00000000);
            mTvReasonFour.setBackgroundColor(0x00000000);
            mTvReasonFive.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight20));
        });

        mBtnSend.setOnClickListener(v -> {
            mOnReportListener.onClickSendReport(mReasonId);
            dismiss();
        });
        mBtnCancel.setOnClickListener(v -> dismiss());
    }

    public interface OnReportListener {
        void onClickSendReport(int reasonId);
    }
}
