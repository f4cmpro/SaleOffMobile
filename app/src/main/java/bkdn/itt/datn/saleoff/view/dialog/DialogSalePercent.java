package bkdn.itt.datn.saleoff.view.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import bkdn.itt.datn.saleoff.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 4/2/2018.
 */
public class DialogSalePercent extends DialogFragment {

    /*
    * Fields
    * */
    private OnChooseSalePercentListener mListener;

    public static DialogSalePercent newInstance(OnChooseSalePercentListener listener, FragmentManager manager) {
        DialogSalePercent dialogSalePercent = new DialogSalePercent();
        dialogSalePercent.setListener(listener);
        dialogSalePercent.show(manager, DialogSalePercent.class.getSimpleName());
        return dialogSalePercent;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        NumberPicker numberPicker = new NumberPicker(getContext());
        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(19);
        numberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        numberPicker.setDisplayedValues(getResources().getStringArray(R.array.sale_percent));
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose Sale Percent Value: ");
        builder.setView(numberPicker);
        builder.setPositiveButton("OK", (dialog, which) -> {
            mListener.onChooseSalePercent(numberPicker.getValue());
        });

        builder.setNegativeButton("CANCEL", (dialog, which) -> dismiss());

        return builder.create();
    }

    private void setListener(OnChooseSalePercentListener listener) {
        mListener = listener;
    }

    public interface OnChooseSalePercentListener {
        void onChooseSalePercent(int value);
    }
}
