package bkdn.itt.datn.saleoff.view.dialog;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import bkdn.itt.datn.saleoff.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 5/14/2018.
 */
public class DialogSelectLocation extends DialogFragment {

    /*
    * Widgets
    * */
    @BindView(R.id.edtSearchLocation)
    EditText mEdtSearch;
    @BindView(R.id.lvLocation)
    ListView mLvLocation;

    /*
    * Fields
    * */
    ArrayAdapter<String> mStringArrayAdapter;
    private View mRootView;
    private String[] mCities;
    private OnSelectLocationListener mListener;

    public static DialogSelectLocation newInstance() {
        DialogSelectLocation dialogSelectLocation = new DialogSelectLocation();
        return dialogSelectLocation;
    }

    public void setListener(OnSelectLocationListener listener) {
        mListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.dialog_select_location, container, false);
            ButterKnife.bind(this, mRootView);
            setViews();
            setActions();
        }
        return mRootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(getResources().getDimensionPixelSize(R.dimen._280sdp), getResources().getDimensionPixelSize(R.dimen._340sdp));
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    private void setViews() {
        mCities = getResources().getStringArray(R.array.cities_in_viet_nam);
        mStringArrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, mCities);
        mLvLocation.setAdapter(mStringArrayAdapter);
    }

    private void setActions() {
        mEdtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mStringArrayAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mLvLocation.setOnItemClickListener((parent, view, position, id) -> {
            mListener.onSelectNational(mStringArrayAdapter.getItem(position));
            dismiss();
        });
    }

    public interface OnSelectLocationListener {
        void onSelectNational(String city);
    }
}
