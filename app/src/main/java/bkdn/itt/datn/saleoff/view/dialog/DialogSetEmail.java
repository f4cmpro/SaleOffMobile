package bkdn.itt.datn.saleoff.view.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import bkdn.itt.datn.saleoff.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 3/21/2018.
 */
public class DialogSetEmail extends DialogFragment {
    private static final String BUNDLE_KEY = "email";
    @BindView(R.id.btnSetEmailDone)
    Button mBtnDone;
    @BindView(R.id.btnSetEmailCancel)
    Button mBtnCancel;
    @BindView(R.id.edtSetEmail)
    EditText mEdtSetEmail;
    private View mRootView;
    private OnSetEmailListener mListener;
    private String mEmail;

    public static DialogSetEmail newInstance(String phoneNumber, OnSetEmailListener listener, FragmentManager manager) {
        DialogSetEmail dialogSetEmail = new DialogSetEmail();
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_KEY, phoneNumber);
        dialogSetEmail.setArguments(bundle);
        dialogSetEmail.setListener(listener);
        dialogSetEmail.show(manager, BUNDLE_KEY);
        return dialogSetEmail;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.dialog_set_email, container, false);
            ButterKnife.bind(this, mRootView);
            initViews();
            initActions();
        }
        return mRootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(getResources().getDimensionPixelSize(R.dimen._240sdp), ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    private void initActions() {
        mBtnDone.setOnClickListener(v -> {
            mListener.onSetEmail(mEdtSetEmail.getText().toString());
            dismiss();
        });
        mBtnCancel.setOnClickListener(v -> dismiss());
    }

    private void initViews() {
        mEdtSetEmail.setRawInputType(InputType.TYPE_CLASS_TEXT);
        mEmail = getArguments().getString(BUNDLE_KEY);
        if (mEmail != null && !mEmail.isEmpty()
                && !mEmail.equals(getString(R.string.error_not_setup_yet))) {
            mEdtSetEmail.setText(mEmail);
        }
    }

    private void setListener(OnSetEmailListener listener) {
        mListener = listener;
    }

    public interface OnSetEmailListener {
        void onSetEmail(String email);
    }
}
