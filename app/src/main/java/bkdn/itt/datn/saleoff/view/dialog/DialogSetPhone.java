package bkdn.itt.datn.saleoff.view.dialog;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import bkdn.itt.datn.saleoff.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 3/21/2018.
 */
public class DialogSetPhone extends DialogFragment {
    private static final String BUNDLE_KEY = "number_phone";
    @BindView(R.id.btnSetPhoneDone)
    Button mBtnDone;
    @BindView(R.id.btnSetPhoneCancel)
    Button mBtnCancel;
    @BindView(R.id.edtSetPhone)
    EditText mEdtSetPhone;
    private View mRootView;
    private String mPhoneNumber;
    private OnSetPhoneListener mListener;

    public static DialogSetPhone newInstance(String phoneNumber, OnSetPhoneListener listener, FragmentManager manager) {
        DialogSetPhone dialogSetPhone = new DialogSetPhone();
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_KEY, phoneNumber);
        dialogSetPhone.setArguments(bundle);
        dialogSetPhone.setListener(listener);
        dialogSetPhone.show(manager, BUNDLE_KEY);
        return dialogSetPhone;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.dialog_set_phone, container, false);
            ButterKnife.bind(this, mRootView);
            initViews();
            initActions();
        }

        return mRootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(getResources().getDimensionPixelSize(R.dimen._240sdp), ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    private void initActions() {
        mBtnDone.setOnClickListener(v -> {
            mListener.onSetPhone(mEdtSetPhone.getText().toString());
            dismiss();
        });
        mBtnCancel.setOnClickListener(v -> dismiss());
    }

    private void initViews() {
        mEdtSetPhone.setRawInputType(InputType.TYPE_CLASS_TEXT);
        mPhoneNumber = getArguments().getString(BUNDLE_KEY);
        if (mPhoneNumber != null && !mPhoneNumber.isEmpty() &&
                !mPhoneNumber.equals(getString(R.string.error_not_setup_yet))) {
            mEdtSetPhone.setText(mPhoneNumber);
        }
    }

    private void setListener(OnSetPhoneListener listener) {
        mListener = listener;
    }


    public interface OnSetPhoneListener {
        void onSetPhone(String phoneNumber);
    }
}
