package bkdn.itt.datn.saleoff.view.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import bkdn.itt.datn.saleoff.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 4/23/2018.
 */
public class DialogSetUserName extends android.support.v4.app.DialogFragment {
    private static final String BUNDLE_KEY = "email";
    @BindView(R.id.btnSetEmailDone)
    Button mBtnDone;
    @BindView(R.id.btnSetEmailCancel)
    Button mBtnCancel;
    @BindView(R.id.edtSetEmail)
    EditText mEdtSetUserName;
    private View mRootView;
    private OnSetUserNameListener mListener;
    private String mUserName;

    public static DialogSetUserName newInstance(String userName, OnSetUserNameListener listener, FragmentManager manager) {
        DialogSetUserName dialogSetUserName = new DialogSetUserName();
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_KEY, userName);
        dialogSetUserName.setArguments(bundle);
        dialogSetUserName.setListener(listener);
        dialogSetUserName.show(manager, BUNDLE_KEY);
        return dialogSetUserName;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.dialog_set_user_name, container, false);
            ButterKnife.bind(this, mRootView);
            initViews();
            initActions();
        }
        return mRootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(getResources().getDimensionPixelSize(R.dimen._300sdp), ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    private void initActions() {
        mBtnDone.setOnClickListener(v -> {
            mListener.onSetUserName(mEdtSetUserName.getText().toString());
            dismiss();
        });
        mBtnCancel.setOnClickListener(v -> dismiss());
    }

    private void initViews() {
        mEdtSetUserName.setRawInputType(InputType.TYPE_CLASS_TEXT);
        mUserName = getArguments().getString(BUNDLE_KEY);
        if (mUserName != null && !mUserName.isEmpty() && !mUserName.equals(getString(R.string.error_not_setup_yet))) {
            mEdtSetUserName.setText(mUserName);
        }
    }

    private void setListener(OnSetUserNameListener listener) {
        mListener = listener;
    }

    public interface OnSetUserNameListener {
        void onSetUserName(String userName);
    }
}
