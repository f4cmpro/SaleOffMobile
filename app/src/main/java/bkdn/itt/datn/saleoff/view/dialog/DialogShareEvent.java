package bkdn.itt.datn.saleoff.view.dialog;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.Event;
import bkdn.itt.datn.saleoff.model.model.User;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.SharedPreferencesUtils;
import bkdn.itt.datn.saleoff.view.adapter.AdapterPresenter;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 6/1/2018.
 */
public class DialogShareEvent extends DialogFragment {
    private static final String KEY_BUNDLE_SHARE_EVENT = "KEY_BUNDLE_SHARE_EVENT";
    @BindView(R.id.imvShareEventAvatar)
    ImageView mImvUserAvatar;
    @BindView(R.id.tvShareEventUserName)
    TextView mTvUserName;
    @BindView(R.id.imvEventShareCover)
    ImageView mImvEventCover;
    @BindView(R.id.tvEventShareTitle)
    TextView mTvEventTitle;
    @BindView(R.id.tvEventShareStartDate)
    TextView mTvEventStartDate;
    @BindView(R.id.tvEventShareEndDate)
    TextView mTvEventEndDate;
    @BindView(R.id.tvEventShareDes)
    TextView mTvEventDes;
    @BindView(R.id.imbShareEventClose)
    ImageButton mImbClose;
    @BindView(R.id.edtShareEventDescription)
    EditText mEdtShareDescription;
    @BindView(R.id.btnShareEventDone)
    Button mBtnShare;

    private OnShareEventListener mListener;
    private View mRootView;
    private Event mShareEvent;
    private User me;
    private String mDescription;

    public static DialogShareEvent newInstance(Event shareEvent) {
        DialogShareEvent dialogShareEvent = new DialogShareEvent();
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_BUNDLE_SHARE_EVENT, shareEvent);
        dialogShareEvent.setArguments(bundle);
        return dialogShareEvent;
    }

    public void setListener(OnShareEventListener listener) {
        mListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.dialog_share_event, container, false);
            ButterKnife.bind(this, mRootView);
            setValues();
            setViews();
            setActions();
        }
        return mRootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(getResources().getDimensionPixelSize(R.dimen._280sdp),
                    getResources().getDimensionPixelSize(R.dimen._340sdp));
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    private void setValues() {
        mShareEvent = getArguments().getParcelable(KEY_BUNDLE_SHARE_EVENT);
        me = SharedPreferencesUtils.getInstance(getContext()).getUserProfile();
    }

    private void setViews() {
        if (mShareEvent != null) {
            if (me != null) {
                //set user avatar
                Glide.with(getContext()).load(me.getAvatar()).into(mImvUserAvatar);
                //set name
                mTvUserName.setText(me.getUsername());
            }
            //set cover
            Glide.with(getContext()).load(mShareEvent.getCover()).into(mImvEventCover);
            //set Title
            mTvEventTitle.setText(mShareEvent.getTitle());
            //set start date
            mTvEventStartDate.setText(mShareEvent.getStartDate());
            //st end date
            mTvEventEndDate.setText(mShareEvent.getEndDate());
            //set description
            mTvEventDes.setText(mShareEvent.getDetail());
        }
    }

    private void setActions() {
        mEdtShareDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mDescription = s.toString().trim();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mImbClose.setOnClickListener(v -> dismiss());
        mBtnShare.setOnClickListener(v -> {
            mListener.shareEvent(mDescription);
            dismiss();
        });

    }


    public interface OnShareEventListener {
        void shareEvent(String description);
    }
}
