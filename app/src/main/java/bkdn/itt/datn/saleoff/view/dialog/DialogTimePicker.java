package bkdn.itt.datn.saleoff.view.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TimePicker;

import java.util.Calendar;

import bkdn.itt.datn.saleoff.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 3/21/2018.
 */
public class DialogTimePicker extends DialogFragment {
    @BindView(R.id.timePickerSpinner)
    TimePicker mTimePicker;
    @BindView(R.id.btnTimePickerCancel)
    Button mBtnCancel;
    @BindView(R.id.btnTimePickerDone)
    Button mBtnDone;
    private View mRootView;
    private OnPickTimeListener mListener;

    public static DialogTimePicker newInstance(OnPickTimeListener listener, FragmentManager manager) {
        DialogTimePicker dialogDatePicker = new DialogTimePicker();
        dialogDatePicker.setListener(listener);
        dialogDatePicker.show(manager, DialogTimePicker.class.getSimpleName());
        return dialogDatePicker;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.dialog_time_picker, container, false);
            ButterKnife.bind(this, mRootView);
            initActions();
        }
        return mRootView;
    }

    private void initActions() {
        mBtnDone.setOnClickListener(v -> selectTime());
        mBtnCancel.setOnClickListener(v -> dismiss());
    }


    private void setListener(OnPickTimeListener listener) {
        mListener = listener;
    }

    private void selectTime() {
        int hour = mTimePicker.getCurrentHour();
        int minute = mTimePicker.getCurrentMinute();
        mListener.onPickTime(hour, minute);
        dismiss();
    }

    public interface OnPickTimeListener {
        void onPickTime(int hour, int minutes);
    }
}
