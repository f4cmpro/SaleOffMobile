package bkdn.itt.datn.saleoff.view.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import java.lang.reflect.ParameterizedType;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.presenter.PresenterBase;
import bkdn.itt.datn.saleoff.view.iface.IViewBase;

/**
 * Created by TuDLT on 12/26/17.
 */

public abstract class FragmentBase<P extends PresenterBase> extends Fragment {
    private P viewPresenter;


    protected P getPresenter(IViewBase iface) {
        try {
            if (this.viewPresenter == null) {
                String e = ((Class) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0]).getName();
                Class classDefinition = Class.forName(e);
                this.viewPresenter = (P) classDefinition.newInstance();
                this.viewPresenter.setIFace(iface);
                this.viewPresenter.onInit();
                return this.viewPresenter;
            }
        } catch (InstantiationException var4) {
            var4.printStackTrace();
        } catch (IllegalAccessException var5) {
            var5.printStackTrace();
        } catch (ClassNotFoundException var6) {
            var6.printStackTrace();
        } catch (java.lang.InstantiationException var7) {
            var7.printStackTrace();
        }
        return this.viewPresenter;
    }

    protected void replaceFragment(FragmentBase fragment, int containViewId, boolean addToBackStack) {
        String TAB = fragment.getClass().getName();
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.view_enter_from_left, R.anim.view_exit_to_right, R.anim.view_enter_from_right, R.anim.view_exit_to_left);
        if (addToBackStack) {
            transaction.addToBackStack(TAB);
        }
        transaction.replace(containViewId, fragment, TAB);
        transaction.commit();
        getChildFragmentManager().executePendingTransactions();
    }

}
