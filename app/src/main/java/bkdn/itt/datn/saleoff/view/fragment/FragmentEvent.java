package bkdn.itt.datn.saleoff.view.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.model.Event;
import bkdn.itt.datn.saleoff.presenter.PresenterEvent;
import bkdn.itt.datn.saleoff.utils.CustomPaginationScrollListener;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.activity.ActivityEventDetail;
import bkdn.itt.datn.saleoff.view.adapter.AdapterBase;
import bkdn.itt.datn.saleoff.view.adapter.AdapterEvent;
import bkdn.itt.datn.saleoff.view.iface.IFragmentEvent;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentEvent extends FragmentBase<PresenterEvent>
        implements IFragmentEvent, AdapterBase.OnBaseItemClickListener<Event> {

    /*
    * Widgets
    * */
    @BindView(R.id.btnEventHappening)
    Button mBtnHappening;
    @BindView(R.id.btnEventGoingHappen)
    Button mBtnGoingHappen;
    @BindView(R.id.btnEventHappened)
    Button mBtnHappened;
    @BindView(R.id.btnEventMyJoin)
    Button mBtnEventJoined;
    @BindView(R.id.recyclerEvent)
    RecyclerView mRecyclerEvent;
    @BindView(R.id.swipeRefreshEvent)
    SwipeRefreshLayout mRefreshEvent;
    @BindView(R.id.viewEventEmpty)
    View mViewEventEmpty;
    @BindView(R.id.tvLayoutNotFoundItem)
    TextView mTvNotFound;

    /*
    * Fields
    * */
    private View mRootView;
    private AdapterEvent mAdapterEvent;
    private boolean mIsRefresh;
    private boolean mIsLoadMore;
    private int mHappeningCurrentPage;
    private int mGoingHappenCurrentPage;
    private int mHappenedCurrentPage;


    public static FragmentEvent newInstance() {
        return new FragmentEvent();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.fragment_event, container, false);
            ButterKnife.bind(this, mRootView);
            initValues();
            initViews();
            initActions();
        }
        return mRootView;
    }

    private void initValues() {
        mHappeningCurrentPage = ConstantDefine.FIRST_PAGE;
        getPresenter(this).getEventHappening(true, mHappeningCurrentPage);

    }

    private void initViews() {
        mBtnHappening.setSelected(true);
        changeBtnEventWhenSelected(mBtnHappening);
        mAdapterEvent = new AdapterEvent(getContext(), ConstantDefine.EVENT_HAPPENING);
        mAdapterEvent.setListener(this);
        RecyclerViewUtils.Create().setUpVertical(getContext(), mRecyclerEvent).setAdapter(mAdapterEvent);
    }

    private void initActions() {
        mBtnHappening.setOnClickListener(v -> {
            mViewEventEmpty.setVisibility(View.GONE);
            mRecyclerEvent.setVisibility(View.VISIBLE);
            if (mBtnHappening.isSelected()) {
                return;
            }
            mAdapterEvent.clearData();
            mHappeningCurrentPage = ConstantDefine.FIRST_PAGE;
            getPresenter(FragmentEvent.this).getEventHappening(true, mHappeningCurrentPage);
            mBtnHappening.setSelected(true);
            mBtnGoingHappen.setSelected(false);
            mBtnHappened.setSelected(false);
            mBtnEventJoined.setSelected(false);
            changeBtnEventWhenSelected(mBtnHappening);
            changeBtnEventWhenSelected(mBtnGoingHappen);
            changeBtnEventWhenSelected(mBtnHappened);
            changeBtnEventWhenSelected(mBtnEventJoined);

        });

        mBtnGoingHappen.setOnClickListener(v -> {
            mViewEventEmpty.setVisibility(View.GONE);
            mRecyclerEvent.setVisibility(View.VISIBLE);
            if (mBtnGoingHappen.isSelected()) {
                return;
            }
            mAdapterEvent.clearData();
            mGoingHappenCurrentPage = ConstantDefine.FIRST_PAGE;
            getPresenter(FragmentEvent.this).getEventGoingHappen(true, mGoingHappenCurrentPage);
            mBtnHappening.setSelected(false);
            mBtnGoingHappen.setSelected(true);
            mBtnHappened.setSelected(false);
            mBtnEventJoined.setSelected(false);
            changeBtnEventWhenSelected(mBtnHappening);
            changeBtnEventWhenSelected(mBtnGoingHappen);
            changeBtnEventWhenSelected(mBtnHappened);
            changeBtnEventWhenSelected(mBtnEventJoined);
        });

        mBtnHappened.setOnClickListener(v -> {
            mViewEventEmpty.setVisibility(View.GONE);
            mRecyclerEvent.setVisibility(View.VISIBLE);
            if (mBtnHappened.isSelected()) {
                return;
            }
            mAdapterEvent.clearData();
            mHappenedCurrentPage = ConstantDefine.FIRST_PAGE;
            getPresenter(FragmentEvent.this).getEventHappened(true, mHappenedCurrentPage);
            mBtnHappening.setSelected(false);
            mBtnGoingHappen.setSelected(false);
            mBtnHappened.setSelected(true);
            mBtnEventJoined.setSelected(false);
            changeBtnEventWhenSelected(mBtnHappening);
            changeBtnEventWhenSelected(mBtnGoingHappen);
            changeBtnEventWhenSelected(mBtnHappened);
            changeBtnEventWhenSelected(mBtnEventJoined);
        });

        mBtnEventJoined.setOnClickListener(v -> {
            mViewEventEmpty.setVisibility(View.GONE);
            mRecyclerEvent.setVisibility(View.VISIBLE);
            if (mBtnEventJoined.isSelected()) {
                return;
            }
            mAdapterEvent.clearData();
            getPresenter(FragmentEvent.this).getEventJoined(true);
            mBtnHappening.setSelected(false);
            mBtnGoingHappen.setSelected(false);
            mBtnHappened.setSelected(false);
            mBtnEventJoined.setSelected(true);
            changeBtnEventWhenSelected(mBtnHappening);
            changeBtnEventWhenSelected(mBtnGoingHappen);
            changeBtnEventWhenSelected(mBtnHappened);
            changeBtnEventWhenSelected(mBtnEventJoined);
        });

        mRefreshEvent.setOnRefreshListener(() -> {
            mIsRefresh = true;
            if (mBtnHappening.isSelected()) {
                getPresenter(FragmentEvent.this).getEventHappening(false, ConstantDefine.FIRST_PAGE);
            } else if (mBtnGoingHappen.isSelected()) {
                getPresenter(FragmentEvent.this).getEventGoingHappen(false, ConstantDefine.FIRST_PAGE);

            } else if (mBtnHappened.isSelected()) {
                getPresenter(FragmentEvent.this).getEventHappened(false, ConstantDefine.FIRST_PAGE);

            } else {
                getPresenter(FragmentEvent.this).getEventJoined(false);
            }
        });

        mRecyclerEvent.addOnScrollListener(new CustomPaginationScrollListener(RecyclerViewUtils.Create()
                .getLinearLayoutManager(mRecyclerEvent)) {
            @Override
            protected void loadMoreItems() {
                mIsLoadMore = true;
                if (mBtnHappening.isSelected()) {
                    mHappeningCurrentPage++;
                    getPresenter(FragmentEvent.this).getEventHappening(false, mHappeningCurrentPage);
                } else if (mBtnGoingHappen.isSelected()) {
                    mGoingHappenCurrentPage++;
                    getPresenter(FragmentEvent.this).getEventGoingHappen(false, mGoingHappenCurrentPage);

                } else if (mBtnEventJoined.isSelected()) {
                    mHappenedCurrentPage++;
                    getPresenter(FragmentEvent.this).getEventHappened(false, mHappenedCurrentPage);
                }
            }

            @Override
            protected boolean isLoading() {
                return mIsLoadMore;
            }

            @Override
            protected void enableRefreshLayout(boolean isEnable) {

            }
        });

    }

    private void changeBtnEventWhenSelected(Button button) {
        if (button.isSelected()) {
            button.setBackgroundResource(R.drawable.bg_btn_event_selected);
            button.setTextColor(getResources().getColor(R.color.colorEventSelected));
            button.setTextSize(12);
        } else {
            button.setBackgroundResource(R.drawable.bg_btn_event_default);
            button.setTextColor(getResources().getColor(R.color.colorBlack));
            button.setTextSize(10);
        }
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
        if (mIsRefresh) {
            mIsRefresh = false;
            mRefreshEvent.setRefreshing(mIsRefresh);
        } else {
            mIsLoadMore = false;
        }
    }

    @Override
    public void getEventHappeningSuccess(List<Event> events) {
        mAdapterEvent.changeEventStatus(ConstantDefine.EVENT_HAPPENING);
        if (mIsRefresh) {
            mHappeningCurrentPage = ConstantDefine.FIRST_PAGE;
            mAdapterEvent.refreshData(events);
        } else {
            mAdapterEvent.addData(events);
        }
    }

    @Override
    public void getEventHappeningError(String errorMessage) {
        if (mIsLoadMore) {
            mHappeningCurrentPage--;
        }
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getEventGoingHappenSuccess(List<Event> events) {
        mAdapterEvent.changeEventStatus(ConstantDefine.EVENT_GOING_HAPPEN);
        if (mIsRefresh) {
            mGoingHappenCurrentPage = ConstantDefine.FIRST_PAGE;
            mAdapterEvent.refreshData(events);
        } else {
            mAdapterEvent.addData(events);
        }
    }

    @Override
    public void getEventGoingHappenError(String errorMessage) {
        if (mIsLoadMore) {
            mGoingHappenCurrentPage--;
        }
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getEventHappenedSuccess(List<Event> events) {
        mAdapterEvent.changeEventStatus(ConstantDefine.EVENT_HAPPENED);
        if (mIsRefresh) {
            mHappenedCurrentPage = ConstantDefine.FIRST_PAGE;
            mAdapterEvent.refreshData(events);
        } else {
            mAdapterEvent.addData(events);
        }
    }

    @Override
    public void getEventHappenedError(String errorMessage) {
        if (mIsLoadMore) {
            mHappenedCurrentPage--;
        }
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getEventJoinedSuccess(List<Event> events) {
        mAdapterEvent.changeEventStatus(ConstantDefine.EVENT_JOINED);
        if (events.isEmpty()) {
            mViewEventEmpty.setVisibility(View.VISIBLE);
            mRecyclerEvent.setVisibility(View.GONE);
            mTvNotFound.setText("Bạn chưa tham gia sự kiện nào!");
            return;
        }
        mViewEventEmpty.setVisibility(View.GONE);
        mRecyclerEvent.setVisibility(View.VISIBLE);
        if (mIsRefresh) {
            mHappenedCurrentPage = ConstantDefine.FIRST_PAGE;
            mAdapterEvent.refreshData(events);
        } else {
            mAdapterEvent.addData(events);
        }

    }

    @Override
    public void getEventJoinedError(String errorMessage) {
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(Event event, int position) {
        Intent intent = new Intent(getContext(), ActivityEventDetail.class);
        intent.putExtra(GlobalDefine.KEY_INTENT_EVENT_ID, event.getEventId());
        startActivity(intent);
    }
}
