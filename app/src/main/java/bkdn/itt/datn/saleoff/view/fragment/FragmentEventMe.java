package bkdn.itt.datn.saleoff.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.model.Event;
import bkdn.itt.datn.saleoff.presenter.PresenterEventMe;
import bkdn.itt.datn.saleoff.utils.CustomPaginationScrollListener;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.adapter.AdapterEvent;
import bkdn.itt.datn.saleoff.view.iface.IFragmentEventMe;
import butterknife.BindView;
import es.dmoral.toasty.Toasty;

/**
 * Created by TuDLT on 5/29/2018.
 */
public class FragmentEventMe extends FragmentBase<PresenterEventMe>
        implements IFragmentEventMe {


    /*
    * Widgets
    * */
    @BindView(R.id.swipeRefreshEventMe)
    SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.recyclerEventMe)
    RecyclerView mRecyclerView;
    /*
    * Fields
    * */
    private View mRootView;
    private AdapterEvent mAdapterEvent;
    private int mCurrentPage;
    private boolean mIsLoadMore;
    private boolean mIsRefresh;

    public static FragmentEventHot newInstance() {
        return new FragmentEventHot();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.fragment_event_me, container, false);
            iniValues();
            initViews();
            initActions();
        }
        return mRootView;
    }

    private void iniValues() {
        mCurrentPage = ConstantDefine.FIRST_PAGE;
        getPresenter(this).getAllMyEvents(true, mCurrentPage);
    }

    private void initViews() {
        mAdapterEvent = new AdapterEvent(getContext(), ConstantDefine.EVENT_JOINED);
        RecyclerViewUtils.Create().setUpVertical(getContext(), mRecyclerView).setAdapter(mAdapterEvent);
    }

    private void initActions() {
        mRefreshLayout.setOnRefreshListener(() -> {
            mIsRefresh = true;
            getPresenter(FragmentEventMe.this).getAllMyEvents(false, ConstantDefine.FIRST_PAGE);
        });
        mRecyclerView.addOnScrollListener(new CustomPaginationScrollListener(RecyclerViewUtils.Create()
                .getLinearLayoutManager(mRecyclerView)) {
            @Override
            protected void loadMoreItems() {
                mIsLoadMore = true;
                mCurrentPage++;
                getPresenter(FragmentEventMe.this).getAllMyEvents(false, mCurrentPage);
            }

            @Override
            protected boolean isLoading() {
                return mIsLoadMore;
            }

            @Override
            protected void enableRefreshLayout(boolean isEnable) {

            }
        });
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
        if (mIsRefresh) {
            mIsRefresh = false;
            mRefreshLayout.setRefreshing(false);
        } else {
            mIsLoadMore = false;
        }
    }

    @Override
    public void getAllMyEvents(List<Event> events) {
        if (mIsRefresh) {
            mCurrentPage = ConstantDefine.FIRST_PAGE;
            mAdapterEvent.refreshData(events);
        } else {
            mAdapterEvent.addData(events);
        }
    }

    @Override
    public void getError(String errorMessage) {
        if (mIsLoadMore) {
            mCurrentPage--;
        }
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }
}
