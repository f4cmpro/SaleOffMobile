package bkdn.itt.datn.saleoff.view.fragment;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.view.adapter.AdapterFollowPager;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentFollow extends FragmentBase {
    /*
    * Widgets
    * */
    @BindView(R.id.tabLayoutFollow)
    TabLayout mTabLayoutFollow;
    @BindView(R.id.viewPagerFollow)
    ViewPager mPagerFollow;

    /*
    * Fields
    * */
    private View mRootView;
    private AdapterFollowPager mAdapterFollowPager;
    private List<FragmentBase> mFragmentBases;

    public static FragmentBase newInstance() {
        FragmentFollow fragmentFollow = new FragmentFollow();
        return fragmentFollow;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.fragment_follow, container, false);
            ButterKnife.bind(this, mRootView);
            initViews();
        }
        return mRootView;
    }

    private void initViews() {
        mFragmentBases = new ArrayList<>();
        mFragmentBases.add(FragmentFollowShop.newInstance());
        mFragmentBases.add(FragmentFollowUser.newInstance());
        mAdapterFollowPager = new AdapterFollowPager(getContext(), getChildFragmentManager(), mFragmentBases);
        mPagerFollow.setAdapter(mAdapterFollowPager);
        mTabLayoutFollow.setupWithViewPager(mPagerFollow);
    }

}
