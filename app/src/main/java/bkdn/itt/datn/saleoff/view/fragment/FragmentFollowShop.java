package bkdn.itt.datn.saleoff.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.model.ShopInfo;
import bkdn.itt.datn.saleoff.presenter.PresenterFollowShop;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.activity.ActivityShopPublic;
import bkdn.itt.datn.saleoff.view.adapter.AdapterFollowShop;
import bkdn.itt.datn.saleoff.view.adapter.AdapterSuggestionShop;
import bkdn.itt.datn.saleoff.view.iface.IFragmentFollowShop;
import bkdn.itt.datn.saleoff.view.iface.OnClickFollowListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

/**
 * Created by TuDLT on 4/15/2018.
 */
public class FragmentFollowShop extends FragmentBase<PresenterFollowShop> implements IFragmentFollowShop,
        AdapterFollowShop.OnClickFollowShopListener {

    /*
    * Widgets
    * */
    @BindView(R.id.recyclerFollowShop)
    RecyclerView mRecyclerFollowShop;
    @BindView(R.id.viewFollowShopNotFoundItem)
    View mViewNotFoundItem;
    @BindView(R.id.recyclerFollowShopSuggestion)
    RecyclerView mRecyclerSuggestion;
    @BindView(R.id.swipeRefreshFollowShop)
    SwipeRefreshLayout mRefreshLayout;
    /*
    * Fields
    * */
    private View mRootView;
    private AdapterFollowShop mAdapterFollowShop;
    private AdapterSuggestionShop mAdapterSuggestionShop;
    private boolean mIsRefreshFollow;
    private boolean mIsRefreshSuggestion;
    private boolean mIsLoadMore;
    private OnClickFollowListener mOnClickFollowListener;

    public static FragmentBase newInstance() {
        return new FragmentFollowShop();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = LayoutInflater.from(getContext()).inflate(R.layout.fragment_follow_shop, container,
                    false);
            ButterKnife.bind(this, mRootView);
            getPresenter(this).getFollowedShops();
            getPresenter(this).getSuggestedShops();
            initViews();
            initActions();
        }
        return mRootView;
    }

    private void initViews() {
        mAdapterFollowShop = new AdapterFollowShop(getContext(), this);
        RecyclerViewUtils.Create().setUpVertical(getContext(), mRecyclerFollowShop)
                .setAdapter(mAdapterFollowShop);

        mAdapterSuggestionShop = new AdapterSuggestionShop(getContext(), this);
        RecyclerViewUtils.Create().setUpHorizontal(getContext(), mRecyclerSuggestion)
                .setAdapter(mAdapterSuggestionShop);
    }


    private void initActions() {
        mRefreshLayout.setOnRefreshListener(() -> {
            mIsRefreshFollow = true;
            mIsRefreshSuggestion = true;
            getPresenter(this).getFollowedShops();
            getPresenter(this).getSuggestedShops();
        });
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
        mRefreshLayout.setRefreshing(false);
    }

    @Override
    public void getFollowShops(List<ShopInfo> shopInfoList) {
        if (shopInfoList != null && !shopInfoList.isEmpty()) {
            if (mIsRefreshFollow) {
                mIsRefreshFollow = false;
                mAdapterFollowShop.refreshShopList(shopInfoList);
            } else {
                mIsLoadMore = false;
                mAdapterFollowShop.loadMoreShopList(shopInfoList);
            }
            mRecyclerFollowShop.setVisibility(View.VISIBLE);
            mViewNotFoundItem.setVisibility(View.GONE);
        } else {
            mRecyclerFollowShop.setVisibility(View.GONE);
            ((TextView) mViewNotFoundItem.findViewById(R.id.tvLayoutNotFoundItem))
                    .setText(getString(R.string.warning_not_follow_shop_yet));
            mViewNotFoundItem.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void getSuggestionShop(List<ShopInfo> shopInfoList) {
        if (mIsRefreshSuggestion) {
            mIsRefreshSuggestion = false;
            mAdapterSuggestionShop.refreshSuggestedShops(shopInfoList);
        } else {
            mIsLoadMore = false;
            mAdapterSuggestionShop.loadMoreSuggestedShops(shopInfoList);
        }
    }

    @Override
    public void getError(String errorMessage) {
        mIsRefreshSuggestion = false;
        mIsRefreshFollow = false;
        mIsLoadMore = false;
        Toasty.warning(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void followShopSuccess() {
        mOnClickFollowListener.success();
        Toasty.success(getContext(),getString(R.string.followed), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void followShopFail(String errorMessage) {
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void unFollowShopSuccess() {
        Toasty.success(getContext(), getString(R.string.unfollowed), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClickFollow(int shopId, int bonusCode, OnClickFollowListener onClickFollowListener) {
        mOnClickFollowListener = onClickFollowListener;
        getPresenter(FragmentFollowShop.this).followShop(shopId, bonusCode);

    }
    @Override
    public void onClickUnFollow(int shopId) {
        getPresenter(this).unFollowShop(shopId);
    }

    @Override
    public void onWatchDetailShop(int shopId) {
        Intent intent = new Intent(getContext(), ActivityShopPublic.class);
        intent.putExtra(GlobalDefine.KEY_INTENT_SHOP_ID, shopId);
        startActivity(intent);
    }

}
