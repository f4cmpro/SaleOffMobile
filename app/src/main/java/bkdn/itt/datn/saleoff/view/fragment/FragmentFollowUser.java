package bkdn.itt.datn.saleoff.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.model.UserInfo;
import bkdn.itt.datn.saleoff.presenter.PresenterFollowUser;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.activity.ActivityUserPublic;
import bkdn.itt.datn.saleoff.view.adapter.AdapterFollowUser;
import bkdn.itt.datn.saleoff.view.adapter.AdapterSuggestionUser;
import bkdn.itt.datn.saleoff.view.iface.IFragmentFollowUser;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

/**
 * Created by TuDLT on 4/15/2018.
 */
public class FragmentFollowUser extends FragmentBase<PresenterFollowUser> implements IFragmentFollowUser,
        AdapterFollowUser.OnClickFollowUserListener {
    /*
* Widgets
* */
    @BindView(R.id.recyclerFollowUser)
    RecyclerView mRecyclerFollowUser;
    @BindView(R.id.viewFollowUserNotFoundItem)
    View mViewNotFoundItem;
    @BindView(R.id.recyclerFollowUserSuggestion)
    RecyclerView mRecyclerSuggestion;
    @BindView(R.id.swipeRefreshFollowUser)
    SwipeRefreshLayout mRefreshLayout;
    /*
    * Fields
    * */
    private View mRootView;
    private AdapterFollowUser mAdapterFollowUser;
    private AdapterSuggestionUser mAdapterSuggestionUser;
    private boolean mIsRefreshFollow;
    private boolean mIsRefreshSuggestion;
    private boolean mIsLoadMore;

    public static FragmentBase newInstance() {
        return new FragmentFollowUser();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = LayoutInflater.from(getContext()).inflate(R.layout.fragment_follow_user, container,
                    false);
            ButterKnife.bind(this, mRootView);
            getPresenter(this).getFollowedUser();
            getPresenter(this).getSuggestedUser();
            initViews();
            initActions();
        }
        return mRootView;
    }

    private void initViews() {
        mAdapterFollowUser = new AdapterFollowUser(getContext(), this);
        RecyclerViewUtils.Create().setUpVertical(getContext(), mRecyclerFollowUser)
                .setAdapter(mAdapterFollowUser);

        mAdapterSuggestionUser = new AdapterSuggestionUser(getContext(), this);
        RecyclerViewUtils.Create().setUpHorizontal(getContext(), mRecyclerSuggestion)
                .setAdapter(mAdapterSuggestionUser);
    }


    private void initActions() {
        mRefreshLayout.setOnRefreshListener(() -> {
            mIsRefreshFollow = true;
            mIsRefreshSuggestion = true;
            getPresenter(this).getFollowedUser();
            getPresenter(this).getSuggestedUser();
        });
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
        mRefreshLayout.setRefreshing(false);
    }

    @Override
    public void getFollowUser(List<UserInfo> userInfoList) {
        if (userInfoList != null && !userInfoList.isEmpty()) {
            if (mIsRefreshFollow) {
                mIsRefreshFollow = false;
                mAdapterFollowUser.refreshUserList(userInfoList);
            } else {
                mIsLoadMore = false;
                mAdapterFollowUser.loadMoreUserList(userInfoList);
            }
            mRecyclerFollowUser.setVisibility(View.VISIBLE);
            mViewNotFoundItem.setVisibility(View.GONE);
        } else {
            mRecyclerFollowUser.setVisibility(View.GONE);
            ((TextView) mViewNotFoundItem.findViewById(R.id.tvLayoutNotFoundItem))
                    .setText(getString(R.string.warning_not_follow_user_yet));
            mViewNotFoundItem.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void getSuggestionUser(List<UserInfo> shopInfoList) {
        if (mIsRefreshSuggestion) {
            mIsRefreshSuggestion = false;
            mAdapterSuggestionUser.refreshSuggestedUsers(shopInfoList);
        } else {
            mIsLoadMore = false;
            mAdapterSuggestionUser.loadMoreSuggestedUsers(shopInfoList);
        }
    }

    @Override
    public void followUserSuccess() {
        Toasty.success(getContext(), "Followed!", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void unFollowUserSuccess() {
        Toasty.success(getContext(), "UnFollowed!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getError(String errorMessage) {
        mIsRefreshSuggestion = false;
        mIsRefreshFollow = false;
        mIsLoadMore = false;
        Toasty.warning(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClickFollow(int userId) {
        getPresenter(this).followUser(userId);

    }

    @Override
    public void onClickUnFollow(int userId) {
        getPresenter(this).unFollowUser(userId);

    }

    @Override
    public void onWatchUserDetail(int userId) {
        Intent intent = new Intent(getContext(), ActivityUserPublic.class);
        intent.putExtra(GlobalDefine.KEY_INTENT_USER_ID, userId);
        startActivity(intent);
    }
}
