package bkdn.itt.datn.saleoff.view.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.presenter.PresenterHistory;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.view.adapter.AdapterHistory;
import butterknife.BindView;
import butterknife.ButterKnife;


public class FragmentHistory extends FragmentBase<PresenterHistory> {
    /*
    * Widgets
    * */
    @BindView(R.id.edtHistorySearch)
    EditText mEdtSearch;
    @BindView(R.id.recyclerHistory)
    RecyclerView mRecyclerHistory;

    /*
    * Fields
    * */
    private View mRootView;
    private AdapterHistory mAdapterHistory;

    public static FragmentHistory newInstance() {
        FragmentHistory fragmentHistory = new FragmentHistory();
        return fragmentHistory;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.fragment_history, container, false);
            ButterKnife.bind(this, mRootView);
            initViews();
            initValues();
            initActions();

        }
        return mRootView;
    }

    private void initViews() {
        RecyclerViewUtils.Create().setUpVertical(getContext(), mRecyclerHistory);
        mAdapterHistory = new AdapterHistory(getContext());
        mRecyclerHistory.setAdapter(mAdapterHistory);
    }

    private void initValues() {

    }

    private void initActions() {

    }


}
