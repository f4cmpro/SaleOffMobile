package bkdn.itt.datn.saleoff.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.model.User;
import bkdn.itt.datn.saleoff.model.net.NetworkUtils;
import bkdn.itt.datn.saleoff.presenter.PresenterMe;
import bkdn.itt.datn.saleoff.utils.DialogUtils;
import bkdn.itt.datn.saleoff.utils.ImagesUtils;
import bkdn.itt.datn.saleoff.utils.SharedPreferencesUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.activity.ActivityBase;
import bkdn.itt.datn.saleoff.view.activity.ActivityLogin;
import bkdn.itt.datn.saleoff.view.activity.ActivityMyPost;
import bkdn.itt.datn.saleoff.view.activity.ActivityPostCreate;
import bkdn.itt.datn.saleoff.view.activity.ActivityProfile;
import bkdn.itt.datn.saleoff.view.activity.ActivityShopList;
import bkdn.itt.datn.saleoff.view.dialog.DialogErrorMessage;
import bkdn.itt.datn.saleoff.view.dialog.DialogPhotoFrom;
import bkdn.itt.datn.saleoff.view.iface.IFragmentMe;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

/**
 * Created by TuDLT on 5/8/2018.
 */
public class FragmentMe extends FragmentBase<PresenterMe> implements IFragmentMe {

    /*
    * Widgets
    * */
    @BindView(R.id.rlMeProfile)
    RelativeLayout mRlEditProfile;
    @BindView(R.id.llMeCreatePost)
    LinearLayout mLlCreatePost;
    @BindView(R.id.llMeYourPost)
    LinearLayout mLlYourPost;
    @BindView(R.id.llMeYourShop)
    LinearLayout mLlYourShop;
    @BindView(R.id.llMeWorkingShop)
    LinearLayout mLlWorkingShop;
    @BindView(R.id.llMeCreateShop)
    LinearLayout mLlCreateShop;
    @BindView(R.id.llMeFollowers)
    LinearLayout mLlFollowers;
    @BindView(R.id.llMeAppInformation)
    LinearLayout mLlAppInfo;
    @BindView(R.id.llMeAppSetting)
    LinearLayout mLlAppSetting;
    @BindView(R.id.btnMeLogOut)
    Button mBtnLogOut;
    @BindView(R.id.imvMeAvatar)
    ImageView mImvAvatar;
    @BindView(R.id.tvMeUserName)
    TextView mTvMyName;
    @BindView(R.id.tvMeSaleCode)
    TextView mTvSaleCode;

    /*
    * Fields
    * */
    private View mRootView;
    private User me;

    public static FragmentMe newInstance() {
        FragmentMe fragmentMe = new FragmentMe();
        Bundle bundle = new Bundle();
        fragmentMe.setArguments(bundle);
        return fragmentMe;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.fragment_me, container, false);
            ButterKnife.bind(this, mRootView);
            me = SharedPreferencesUtils.getInstance(getContext()).getUserProfile();
            initViews();
            initActions();
        }
        return mRootView;
    }

    public void initViews() {
        if (me != null) {
            //set my avatar
            Glide.with(getContext()).load(me.getAvatar()).into(mImvAvatar);
            //set my name
            mTvMyName.setText(me.getUsername());
            //set bonus code
            mTvSaleCode.setText(String.valueOf(me.getBonus()));
        }
    }

    public void initActions() {
        //click edit profile
        mRlEditProfile.setOnClickListener(v -> {
            Intent profileIntent = new Intent(getContext(), ActivityProfile.class);
            startActivityForResult(profileIntent, ConstantDefine.EDIT_PROFILE_REQUEST);
        });

        //click create post
        mLlCreatePost.setOnClickListener(v -> {
//            DialogPhotoFrom.newInstance(getFragmentManager(), new DialogPhotoFrom.OnPhotoFromCallBack() {
//                @Override
//                public void fromCamera() {
//                    ImagesUtils.openCamera(getContext());
//                }
//
//                @Override
//                public void fromGallery() {
//                    ImagesUtils.openGallery(getContext());
//                }
//            });
            Intent profileIntent = new Intent(getContext(), ActivityPostCreate.class);
            startActivityForResult(profileIntent, ConstantDefine.CREATE_MY_POST_REQUEST);
        });

        //click your post
        mLlYourPost.setOnClickListener(v -> {
            ((ActivityBase) getContext()).startActivity(ActivityMyPost.class);
        });

        //click followers to you
        mLlFollowers.setOnClickListener(v -> {

        });

        //click your shop
        mLlYourShop.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), ActivityShopList.class);
            intent.putExtra(GlobalDefine.ROLE, GlobalDefine.OWNER_SHOP);
            startActivity(intent);

        });

        //click you working at shop?
        mLlWorkingShop.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), ActivityShopList.class);
            intent.putExtra(GlobalDefine.ROLE, GlobalDefine.MEMBER_SHOP);
            startActivity(intent);
        });

        //click add your shop more
        mLlCreateShop.setOnClickListener(v -> {

        });

        //click to watch app info and feed back
        mLlAppInfo.setOnClickListener(v -> {

        });

        //click to setting app
        mLlAppSetting.setOnClickListener(v -> {

        });

        //click to log out
        mBtnLogOut.setOnClickListener(v -> {
            logOut();
        });

    }

    private void logOut() {
        DialogUtils.createConfirmDialog(getContext(), "Bạn muốn đăng xuất:", me.getUsername(),
                new DialogUtils.DialogConfirmCallback() {
                    @Override
                    public void onClickPositiveButton() {
                        if (NetworkUtils.isConnected(getContext())) {
                            getPresenter(FragmentMe.this).logOut();
                        } else {
                            DialogErrorMessage.newInstance(getString(R.string.error_not_connect))
                                    .show(getFragmentManager(), "log out");
                        }
                    }

                    @Override
                    public void onClickNegativeButton() {

                    }
                });
    }

    @Override
    public void logOutSuccess() {
        Intent intent = new Intent(getContext(), ActivityLogin.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        ((ActivityBase) getContext()).finish();
    }

    @Override
    public void logOutFail(String message) {
        Toasty.error(getContext(), message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
    }
}
