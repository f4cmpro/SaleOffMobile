package bkdn.itt.datn.saleoff.view.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.model.ShopInfo;
import bkdn.itt.datn.saleoff.presenter.PresenterMyShop;
import bkdn.itt.datn.saleoff.utils.CustomPaginationScrollListener;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.activity.ActivityShopCreate;
import bkdn.itt.datn.saleoff.view.activity.ActivityShopList;
import bkdn.itt.datn.saleoff.view.adapter.AdapterMyShop;
import bkdn.itt.datn.saleoff.view.iface.IFragmentMyShop;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class FragmentMyShop extends FragmentBase<PresenterMyShop> implements IFragmentMyShop{

    /*
    * Widgets
    * */
    @BindView(R.id.recyclerMyShop)
    RecyclerView mRecyclerShop;
    @BindView(R.id.llMyShopCreateShop)
    LinearLayout mLlCreateShop;
    @BindView(R.id.viewMyPostNotFoundPost)
    View mViewNotFoundItem;
    @BindView(R.id.swipeRefreshMyShop)
    SwipeRefreshLayout mSwipeRefreshMyShop;
    /*
    * Fields
    * */
    private View mRootView;
    private AdapterMyShop mAdapterShop;
    private boolean mIsMyShop;
    private boolean mIsRefresh;
    private boolean mIsLoadMore;

    public static FragmentMyShop newInstance(boolean isMyShop) {
        FragmentMyShop fragmentMyShop = new FragmentMyShop();
        Bundle bundle = new Bundle();
        bundle.putBoolean(GlobalDefine.KEY_BUNDLE_IS_MY_SHOP, isMyShop);
        fragmentMyShop.setArguments(bundle);
        return fragmentMyShop;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.fragment_my_shop, container, false);
            ButterKnife.bind(this, mRootView);
            initValues();
            initViews();
            initActions();
        }
        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initValues() {
        mIsMyShop = getArguments().getBoolean(GlobalDefine.KEY_BUNDLE_IS_MY_SHOP);
        mIsRefresh = true;
        if (mIsMyShop) {
            getPresenter(this).getOwnerShop();
        } else {
            getPresenter(this).getWorkingShop();
        }

    }

    private void initViews() {
        if (mIsMyShop) {
            mLlCreateShop.setVisibility(View.VISIBLE);
        } else {
            mLlCreateShop.setVisibility(View.GONE);
        }
        mAdapterShop = new AdapterMyShop(getContext());
        RecyclerViewUtils.Create().setUpVertical(getContext(), mRecyclerShop).setAdapter(mAdapterShop);
    }

    private void initActions() {
        mSwipeRefreshMyShop.setOnRefreshListener(() -> {
            mIsRefresh = true;
            if (mIsMyShop) {
                getPresenter(FragmentMyShop.this).getOwnerShop();
            } else {
                getPresenter(FragmentMyShop.this).getWorkingShop();
            }
        });
        mLlCreateShop.setOnClickListener(v -> getActivity()
                .startActivityForResult(new Intent(getContext(), ActivityShopCreate.class),
                        ConstantDefine.CREATE_SHOP_REQUEST));
        mRecyclerShop.addOnScrollListener(new CustomPaginationScrollListener(RecyclerViewUtils.
                Create().getLinearLayoutManager(mRecyclerShop)) {
            @Override
            protected void loadMoreItems() {
                mIsLoadMore = true;
                if (mIsMyShop) {
                    // getPresenter(FragmentMyShop.this).getOwnerShop();
                } else {
                    //getPresenter(FragmentMyShop.this).getWorkingShop();
                }
            }

            @Override
            public boolean isLoading() {
                return mIsLoadMore;
            }

            @Override
            protected void enableRefreshLayout(boolean isEnable) {

            }

        });
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());

    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
        mSwipeRefreshMyShop.setRefreshing(false);

    }

    @Override
    public void getOwnerShopSuccess(List<ShopInfo> shops) {
        if (shops != null && !shops.isEmpty()) {
            if (mIsRefresh) {
                mIsRefresh = false;
                mAdapterShop.refreshShopInfo(shops);
            } else if (mIsLoadMore) {
                mIsLoadMore = false;
                mAdapterShop.loadMoreShops(shops);
            }
            mRecyclerShop.setVisibility(View.VISIBLE);
            mViewNotFoundItem.setVisibility(View.GONE);
        } else {
            mRecyclerShop.setVisibility(View.GONE);
            ((TextView) mViewNotFoundItem.findViewById(R.id.tvLayoutNotFoundItem))
                    .setText(getString(R.string.warning_not_have_my_shop));
            mViewNotFoundItem.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void getWorkingShopSuccess(List<ShopInfo> shops) {
        if (shops != null && !shops.isEmpty()) {
            if (mIsRefresh) {
                mIsRefresh = false;
                mAdapterShop.refreshShopInfo(shops);
            } else {
                mIsLoadMore = false;
                mAdapterShop.loadMoreShops(shops);
            }
            mRecyclerShop.setVisibility(View.VISIBLE);
            mViewNotFoundItem.setVisibility(View.GONE);
        } else {
            mRecyclerShop.setVisibility(View.GONE);
            ((TextView) mViewNotFoundItem.findViewById(R.id.tvLayoutNotFoundItem))
                    .setText(getString(R.string.warning_not_have_working_shop));
            mViewNotFoundItem.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void getErrorMessage(String message) {
        Toasty.error(getContext(), message, Toast.LENGTH_SHORT).show();
    }

}
