package bkdn.itt.datn.saleoff.view.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.local.DBHelper;
import bkdn.itt.datn.saleoff.model.model.Category;
import bkdn.itt.datn.saleoff.model.model.Event;
import bkdn.itt.datn.saleoff.model.model.Post;
import bkdn.itt.datn.saleoff.model.model.User;
import bkdn.itt.datn.saleoff.model.model.UserInfo;
import bkdn.itt.datn.saleoff.presenter.PresenterNewsFeed;
import bkdn.itt.datn.saleoff.utils.CustomPaginationScrollListener;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.SharedPreferencesUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.activity.ActivityComment;
import bkdn.itt.datn.saleoff.view.activity.ActivityEventDetail;
import bkdn.itt.datn.saleoff.view.activity.ActivityPostDetail;
import bkdn.itt.datn.saleoff.view.activity.ActivityShare;
import bkdn.itt.datn.saleoff.view.adapter.AdapterCategory;
import bkdn.itt.datn.saleoff.view.adapter.AdapterNewsFeed;
import bkdn.itt.datn.saleoff.view.iface.IFragmentNewsFeed;
import bkdn.itt.datn.saleoff.view.iface.OnClickLikeListener;
import bkdn.itt.datn.saleoff.view.iface.OnClickUnLikeListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class FragmentNewsFeed extends FragmentBase<PresenterNewsFeed> implements IFragmentNewsFeed,
        AdapterNewsFeed.OnClickPostItemListener, AdapterCategory.OnClickCatItemListener {
    /*
    * Widgets
    * */
    @BindView(R.id.recyclerHomePosts)
    RecyclerView mRecyclerNewsFeed;
    @BindView(R.id.swipeRefreshNewsFeed)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.recyclerNewsCat)
    RecyclerView mRecyclerCat;
    @BindView(R.id.imvNewsFeedBackTop)
    ImageView mImvBackTop;
    @BindView(R.id.nestedScrollViewNewsFeed)
    NestedScrollView mNestedScrollView;
    @BindView(R.id.viewNewsFeedEmpty)
    View mViewPostEmpty;

    /*
    * Fields
    * */
    private static final int CARE_ID = 0;
    private User mUser;
    private View mRootView;
    private DBHelper mDBHelper;
    private UserInfo mUserInfo;
    private List<Category> mCategories;
    private AdapterNewsFeed mAdapterNewsFeed;
    private AdapterCategory mAdapterCategory;
    private OnClickLikeListener mOnClickLikeListener;
    private OnClickUnLikeListener mOnClickUnLikeListener;
    private int mLastId;
    private int mYourCareId;
    private int mCategoryId = 1;
    private boolean isRefresh;
    private boolean isLoadMore;
    private boolean mIsFollow;
    private boolean mIsYourCare;


    public static FragmentNewsFeed newInstance() {
        return new FragmentNewsFeed();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.fragment_news_feed, container, false);
            ButterKnife.bind(this, mRootView);
            initValues();
            initViews();
            initActions();
        }
        return mRootView;
    }

    private void initValues() {
        mLastId = ConstantDefine.DEFAULT_LAST_ID;
        mDBHelper = new DBHelper(getContext());
        if (mDBHelper.isCategoryExist()) {
            mCategories = mDBHelper.getAllCategory();
            setYourCare();
        } else {
            getPresenter(this).getAllCategory();
        }
        mUser = SharedPreferencesUtils.getInstance(getContext()).getUserProfile();
        if (mUser != null) {
            mUserInfo = new UserInfo();
            mUserInfo.setId(mUser.getId());
            mUserInfo.setUserName(mUser.getUsername());
            mUserInfo.setAvatar(mUser.getAvatar());
        }

    }

    private void initViews() {
        if (mCategories != null) {
            mAdapterCategory = new AdapterCategory(getContext(), mCategories);
            mAdapterCategory.setListener(this);
            RecyclerViewUtils.Create().setUpHorizontal(getContext(), mRecyclerCat)
                    .setAdapter(mAdapterCategory);
            mYourCareId = mCategories.size();
        }
        mAdapterNewsFeed = new AdapterNewsFeed(getContext(), ConstantDefine.PUBLIC);
        mAdapterNewsFeed.setListener(this);
        RecyclerViewUtils.Create().setUpVertical(getContext(), mRecyclerNewsFeed)
                .setAdapter(mAdapterNewsFeed);
        Utils.showLoadingDialog(getContext());
        getPresenter(this).getNewsFeedFollowing(true, mCategoryId, mLastId);
    }

    private void initActions() {
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            isRefresh = true;
            mLastId = ConstantDefine.DEFAULT_LAST_ID;
            if (mCategoryId == CARE_ID) {
                getPresenter(FragmentNewsFeed.this).getCareFollow(false, mLastId);
            } else {
                getPresenter(FragmentNewsFeed.this).getNewsFeedFollowing(false, mCategoryId, mLastId);
            }
        });
        mRecyclerNewsFeed.addOnScrollListener(new CustomPaginationScrollListener((LinearLayoutManager)
                mRecyclerNewsFeed.getLayoutManager()) {
            @Override
            protected void loadMoreItems() {
                isLoadMore = true;
                if (mIsFollow) {
                    if (mCategoryId == CARE_ID) {
                        getPresenter(FragmentNewsFeed.this).getCareFollow(false, mLastId);
                    } else {
                        getPresenter(FragmentNewsFeed.this).getNewsFeedFollowing(false, mCategoryId, mLastId);
                    }
                } else {
                    if (mCategoryId == CARE_ID) {
                        getPresenter(FragmentNewsFeed.this).getCareNotFollow(mLastId);
                    } else {
                        getPresenter(FragmentNewsFeed.this).getNewsFeedNotFollowing(mCategoryId, mLastId);
                    }
                }

            }

            @Override
            public boolean isLoading() {
                return isLoadMore;
            }

            @Override
            protected void enableRefreshLayout(boolean isEnable) {

            }

        });
        mImvBackTop.setOnClickListener(v -> mNestedScrollView.smoothScrollTo(0, 0));
    }


    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        mSwipeRefreshLayout.setRefreshing(false);
        Utils.hideLoadingDialog();
    }

    @Override
    public void getFollowingNewsFeed(List<Post> posts, int lastId) {
        if (isRefresh) {
            isRefresh = false;
            mAdapterNewsFeed.refreshData(posts);
        } else {
            isLoadMore = false;
            mAdapterNewsFeed.addData(posts);
        }
        mIsFollow = true;
        mLastId = lastId;
        isPostEmpty();
    }

    @Override
    public void getFollowingNewsFeedEmpty() {
        isLoadMore = false;
        mIsFollow = false;
        mLastId = ConstantDefine.DEFAULT_LAST_ID;
        getPresenter(this).getNewsFeedNotFollowing(mCategoryId, mLastId);
    }


    @Override
    public void getNotFollowingNewsFeed(List<Post> posts, int lastId) {
        if (isRefresh) {
            isRefresh = false;
            mAdapterNewsFeed.refreshData(posts);
        } else {
            isLoadMore = false;
            mAdapterNewsFeed.addData(posts);
        }
        mLastId = lastId;
        isPostEmpty();
    }

    @Override
    public void getNotFollowingNewsFeedEmpty() {
        isLoadMore = false;
    }

    private void isPostEmpty() {
        if (mAdapterNewsFeed.getItemCount() == 0) {
            mRecyclerNewsFeed.setVisibility(View.GONE);
            mImvBackTop.setVisibility(View.GONE);
            mViewPostEmpty.setVisibility(View.VISIBLE);
        } else {
            mRecyclerNewsFeed.setVisibility(View.VISIBLE);
            mImvBackTop.setVisibility(View.VISIBLE);
            mViewPostEmpty.setVisibility(View.GONE);
        }
    }

    @Override
    public void getError(String errorMessage) {
        isLoadMore = false;
        isRefresh = false;
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getCategories(List<Category> categories) {
        mDBHelper.setCategoryList(categories);
        mCategories = mDBHelper.getAllCategory();
        setYourCare();
        initViews();
    }

    @Override
    public void getCareFollow(List<Post> posts, int lastId) {
        if (isRefresh) {
            isRefresh = false;
            mAdapterNewsFeed.refreshData(posts);
        } else {
            isLoadMore = false;
            mAdapterNewsFeed.addData(posts);
        }
        mIsFollow = true;
        mLastId = lastId;
        isPostEmpty();
    }

    @Override
    public void getCareFollowEmpty() {
        isLoadMore = false;
        isRefresh = false;
        mIsFollow = false;
        mLastId = ConstantDefine.DEFAULT_LAST_ID;
        getPresenter(this).getCareNotFollow(mLastId);
    }

    @Override
    public void getCareNotFollow(List<Post> posts, int lastId) {
        isLoadMore = false;
        mAdapterNewsFeed.addData(posts);
        mLastId = lastId;
        isPostEmpty();
    }

    @Override
    public void getCareNotFollowEmpty() {
        isLoadMore = false;
    }

    @Override
    public void reportSuccess() {
        Toasty.success(getContext(), getString(R.string.report_success), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void likePostSuccess() {
        mOnClickLikeListener.success();
    }

    @Override
    public void likePostFail(String failMessage) {
        Toasty.error(getContext(), failMessage, Toast.LENGTH_SHORT).show();
        mOnClickLikeListener.error();
    }

    @Override
    public void unLikePostSuccess() {
        mOnClickUnLikeListener.success();
    }

    @Override
    public void unLikePostFail(String failMessage) {
        mOnClickUnLikeListener.error();

    }

    @Override
    public void onClickPostItem(Post post) {
        Intent intent = new Intent(getContext(), ActivityPostDetail.class);
        intent.putExtra(GlobalDefine.KEY_INTENT_POST, post);
        startActivity(intent);
    }

    @Override
    public void onClickComment(int postId) {
        Intent intent = new Intent(getContext(), ActivityComment.class);
        intent.putExtra(GlobalDefine.KEY_INTENT_USER_INFO, mUserInfo);
        intent.putExtra(GlobalDefine.KEY_INTENT_POST_ID, postId);
        startActivity(intent);
    }

    @Override
    public void onClickLike(int postId, OnClickLikeListener likeListener) {
        mOnClickLikeListener = likeListener;
        getPresenter(this).likeNewsPost(postId);
    }

    @Override
    public void onClickUnLike(int postId, OnClickUnLikeListener unLikeListener) {
        mOnClickUnLikeListener = unLikeListener;
        getPresenter(this).unLikeNewsPost(postId);
    }

    @Override
    public void onCLickShare(Post sharedPost) {
        Intent intent = new Intent(getContext(), ActivityShare.class);
        intent.putExtra(GlobalDefine.KEY_INTENT_POST, sharedPost);
        startActivity(intent);
    }

    @Override
    public void onClickEventDetail(Event event) {
        Intent intent = new Intent(getContext(), ActivityEventDetail.class);
        intent.putExtra(GlobalDefine.KEY_INTENT_EVENT, event);
        startActivity(intent);
    }

    @Override
    public void onClickReport(int postId, int reasonId) {
        getPresenter(this).reportPost(postId, reasonId);
    }

    @Override
    public void onClickCatItem(int catId) {
        mCategoryId = catId;
        mLastId = ConstantDefine.DEFAULT_LAST_ID;
        mAdapterNewsFeed.clearData();
        if (mCategoryId == CARE_ID) {
            getPresenter(this).getCareFollow(true, mLastId);
        } else {
            getPresenter(this).getNewsFeedFollowing(true, mCategoryId, mLastId);
        }
    }

    private void setYourCare() {
        //set your care
        Category yourCare = new Category();
        yourCare.setName("Bạn quan tâm");
        yourCare.setImvRes(R.drawable.your_care);
        mCategories.add(0, yourCare);
    }
}
