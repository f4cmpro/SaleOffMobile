package bkdn.itt.datn.saleoff.view.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.local.DBHelper;
import bkdn.itt.datn.saleoff.model.model.Notification;
import bkdn.itt.datn.saleoff.model.model.NotificationTarget;
import bkdn.itt.datn.saleoff.presenter.PresenterNotification;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.activity.ActivityComment;
import bkdn.itt.datn.saleoff.view.activity.ActivityEventDetail;
import bkdn.itt.datn.saleoff.view.activity.ActivityPostDetail;
import bkdn.itt.datn.saleoff.view.activity.ActivityShopPrivate;
import bkdn.itt.datn.saleoff.view.activity.ActivityShopPublic;
import bkdn.itt.datn.saleoff.view.activity.ActivityUserPublic;
import bkdn.itt.datn.saleoff.view.adapter.AdapterNotification;
import bkdn.itt.datn.saleoff.view.iface.IFragmentNotification;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class FragmentNotification extends FragmentBase<PresenterNotification> implements
        IFragmentNotification, AdapterNotification.OnClickNotificationItemListener {
    /*
        * Widgets
        * */
    @BindView(R.id.recyclerNotification)
    RecyclerView mRecyclerNotification;
    @BindView(R.id.swipeRefreshNotification)
    SwipeRefreshLayout mRefreshLayout;
    /*
    * Fields
    * */
    private static final String TARGET_POST = "post";
    private static final String TARGET_SHOP = "shop";
    private static final String TARGET_USER = "user";
    private static final String TARGET_EVENT = "event";
    private static final String TARGET_COMMENT = "comment";
    private View mRootView;
    private AdapterNotification mAdapterNotification;
    private NotificationTarget mTarget;
    private int mPosition;
    private DBHelper mDBHelper;

    public static FragmentNotification newInstance() {
        FragmentNotification fragmentNotification = new FragmentNotification();
        return fragmentNotification;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.fragment_notification, container, false);
            ButterKnife.bind(this, mRootView);
            mAdapterNotification = new AdapterNotification(getContext(), this);
            RecyclerViewUtils.Create().setUpVertical(getContext(), mRecyclerNotification)
                    .setAdapter(mAdapterNotification);
            getPresenter(this).getNotifications(true);
            mRefreshLayout.setOnRefreshListener(() -> {
                getPresenter(this).getNotifications(false);
            });
        }
        return mRootView;
    }


    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
        mRefreshLayout.setRefreshing(false);
    }

    @Override
    public void getNotificationsSuccess(List<Notification> notifications) {
        mAdapterNotification.refreshListNotification(notifications);
    }

    @Override
    public void deleteNotificationSuccess() {
        mAdapterNotification.removeNotification(mPosition);
    }

    @Override
    public void getError(String errorMessage) {
        Toasty.warning(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
        mRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onClickNotificationItem(int position, String notificationId, NotificationTarget target) {
        //getPresenter(this).deleteNotification(notificationId);
        mTarget = target;
        mPosition = position;
        if (mTarget.getType().equals(TARGET_POST)) {
            Intent intent = new Intent(getContext(), ActivityPostDetail.class);
            intent.putExtra(GlobalDefine.KEY_INTENT_POST_ID, mTarget.getId());
            startActivity(intent);
        } else if (mTarget.getType().equals(TARGET_SHOP)) {
            Intent intent = new Intent(getContext(), ActivityShopPrivate.class);
            intent.putExtra(GlobalDefine.KEY_INTENT_SHOP_ID, mTarget.getId());
            startActivity(intent);
        } else if (mTarget.getType().equals(TARGET_USER)) {
            Intent intent = new Intent(getContext(), ActivityUserPublic.class);
            intent.putExtra(GlobalDefine.KEY_INTENT_USER_ID, mTarget.getId());
            startActivity(intent);
        } else if (mTarget.getType().equals(TARGET_COMMENT)) {
            Intent intent = new Intent(getContext(), ActivityComment.class);
            intent.putExtra(GlobalDefine.KEY_INTENT_POST_ID, mTarget.getId());
            startActivity(intent);
        } else {
            Intent intent = new Intent(getContext(), ActivityEventDetail.class);
            intent.putExtra(GlobalDefine.KEY_INTENT_EVENT_ID, mTarget.getId());
            startActivity(intent);
        }
    }
}
