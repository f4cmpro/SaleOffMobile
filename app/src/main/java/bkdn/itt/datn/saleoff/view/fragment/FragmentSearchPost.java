package bkdn.itt.datn.saleoff.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.local.DBHelper;
import bkdn.itt.datn.saleoff.model.model.Category;
import bkdn.itt.datn.saleoff.model.model.Post;
import bkdn.itt.datn.saleoff.model.model.SubCategory;
import bkdn.itt.datn.saleoff.presenter.PresenterSearchPost;
import bkdn.itt.datn.saleoff.utils.CustomPaginationScrollListener;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.activity.ActivityPostDetail;
import bkdn.itt.datn.saleoff.view.adapter.AdapterSearchPost;
import bkdn.itt.datn.saleoff.view.iface.IFragmentSearchPost;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

/**
 * Created by TuDLT on 4/26/2018.
 */
public class FragmentSearchPost extends FragmentBase<PresenterSearchPost>
        implements IFragmentSearchPost, AdapterSearchPost.OnClickItemSearchPostListener {
    /*
    * Widgets
    * */
    @BindView(R.id.spinnerSearchPostType)
    Spinner mSpinnerType;
    @BindView(R.id.spinnerSearchPostFollow)
    Spinner mSpinnerFollow;
    @BindView(R.id.spinnerSearchPostCat)
    Spinner mSpinnerCat;
    @BindView(R.id.spinnerSearchPostSubCat)
    Spinner mSpinnerSubCat;
    @BindView(R.id.recyclerSearchPost)
    RecyclerView mRecyclerSearchPost;
    @BindView(R.id.swipeRefreshSearchPost)
    SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.llSearchPostSubCat)
    LinearLayout mLlSubCat;

    /*
    * Fields
    * */
    private View mRootView;
    private String mTextKey;
    private String mType;
    private boolean mIsFollow;
    private int mCatId;
    private int mSubCatId;
    private List<String> mTypes;
    private List<String> mFollows;
    private List<Category> mCategories;
    private List<SubCategory> mSubCategoriesById;
    private AdapterSearchPost mAdapterSearchPost;
    private boolean mIsRefresh;
    private boolean mIsLoadMore;
    private int mCurrentPage;
    private DBHelper mDBHelper;

    public static FragmentSearchPost newInstance() {
        return new FragmentSearchPost();
    }

    public void searchPost(String inputKey) {
        mTextKey = inputKey;
        if (mTextKey == null || mTextKey.isEmpty()) {
            Toasty.warning(getContext(), getString(R.string.error_not_input_search_key), Toast.LENGTH_SHORT).show();
            return;
        }
        mCurrentPage = ConstantDefine.FIRST_PAGE;
        getPresenter(this).searchPost(mCurrentPage, mTextKey, mType, mIsFollow, mCatId, mSubCatId);
        mAdapterSearchPost.clearPostList();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_search_post, container, false);
        ButterKnife.bind(this, mRootView);
        initValues();
        initViews();
        initActions();
        return mRootView;
    }

    private void initValues() {
        mDBHelper = new DBHelper(getContext());
        if (mDBHelper.isCategoryExist()) {
            mCategories = mDBHelper.getAllCategory();
        }else{
            getPresenter(this).getCategories();
        }
        if(!mDBHelper.isSubCategoryExist()){
            getPresenter(this).getSubCategories();
        }
        mTypes = Arrays.asList(getResources().getStringArray(R.array.type_search_post));
        mFollows = Arrays.asList(getResources().getStringArray(R.array.follow_search_post));
        mType = "shop";
        mIsFollow = true;
        mCatId = 1;
        mCurrentPage = ConstantDefine.FIRST_PAGE;
    }

    private void initViews() {
        if (mTypes != null) {
            ArrayAdapter<String> typesAdapter = new ArrayAdapter<>(getContext(),
                    android.R.layout.simple_spinner_item, mTypes);
            typesAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            mSpinnerType.setAdapter(typesAdapter);
        }
        if (mFollows != null) {
            ArrayAdapter<String> followsAdapter = new ArrayAdapter<>(getContext(),
                    android.R.layout.simple_spinner_item, mFollows);
            followsAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            mSpinnerFollow.setAdapter(followsAdapter);
        }
        initCategorySpinner();
        mAdapterSearchPost = new AdapterSearchPost(getContext(), this);
        RecyclerViewUtils.Create().setUpVertical(getContext(), mRecyclerSearchPost)
                .setAdapter(mAdapterSearchPost);
    }

    private void initCategorySpinner() {
        if (mCategories != null) {
            List<String> categoryNames = new ArrayList<>();
            for (Category item : mCategories) {
                categoryNames.add(item.getName());
            }
            ArrayAdapter<String> categoriesAdapter = new ArrayAdapter<>(getContext(),
                    android.R.layout.simple_spinner_item, categoryNames);
            categoriesAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            mSpinnerCat.setAdapter(categoriesAdapter);
        }
    }

    private void initActions() {
        mSpinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mType = position == 0 ? "shop" : "user";
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mSpinnerFollow.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mIsFollow = position == 0 ? true : false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mSpinnerCat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mCatId = mCategories.get(position).getId();
                if (mCatId != 0) {
                    initSubCategorySpinner();
                } else {
                    mLlSubCat.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mSpinnerSubCat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mSubCatId = mSubCategoriesById.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mRecyclerSearchPost.addOnScrollListener(new CustomPaginationScrollListener(RecyclerViewUtils
                .Create().getLinearLayoutManager(mRecyclerSearchPost)) {
            @Override
            protected void loadMoreItems() {
                mIsLoadMore = true;
                mCurrentPage++;
                getPresenter(FragmentSearchPost.this).searchPost(mCurrentPage, mTextKey, mType,
                        mIsFollow, mCatId, mSubCatId);
            }

            @Override
            protected boolean isLoading() {
                return mIsLoadMore;
            }

            @Override
            protected void enableRefreshLayout(boolean isEnable) {

            }
        });
        mRefreshLayout.setOnRefreshListener(() -> {
            getPresenter(this).
                    searchPost(ConstantDefine.FIRST_PAGE, mTextKey, mType, mIsFollow, mCatId, mSubCatId);
        });
    }

    private void initSubCategorySpinner() {
        mSubCategoriesById = mDBHelper.getListSubCatByCat(mCatId);
        if (!mSubCategoriesById.isEmpty()) {
            List<String> subCategoryNames = new ArrayList<>();
            for (SubCategory item : mSubCategoriesById) {
                subCategoryNames.add(item.getName());
            }
            ArrayAdapter<String> subCategoriesAdapter = new ArrayAdapter<>(getContext(),
                    android.R.layout.simple_spinner_item, subCategoryNames);
            subCategoriesAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            mSpinnerSubCat.setAdapter(subCategoriesAdapter);
            mLlSubCat.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
        mRefreshLayout.setRefreshing(false);
    }


    @Override
    public void searchPostSuccess(List<Post> posts) {
        if (mIsRefresh) {
            mIsRefresh = false;
            mCurrentPage = ConstantDefine.FIRST_PAGE;
            if (posts.isEmpty()) {
                Toasty.info(getContext(), getString(R.string.error_not_found_search),
                        Toast.LENGTH_SHORT).show();
            } else {
                mAdapterSearchPost.refreshPostList(posts);
            }
        } else {
            if (posts.isEmpty()) {
                Toasty.info(getContext(), getString(R.string.error_not_found_search),
                        Toast.LENGTH_SHORT).show();
                mCurrentPage--;
                mRecyclerSearchPost.stopScroll();
            } else {
                mAdapterSearchPost.addPostList(posts);
            }
            mIsLoadMore = false;
        }
    }

    @Override
    public void getError(String errorMessage) {
        if (mIsRefresh) {
            mIsRefresh = false;
        } else if (mIsLoadMore) {
            mIsLoadMore = false;
            mCurrentPage--;
        }
        Toasty.warning(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getCategoriesSuccess(List<Category> categories) {
        mDBHelper.setCategoryList(categories);
        mCategories = mDBHelper.getAllCategory();
        initViews();
    }

    @Override
    public void getSubCategoriesSuccess(List<SubCategory> listSubCategory) {
        mDBHelper.setSubCategoryList(listSubCategory);
    }

    @Override
    public void onClickItem(Post post) {
        Intent intent = new Intent(getContext(), ActivityPostDetail.class);
        intent.putExtra(GlobalDefine.KEY_INTENT_POST_ID, post.getPostId());
        startActivity(intent);
    }
}