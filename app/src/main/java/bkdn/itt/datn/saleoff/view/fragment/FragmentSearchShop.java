package bkdn.itt.datn.saleoff.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.local.DBHelper;
import bkdn.itt.datn.saleoff.model.model.Category;
import bkdn.itt.datn.saleoff.model.model.Shop;
import bkdn.itt.datn.saleoff.model.model.SubCategory;
import bkdn.itt.datn.saleoff.presenter.PresenterSearchShop;
import bkdn.itt.datn.saleoff.utils.CustomPaginationScrollListener;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.activity.ActivityPostDetail;
import bkdn.itt.datn.saleoff.view.activity.ActivityShopPublic;
import bkdn.itt.datn.saleoff.view.adapter.AdapterSearchPost;
import bkdn.itt.datn.saleoff.view.adapter.AdapterSearchShop;
import bkdn.itt.datn.saleoff.view.iface.IFragmentSearchShop;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

/**
 * Created by TuDLT on 4/26/2018.
 */
public class FragmentSearchShop extends FragmentBase<PresenterSearchShop>
        implements IFragmentSearchShop, AdapterSearchShop.OnClickItemSearchShopListener {

    public FragmentSearchShop() {
    }

    public static FragmentSearchShop newInstance() {
        return new FragmentSearchShop();
    }

    /*
   * Widgets
   * */

    @BindView(R.id.spinnerSearchShopFollow)
    Spinner mSpinnerFollow;
    @BindView(R.id.spinnerSearchShopCat)
    Spinner mSpinnerCat;
    @BindView(R.id.recyclerSearchShop)
    RecyclerView mRecyclerSearchShop;
    @BindView(R.id.swipeRefreshSearchShop)
    SwipeRefreshLayout mRefreshLayout;

    /*
    * Fields
    * */
    private View mRootView;
    private String mTextKey;
    private boolean mIsFollow;
    private int mCatId;
    private List<String> mFollows;
    private List<Category> mCategories;
    private AdapterSearchShop mAdapterSearchShop;
    private boolean mIsRefresh;
    private boolean mIsLoadMore;
    private int mCurrentPage;
    private DBHelper mDBHelper;
    private int mFollowPosition;
    private int mUnFollowPosition;

    public void searchShop(String inputKey) {
        mTextKey = inputKey;
        if (mTextKey == null || mTextKey.isEmpty()) {
            Toasty.warning(getContext(), getString(R.string.error_not_input_search_key), Toast.LENGTH_SHORT).show();
            return;
        }
        mCurrentPage = ConstantDefine.FIRST_PAGE;
        getPresenter(this).searchShop(mCurrentPage, mTextKey, mIsFollow, mCatId);
        mAdapterSearchShop.clearShopList();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_search_shop, container, false);
        ButterKnife.bind(this, mRootView);
        initValues();
        initViews();
        initActions();
        return mRootView;
    }

    private void initValues() {
        mDBHelper = new DBHelper(getContext());
        if (mDBHelper.isCategoryExist()) {
            mCategories = mDBHelper.getAllCategory();
        }else{
            getPresenter(this).getCategories();
        }
        mFollows = Arrays.asList(getResources().getStringArray(R.array.follow_search_post));
        mIsFollow = true;
        mCatId = 1;
        mCurrentPage = ConstantDefine.FIRST_PAGE;
    }

    private void initViews() {
        if (mFollows != null) {
            ArrayAdapter<String> followsAdapter = new ArrayAdapter<>(getContext(),
                    android.R.layout.simple_spinner_item, mFollows);
            followsAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            mSpinnerFollow.setAdapter(followsAdapter);
        }
        initCategorySpinner();
        mAdapterSearchShop = new AdapterSearchShop(getContext(), this);
        RecyclerViewUtils.Create().setUpVertical(getContext(), mRecyclerSearchShop)
                .setAdapter(mAdapterSearchShop);
    }

    private void initCategorySpinner() {
        if (mCategories != null) {
            List<String> categoryNames = new ArrayList<>();
            for (Category item : mCategories) {
                categoryNames.add(item.getName());
            }
            ArrayAdapter<String> categoriesAdapter = new ArrayAdapter<>(getContext(),
                    android.R.layout.simple_spinner_item, categoryNames);
            categoriesAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            mSpinnerCat.setAdapter(categoriesAdapter);
        }
    }

    private void initActions() {
        mSpinnerFollow.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mIsFollow = position == 0 ? true : false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mSpinnerCat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mCatId = mCategories.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mRecyclerSearchShop.addOnScrollListener(new CustomPaginationScrollListener(RecyclerViewUtils
                .Create().getLinearLayoutManager(mRecyclerSearchShop)) {
            @Override
            protected void loadMoreItems() {
                mIsLoadMore = true;
                mCurrentPage++;
                getPresenter(FragmentSearchShop.this).searchShop(mCurrentPage, mTextKey,
                        mIsFollow, mCatId);
            }

            @Override
            protected boolean isLoading() {
                return mIsLoadMore;
            }

            @Override
            protected void enableRefreshLayout(boolean isEnable) {

            }
        });
        mRefreshLayout.setOnRefreshListener(() -> getPresenter(this).
                searchShop(ConstantDefine.FIRST_PAGE, mTextKey, mIsFollow, mCatId));
    }

    @Override
    public void onClickItem(Shop shop) {
        Intent intent = new Intent(getContext(), ActivityShopPublic.class);
        intent.putExtra(GlobalDefine.KEY_INTENT_SHOP, shop);
        startActivity(intent);
    }

    @Override
    public void onClickFollow(int shopId, int position) {
        mFollowPosition = position;
        getPresenter(this).followShop(shopId, 0);
    }

    @Override
    public void onClickUnFollow(int shopId, int position) {
        mUnFollowPosition = position;
        getPresenter(this).unFollowShop(shopId);
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
        mRefreshLayout.setRefreshing(false);
    }


    @Override
    public void getSearchShopSuccess(List<Shop> shops) {
        if (!shops.isEmpty()) {
            for (Shop item : shops) {
                item.setIsFollow(mIsFollow ? 1 : 0);
            }
            if (mIsRefresh) {
                mIsRefresh = false;
                mCurrentPage = ConstantDefine.FIRST_PAGE;
                mAdapterSearchShop.refreshShopList(shops);
            } else {
                mIsLoadMore = false;
                mAdapterSearchShop.addShopList(shops);
            }
        } else {
            Toasty.info(getContext(), getString(R.string.error_not_found_search),
                    Toast.LENGTH_SHORT).show();
            if (mIsLoadMore) {
                mCurrentPage--;
                mIsLoadMore = false;
            } else if (mIsRefresh) {
                mIsRefresh = false;
                mCurrentPage = ConstantDefine.FIRST_PAGE;
            }
        }

    }

    @Override
    public void getError(String errorMessage) {
        if (mIsRefresh) {
            mIsRefresh = false;
        } else if (mIsLoadMore) {
            mIsLoadMore = false;
            mCurrentPage--;
        }
        Toasty.warning(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getCategoriesSuccess(List<Category> categories) {
        mDBHelper.setCategoryList(categories);
        mCategories = mDBHelper.getAllCategory();
        initCategorySpinner();
    }

    @Override
    public void followShopFail() {
        mAdapterSearchShop.setFollowFail(mFollowPosition);
    }

    @Override
    public void unFollowShopFail() {
        mAdapterSearchShop.setUnFollowFail(mUnFollowPosition);
    }

}
