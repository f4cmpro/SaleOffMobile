package bkdn.itt.datn.saleoff.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.local.DBHelper;
import bkdn.itt.datn.saleoff.model.model.User;
import bkdn.itt.datn.saleoff.presenter.PresenterSearchUser;
import bkdn.itt.datn.saleoff.utils.CustomPaginationScrollListener;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.activity.ActivityShopPublic;
import bkdn.itt.datn.saleoff.view.activity.ActivityUserPublic;
import bkdn.itt.datn.saleoff.view.adapter.AdapterSearchShop;
import bkdn.itt.datn.saleoff.view.adapter.AdapterSearchUser;
import bkdn.itt.datn.saleoff.view.iface.IFragmentSearchUser;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

/**
 * Created by TuDLT on 4/26/2018.
 */
public class FragmentSearchUser extends FragmentBase<PresenterSearchUser> implements
        AdapterSearchUser.OnClickItemSearchUserListener, IFragmentSearchUser {
       /*
   * Widgets
   * */

    @BindView(R.id.spinnerSearchUserFollow)
    Spinner mSpinnerFollow;
    @BindView(R.id.recyclerSearchUser)
    RecyclerView mRecyclerSearchUser;
    @BindView(R.id.swipeRefreshSearchUser)
    SwipeRefreshLayout mRefreshLayout;

    /*
    * Fields
    * */
    private View mRootView;
    private String mTextKey;
    private boolean mIsFollow;
    private List<String> mFollows;
    private AdapterSearchUser mAdapterSearchUser;
    private boolean mIsRefresh;
    private boolean mIsLoadMore;
    private int mCurrentPage;
    private DBHelper mDBHelper;
    private int mFollowPosition;
    private int mUnFollowPosition;


    public static FragmentSearchUser newInstance() {
        return new FragmentSearchUser();
    }

    public void searchUser(String inputKey) {
        mTextKey = inputKey;
        if (mTextKey == null || mTextKey.isEmpty()) {
            Toasty.warning(getContext(), getString(R.string.error_not_input_search_key), Toast.LENGTH_SHORT).show();
            return;
        }
        mCurrentPage = ConstantDefine.FIRST_PAGE;
        getPresenter(this).searchUser(mCurrentPage, mTextKey, mIsFollow);
        mAdapterSearchUser.clearUserList();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_search_user, container, false);
        ButterKnife.bind(this, mRootView);
        initValues();
        initViews();
        initActions();
        return mRootView;
    }

    private void initValues() {
        mDBHelper = new DBHelper(getContext());
        mFollows = Arrays.asList(getResources().getStringArray(R.array.follow_search_post));
        mIsFollow = true;
        mCurrentPage = ConstantDefine.FIRST_PAGE;
    }

    private void initViews() {
        if (mFollows != null) {
            ArrayAdapter<String> followsAdapter = new ArrayAdapter<>(getContext(),
                    android.R.layout.simple_spinner_item, mFollows);
            followsAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            mSpinnerFollow.setAdapter(followsAdapter);
        }
        mAdapterSearchUser = new AdapterSearchUser(getContext(), this);
        RecyclerViewUtils.Create().setUpVertical(getContext(), mRecyclerSearchUser)
                .setAdapter(mAdapterSearchUser);
    }

    private void initActions() {
        mSpinnerFollow.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mIsFollow = position == 0 ? true : false;
                mAdapterSearchUser.setFollow(mIsFollow);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mRecyclerSearchUser.addOnScrollListener(new CustomPaginationScrollListener(RecyclerViewUtils
                .Create().getLinearLayoutManager(mRecyclerSearchUser)) {
            @Override
            protected void loadMoreItems() {
                mIsLoadMore = true;
                mCurrentPage++;
                getPresenter(FragmentSearchUser.this).searchUser(mCurrentPage, mTextKey,
                        mIsFollow);
            }

            @Override
            protected boolean isLoading() {
                return mIsLoadMore;
            }

            @Override
            protected void enableRefreshLayout(boolean isEnable) {

            }
        });
        mRefreshLayout.setOnRefreshListener(() -> getPresenter(this).
                searchUser(ConstantDefine.FIRST_PAGE, mTextKey, mIsFollow));
    }

    @Override
    public void onClickItem(User user) {
        Intent intent = new Intent(getContext(), ActivityUserPublic.class);
        intent.putExtra(GlobalDefine.KEY_INTENT_USER, user);
        startActivity(intent);
    }

    @Override
    public void onClickFollow(int userId, int position) {
        mFollowPosition = position;
        getPresenter(this).followUser(userId);
    }

    @Override
    public void onClickUnFollow(int userId, int position) {
        mUnFollowPosition = position;
        getPresenter(this).unFollowUser(userId);
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());

    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
        mRefreshLayout.setRefreshing(false);
    }

    @Override
    public void getSearchUsersSuccess(List<User> users) {
        if (!users.isEmpty()) {
            for (User item : users) {
                item.setIsFollow(mIsFollow ? 1 : 0);
            }
            if (mIsRefresh) {
                mIsRefresh = false;
                mCurrentPage = ConstantDefine.FIRST_PAGE;
                mAdapterSearchUser.refreshUserList(users);
            } else {
                mIsLoadMore = false;
                mAdapterSearchUser.addUserList(users);
            }
        } else {
            Toasty.info(getContext(), getString(R.string.error_not_found_search),
                    Toast.LENGTH_SHORT).show();
            if (mIsLoadMore) {
                mCurrentPage--;
                mIsLoadMore = false;
            } else if (mIsRefresh) {
                mIsRefresh = false;
                mCurrentPage = ConstantDefine.FIRST_PAGE;
            }
        }
    }

    @Override
    public void getError(String errorMessage) {
        if (mIsRefresh) {
            mIsRefresh = false;
        } else if (mIsLoadMore) {
            mIsLoadMore = false;
            mCurrentPage--;
        }
        Toasty.warning(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void FollowUserFail() {
        mAdapterSearchUser.setFollowFail(mFollowPosition);

    }

    @Override
    public void unFollowUserFail() {
        mAdapterSearchUser.setUnFollowFail(mUnFollowPosition);
    }
}
