package bkdn.itt.datn.saleoff.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.model.User;
import bkdn.itt.datn.saleoff.presenter.PresenterEmployee;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.activity.ActivityShopMemberAdd;
import bkdn.itt.datn.saleoff.view.adapter.AdapterShopEmployee;
import bkdn.itt.datn.saleoff.view.iface.IFragmentEmployee;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

/**
 * Created by TuDLT on 5/6/2018.
 */
public class FragmentShopEmployee extends FragmentBase<PresenterEmployee>
        implements IFragmentEmployee, AdapterShopEmployee.OnDeletedMemberListener {

    /*
* Widgets
* */
    @BindView(R.id.recyclerShopMemberMore)
    RecyclerView mRecyclerMore;
    @BindView(R.id.edtShopMemberMoreSearch)
    EditText mEdtSearch;
    @BindView(R.id.imbShopEmployeeAdd)
    ImageButton mImbAddEmployee;

    /*
    * Fields
    * */
    AdapterShopEmployee mAdapterShopEmployee;
    private View mRootView;
    private int mShopId;
    private int mDeletedPosition;

    public static FragmentShopEmployee newInstance(int shopId) {
        FragmentShopEmployee fragmentShopEmployee = new FragmentShopEmployee();
        Bundle bundle = new Bundle();
        bundle.putInt(GlobalDefine.KEY_BUNDLE_SHOP_INFO, shopId);
        fragmentShopEmployee.setArguments(bundle);
        return fragmentShopEmployee;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.fragment_shop_employee, container, false);
            ButterKnife.bind(this, mRootView);
            initValues();
            initViews();
            initActions();
        }
        return mRootView;
    }

    private void initValues() {
        mShopId = getArguments().getInt(GlobalDefine.KEY_BUNDLE_SHOP_INFO);
        getPresenter(this).getAllEmployees(mShopId);

    }

    private void initViews() {
        RecyclerViewUtils.Create().setUpVertical(getContext(), mRecyclerMore);
        mAdapterShopEmployee = new AdapterShopEmployee(getContext(), this);
        mRecyclerMore.setAdapter(mAdapterShopEmployee);
    }

    private void initActions() {
        mEdtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mImbAddEmployee.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), ActivityShopMemberAdd.class);
            intent.putExtra(GlobalDefine.KEY_INTENT_SHOP_ID, mShopId);
            startActivityForResult(intent, ConstantDefine.ADD_SHOP_MEMBERS_REQUEST);
        });
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
    }

    @Override
    public void getAllEmployeesSuccess(List<User> users) {
        mAdapterShopEmployee.setListUsers(users);
    }

    @Override
    public void deleteEmployeeSuccess() {
        Toasty.success(getContext(), getString(R.string.delete_employee_success), Toast.LENGTH_SHORT).show();
        mAdapterShopEmployee.deleteEmployeeSuccess(mDeletedPosition);
    }

    @Override
    public void deleteEmployeeFail(String errorMessage) {
        Toasty.error(getContext(), getString(R.string.error_delete_employee_fail), Toast.LENGTH_SHORT).show();
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
        getPresenter(this).getAllEmployees(mShopId);
    }

    @Override
    public void getError(String errorMessage) {
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDeletedEmployee(int userId, int position) {
        mDeletedPosition = position;
        getPresenter(this).deleteEmployee(mShopId, userId);
    }
}
