package bkdn.itt.datn.saleoff.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.model.Event;
import bkdn.itt.datn.saleoff.model.model.Shop;
import bkdn.itt.datn.saleoff.presenter.PresenterShopEvent;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.activity.ActivityEventDetail;
import bkdn.itt.datn.saleoff.view.activity.ActivityEventManage;
import bkdn.itt.datn.saleoff.view.adapter.AdapterBase;
import bkdn.itt.datn.saleoff.view.adapter.AdapterEvent;
import bkdn.itt.datn.saleoff.view.iface.IFragmentShopEvent;
import bkdn.itt.datn.saleoff.view.iface.IViewBase;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

/**
 * Created by TuDLT on 5/28/2018.
 */
public class FragmentShopEvent extends FragmentBase<PresenterShopEvent> implements IFragmentShopEvent, AdapterBase.OnBaseItemClickListener<Event> {

    /*
    * Widgets
    * */
    @BindView(R.id.swipeRefreshShopEvent)
    SwipeRefreshLayout mSwipeRefreshEvent;
    @BindView(R.id.recyclerShopEvent)
    RecyclerView mRecyclerEvent;

    /*
    * Fields
    * */
    private View mRootView;
    private AdapterEvent mAdapterEvent;
    private Shop mShop;

    public static FragmentShopEvent newInstance(Shop shop, int status) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(GlobalDefine.KEY_BUNDLE_SHOP_INFO, shop);
        bundle.putInt(GlobalDefine.KEY_BUNDLE_STATUS, status);
        FragmentShopEvent fragmentShopEvent = new FragmentShopEvent();
        fragmentShopEvent.setArguments(bundle);
        return fragmentShopEvent;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.fragment_shop_event, container, false);
            ButterKnife.bind(this, mRootView);
            initValues();
            initViews();
            initActions();
        }
        return mRootView;
    }

    private void initValues() {
        mShop = getArguments().getParcelable(GlobalDefine.KEY_BUNDLE_SHOP_INFO);
        if (mShop != null) {
            getPresenter(this).getAllShopEvents(true, mShop.getShopId());
        }
    }

    private void initViews() {
        mAdapterEvent = new AdapterEvent(getContext(), ConstantDefine.EVENT_OF_SHOP);
        mAdapterEvent.setListener(this);
        RecyclerViewUtils.Create().setUpVertical(getContext(), mRecyclerEvent).setAdapter(mAdapterEvent);
    }

    private void initActions() {
        mSwipeRefreshEvent.setOnRefreshListener(() -> {
            getPresenter(this).getAllShopEvents(false, mShop.getShopId());
        });
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
        mSwipeRefreshEvent.setRefreshing(false);
    }

    @Override
    public void getAllEventsSuccess(List<Event> events) {
        mAdapterEvent.refreshData(events);
    }

    @Override
    public void getAllEventsError(String errorMessage) {
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(Event event, int position) {
        Intent intent = new Intent(getContext(), ActivityEventManage.class);
        intent.putExtra(GlobalDefine.KEY_INTENT_EVENT, event);
        startActivity(intent);
    }
}
