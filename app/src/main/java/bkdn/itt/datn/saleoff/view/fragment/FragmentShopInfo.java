package bkdn.itt.datn.saleoff.view.fragment;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.model.Shop;
import bkdn.itt.datn.saleoff.presenter.PresenterShopInfo;
import bkdn.itt.datn.saleoff.utils.MapUtils;
import bkdn.itt.datn.saleoff.view.iface.IFragmentShopInfo;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

/**
 * Created by TuDLT on 3/25/2018.
 */
public class FragmentShopInfo extends FragmentBase<PresenterShopInfo> implements IFragmentShopInfo, OnMapReadyCallback {
    /*
    * Widgets
    * */
    @BindView(R.id.tvShopInfoFollowers)
    TextView mTvFollowers;
    @BindView(R.id.tvShopInfoCate)
    TextView mTvCat;
    @BindView(R.id.tvShopInfoPhone)
    TextView mTvPhone;
    @BindView(R.id.tvShopInfoAddress)
    TextView mTvAddress;
    @BindView(R.id.tvShopInfoWeb)
    TextView mTvWeb;
    @BindView(R.id.tvShopInfoRate)
    TextView mTvRate;
    @BindView(R.id.tvShopInfoRaters)
    TextView mTvRaters;
    //    @BindView(R.id.swipeRefreshLayoutShopPublic)
//    SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.nestedScrollViewShopInfo)
    NestedScrollView mNestedScrollView;
    /*
    * Fields
    * */
    private View mRootView;
    private Shop mShop;
    private MySupportMapFragment mMapFragment;
    private GoogleMap mMap;

    public static FragmentShopInfo newInstance(Shop shop) {
        FragmentShopInfo fragmentShopInfo = new FragmentShopInfo();
        Bundle bundle = new Bundle();
        bundle.putParcelable(GlobalDefine.KEY_BUNDLE_SHOP_INFO, shop);
        fragmentShopInfo.setArguments(bundle);
        return fragmentShopInfo;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.fragment_shop_info, container, false);
            ButterKnife.bind(this, mRootView);
            mShop = getArguments().getParcelable(GlobalDefine.KEY_BUNDLE_SHOP_INFO);
            mMapFragment = (MySupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapPostDetailAddress);
            mMapFragment.getMapAsync(this);
        }
        return mRootView;
    }

    private void initViews() {
        if (mShop != null) {
            if (mShop.getFollowers() > 0) {
                mTvFollowers.setText(String.valueOf(mShop.getFollowers()));
            } else {
                mTvFollowers.setTextColor(getResources().getColor(R.color.colorBlackLight));
                mTvFollowers.setText(R.string.no_follow_shop);
            }

            if (mShop.getCateName() != null && !mShop.getCateName().isEmpty()) {
                mTvCat.setText(mShop.getCateName());
            } else {
                mTvCat.setTextColor(getResources().getColor(R.color.colorBlackLight));
                mTvCat.setText(R.string.unknown);
            }

            if (mShop.getPhone() != null && !mShop.getPhone().isEmpty()) {
                mTvPhone.setText(mShop.getPhone());
            } else {
                mTvWeb.setTextColor(getResources().getColor(R.color.colorBlackLight));
                mTvPhone.setText(R.string.unknown);
            }


            if (mShop.getWeb() != null && !mShop.getWeb().isEmpty()) {
                mTvWeb.setText(mShop.getWeb());
            } else {
                mTvWeb.setTextColor(getResources().getColor(R.color.colorBlackLight));
                mTvWeb.setText(R.string.unknown);
            }

            if (mShop.getAddress() != null && !mShop.getAddress().isEmpty()) {
                mTvAddress.setText(mShop.getAddress());
            } else {
                mTvAddress.setTextColor(getResources().getColor(R.color.colorBlackLight));
                mTvAddress.setText(R.string.unknown);
            }

            if (mShop.getRate() > 0) {
                mTvRate.setText(String.valueOf(mShop.getTotalRate()));
                mTvRaters.setText(String.valueOf(mShop.getTotalRate()) + " raters");
            } else {
                mTvRate.setTextColor(getResources().getColor(R.color.colorBlackLight));
                mTvRate.setText(R.string.not_rate);
                mTvRaters.setVisibility(View.GONE);
            }

            //set map
            if (mShop.getAddress() != null && mMap != null) {
                MapUtils.getInstance(mMap).markPlaceOnMap(getContext(), mMap, mShop.getName(),
                        mShop.getAddress());
            }
        }
    }


    private void initActions() {
        //mRefreshLayout.setOnRefreshListener(() -> getPresenter(this).getShopDetail(mShop.getShopId()));
        mMapFragment.setListener(() -> mNestedScrollView.requestDisallowInterceptTouchEvent(true));

    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void getShopDetailSuccess(Shop shop) {
        //mRefreshLayout.setRefreshing(false);
        mShop = shop;
        initViews();
    }

    @Override
    public void getError(String errorMessage) {
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        initViews();
        initActions();
    }
}
