package bkdn.itt.datn.saleoff.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.daimajia.swipe.SwipeLayout;

import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.model.model.User;
import bkdn.itt.datn.saleoff.presenter.PresenterShopMembers;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.adapter.AdapterShopMember;
import bkdn.itt.datn.saleoff.view.iface.IFragmentShopMembers;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

/**
 * Created by TuDLT on 5/18/2018.
 */
public class FragmentShopMembers extends FragmentBase<PresenterShopMembers>
        implements IFragmentShopMembers, AdapterShopMember.OnMinusBonusListener {

    /*
    * Widgets
    * */
    @BindView(R.id.edtShopMemberSearch)
    EditText mEdtShopMemberSearch;
    @BindView(R.id.recyclerShopMember)
    RecyclerView mRecyclerView;
    @BindView(R.id.swipeLayoutShopMember)
    SwipeRefreshLayout mRefreshLayout;
    /*
    * Fields
    * */
    private static final String KEY_BUNDLE = "KEY BUNDLE";
    private int mShopId;
    private View mRootView;
    private AdapterShopMember mAdapterShopMember;
    private OnResponseFromServerListener mServerListener;

    public static FragmentShopMembers newInstance(int shopId) {
        FragmentShopMembers fragmentShopMembers = new FragmentShopMembers();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_BUNDLE, shopId);
        fragmentShopMembers.setArguments(bundle);
        return fragmentShopMembers;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.fragment_shop_members, container, false);
            ButterKnife.bind(this, mRootView);
            mShopId = getArguments().getInt(KEY_BUNDLE);
            mAdapterShopMember = new AdapterShopMember(getContext(), this);
            RecyclerViewUtils.Create().setUpVertical(getContext(), mRecyclerView)
                    .setAdapter(mAdapterShopMember);
            getPresenter(this).getShopMembers(mShopId);
            mRefreshLayout.setOnRefreshListener(() -> getPresenter(FragmentShopMembers.this)
                    .getShopMembers(mShopId));
        }
        return mRootView;
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
        mRefreshLayout.setRefreshing(false);
    }

    @Override
    public void getShopMembersSuccess(List<User> users) {
        mAdapterShopMember.refreshData(users);
    }

    @Override
    public void getShopMembersFail(String errorMessage) {
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void minusBonusSuccess() {
        mServerListener.onSuccess();
    }

    @Override
    public void minusBonusFail(String errorMessage) {
        mServerListener.onError();
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onMinusBonus(int userId, int minusPoint, OnResponseFromServerListener serverListener) {
        mServerListener = serverListener;
        getPresenter(this).minusBonus(mShopId, userId, minusPoint);
    }

    public interface OnResponseFromServerListener {
        void onSuccess();

        void onError();
    }
}
