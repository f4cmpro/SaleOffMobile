package bkdn.itt.datn.saleoff.view.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.ConstantDefine;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import bkdn.itt.datn.saleoff.model.model.Post;
import bkdn.itt.datn.saleoff.model.model.Shop;
import bkdn.itt.datn.saleoff.model.model.Category;
import bkdn.itt.datn.saleoff.presenter.PresenterShopPost;
import bkdn.itt.datn.saleoff.utils.CustomPaginationScrollListener;
import bkdn.itt.datn.saleoff.utils.RecyclerViewUtils;
import bkdn.itt.datn.saleoff.utils.Utils;
import bkdn.itt.datn.saleoff.view.activity.ActivityComment;
import bkdn.itt.datn.saleoff.view.activity.ActivityPostDetail;
import bkdn.itt.datn.saleoff.view.activity.ActivityPostEdit;
import bkdn.itt.datn.saleoff.view.activity.ActivityShopPrivate;
import bkdn.itt.datn.saleoff.view.adapter.AdapterShopNewsFeed;
import bkdn.itt.datn.saleoff.view.iface.IFragmentShopPost;
import bkdn.itt.datn.saleoff.view.iface.OnClickLikeListener;
import bkdn.itt.datn.saleoff.view.iface.OnClickUnLikeListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

/**
 * Created by TuDLT on 3/25/2018.
 */
public class FragmentShopPost extends FragmentBase<PresenterShopPost> implements
        AdapterShopNewsFeed.OnClickPostItemListener, IFragmentShopPost{
    /*
    * Widgets
    * */
    @BindView(R.id.recyclerShopPost)
    RecyclerView mRecyclerShopPost;
    @BindView(R.id.swipeRefreshShopPost)
    SwipeRefreshLayout mSwipeRefreshLayout;
    /*
    * Fields
    * */
    private AdapterShopNewsFeed mAdapterPost;
    private View mRootView;
    private Shop mShop;
    private int mCurrentPage;
    private boolean isRefresh;
    private boolean isLoadMore;
    private int mStatus; //public || private
    private OnClickLikeListener mOnClickLikeListener;
    private OnClickUnLikeListener mOnClickUnLikeListener;

    public static FragmentShopPost newInstance(Shop shop, int status) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(GlobalDefine.KEY_BUNDLE_SHOP_INFO, shop);
        bundle.putInt(GlobalDefine.KEY_BUNDLE_STATUS, status);
        FragmentShopPost fragmentShopPost = new FragmentShopPost();
        fragmentShopPost.setArguments(bundle);
        return fragmentShopPost;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.fragment_shop_post, container, false);
            ButterKnife.bind(this, mRootView);
            initValues();
            initViews();
            initActions();
        }
        return mRootView;
    }

    private void initValues() {
        mCurrentPage = ConstantDefine.FIRST_PAGE;
        mShop = getArguments().getParcelable(GlobalDefine.KEY_BUNDLE_SHOP_INFO);
        mStatus = getArguments().getInt(GlobalDefine.KEY_BUNDLE_STATUS);
        if (mShop != null) {
            Utils.showLoadingDialog(getContext());
            getPresenter(this).getShopPost(mCurrentPage, mShop.getShopId());
        }

    }

    private void initViews() {
        mAdapterPost = new AdapterShopNewsFeed(getContext(), this, mStatus);
        mAdapterPost.setShopInfo(mShop.getAvatar(), mShop.getName());
        RecyclerViewUtils.Create().setUpVertical(getContext(), mRecyclerShopPost).setAdapter(mAdapterPost);
    }

    private void initActions() {
        mRecyclerShopPost.addOnScrollListener(new CustomPaginationScrollListener(RecyclerViewUtils.
                Create().getLinearLayoutManager(mRecyclerShopPost)) {
            @Override
            protected void loadMoreItems() {
                isLoadMore = true;
                mCurrentPage++;
                getPresenter(FragmentShopPost.this).getShopPost(mCurrentPage, mShop.getShopId());
            }

            @Override
            public boolean isLoading() {
                return isLoadMore;
            }

            @Override
            protected void enableRefreshLayout(boolean isEnable) {
            }
        });
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            isRefresh = true;
            getPresenter(FragmentShopPost.this).getShopPost(ConstantDefine.FIRST_PAGE, mShop.getShopId());
        });
    }

    @Override
    public void onClickPostItem(Post post) {
        Intent intent = new Intent(getContext(), ActivityPostDetail.class);
        intent.putExtra(GlobalDefine.KEY_INTENT_POST, post);
        startActivity(intent);
    }

    @Override
    public void onClickDeletePostItem(int postId) {
        getPresenter(this).deleteShopPost(postId, mShop.getShopId());
    }

    @Override
    public void onClickEditPostItem(Post post) {
        Intent intent = new Intent(getContext(), ActivityPostEdit.class);
        intent.putExtra(GlobalDefine.KEY_INTENT_POST, post);
        Category category = new Category();
        category.setId(mShop.getCateId());
        category.setName(mShop.getCateName());
        intent.putExtra(GlobalDefine.KEY_INTENT_CATEGORY, category);
        ((Activity) getContext()).startActivityForResult(intent, ConstantDefine.EDIT_SHOP_POST_REQUEST);
    }

    @Override
    public void onClickLike(int postId, OnClickLikeListener onClickLikeListener) {
        mOnClickLikeListener = onClickLikeListener;
        getPresenter(this).likePost(postId);
    }

    @Override
    public void onClickUnLike(int postId, OnClickUnLikeListener onClickUnLikeListener) {
        mOnClickUnLikeListener = onClickUnLikeListener;
        getPresenter(this).unLikePost(postId);
    }

    @Override
    public void onClickComment(int postId) {
        Intent intent = new Intent(getContext(), ActivityComment.class);
        intent.putExtra(GlobalDefine.KEY_INTENT_POST_ID, postId);
        startActivity(intent);
    }

    @Override
    public void showLoading() {
        Utils.showLoadingDialog(getContext());
    }

    @Override
    public void hideLoading() {
        Utils.hideLoadingDialog();
    }

    @Override
    public void getListShopPostSuccess(List<Post> posts) {
        if (isRefresh) {
            isRefresh = false;
            mSwipeRefreshLayout.setRefreshing(false);
            mAdapterPost.setRefreshPosts(posts);
        } else {
            isLoadMore = false;
            mAdapterPost.setPosts(posts);
        }
    }

    @Override
    public void getError(String errorMessage) {
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
        isRefresh = false;
        isLoadMore = false;
    }

    @Override
    public void deleteShopPostSuccess(String message) {
        Toasty.success(getContext(), message, Toast.LENGTH_SHORT).show();
        isRefresh = true;
        getPresenter(this).getShopPost(ConstantDefine.FIRST_PAGE, mShop.getShopId());
    }

    @Override
    public void likePostSuccess() {
        mOnClickLikeListener.success();
    }

    @Override
    public void likePostFail(String errorMessage) {
        Toasty.error(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
        mOnClickLikeListener.error();
    }

    @Override
    public void unLikePostSuccess() {
        mOnClickUnLikeListener.success();
    }

    @Override
    public void unLikePostFail(String errorMessage) {
        mOnClickUnLikeListener.error();
    }
}
