package bkdn.itt.datn.saleoff.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import bkdn.itt.datn.saleoff.R;
import bkdn.itt.datn.saleoff.global.GlobalDefine;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TuDLT on 3/24/2018.
 */
public class FragmentSlider extends Fragment {

    private View mRootView;
    @BindView(R.id.imvSlider)
    ImageView mImvSlider;

    public static FragmentSlider newInstance(String imageUrl) {
        Bundle bundle = new Bundle();
        bundle.putString(GlobalDefine.KEY_BUNDLE_SLIDER, imageUrl);
        FragmentSlider fragmentSlider = new FragmentSlider();
        fragmentSlider.setArguments(bundle);
        return fragmentSlider;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.fragment_slider, container, false);
            ButterKnife.bind(this, mRootView);
            String imgUrl = getArguments().getString(GlobalDefine.KEY_BUNDLE_SLIDER);
            if (imgUrl != null) {
                Glide.with(mRootView).load(imgUrl).into(mImvSlider);
            } else {
                mImvSlider.setBackgroundResource(R.drawable.ic_cover);
            }
        }
        return mRootView;
    }
}
