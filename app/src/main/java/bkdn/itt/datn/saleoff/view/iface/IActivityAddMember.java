package bkdn.itt.datn.saleoff.view.iface;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.UserInfo;

/**
 * Created by TuDLT on 3/23/2018.
 */
public interface IActivityAddMember extends IViewBase{

    void getUsersToAddMemberSuccess(List<UserInfo> listUserInfo);

    void getError(String errorMessage);

    void addMembersSuccess();
}
