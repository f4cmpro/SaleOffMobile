package bkdn.itt.datn.saleoff.view.iface;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Category;

/**
 * Created by TuDLT on 4/6/2018.
 */
public interface IActivityCategory extends IViewBase {
    void getAllCategorySuccess(List<Category> categories);

    void getAllCategoryFail(String errorMessage);
}
