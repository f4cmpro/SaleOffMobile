package bkdn.itt.datn.saleoff.view.iface;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Comment;

/**
 * Created by TuDLT on 4/20/2018.
 */
public interface IActivityComment extends IViewBase{
    void addCommentSuccess(Comment comment);
    void editCommentSuccess();
    void getError(String errorMessage);
    void getCommentsSuccess(List<Comment> comments, int lastId);
}
