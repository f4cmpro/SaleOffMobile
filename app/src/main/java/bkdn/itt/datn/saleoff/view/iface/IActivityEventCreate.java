package bkdn.itt.datn.saleoff.view.iface;

/**
 * Created by TuDLT on 5/28/2018.
 */
public interface IActivityEventCreate extends IViewBase{

    void createEventSuccess();
    void createEventError(String errorMessage);
}
