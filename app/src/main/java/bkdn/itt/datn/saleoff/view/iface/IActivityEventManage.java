package bkdn.itt.datn.saleoff.view.iface;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Event;
import bkdn.itt.datn.saleoff.model.model.Participant;
import bkdn.itt.datn.saleoff.model.model.User;

/**
 * Created by TuDLT on 5/31/2018.
 */
public interface IActivityEventManage extends IViewBase {
    void getEventDetailSuccess(Event event);
    void getEventDetailError(String errorMessage);

    void getParticipantsSuccess(List<Participant> participants);

    void getParticipantsError(String errorMessage);

    void useEventPointSuccess();
    void useEventPointError(String errorMessage);

}
