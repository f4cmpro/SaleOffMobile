package bkdn.itt.datn.saleoff.view.iface;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Category;
import bkdn.itt.datn.saleoff.model.model.SubCategory;

/**
 * Created by TuDLT on 5/14/2018.
 */
public interface IActivityInterested extends IViewBase{
    void getCategories(List<Category> categories);
    void getError(String messageError);

    void getSubCategories(List<SubCategory> listSubCategory);
}
