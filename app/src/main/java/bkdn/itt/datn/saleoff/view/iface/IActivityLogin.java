package bkdn.itt.datn.saleoff.view.iface;

import bkdn.itt.datn.saleoff.model.model.User;
import bkdn.itt.datn.saleoff.view.activity.ActivityLogin;

/**
 * Created by TuDLT on 3/3/2018.
 */
public interface IActivityLogin extends IViewBase {

    ActivityLogin getActivity();
    void loginFacebookSuccess();
    void loginFacebookFail(String failMessage);
    void loginGoogleSuccess();
    void loginGoogleFail(String failMessage);

    void getProfileSuccess(User user);

    void requestFail(String errorMessage);
}
