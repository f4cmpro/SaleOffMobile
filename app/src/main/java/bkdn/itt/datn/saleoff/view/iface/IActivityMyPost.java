package bkdn.itt.datn.saleoff.view.iface;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Post;

/**
 * Created by TuDLT on 3/19/2018.
 */
public interface IActivityMyPost extends IViewBase{
    void getMyPostSuccess(List<Post> posts);

    void getError(String errorMessage);

    void deleteUserPostSuccess();

    void likePostFail(String errorMessage);
    void likePostSuccess();

    void unLikePostFail(String errorMessage);
    void unLikePostSuccess();
}
