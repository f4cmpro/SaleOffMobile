package bkdn.itt.datn.saleoff.view.iface;

import java.util.List;

/**
 * Created by TuDLT on 4/7/2018.
 */
public interface IActivityPostCreate extends IViewBase{
    void createPostSuccess(String message);
    void createPostFail(String errorMessage);
}
