package bkdn.itt.datn.saleoff.view.iface;

/**
 * Created by TuDLT on 4/18/2018.
 */
public interface IActivityPostEdit extends IViewBase {
    void editPostSuccess();
    void editPostError(String errorMessage);
}
