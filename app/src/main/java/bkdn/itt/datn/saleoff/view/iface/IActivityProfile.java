package bkdn.itt.datn.saleoff.view.iface;


import bkdn.itt.datn.saleoff.model.model.User;

/**
 * Created by TuDLT on 3/3/2018.
 */
public interface IActivityProfile extends IViewBase {


    void editProfileSuccess(User user);

    void editProfileFail(String string);
}
