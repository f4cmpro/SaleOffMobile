package bkdn.itt.datn.saleoff.view.iface;

/**
 * Created by TuDLT on 5/16/2018.
 */
public interface IActivityShare extends IViewBase {
    void shareSuccess();
    void shareError(String errorMessage);
}
