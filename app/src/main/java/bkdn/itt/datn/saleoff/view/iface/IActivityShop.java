package bkdn.itt.datn.saleoff.view.iface;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Shop;
import bkdn.itt.datn.saleoff.model.model.User;

/**
 * Created by TuDLT on 3/3/2018.
 */
public interface IActivityShop extends IViewBase {
    void getError(String errorMessage);

    void getShopDetailSuccess(Shop shop);

    void ratingShopSuccess();

    void reportSuccess();
}
