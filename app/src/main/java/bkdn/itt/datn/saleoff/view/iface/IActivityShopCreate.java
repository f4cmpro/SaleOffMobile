package bkdn.itt.datn.saleoff.view.iface;

import bkdn.itt.datn.saleoff.model.model.Shop;

/**
 * Created by TuDLT on 3/18/2018.
 */
public interface IActivityShopCreate extends IViewBase{
    void createShopSuccess(Shop shop);
    void getErrorMessage(String messageError);
}
