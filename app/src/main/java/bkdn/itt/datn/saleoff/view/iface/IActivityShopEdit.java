package bkdn.itt.datn.saleoff.view.iface;

/**
 * Created by TuDLT on 5/7/2018.
 */
public interface IActivityShopEdit extends IViewBase{

    void editShopSuccess();
    void getError(String errorMessage);
}
