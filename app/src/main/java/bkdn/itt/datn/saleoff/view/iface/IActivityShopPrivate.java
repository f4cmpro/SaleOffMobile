package bkdn.itt.datn.saleoff.view.iface;

import bkdn.itt.datn.saleoff.model.model.Shop;

/**
 * Created by TuDLT on 5/6/2018.
 */
public interface IActivityShopPrivate extends IViewBase {
    void getShopDetailSuccess(Shop shop);

    void getError(String errorMessage);

    void requestDeleteShopSuccess();
}
