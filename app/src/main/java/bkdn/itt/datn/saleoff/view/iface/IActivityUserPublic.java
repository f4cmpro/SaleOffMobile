package bkdn.itt.datn.saleoff.view.iface;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Post;
import bkdn.itt.datn.saleoff.model.model.User;

/**
 * Created by TuDLT on 5/3/2018.
 */
public interface IActivityUserPublic extends IViewBase{
    void getUserPostSuccess(List<Post> posts, int lastId);

    void getError(String errorMessage);

    void getUserByIdSuccess(User user);

    void followUserFail();

    void unFollowUserFail();

    void likePostFail();

    void unLikeFail();

    void reportSuccess();
}
