package bkdn.itt.datn.saleoff.view.iface;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.SubCategory;

/**
 * Created by TuDLT on 4/7/2018.
 */
public interface IAcvititySubCategory extends IViewBase {
    void getSubCategorySuccess(List<SubCategory> subCats);
    void getSubCategoryFail(String errorMessage);
}
