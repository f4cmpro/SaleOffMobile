package bkdn.itt.datn.saleoff.view.iface;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.User;

/**
 * Created by TuDLT on 5/6/2018.
 */
public interface IFragmentEmployee extends IViewBase {

    void getAllEmployeesSuccess(List<User> users);

    void deleteEmployeeSuccess();

    void deleteEmployeeFail(String errorMessage);

    void getError(String errorMessage);
}
