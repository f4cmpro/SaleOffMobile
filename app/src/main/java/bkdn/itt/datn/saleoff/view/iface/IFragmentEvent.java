package bkdn.itt.datn.saleoff.view.iface;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Event;

/**
 * Created by TuDLT on 5/27/2018.
 */
public interface IFragmentEvent extends IViewBase{
    void getEventHappeningSuccess(List<Event> events);
    void getEventHappeningError(String errorMessage);

    void getEventGoingHappenSuccess(List<Event> events);
    void getEventGoingHappenError(String errorMessage);

    void getEventHappenedSuccess(List<Event> events);
    void getEventHappenedError(String errorMessage);

    void getEventJoinedSuccess(List<Event> events);
    void getEventJoinedError(String errorMessage);
}
