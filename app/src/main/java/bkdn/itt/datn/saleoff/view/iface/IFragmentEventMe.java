package bkdn.itt.datn.saleoff.view.iface;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Event;

/**
 * Created by TuDLT on 5/29/2018.
 */
public interface IFragmentEventMe extends IViewBase {
    void getAllMyEvents(List<Event> events);
    void getError(String errorMessage);
}
