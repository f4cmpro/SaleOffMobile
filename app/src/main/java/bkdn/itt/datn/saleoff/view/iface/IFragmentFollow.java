package bkdn.itt.datn.saleoff.view.iface;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.SubCategory;
import bkdn.itt.datn.saleoff.model.model.Shop;
import bkdn.itt.datn.saleoff.model.model.User;

/**
 * Created by TuDLT on 3/3/2018.
 */
public interface IFragmentFollow extends IViewBase {

    void showFollowedShops(List<Shop> shops);

    void showSuggestedShops(List<Shop> shops);

    void showFollowedUsers(List<User> users);

    void showSuggestedUsers(List<User> users);

    void showFollowedProducts(List<SubCategory> subCategories);

    void showSuggestedProducts(List<SubCategory> subCategories);

    void showFailMessage(String message);
}
