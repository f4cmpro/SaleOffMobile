package bkdn.itt.datn.saleoff.view.iface;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.ShopInfo;

/**
 * Created by TuDLT on 4/15/2018.
 */
public interface IFragmentFollowShop extends IViewBase{
    void getFollowShops(List<ShopInfo> shopInfoList);
    void getSuggestionShop(List<ShopInfo> shopInfoList);
    void getError(String errorMessage);

    void followShopSuccess();
    void followShopFail(String errorMessage);

    void unFollowShopSuccess();

}
