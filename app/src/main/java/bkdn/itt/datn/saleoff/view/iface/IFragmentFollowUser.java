package bkdn.itt.datn.saleoff.view.iface;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.UserInfo;

/**
 * Created by TuDLT on 4/15/2018.
 */
public interface IFragmentFollowUser extends IViewBase {
    void getFollowUser(List<UserInfo> shopInfoList);
    void getSuggestionUser(List<UserInfo> shopInfoList);
    void followUserSuccess();

    void unFollowUserSuccess();

    void getError(String errorMessage);
}
