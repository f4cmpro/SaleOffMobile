package bkdn.itt.datn.saleoff.view.iface;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Category;
import bkdn.itt.datn.saleoff.model.model.SubCategory;

/**
 * Created by TuDLT on 4/20/2018.
 */
public interface IFragmentHome extends IViewBase{
    void getCategoriesSuccess(List<Category> categories);
    void getError(String errorMessage);

    void getSubCategoriesSuccess(List<SubCategory> listSubCategory);
}
