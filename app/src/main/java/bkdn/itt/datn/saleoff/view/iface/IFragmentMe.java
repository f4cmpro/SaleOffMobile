package bkdn.itt.datn.saleoff.view.iface;

/**
 * Created by TuDLT on 5/8/2018.
 */
public interface IFragmentMe extends IViewBase {
    void logOutSuccess();

    void logOutFail(String message);
}
