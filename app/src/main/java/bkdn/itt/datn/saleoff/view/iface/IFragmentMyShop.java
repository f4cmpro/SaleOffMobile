package bkdn.itt.datn.saleoff.view.iface;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.ShopInfo;

/**
 * Created by TuDLT on 3/19/2018.
 */
public interface IFragmentMyShop extends IViewBase {
    void getOwnerShopSuccess(List<ShopInfo> shops);
    void getWorkingShopSuccess(List<ShopInfo> shops);
    void getErrorMessage(String message);
}
