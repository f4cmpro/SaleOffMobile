package bkdn.itt.datn.saleoff.view.iface;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Category;
import bkdn.itt.datn.saleoff.model.model.Post;

/**
 * Created by TuDLT on 3/3/2018.
 */
public interface IFragmentNewsFeed extends IViewBase {
    void getFollowingNewsFeed(List<Post> posts, int lastId);
    void getFollowingNewsFeedEmpty();
    void getNotFollowingNewsFeedEmpty();

    void getNotFollowingNewsFeed(List<Post> posts, int lastId);

    void likePostSuccess();

    void likePostFail(String failMessage);

    void unLikePostSuccess();

    void unLikePostFail(String failMessage);

    void getError(String errorMessage);

    void getCategories(List<Category> categories);

    void getCareFollow(List<Post> posts, int lastId);

    void getCareFollowEmpty();

    void getCareNotFollow(List<Post> posts, int lastId);

    void getCareNotFollowEmpty();

    void reportSuccess();
}
