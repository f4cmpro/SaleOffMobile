package bkdn.itt.datn.saleoff.view.iface;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Notification;

/**
 * Created by TuDLT on 3/3/2018.
 */
public interface IFragmentNotification extends IViewBase {
    void    getNotificationsSuccess(List<Notification> notifications);

    void deleteNotificationSuccess();

    void getError(String errorMessage);
}
