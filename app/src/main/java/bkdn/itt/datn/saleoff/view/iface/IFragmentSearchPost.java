package bkdn.itt.datn.saleoff.view.iface;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Category;
import bkdn.itt.datn.saleoff.model.model.Post;
import bkdn.itt.datn.saleoff.model.model.SubCategory;

/**
 * Created by TuDLT on 4/26/2018.
 */
public interface IFragmentSearchPost extends IViewBase {
    void searchPostSuccess(List<Post> posts);
    void getError(String errorMessage);

    void getCategoriesSuccess(List<Category> categories);

    void getSubCategoriesSuccess(List<SubCategory> listSubCategory);
}
