package bkdn.itt.datn.saleoff.view.iface;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Category;
import bkdn.itt.datn.saleoff.model.model.Shop;

/**
 * Created by TuDLT on 4/26/2018.
 */
public interface IFragmentSearchShop extends IViewBase {

    void getSearchShopSuccess(List<Shop> shops);
    void getError(String errorMessage);
    void getCategoriesSuccess(List<Category> categories);

    void followShopFail();

    void unFollowShopFail();
}
