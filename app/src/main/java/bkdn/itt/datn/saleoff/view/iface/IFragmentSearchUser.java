package bkdn.itt.datn.saleoff.view.iface;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.User;

/**
 * Created by TuDLT on 4/26/2018.
 */
public interface IFragmentSearchUser  extends IViewBase{

    void getSearchUsersSuccess(List<User> users);
    void getError(String errorMessage);

    void FollowUserFail();

    void unFollowUserFail();
}
