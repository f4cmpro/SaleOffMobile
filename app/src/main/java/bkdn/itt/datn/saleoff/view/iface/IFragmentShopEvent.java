package bkdn.itt.datn.saleoff.view.iface;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Event;

/**
 * Created by TuDLT on 5/28/2018.
 */
public interface IFragmentShopEvent extends IViewBase{

    void getAllEventsSuccess(List<Event> events);
    void getAllEventsError(String errorMessage);
}
