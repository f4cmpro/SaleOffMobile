package bkdn.itt.datn.saleoff.view.iface;

import bkdn.itt.datn.saleoff.model.model.Shop;

/**
 * Created by TuDLT on 3/25/2018.
 */
public interface IFragmentShopInfo extends IViewBase{

    void getShopDetailSuccess(Shop shop);

    void getError(String errorMessage);
}
