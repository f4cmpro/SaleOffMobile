package bkdn.itt.datn.saleoff.view.iface;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.User;

/**
 * Created by TuDLT on 5/18/2018.
 */
public interface IFragmentShopMembers extends IViewBase {
    void getShopMembersSuccess(List<User> users);

    void getShopMembersFail(String errorMessage);


    void minusBonusSuccess();

    void minusBonusFail(String errorMessage);
}
