package bkdn.itt.datn.saleoff.view.iface;

import java.util.List;

import bkdn.itt.datn.saleoff.model.model.Post;

/**
 * Created by TuDLT on 3/25/2018.
 */
public interface IFragmentShopPost extends IViewBase {
    void getListShopPostSuccess(List<Post> posts);
    void getError(String errorMessage);
    void deleteShopPostSuccess(String message);

    void likePostSuccess();
    void likePostFail(String errorMessage);

    void unLikePostSuccess();
    void unLikePostFail(String errorMessage);
}
