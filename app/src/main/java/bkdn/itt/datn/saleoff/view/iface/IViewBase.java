package bkdn.itt.datn.saleoff.view.iface;

import android.content.Context;

/**
 * Created by TuDLT on 12/26/17.
 */

public interface IViewBase {
    Context getContext();

    void showLoading();

    void hideLoading();

}
