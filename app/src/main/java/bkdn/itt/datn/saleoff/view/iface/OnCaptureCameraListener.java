package bkdn.itt.datn.saleoff.view.iface;

/**
 * Created by TuDLT on 5/19/2018.
 */
public interface OnCaptureCameraListener {
    void onCaptureCamera();
}
