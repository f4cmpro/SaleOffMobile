package bkdn.itt.datn.saleoff.view.iface;

/**
 * Created by TuDLT on 5/18/2018.
 */
public interface OnClickFollowListener {
    void success();
    void fail();
}
